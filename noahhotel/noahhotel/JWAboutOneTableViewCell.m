//
//  JWAboutOneTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/22.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "JWAboutOneTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PicturesModel.h"

@interface JWAboutOneTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *hotelNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *levelLabel;
@property (nonatomic, weak) IBOutlet UILabel *lastDecorateLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *areaLabel;
@property (nonatomic, weak) IBOutlet UILabel *guestRoomNumberLabel;
@property (nonatomic, weak) IBOutlet UILabel *maxFeastPersonLabel;
@property (nonatomic, weak) IBOutlet UILabel *maxMeetingPersonLabel;
@property (nonatomic, weak) IBOutlet UILabel *meetingRoomNumberLabel;
@property (nonatomic, weak) IBOutlet UILabel *startBusinessTime;

@property (nonatomic, weak) IBOutlet UIImageView *titleImage;

@property (nonatomic, weak) IBOutlet UIButton *agreeBtn;
@property (nonatomic, weak) IBOutlet UIImageView *agreeImageView;
@property (nonatomic, weak) IBOutlet UILabel *agreeLabel;
@end

@implementation JWAboutOneTableViewCell

- (IBAction)addressClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(addressClick)]) {
        [self.delegate addressClick];
    }
}

- (IBAction)agreeBtnClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(agreeBtnCLick)]) {
        [self.delegate agreeBtnCLick];
    }
}

- (IBAction)photoClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(photoClick)]) {
        [self.delegate photoClick];
    }
}

- (void)setShowAgree:(BOOL)showAgree
{
    _showAgree = showAgree;
    
    self.agreeBtn.hidden = !showAgree;
    self.agreeImageView.hidden = !showAgree;
    self.agreeLabel.hidden = !showAgree;
}

- (void)setModel:(AboutModel *)model
{
    _model = model;
    
    PicturesModel *picture = [model.logo firstObject];
    
    [self.titleImage sd_setImageWithURL:[NSURL URLWithString:picture.picture]];
    
    self.hotelNameLabel.text = model.hotelName;
    self.levelLabel.text = model.level;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy"];
    
    self.lastDecorateLabel.text = [NSString stringWithFormat:@"%@年", [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:model.lastDecorate.doubleValue]]];
    self.addressLabel.text = model.address;
    self.hotelNameLabel.text = model.hotelName;
    
    self.areaLabel.text = [NSString stringWithFormat:@"%@/%@/%@/%@", model.city, model.subway, model.zone, model.businessArea];
    
    self.guestRoomNumberLabel.text = model.guestRoomNumber.stringValue;
    self.maxFeastPersonLabel.text = [NSString stringWithFormat:@"%@人", model.maxFeastPerson.stringValue];
    self.maxMeetingPersonLabel.text = [NSString stringWithFormat:@"%@人", model.maxMeetingPerson.stringValue];
    self.meetingRoomNumberLabel.text = model.meetingRoomNumber.stringValue;
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
    
    NSString *str = [NSString stringWithFormat:@"%@", [dateFormatter2 stringFromDate:[NSDate dateWithTimeIntervalSince1970:model.startBusinessTime.doubleValue]]];
    
    if (str.length == 10) {
        self.startBusinessTime.text = [NSString stringWithFormat:@"%@年%@月%@日",[str substringWithRange:NSMakeRange(0, 4)],[str substringWithRange:NSMakeRange(5, 2)],[str substringWithRange:NSMakeRange(8, 2)]];
    }
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"JWAboutOneTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    
    return [tableView dequeueReusableCellWithIdentifier:identifier];
}

@end
