//
//  DetailViewOtherTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/27.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailViewOtherTableViewCellDelegate <NSObject>

- (void)location;

@end

@interface DetailViewOtherTableViewCell : UITableViewCell

@property (nonatomic, assign) id<DetailViewOtherTableViewCellDelegate> delegate;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *title;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
