#import "CycleImageCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CycleImageCell()

/**
 *  轮播图片
 */
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end
@implementation CycleImageCell

- (void)setImageURL:(NSURL *)imageURL {
    _imageURL = imageURL;
    
    [self.imageView sd_setImageWithURL:imageURL];
}

@end
