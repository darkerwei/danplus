//
//  SuTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/23.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "SuTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SuTableViewCell()

@property (nonatomic, weak) IBOutlet UIImageView *image;
@property (nonatomic, weak) IBOutlet UILabel *title;
@property (nonatomic, weak) IBOutlet UILabel *subtitle;

@end

@implementation SuTableViewCell

- (void)setModel:(SupplyValueModel *)model
{
    _model = model;
    
    self.title.text = model.title;
    self.subtitle.text = model.name;
    [self.image sd_setImageWithURL:[NSURL URLWithString:model.picture]];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"SuTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    
    return [tableView dequeueReusableCellWithIdentifier:identifier];
}

@end
