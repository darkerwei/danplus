//
//  HotelInsideTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/28.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "HotelInsideTableViewCell.h"
#import "KeyValueModel.h"

@interface HotelInsideTableViewCell()

@property (nonatomic, weak) IBOutlet UIButton *btn1;
@property (nonatomic, weak) IBOutlet UIButton *btn2;
@property (nonatomic, weak) IBOutlet UIButton *btn3;
@property (nonatomic, weak) IBOutlet UIButton *btn4;
@property (nonatomic, weak) IBOutlet UIButton *btn5;
@property (nonatomic, weak) IBOutlet UIButton *btn6;
@property (nonatomic, weak) IBOutlet UIButton *btn7;
@property (nonatomic, weak) IBOutlet UIButton *btn8;
@property (nonatomic, weak) IBOutlet UIButton *btn9;
@property (nonatomic, weak) IBOutlet UIButton *btn10;
@property (nonatomic, weak) IBOutlet UIButton *btn11;
@property (nonatomic, weak) IBOutlet UIButton *btn12;
@property (nonatomic, weak) IBOutlet UIButton *btn13;
@property (nonatomic, weak) IBOutlet UIButton *btn14;
@property (nonatomic, weak) IBOutlet UIButton *btn15;
@property (nonatomic, weak) IBOutlet UIButton *btn16;
@property (nonatomic, weak) IBOutlet UIButton *btn17;
@property (nonatomic, weak) IBOutlet UIButton *btn18;
@property (nonatomic, weak) IBOutlet UIButton *btn19;
@property (nonatomic, weak) IBOutlet UIButton *btn20;

@property (nonatomic, weak) IBOutlet UILabel *titleArray;

@property (nonatomic, strong) NSArray *btnArray;

@property (nonatomic, weak) IBOutlet UIView *distance;

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;

@end

@implementation HotelInsideTableViewCell

- (NSArray *)btnArray
{
    if (!_btnArray) {
        _btnArray = @[self.btn1, self.btn2, self.btn3, self.btn4, self.btn5, self.btn6, self.btn7, self.btn8, self.btn9, self.btn10, self.btn11, self.btn12, self.btn13, self.btn14, self.btn15, self.btn16, self.btn17, self.btn18, self.btn19, self.btn20];
    }
    
    return _btnArray;
}

- (void)setShow:(BOOL)show
{
    _show = show;
    
//    self.distance.hidden = show;
    
//    [self.distance.layer setBorderWidth:1.0];
//    [self.distance.layer setBorderColor:[[UIColor colorWithRed:220 / 255.0 green:220 / 255.0 blue:220 / 255.0 alpha:1.0] CGColor]];
}

- (void)setName:(NSString *)name
{
    _name = name;
    
    self.nameLabel.text = name;
}

- (void)setArray:(NSArray *)array
{
    _array = array;
    
    for (int i = 0; i < array.count; ++i) {
        
        if (i > 19) {
            return;
        }
        
        UIButton *btn = self.btnArray[i];
        
        if (i % 2 == 0) {
        
         KeyValueModel *keyValue = array[i];
        
            [btn setTitle:keyValue.value forState:UIControlStateNormal];
            
            [btn.layer setMasksToBounds:YES];
            [btn.layer setCornerRadius:4.0];
            btn.layer.borderWidth = 1;
            btn.layer.borderColor = [UIColorFromRGB(0xd3d3d3) CGColor];
            btn.titleLabel.lineBreakMode = 0;
            
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            
            KeyValueModel *keyValue = array[i];
            [btn setTitle:keyValue.value forState:UIControlStateNormal];
            [btn.layer setMasksToBounds:YES];
            [btn.layer setCornerRadius:4.0];
            btn.layer.borderWidth = 1;
            btn.layer.borderColor = [UIColorFromRGB(0xd3d3d3) CGColor];
            btn.titleLabel.lineBreakMode = 0;
            
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    
    self.titleArray.text = title;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"HotelInsideTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    HotelInsideTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

- (void)btnClick:(UIButton *)button
{
    button.selected = !button.selected;
    
    if (button.selected == YES) {
        button.backgroundColor = UIColorFromRGB(0xffdadb);
        button.layer.borderColor = [UIColorFromRGB(0xf4474b) CGColor];
    } else {
        button.backgroundColor = [UIColor whiteColor];
        button.layer.borderColor = [UIColorFromRGB(0xd3d3d3) CGColor];
    }
}

- (IBAction)selectSome
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectSome)]) {
        [self.delegate selectSome];
    }
}

@end
