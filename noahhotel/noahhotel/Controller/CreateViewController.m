//
//  CreateViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "CreateViewController.h"
#import "FormatModel.h"
#import "Networkservice.h"
#import "UserModel.h"
#import <SVProgressHUD.h>
#import <SMS_SDK/SMS_SDK.h>

@interface CreateViewController ()

/**
 *  负责人姓名
 */
@property (nonatomic, weak) IBOutlet UITextField *username;
/**
 *  密码
 */
@property (nonatomic, weak) IBOutlet UITextField *password;
/**
 *  确认密码
 */
@property (nonatomic, weak) IBOutlet UITextField *passwordAgain;
/**
 *  手机号
 */
@property (nonatomic, weak) IBOutlet UITextField *telephone;
/**
 *  验证码
 */
@property (nonatomic, weak) IBOutlet UITextField *verificationCode;
/**
 *  公司名称
 */
@property (nonatomic, weak) IBOutlet UITextField *company;
@property (nonatomic, weak) IBOutlet UITextField *name;
/**
 *  验证码
 */
@property (nonatomic, weak) IBOutlet UIButton *getVerificationCodeButton;

@end

@implementation CreateViewController

- (IBAction)create
{
    if (![FormatModel isValidFormatWithTelephone:self.telephone.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.telephone.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    if (![FormatModel isValidFormatWithVerificationCode:self.verificationCode.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.verificationCode.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    if (![FormatModel isValidFormatWithName:self.username.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.username.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    if (!(self.name.text.length > 0 && self.name.text.length < 16)) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.name.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    if (!(self.company.text.length > 0 && self.company.text.length < 33)) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.company.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    if (![FormatModel isValidFormatWithPassword:self.password.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.password.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    if (![self.password.text isEqualToString:self.passwordAgain.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@和%@不相同", self.password.placeholder, self.passwordAgain.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    UserModel *user = [UserModel sharedInstance];
    user.username = self.username.text;
    user.company = self.company.text;
    user.password = self.password.text;
    user.telephone = self.telephone.text;
    user.name = self.name.text;
    user.code = self.verificationCode.text;
    
    [[Networkservice sharedInstance] createWithUser:user success:^(UserModel *user) {
        [SVProgressHUD showSuccessWithStatus:@"注册成功"];
        
        DDLogVerbose(@"%@", user);
        
        user = [UserModel getUser];
        
        [[Networkservice sharedInstance] loginWithUser:user success:^(UserModel *user) {
            
            DDLogVerbose(@"%@", user);
            
            [self skip];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        }];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
    
    [self.view endEditing:YES];
}

- (IBAction)getVerificationCode
{
    if (![FormatModel isValidFormatWithTelephone:self.telephone.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.telephone.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
//    [SVProgressHUD showWithStatus:@"正在发送验证码"];
//    [SMS_SDK getVerificationCodeBySMSWithPhone:self.telephone.text zone:SMS_SDK_Zone result:^(SMS_SDKError *error) {
//        if (error == nil) {
//            [SVProgressHUD showSuccessWithStatus:@"验证码发送成功"];
//            
//            [self getVerificationCodeCountdown];
//        } else {
//            [SVProgressHUD showErrorWithStatus:@"验证码发送失败"];
//        }
//    }];
    
//    [[Networkservice sharedInstance] getCodeWithsuccess:^(NSNumber *code) {
//        self.verificationCode.text = code.stringValue;
//    } failure:^(NSError *error) {
//        
//    }];
    
    [[Networkservice sharedInstance] getCodeWithTel:self.telephone.text success:^(NSString *code) {
        
        self.verificationCode.text = code;
        
        [self getVerificationCodeCountdown];
        
    } failure:^(NSError *error) {
        
    }];
    
    [self.view endEditing:YES];
}

/** 发送验证码按钮倒计时 */
- (void)getVerificationCodeCountdown
{
    // 倒计时总时间
    __block int timeout = 60;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    // 设置每秒执行一次
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0);
    
    dispatch_source_set_event_handler(_timer, ^{
        // 进行倒计时
        if (timeout <= 0) {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.getVerificationCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
                self.getVerificationCodeButton.userInteractionEnabled = YES;
                self.getVerificationCodeButton.alpha = 1;
            });
        } else {
            NSString *strTime = [NSString stringWithFormat:@"%.2d", timeout];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.getVerificationCodeButton.titleLabel.text = [NSString stringWithFormat:@"%@s",strTime];
                [self.getVerificationCodeButton setTitle:[NSString stringWithFormat:@"%@s",strTime] forState:UIControlStateNormal];
                self.getVerificationCodeButton.userInteractionEnabled = NO;
                self.getVerificationCodeButton.alpha = 0.5;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

@end
