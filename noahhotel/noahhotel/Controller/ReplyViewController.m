//
//  ReplyViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "ReplyViewController.h"
#import <SVProgressHUD.h>
#import "Networkservice.h"
#import "BaseModel.h"
#import "ReplyModel.h"
#import "NeedViewController.h"

@interface ReplyViewController () <UITextViewDelegate>

/**
 *  留言内容
 */
@property (weak, nonatomic) IBOutlet UILabel *feedback;
/**
 *  留言内容
 */
@property (nonatomic, weak) IBOutlet UITextView *content;
/**
 *  联系方式
 */
@property (nonatomic, weak) IBOutlet UITextField *contact;
@end

@implementation ReplyViewController

- (IBAction)reply
{
    if (self.content.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"留言内容不能为空"];
        return;
    }
    
    if (self.contact.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"联系方式不能为空"];
        return;
    }
    
    
    
    if (self.hotelId) {
        ReplyModel *reply = [[ReplyModel alloc] init];
        reply.content = self.content.text;
        reply.phone = self.contact.text;
        
        [SVProgressHUD showWithStatus:@"正在留言"];
        [[Networkservice sharedInstance] hotelReplyWithId:self.hotelId Reply:reply success:^(ReplyModel *reply) {
            [SVProgressHUD showSuccessWithStatus:@"留言成功"];
            
            DDLogVerbose(@"%@", reply);
            
            [self back];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        }];
        
        return;
    }
    
    ReplyModel *reply = [[ReplyModel alloc] init];
    reply.content = self.content.text;
    reply.phone = self.contact.text;
    
    
    
    [SVProgressHUD showWithStatus:@"正在留言"];
    [[Networkservice sharedInstance] replyWithId:self.myId Reply:reply success:^(ReplyModel *reply) {
        [SVProgressHUD showSuccessWithStatus:@"留言成功"];
        
        
        
        DDLogVerbose(@"%@", reply);
        
        [self back];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    self.feedback.hidden = (textView.text.length != 0);
}

@end
