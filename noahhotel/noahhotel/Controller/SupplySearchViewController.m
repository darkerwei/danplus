//
//  SupplySearchViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SupplySearchViewController.h"
#import "CycleViewController.h"
#import "Networkservice.h"
#import "HotelModel.h"
#import <SVProgressHUD.h>
#import "DetailViewController.h"
#import "StoryboardModel.h"
#import "MySearchBar.h"
#import "DemandModel.h"
#import "DemandTableViewCell.h"
#import "ShadeView.h"
#import "MyDemandViewController.h"
#import "DemandDetailsViewController.h"
#import "SupplyValueModel.h"
#import "SupplyDetailViewController.h"
#import "SuTableViewCell.h"

@interface SupplySearchViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *demandArray;

@end

@implementation SupplySearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SVProgressHUD showWithStatus:@"正在获取舞台供应商列表"];
    [[Networkservice sharedInstance] searchWithId:self.myId name:@"" rows:@"10" page:@"1" success:^(NSArray *supplyValueArray) {
        [SVProgressHUD showSuccessWithStatus:@"获取舞台供应商列表成功"];
        
        
        self.demandArray = [NSArray arrayWithArray:supplyValueArray];
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.demandArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *TableSampleIdentifier = @"cell";
//    //    用TableSampleIdentifier表示需要重用的单元
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
//    //    如果如果没有多余单元，则需要创建新的单元
//    if (!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
//    }
//    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    cell.textLabel.text = [self.demandArray[indexPath.row] title];
//    cell.textLabel.font = [UIFont systemFontOfSize:14];
//    cell.textLabel.textColor = [UIColor colorWithRed:95 / 255.0 green:95 / 255.0 blue:95 / 255.0 alpha:1.0];
//    cell.detailTextLabel.text = [ name];
//    cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
//    cell.detailTextLabel.textColor = [UIColor colorWithRed:142 / 255.0 green:142 / 255.0 blue:142 / 255.0 alpha:1.0];
//    
//    NSLog(@"%@", [self.demandArray[indexPath.row] picture]);
    
    SuTableViewCell *cell = [SuTableViewCell cellWithTableView:tableView];
    cell.model = self.demandArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

#pragma mark - UITabBarDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SupplyDetailViewController_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[SupplyDetailViewController class]]) {
        SupplyDetailViewController *supplyDetailViewController = (SupplyDetailViewController *)viewController;
        supplyDetailViewController.myId = [self.demandArray[indexPath.row] myID];
        supplyDetailViewController.title = [self.demandArray[indexPath.row] name];
        [self.navigationController pushViewController:supplyDetailViewController animated:YES];
    }
}

@end
