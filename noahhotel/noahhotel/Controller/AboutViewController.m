//
//  AboutViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutViewController.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "AboutModel.h"
//#import "AboutOneTableViewCell.h"
#import "JWAboutOneTableViewCell.h"
//#import "AboutTwoTableViewCell.h"
#import "AboutThreeTableViewCell.h"
#import "AboutFourTableViewCell.h"
#import "AboutFiveTableViewCell.h"
#import "AboutSixTableViewCell.h"
#import "AboutButtonTableViewCell.h"
#import "StoryboardModel.h"
#import "ReplyViewController.h"
#import "BaseModel.h"
#import "CooperationViewController.h"
#import "UserModel.h"
#import "NavigationController.h"
#import "MapViewController.h"
#import "AgreeViewController.h"
#import "PhotoViewController.h"
#import "HotelDescriptionTableViewCell.h"
#import "ServiceTableViewCell.h"
#import "SupportingTableViewCell.h"

@interface AboutViewController () <AboutButtonTableViewCellDelegate, JWAboutOneTableViewCellDelegate, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic ,strong) AboutModel *about;

@property (nonatomic, assign) int number;

@property (nonatomic, assign) int one;
@property (nonatomic, assign) int two;
@property (nonatomic, assign) int three;
@property (nonatomic, assign) int four;
@property (nonatomic, assign) int five;
@property (nonatomic, assign) int six;
@property (nonatomic, assign) int other;
@property (nonatomic, assign) int desc;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.separatorStyle = NO;
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.title = self.hotel.hotelName;
    
    [[Networkservice sharedInstance] aboutWithId:self.hotel.myId success:^(AboutModel *about) {
        
        DDLogVerbose(@"%@", about);
        
        self.about = about;
        
        self.number = 9;
        self.one = (int)self.about.rooms.count + 1;
        self.two = (int)self.about.meetings.count + 1;
        self.three = (int)self.about.feasts.count;
        self.four = 2;
        self.five = 1;
        self.six = 1;
        self.other = 1;
        self.desc = 0;
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.number;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else if (section == 2) {
        return self.one;
    }
    else if (section == 3) {
        return self.two;
    }
    else if (section == 4) {
        return self.three;
    }
    else if (section == 5) {
        return self.four;
    }
    else if (section == 6) {
        return self.five;
    }
    else if (section == 7) {
        return self.six;
    }
    else if (section == 8) {
        return self.other;
    }
    else if (section == 1) {
        return self.desc;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        JWAboutOneTableViewCell *cell = [JWAboutOneTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.about;
        cell.delegate = self;
        
        NSNumber *number = [[NSUserDefaults standardUserDefaults] objectForKey:@"approve"];
        if (number.intValue == 1) {
            cell.showAgree = YES;
        } else {
            cell.showAgree = NO;
        }
        
        return cell;
        
        return cell;
    } else if (indexPath.section == 2) {
        
        if (indexPath.row < self.one - 1) {
            AboutThreeTableViewCell *cell = [AboutThreeTableViewCell cellWithTableView:tableView];
            cell.title = @"房型";
            cell.meetingsModel = self.about.rooms[indexPath.row];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        } else {
            ServiceTableViewCell *cell = [ServiceTableViewCell cellWithTableView:tableView];
            cell.desc = self.about.roomDescription;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
        
    }else if (indexPath.section == 3) {
        
        if (indexPath.row < self.two - 1) {
            AboutThreeTableViewCell *cell = [AboutThreeTableViewCell cellWithTableView:tableView];
            cell.title = @"会议室名称";
            cell.meetingsModel = self.about.meetings[indexPath.row];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        } else {
            ServiceTableViewCell *cell = [ServiceTableViewCell cellWithTableView:tableView];
            cell.desc = self.about.meetingDescription;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
        
        
        
    }
    else if (indexPath.section == 4) {
        AboutThreeTableViewCell *cell = [AboutThreeTableViewCell cellWithTableView:tableView];
        cell.title = @"宴会厅名称";
        cell.meetingsModel = self.about.feasts[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
        
    }
    else if (indexPath.section == 5) {
//        AboutFourTableViewCell *cell = [AboutFourTableViewCell cellWithTableView:tableView];
//        cell.about = self.about;
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        
//        return cell;
        
        if (indexPath.row == 0) {
            SupportingTableViewCell *cell = [SupportingTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.titleStr = @"酒店内配套";
            cell.array = self.about.innerSet;
            return cell;
        } else {
            SupportingTableViewCell *cell = [SupportingTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.titleStr = @"酒店外配套";
            cell.array = self.about.withoutSet;
            return cell;
        }
        
        
        
        
    }
    else if (indexPath.section == 6) {
        SupportingTableViewCell *cell = [SupportingTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleStr = @"酒店周边配套（5公里范围内）";
        cell.array = self.about.aroundSet;
        return cell;
        
        
    }
    else if (indexPath.section == 7) {
        AboutSixTableViewCell *cell = [AboutSixTableViewCell cellWithTableView:tableView];
        cell.about = self.about;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
        
    }
    else if (indexPath.section == 8) {
        AboutButtonTableViewCell *cell = [AboutButtonTableViewCell cellWithTableView:tableView];
        cell.about = self.about;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        
        return cell;
        
        
    }
    
    else if (indexPath.section == 1) {
        HotelDescriptionTableViewCell *cell = [HotelDescriptionTableViewCell cellWithTableView:tableView];
        cell.desc = self.about.hotelDescription;
        
        return cell;
    }
    
    else {
        return [[UITableViewCell alloc] init];
    }
}

- (void)addressClick
{
    MapViewController *mapVC = [[MapViewController alloc] init];
    mapVC.hotelModel = self.hotel;
    
    [self.navigationController pushViewController:mapVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        if ((int)self.about.rooms.count == 0) {
            return 0;
        }
        return 30;
    }
    
    if (section == 3) {
        if ((int)self.about.meetings.count == 0) {
            return 0;
        }
        return 30;
    }
    
    if (section == 4) {
        if ((int)self.about.feasts.count == 0) {
            return 0;
        }
        return 30;
    }
    if (section == 5) {
        if (self.about.innerSet.count == 0 || self.about.withoutSet.count == 0) {
            return 0;
        }
        return 30;
    }
    if (section == 6) {
        if (self.about.aroundSet.count == 0) {
            return 0;
        }
        return 30;
    }
    if (section == 7) {
        return 30;
    }
    
    if (section == 1) {
        return 30;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 610;
    } else if (indexPath.section == 2) {
        

        if (indexPath.row < self.one - 1) {
            MeetingsModel *meet = self.about.rooms[indexPath.row];
            
            return 41 + 41 * (int)(meet.content.count * 0.5 + 0.5);
        } else {
            return [self.about.roomDescription boundingRectWithSize:CGSizeMake(238, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 16;

        }
        
        
        
    } else if (indexPath.section == 3) {
        if (indexPath.row < self.two - 1) {
            MeetingsModel *meet = self.about.meetings[indexPath.row];
            
            return 41 + 41 * (int)(meet.content.count * 0.5 + 0.5);
        } else {
            return [self.about.meetingDescription boundingRectWithSize:CGSizeMake(238, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 16;
            
        }
        
        
    } else if (indexPath.section == 4) {
        MeetingsModel *meet = self.about.feasts[indexPath.row];
        
        return 41 + 41 * (int)(meet.content.count * 0.5 + 0.5);
    } else if (indexPath.section == 5) {
        
        if (indexPath.row == 0) {
            return 34 + 38 * (int)(self.about.innerSet.count * 0.34 + 0.7);
        } else {
            return 34 + 38 * (int)(self.about.withoutSet.count * 0.34 + 0.7);
        }
        
        
    } else if (indexPath.section == 6) {
        return 34 + 38 * (int)(self.about.aroundSet.count * 0.34 + 0.7);
    } else if (indexPath.section == 7) {
        return 80;
    }
    else if (indexPath.section == 8) {
        return 80;
    }
    else if (indexPath.section == 1) {
        return [self.about.hotelDescription boundingRectWithSize:CGSizeMake(296, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 16;
    }
    else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    酒店介绍" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        
        return btn;
    }
    else if (section == 2) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    客房" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        return btn;
    }
    
    else if (section == 3) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    会议室" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        return btn;
    }else if (section == 4) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    宴会厅" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        return btn;
    }
    else if (section == 5) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    酒店配套" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        return btn;
    }
    
    else if (section == 6) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    酒店周边环境" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        return btn;
    }
    
    else if (section == 7) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    其他联系方式" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        return btn;
    }
    
    else {
        return [[UIView alloc] init];
    }
}

- (void)btnClick:(UIButton *)btn
{
    if ([btn.titleLabel.text isEqualToString:@"    客房"]) {
        if (self.one == 0) {
            self.one = (int)self.about.rooms.count + 1;
        } else {
            self.one = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    }else if ([btn.titleLabel.text isEqualToString:@"    会议室"]) {
        if (self.two == 0) {
            self.two = (int)self.about.meetings.count + 1;
        } else {
            self.two = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    } else if ([btn.titleLabel.text isEqualToString:@"    宴会厅"]) {
        if (self.three == 0) {
            self.three = (int)self.about.feasts.count;
        } else {
            self.three = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    }else if ([btn.titleLabel.text isEqualToString:@"    酒店配套"]) {
        if (self.four == 0) {
            self.four = 2;
        } else {
            self.four = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    }else if ([btn.titleLabel.text isEqualToString:@"    酒店周边环境"]) {
        if (self.five == 0) {
            self.five = 1;
        } else {
            self.five = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    }else if ([btn.titleLabel.text isEqualToString:@"    其他联系方式"]) {
        if (self.six == 0) {
            self.six = 1;
        } else {
            self.six = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    } else if ([btn.titleLabel.text isEqualToString:@"    酒店介绍"]) {
        if (self.desc == 0) {
            self.desc = 1;
        } else {
            self.desc = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)hotelReply
{
    if ([UserModel isLogin]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_REPLYVIEWCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[ReplyViewController class]]) {
            ReplyViewController *replyViewController = (ReplyViewController *)viewController;
            replyViewController.hotelId = self.hotel.myId;
            [self.navigationController pushViewController:replyViewController animated:YES];
        }
    } else {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[NavigationController class]]) {
            [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
        }
    }
}

- (void)agreeBtnCLick
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_AgreeViewController_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[AgreeViewController class]]) {
        AgreeViewController *replyViewController = (AgreeViewController *)viewController;
        replyViewController.myID = self.hotel.myId.stringValue;
        [self.navigationController pushViewController:replyViewController animated:YES];
    }
}

- (void)photoClick
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:@"PhotoViewController"];
    
    if ([viewController isKindOfClass:[PhotoViewController class]]) {
        PhotoViewController *replyViewController = (PhotoViewController *)viewController;
        replyViewController.title = self.title;
        NSMutableArray *array = [NSMutableArray array];
        [array addObjectsFromArray:self.about.logo];
        [array addObjectsFromArray:self.about.meeting];
        [array addObjectsFromArray:self.about.feast];
        [array addObjectsFromArray:self.about.room];
        [array addObjectsFromArray:self.about.hotel];
        
        if (array.count == 0) {
            return;
        }
        
        replyViewController.photo = array;
        [self.navigationController pushViewController:replyViewController animated:YES];
    }
}

@end
