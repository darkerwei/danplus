//
//  SupplyViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SupplyViewController.h"
#import "MySearchBar.h"
#import "SupplyCollectionViewCell.h"
#import "RecipeCollectionHeaderView.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "SupplyModel.h"
#import "SupplyValueModel.h"
#import "StoryboardModel.h"
#import "SupplySearchViewController.h"
#import "CycleViewController.h"
#import "HotelImageModel.h"
#import "SearchViewController.h"
#import "QFQBannerView.h"
#import "SupplyDetailViewController.h"

@interface SupplyViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, RecipeCollectionHeaderViewDelegate, QFQBannerViewDelegate>

@property (nonatomic, strong) MySearchBar *searchBar;
@property (weak, nonatomic) IBOutlet QFQBannerView *containerView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout;
@property (nonatomic, strong) NSArray *supplyArray;

@property (nonatomic, strong) NSMutableArray *noShow;

@end

@implementation SupplyViewController

- (void)bannerView:(QFQBannerView *)bannerView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    QFQHomeCollecionModel *model = bannerView.data[indexPath.row];
    
    NSLog(@"%@",  model.linkUrl);
    NSLog(@"%@",  model.linkType);
    
    if ([model.linkType isEqualToString:@"2"]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SupplyDetailViewController_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[SupplyDetailViewController class]]) {
            SupplyDetailViewController *supplyDetailViewController = (SupplyDetailViewController *)viewController;
            supplyDetailViewController.myId = [NSNumber numberWithInt:model.linkUrl.intValue];
            [self.navigationController pushViewController:supplyDetailViewController animated:YES];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.containerView.delegate = self;
    
    self.noShow = [[NSMutableArray alloc] init];
    
    [[Networkservice sharedInstance] getSupplyImagesuccess:^(NSArray *hotelImageArray) {
        
        
        NSMutableArray *mArray = [NSMutableArray array];
        for (HotelImageModel *image in hotelImageArray) {
            QFQHomeCollecionModel *model = [QFQHomeCollecionModel new];
            model.imgUrl = image.phonePicture;
            model.linkUrl = image.linkId.stringValue;
            model.linkType = image.type.stringValue;
            [mArray addObject:model];
        }
        
        self.containerView.data = mArray;
    } failure:^(NSError *error) {
        
    }];
    
    // 搜索框
    self.searchBar = [[MySearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 30)];
    self.navigationItem.titleView = self.searchBar;
    self.searchBar.buttonName = @"";
    self.collectionView.contentInset = UIEdgeInsetsMake(160, 0, 0, 0);
    self.searchBar.delegate = self;
    
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
    
    [[Networkservice sharedInstance] getSortWithsuccess:^(NSArray *supplyArray) {
        
        for (SupplyModel *supply in supplyArray) {
            DDLogVerbose(@"%@", supply);
            
        }
        
        self.supplyArray = supplyArray;
        
        [self.collectionView reloadData];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SearchViewController_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[SearchViewController class]]) {
        SearchViewController *searchViewController = (SearchViewController *)viewController;
        searchViewController.title = @"供应商查询";
        [self.navigationController pushViewController:searchViewController animated:YES];
    }
    return NO;
}

- (NSArray *)imageURLs {
    
    NSMutableArray *arrayM = [NSMutableArray array];
    
    for (int i = 1; i <= 3; i++) {
        NSURL *url = [[NSURL alloc] initWithString:@"http://115.28.94.253:8098/upload/2/5/a/3/25a33b4c3bb976a92a88b048aceec575.jpg"];
        
        [arrayM addObject:url];
    }
    
    return arrayM;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    self.containerView.alpha = (1 - (scrollView.contentOffset.y + 110) / 110.0);
    
    self.layout.constant = -(scrollView.contentOffset.y + 160);
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    // 设置容器视图高度
//    CGFloat w = [UIScreen mainScreen].bounds.size.width;
//    CGFloat h = 110;
//    self.containerView.bounds = CGRectMake(0, 0, w, h);
}

#pragma mark - UICollectionViewDatasource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.supplyArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    SupplyModel *supply = self.supplyArray[section];
    
    for (NSNumber *number in self.noShow) {
        if (number.intValue == section) {
            return 0;
        }
    }
    
    return supply.value.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath

{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader){
        
        RecipeCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        headerView.button.tag = indexPath.section;
        headerView.delegate = self;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [headerView addSubview:imageView];
        
        SupplyModel *supply = self.supplyArray[indexPath.section];
        
        headerView.title.text = [NSString stringWithFormat:@"    %@", supply.name];
            
        reusableview = headerView;
        
    }
    
    return reusableview;
}

- (void)buttonClick:(UIButton *)button
{
    for (NSNumber *number in self.noShow) {
        if (number.intValue == button.tag) {
            [self.noShow removeObject:number];
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:button.tag]];
            return;
        }
    }
    
    [self.noShow addObject:[NSNumber numberWithInt:button.tag]];
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:button.tag]];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SupplyModel *supply = self.supplyArray[indexPath.section];
    SupplyValueModel *supplyView = supply.value[indexPath.row];
    
    SupplyCollectionViewCell * cell = [SupplyCollectionViewCell cellWithCollectionView:collectionView indexPath:indexPath];
    cell.supplyValue = supplyView;
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width - 50) * 0.25, ([UIScreen mainScreen].bounds.size.width - 50) * 0.25 + 30);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SupplyModel *supply = self.supplyArray[indexPath.section];
    SupplyValueModel *supplyView = supply.value[indexPath.row];
    
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SupplySearchViewController_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[SupplySearchViewController class]]) {
        SupplySearchViewController *supplySearchViewController = (SupplySearchViewController *)viewController;
        supplySearchViewController.myId = supplyView.myID;
        supplySearchViewController.title = supplyView.name;
        [self.navigationController pushViewController:supplySearchViewController animated:YES];
    }

}

@end
