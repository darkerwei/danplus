//
//  FeedbackViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/13.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "FeedbackViewController.h"
#import <SVProgressHUD.h>
#import "Networkservice.h"
#import "BaseModel.h"

@interface FeedbackViewController () <UITextViewDelegate>

/**
 *  意见反馈
 */
@property (weak, nonatomic) IBOutlet UILabel *feedback;
/**
 *  反馈内容
 */
@property (nonatomic, weak) IBOutlet UITextView *content;
/**
 *  联系方式
 */
@property (nonatomic, weak) IBOutlet UITextField *contact;
@end

@implementation FeedbackViewController

- (IBAction)suggest
{
    if (self.content.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"意见反馈不能为空"];
        return;
    }
    
    if (self.contact.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"联系方式不能为空"];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"正在提交"];
    
    
    [[Networkservice sharedInstance] suggestWithContent:self.content.text contact:self.contact.text success:^(NSNumber *suggestID) {
        [SVProgressHUD showSuccessWithStatus:@"提交成功"];
        
        DDLogVerbose(@"%@", suggestID);
        
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    self.feedback.hidden = (textView.text.length != 0);
}

@end
