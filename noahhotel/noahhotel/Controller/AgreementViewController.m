//
//  AgreementViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/9/28.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "AgreementViewController.h"

@interface AgreementViewController ()

@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end

@implementation AgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"同行合作方案";
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://115.28.94.253:8098/main/agreement/detail/app/%@", self.myID]];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

@end
