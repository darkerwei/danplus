//
//  CooperationViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/9/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "CooperationViewController.h"
#import "AgreeViewController.h"
#import "StoryboardModel.h"
#import "CooperationTableViewCell.h"

@interface CooperationViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CooperationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"同行合作方案协议";
    
    self.tableView.separatorStyle = NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CooperationTableViewCell *cell = [CooperationTableViewCell cellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 500;
}

- (IBAction)agree
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_AgreeViewController_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[AgreeViewController class]]) {
        AgreeViewController *replyViewController = (AgreeViewController *)viewController;
        [self.navigationController pushViewController:replyViewController animated:YES];
    }
}

@end
