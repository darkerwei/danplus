//
//  DetailViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/18.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DetailViewController.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "BaseModel.h"
#import "KeyValueModel.h"
#import "DetailViewTableViewCell.h"
#import "DetailViewOtherTableViewCell.h"
#import "ShadeView.h"
#import "HotelInterfaceModel.h"
#import "DetailView2TableViewCell.h"
#import "HotelInsideTableViewCell.h"
#import "ButtonTableViewCell.h"
#import "HomeViewController.h"
#import "SelectModel.h"
#import "MapViewController.h"
#import "HomeFilterViewController.h"
#import "StoryboardModel.h"
#import "SelectModel.h"

#define CarNumberPickerViewHeight 200

@interface DetailViewController () <UITableViewDelegate, UITableViewDataSource, ShadeViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, ButtonTableViewCellDelegate, HotelInsideTableViewCellDelegate, UISearchBarDelegate, DetailViewOtherTableViewCellDelegate>

@property (nonatomic, strong) HotelInterfaceModel *hotel;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UIPickerView *carNumberPickerView;
@property (strong, nonatomic) ShadeView *shadeView;

@property (strong, nonatomic) NSArray *tempArray;
@property (assign, nonatomic) int tag;

@property (assign, nonatomic) int two;

@property (assign, nonatomic) int three;
@property (assign, nonatomic) int four;

@property (nonatomic, strong) NSMutableDictionary *dict;

@property (nonatomic, assign) int one;

@property (nonatomic, strong) UISearchBar *searchBar;

@end

@implementation DetailViewController

- (NSMutableDictionary *)dict
{
    if (!_dict) {
        
        _dict = [[NSMutableDictionary alloc] init];
        
        NSDictionary *temp = @{ @"21" : @"不限",
                   @"22" : @"不限",
                   @"23" : @"不限",
                   @"24" : @"不限",
                   @"31" : @"不限",
                   @"32" : @"不限",
                   @"33" : @"不限",
                   @"34" : @"不限",
                   @"15" : @"不限",
                   @"16" : @"不限",
                   @"12" : @"不限",
                   @"11" : @"不限",
                    @"13" : @"不限",
                    @"14" : @"不限",
                    @"17" : @"不限",
                    @"18" : @"不限",
                    @"19" : @"不限"
                   };
        
        _dict = [NSMutableDictionary dictionaryWithDictionary:temp];
    }
    
    return _dict;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 去除cell间的分割线
    self.tableView.separatorStyle = NO;
    self.title = @"酒店筛选";
    
    [[Networkservice sharedInstance] hotelInterfaceWithsuccess:^(HotelInterfaceModel *hotel) {
        
        self.hotel = hotel;
        self.two = 4;
        self.three = 4;
        self.four = 3;
        self.one = 5;
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.one;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 9;
    } else if (section == 1) {
        return self.two;
    } else if (section == 2) {
        return self.three;
    } else if (section == 3) {
        return self.four;
    } else if (section == 4) {
        return 1;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        
        if (indexPath.row == 0) {
            DetailViewOtherTableViewCell *cell = [DetailViewOtherTableViewCell cellWithTableView:tableView];
            cell.delegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"";
            cell.tag = 11;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 1) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"行政区域";
            cell.tag = 12;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 2) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"区域";
            cell.tag = 13;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 3) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"地铁";
            cell.tag = 14;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 4) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"车站";
            cell.tag = 15;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 5) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"机场";
            cell.tag = 16;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 6) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"商圈";
            cell.tag = 17;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 7) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"酒店性质";
            cell.tag = 18;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"酒店标准";
            cell.tag = 19;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        }
        
        
        
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            
            DetailView2TableViewCell *cell = [DetailView2TableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"会议房价";
            cell.tag = 24;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 1) {
            DetailView2TableViewCell *cell = [DetailView2TableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"会议室数量";
            cell.tag = 22;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 2) {
            DetailView2TableViewCell *cell = [DetailView2TableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"最大会议室面积";
            cell.tag = 23;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else {
            DetailView2TableViewCell *cell = [DetailView2TableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"最大会议室容纳人数";
            cell.tag = 21;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"报价";
            cell.tag = 31;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 1) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"预计桌数";
            cell.tag = 32;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else if (indexPath.row == 2) {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"场地类型";
            cell.tag = 33;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        } else {
            DetailViewTableViewCell *cell = [DetailViewTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.title = @"装修风格";
            cell.tag = 34;
            cell.name = self.dict[[NSString stringWithFormat:@"%ld", (long)cell.tag]];
            
            return cell;
        }
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            HotelInsideTableViewCell *cell = [HotelInsideTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.array  = self.hotel.innerSet;
            cell.show = YES;
            cell.title = @"酒店内配套";
            
            return cell;
        } else if (indexPath.row == 1) {
            HotelInsideTableViewCell *cell = [HotelInsideTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.array  = self.hotel.withoutSet;
            cell.title = @"酒店外配套";
            cell.show = YES;
            
            return cell;
        } else if (indexPath.row == 2) {
            HotelInsideTableViewCell *cell = [HotelInsideTableViewCell cellWithTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.array  = self.hotel.aroundSet;
            cell.title = @"酒店周边环境";
            cell.show = NO;
            cell.tag = 35;
            cell.delegate = self;
            
            return cell;
        }
    } else if (indexPath.section == 4) {
        ButtonTableViewCell *cell = [ButtonTableViewCell cellWithTableView:tableView];
        cell.delegate = self;
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 44;
    } else if (section == 4) {
        return 10;
    }else {
        return 30;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            self.tempArray = self.hotel.city;
            self.tag = 11;
        } else if (indexPath.row == 1) {
            self.tempArray = self.hotel.division;
            self.tag = 12;
        } else if (indexPath.row == 2) {
            self.tempArray = self.hotel.zone;
            self.tag = 13;
        } else if (indexPath.row == 3) {
            self.tempArray = self.hotel.subway;
            self.tag = 14;
        } else if (indexPath.row == 4) {
            self.tempArray = self.hotel.station;
            self.tag = 15;
        } else if (indexPath.row == 5) {
            self.tempArray = self.hotel.airport;
            self.tag = 16;
        } else if (indexPath.row == 6) {
            self.tempArray = self.hotel.businessArea;
            self.tag = 17;
        } else if (indexPath.row == 7) {
            self.tempArray = self.hotel.hotelPropery;
            self.tag = 18;
        } else {
            self.tempArray = self.hotel.hotelLevel;
            self.tag = 19;
        }
        
        self.shadeView = [[ShadeView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.shadeView.delegate = self;
        [self.shadeView showWithView:self.view];
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            self.tempArray = self.hotel.meetingPrice;
            self.tag = 24;
        } else if (indexPath.row == 1) {
            self.tempArray = self.hotel.meetingNumber;
            self.tag = 22;
        } else if (indexPath.row == 2) {
            self.tempArray = self.hotel.meetingSize;
            self.tag = 23;
        } else if (indexPath.row == 3) {
            self.tempArray = self.hotel.maxMeetingPerson;
            self.tag = 21;
        }
        
        self.shadeView = [[ShadeView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.shadeView.delegate = self;
        [self.shadeView showWithView:self.view];
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            self.tempArray = self.hotel.feastPrice;
            self.tag = 31;
        } else if (indexPath.row == 1) {
            self.tempArray = self.hotel.feastTable;
            self.tag = 32;
        } else if (indexPath.row == 2) {
            self.tempArray = self.hotel.feastPlace;
            self.tag = 33;
        } else if (indexPath.row == 3) {
            self.tempArray = self.hotel.feastStyle;
            self.tag = 34;
        }
        
        self.shadeView = [[ShadeView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.shadeView.delegate = self;
        [self.shadeView showWithView:self.view];
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.tempArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    KeyValueModel *value = self.tempArray[row];
    
    return  value.value;
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    DetailViewOtherTableViewCell *cell = (DetailViewOtherTableViewCell *)[self.tableView viewWithTag:self.tag];
    
    KeyValueModel *temp = self.tempArray[row];
    
    self.dict[[NSString stringWithFormat:@"%ld", (long)self.tag]] = temp.value;
    
    cell.name = temp.value;
}

#pragma mark - ShadeViewDelegate

- (void)show
{
    self.carNumberPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height * 0.4)];
    self.carNumberPickerView.backgroundColor = [UIColor whiteColor];
    self.carNumberPickerView.dataSource = self;
    self.carNumberPickerView.delegate = self;
    [self.view addSubview:self.carNumberPickerView];
    
    [UIView animateWithDuration:ShadeAnimationTime animations:^{
        self.carNumberPickerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height * 0.4, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height * 0.6);
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:ShadeAnimationTime animations:^{
        self.carNumberPickerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height * 0.6);
    } completion:^(BOOL finished) {
        [self.carNumberPickerView removeFromSuperview];
    }];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        // 设置搜索框
        self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
        self.searchBar.delegate = self;
        self.searchBar.placeholder = @"搜索";
        return self.searchBar;
    }
    
    if (section == 1) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    会议选项" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        
        return btn;
    } else if (section == 2) {
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    婚宴选项" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        
        return btn;
    }else if (section == 3) {
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        [btn setTitle:@"    酒店配套" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:50 / 255.0 green:50 / 255.0 blue:50 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = section;
        btn.backgroundColor = [UIColor colorWithRed:235 / 255.0 green:235 / 255.0 blue:235 / 255.0 alpha:1.0];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 30, 15 - 5.5 * 0.7, 21 * 0.7, 11 * 0.7)];
        imageView.image = [UIImage imageNamed:@"下拉"];
        [btn addSubview:imageView];
        
        return btn;
    } else {
        return [[UIView alloc] init];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            return 30 + 30 * (int)(self.hotel.innerSet.count * 0.5 + 0.5);
        } else if (indexPath.row == 1) {
            return 30 + 30 * (int)(self.hotel.withoutSet.count * 0.5 + 0.5);
        } else if (indexPath.row == 2) {
            return 30 + 30 * (int)(self.hotel.aroundSet.count * 0.5 + 0.5);
        }
    }
    
    return 44;
}

- (void)btnClick:(UIButton *)btn
{
    if ([btn.titleLabel.text isEqualToString:@"    会议选项"]) {
        if (self.two == 0) {
            self.two = 4;
        } else {
            self.two = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    } else if ([btn.titleLabel.text isEqualToString:@"    婚宴选项"]) {
        if (self.three == 0) {
            self.three = 4;
        } else {
            self.three = 0;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    } else if ([btn.titleLabel.text isEqualToString:@"    酒店配套"]) {
        if (self.four == 0) {
            self.four = 3;

        } else {
            self.four = 0;

        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)quit
{
    SelectModel *select = [[SelectModel alloc] init];
    
    
    NSString *name = self.dict[@"21"];
    
    if ([name isEqualToString:@"不限"]) {
        select.maxMeetingPerson = @"";
    } else {
        select.maxMeetingPerson = name;
    }
    name = self.dict[@"22"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingNumber = @"";
    } else {
        select.meetingNumber = name;
    }
    name = self.dict[@"23"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingSize = @"";
    } else {
        select.meetingSize = name;
    }
    name = self.dict[@"24"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingPrice = @"";
    } else {
        select.meetingPrice = name;
    }
    
    name = self.dict[@"31"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastPrice = @"";
    } else {
        select.feastPrice = name;
    }
    
    name = self.dict[@"32"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastTable = @"";
    } else {
        select.feastTable = name;
    }
    
    name = self.dict[@"33"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastPlace = @"";
    } else {
        select.feastPlace = name;
    }
    
    name = self.dict[@"34"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastStyle = @"";
    } else {
        select.feastStyle = name;
    }
    
    name = self.dict[@"15"];
    
    if ([name isEqualToString:@"不限"]) {
        select.station = @"";
    } else {
        select.station = name;
    }
    
    name = self.dict[@"16"];
    
    if ([name isEqualToString:@"不限"]) {
        select.airport = @"";
    } else {
        select.airport = name;
    }
    
    name = self.dict[@"12"];
    
    if ([name isEqualToString:@"不限"]) {
        select.division = @"";
    } else {
        select.division = name;
    }
    name = self.dict[@"11"];
    
    if ([name isEqualToString:@"不限"]) {
        select.city = @"";
    } else {
        select.city = name;
    }
    
    name = self.dict[@"17"];
    
    if ([name isEqualToString:@"不限"]) {
        select.businessArea = @"";
    } else {
        select.businessArea = name;
    }
    
    name = self.dict[@"19"];
    
    if ([name isEqualToString:@"不限"]) {
        select.hotelLevel = @"";
    } else {
        select.hotelLevel = name;
    }
    name = self.dict[@"18"];
    
    if ([name isEqualToString:@"不限"]) {
        select.hotelPropery = @"";
    } else {
        select.hotelPropery = name;
    }
    name = self.dict[@"14"];
    
    if ([name isEqualToString:@"不限"]) {
        select.subway = @"";
    } else {
        select.subway = name;
    }
    
    name = self.dict[@"13"];
    
    if ([name isEqualToString:@"不限"]) {
        select.zone = @"";
    } else {
        select.zone = name;
    }
    
    select.hotelName = @"";
    
    if (!select.latitude) {
        select.latitude = @"";
    }
    if (!select.longitude) {
        select.longitude = @"";
    }
    
    [[Networkservice sharedInstance] indexWithPage:@"1" rows:@"10" select:select success:^(NSArray *hotelList) {
        
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:@"HomeFilterViewController"];
        
        if ([viewController isKindOfClass:[HomeFilterViewController class]]) {
            HomeFilterViewController *detailViewController = (HomeFilterViewController *)viewController;
            detailViewController.title = @"酒店筛选结果";
            detailViewController.hotelList = hotelList;
            
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    SelectModel *select = [[SelectModel alloc] init];
    
    
    NSString *name = self.dict[@"21"];
    
    if ([name isEqualToString:@"不限"]) {
        select.maxMeetingPerson = @"";
    } else {
        select.maxMeetingPerson = name;
    }
    name = self.dict[@"22"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingNumber = @"";
    } else {
        select.meetingNumber = name;
    }
    name = self.dict[@"23"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingSize = @"";
    } else {
        select.meetingSize = name;
    }
    name = self.dict[@"24"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingPrice = @"";
    } else {
        select.meetingPrice = name;
    }
    
    name = self.dict[@"31"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastPrice = @"";
    } else {
        select.feastPrice = name;
    }
    
    name = self.dict[@"32"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastTable = @"";
    } else {
        select.feastTable = name;
    }
    
    name = self.dict[@"33"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastPlace = @"";
    } else {
        select.feastPlace = name;
    }
    
    name = self.dict[@"34"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastStyle = @"";
    } else {
        select.feastStyle = name;
    }
    
    name = self.dict[@"15"];
    
    if ([name isEqualToString:@"不限"]) {
        select.station = @"";
    } else {
        select.station = name;
    }
    
    name = self.dict[@"16"];
    
    if ([name isEqualToString:@"不限"]) {
        select.airport = @"";
    } else {
        select.airport = name;
    }
    
    name = self.dict[@"12"];
    
    if ([name isEqualToString:@"不限"]) {
        select.division = @"";
    } else {
        select.division = name;
    }
    name = self.dict[@"11"];
    
    if ([name isEqualToString:@"不限"]) {
        select.city = @"";
    } else {
        select.city = name;
    }
    
    name = self.dict[@"17"];
    
    if ([name isEqualToString:@"不限"]) {
        select.businessArea = @"";
    } else {
        select.businessArea = name;
    }
    
    name = self.dict[@"19"];
    
    if ([name isEqualToString:@"不限"]) {
        select.hotelLevel = @"";
    } else {
        select.hotelLevel = name;
    }
    name = self.dict[@"18"];
    
    if ([name isEqualToString:@"不限"]) {
        select.hotelPropery = @"";
    } else {
        select.hotelPropery = name;
    }
    name = self.dict[@"14"];
    
    if ([name isEqualToString:@"不限"]) {
        select.subway = @"";
    } else {
        select.subway = name;
    }
    
    name = self.dict[@"13"];
    
    if ([name isEqualToString:@"不限"]) {
        select.zone = @"";
    } else {
        select.zone = name;
    }
    
    select.hotelName = searchBar.text;
    
    if (!select.latitude) {
        select.latitude = @"";
    }
    if (!select.longitude) {
        select.longitude = @"";
    }
    
    [[Networkservice sharedInstance] indexWithPage:@"1" rows:@"10" select:select success:^(NSArray *hotelList) {
        
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:@"HomeFilterViewController"];
        
        if ([viewController isKindOfClass:[HomeFilterViewController class]]) {
            HomeFilterViewController *detailViewController = (HomeFilterViewController *)viewController;
            detailViewController.title = @"酒店筛选结果";
            detailViewController.hotelList = hotelList;
            
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)selectSome
{
    self.tempArray = self.hotel.zone;
    self.tag = 35;
    
    self.shadeView = [[ShadeView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.shadeView.delegate = self;
    [self.shadeView showWithView:self.view];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 退出键盘
    [self.view endEditing:YES];
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBar.text = @"";
}

- (void)location
{
    SelectModel *select = [[SelectModel alloc] init];
    
    
    NSString *name = self.dict[@"21"];
    
    if ([name isEqualToString:@"不限"]) {
        select.maxMeetingPerson = @"";
    } else {
        select.maxMeetingPerson = name;
    }
    name = self.dict[@"22"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingNumber = @"";
    } else {
        select.meetingNumber = name;
    }
    name = self.dict[@"23"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingSize = @"";
    } else {
        select.meetingSize = name;
    }
    name = self.dict[@"24"];
    
    if ([name isEqualToString:@"不限"]) {
        select.meetingPrice = @"";
    } else {
        select.meetingPrice = name;
    }
    
    name = self.dict[@"31"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastPrice = @"";
    } else {
        select.feastPrice = name;
    }
    
    name = self.dict[@"32"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastTable = @"";
    } else {
        select.feastTable = name;
    }
    
    name = self.dict[@"33"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastPlace = @"";
    } else {
        select.feastPlace = name;
    }
    
    name = self.dict[@"34"];
    
    if ([name isEqualToString:@"不限"]) {
        select.feastStyle = @"";
    } else {
        select.feastStyle = name;
    }
    
    name = self.dict[@"15"];
    
    if ([name isEqualToString:@"不限"]) {
        select.station = @"";
    } else {
        select.station = name;
    }
    
    name = self.dict[@"16"];
    
    if ([name isEqualToString:@"不限"]) {
        select.airport = @"";
    } else {
        select.airport = name;
    }
    
    name = self.dict[@"12"];
    
    if ([name isEqualToString:@"不限"]) {
        select.division = @"";
    } else {
        select.division = name;
    }
    name = self.dict[@"11"];
    
    if ([name isEqualToString:@"不限"]) {
        select.city = @"";
    } else {
        select.city = name;
    }
    
    name = self.dict[@"17"];
    
    if ([name isEqualToString:@"不限"]) {
        select.businessArea = @"";
    } else {
        select.businessArea = name;
    }
    
    name = self.dict[@"19"];
    
    if ([name isEqualToString:@"不限"]) {
        select.hotelLevel = @"";
    } else {
        select.hotelLevel = name;
    }
    name = self.dict[@"18"];
    
    if ([name isEqualToString:@"不限"]) {
        select.hotelPropery = @"";
    } else {
        select.hotelPropery = name;
    }
    name = self.dict[@"14"];
    
    if ([name isEqualToString:@"不限"]) {
        select.subway = @"";
    } else {
        select.subway = name;
    }
    
    name = self.dict[@"13"];
    
    if ([name isEqualToString:@"不限"]) {
        select.zone = @"";
    } else {
        select.zone = name;
    }
    
    select.hotelName = @"";
    
    MapViewController *mapVC = [[MapViewController alloc] init];
    mapVC.selectModel = select;
    [self.navigationController pushViewController:mapVC animated:YES];
}

@end
