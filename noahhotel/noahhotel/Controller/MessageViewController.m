//
//  MessageViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "MessageViewController.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "DemandModel.h"
#import <MJRefresh.h>
#import "MyTableViewCell.h"
#import "StoryboardModel.h"
#import "DemandDetailsViewController.h"

@interface MessageViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *demandArray;

@property (nonatomic, assign) int rows;

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    if (self.title.length == 0) {
        self.title = @"我发布的需求";
    }
    
    [self refresh];
}

- (void)refresh
{
    [self upRefresh];
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    // 马上进入刷新状态
    [self.tableView.header beginRefreshing];
}

- (void)loadNewData
{
    self.rows = 10;
    
    DDLogVerbose(@"%d", self.rows);
    
    [[Networkservice sharedInstance] myMessageWithWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows]  success:^(NSArray *demandArray) {
        
        if (demandArray.count == self.rows) {
            self.tableView.footer.hidden = NO;
        }
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        
        [self.tableView reloadData];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
    }];
}

- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.automaticallyHidden = NO;
    self.tableView.footer.hidden = YES;
}

- (void)loadMoreData
{
    self.rows += 10;
    
    DDLogVerbose(@"%d", self.rows);
    
    [[Networkservice sharedInstance] myMessageWithWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows]  success:^(NSArray *demandArray) {
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        
        [self.tableView reloadData];
        
        if (demandArray.count < self.rows) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的需求了"];
        }
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.footer endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.demandArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTableViewCell *cell = [MyTableViewCell cellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.name = [self.demandArray[indexPath.row] content];
    cell.date = [self.demandArray[indexPath.row] time];
    
    return cell;
}
#pragma mark - UITabBarDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_DEMANDDETAILSVIEWCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[DemandDetailsViewController class]]) {
        DemandDetailsViewController *demandDetailsViewController = (DemandDetailsViewController *)viewController;
        demandDetailsViewController.title = [self.demandArray[indexPath.row] content];
        demandDetailsViewController.myId = [self.demandArray[indexPath.row] demandId];
        [self.navigationController pushViewController:demandDetailsViewController animated:YES];
    }
}

@end
