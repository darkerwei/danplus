//
//  PwdUpdateViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/18.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "PwdUpdateViewController.h"
#import "FormatModel.h"
#import <SVProgressHUD.h>
#import "Networkservice.h"
#import "UserModel.h"

@interface PwdUpdateViewController ()

/**
 *  旧密码
 */
@property (nonatomic, weak) IBOutlet UITextField *oldPassword;
/**
 *  确认密码
 */
@property (nonatomic, weak) IBOutlet UITextField *passwordAgain;
/**
 *  新密码
 */
@property (nonatomic, weak) IBOutlet UITextField *myNewPassword;

@end

@implementation PwdUpdateViewController

- (IBAction)save
{
    if (![FormatModel isValidFormatWithPassword:self.oldPassword.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.oldPassword.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    if (![FormatModel isValidFormatWithPassword:self.myNewPassword.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.myNewPassword.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    if (![self.myNewPassword.text isEqualToString:self.passwordAgain.text]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@不正确", self.passwordAgain.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    UserModel *user = [UserModel sharedInstance];
    user.oldPassword = self.oldPassword.text;
    user.myNewPassword = self.myNewPassword.text;
    
    [SVProgressHUD showWithStatus:@"正在修改密码"];
    [[Networkservice sharedInstance] pwdUpdateWithUser:user success:^(UserModel *user) {
        [SVProgressHUD showSuccessWithStatus:@"修改密码成功"];
        
        DDLogVerbose(@"%@", user);
        
        [self back];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

@end
