//
//  CooperationViewController.h
//  noahhotel
//
//  Created by darkerwei on 15/9/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"
#import "HotelModel.h"

@interface CooperationViewController : BaseViewController

@property (nonatomic, strong) HotelModel *hotel;

@end
