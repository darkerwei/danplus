//
//  NavigationController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewController.navigationItem.leftBarButtonItem = [self itemWithTarget:self action:@selector(back) image:[UIImage imageNamed:@"返回"]];
    viewController.navigationItem.rightBarButtonItem = [self itemWithTarget:self action:nil image:nil];
    
    [super pushViewController:viewController animated:animated];
}

- (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(UIImage *)image
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    
    // 左对齐
    [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:image forState:UIControlStateNormal];
    return [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (void)back
{
    [self popViewControllerAnimated:YES];
}
- (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 32, 32);
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button setImage:image forState:UIControlStateNormal];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

/**
 *  创建一个带图片的 UIBarButtonItem
 *
 *  @param target 事件 target
 *  @param action 事件 action
 *  @param image  UIBarButtonItem 图片
 *
 *  @return 一个带图片的 UIBarButtonItem
 */
- (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(UIImage *)image;
@end
