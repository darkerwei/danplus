//
//  PhotoViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/9/28.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "PhotoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PicturesModel.h"

@interface PhotoViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *myNewFeaturesScrollView;

@end

@implementation PhotoViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    self.title = [NSString stringWithFormat:@"%d/%d", 1, self.photo.count];
    
    self.myNewFeaturesScrollView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * self.photo.count, 0);
    
    for (int i = 0; i < self.photo.count; ++i) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width * i, 80, [UIScreen mainScreen].bounds.size.width, 200)];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[self.photo[i] picture]]];
        [self.myNewFeaturesScrollView addSubview:imageView];
        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width * i, [UIScreen mainScreen].bounds.size.height - 64 - 90, [UIScreen mainScreen].bounds.size.width, 90)];
//        view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
//        [self.myNewFeaturesScrollView addSubview:view];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width * i + 20, [UIScreen mainScreen].bounds.size.height - 200, [UIScreen mainScreen].bounds.size.width - 40,  180)];
        label.text = [NSString stringWithFormat:@"       %@", [self.photo[i] myDesc]];
        label.font = [UIFont systemFontOfSize:13];
        label.textColor = [UIColor whiteColor];
        label.numberOfLines = 0;
        [self.myNewFeaturesScrollView addSubview:label];
        
        
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = (int)(scrollView.contentOffset.x / [UIScreen mainScreen].bounds.size.width);

    self.title = [NSString stringWithFormat:@"%d/%d", page + 1, self.photo.count];
}

- (void)setPhoto:(NSArray *)photo
{
    _photo = photo;
}

@end
