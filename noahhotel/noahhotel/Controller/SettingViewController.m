//
//  SettingViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/17.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SettingViewController.h"
#import "StoryboardModel.h"
#import "NavigationController.h"
#import "UserModel.h"
#import "TabBarController.h"
#import "StoryboardModel.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>

#define SHOWLOGIN_DELAY 1.0

@interface SettingViewController () <UIActionSheetDelegate>

@property (nonatomic, weak) IBOutlet UIButton *button;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.button.hidden = ![UserModel isLogin];
}

-(IBAction)exit
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:@"退出登录"
                                  otherButtonTitles:nil];
    //    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:@"username"];
        [defaults removeObjectForKey:@"password"];
        [defaults removeObjectForKey:@"approve"];
        [defaults synchronize];
        
        [UserModel deleteUser];
        
        [[Networkservice sharedInstance] logoutWithsuccess:^{
            [SVProgressHUD showSuccessWithStatus:@"退出成功"];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        }];
        
        [self skip];
    }
}

- (void)skip {
    
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_TABBARCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[TabBarController class]]) {
        UIWindow *window = [UIApplication sharedApplication].delegate.window;
        window.rootViewController = (TabBarController *)viewController;
    }
}

/**
 *  显示登录界面
 */
- (void)showLogin
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[NavigationController class]]) {
        [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
    }
}


@end
