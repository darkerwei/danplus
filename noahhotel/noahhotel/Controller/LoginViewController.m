//
//  LoginViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "LoginViewController.h"
#import "FormatModel.h"
#import "Networkservice.h"
#import "UserModel.h"
#import <SVProgressHUD.h>

@interface LoginViewController ()

/**
 *  用户名
 */
@property (nonatomic, weak) IBOutlet UITextField *username;
/**
 *  密码
 */
@property (nonatomic, weak) IBOutlet UITextField *password;

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([UserModel getUser].username) {
        self.username.text = [UserModel getUser].username;
    }
}

- (IBAction)login
{
    if ([FormatModel isValidFormatWithName:self.username.text]) {
        if ([FormatModel isValidFormatWithPassword:self.password.text]) {
            
            UserModel *user = [UserModel sharedInstance];
            user.username = self.username.text;
            user.password = self.password.text;
            
            [SVProgressHUD showWithStatus:@"正在登录"];
            [[Networkservice sharedInstance] loginWithUser:user success:^(UserModel *user) {
                [SVProgressHUD showSuccessWithStatus:@"登录成功"];
                
                DDLogVerbose(@"%@", user);
                
                [[NSUserDefaults standardUserDefaults] setObject:user.approve forKey:@"approve"];
                
                [self skip];
            } failure:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
            }];
            
        } else {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.password.placeholder]];
        }
    } else {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.username.placeholder]];
    }
    
    [self.view endEditing:YES];
}

@end
