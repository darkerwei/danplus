//
//  ReplyViewController.h
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"

@interface ReplyViewController : BaseViewController

@property (nonatomic, strong) NSNumber *myId;

@property (nonatomic, strong) NSNumber *hotelId;

@end
