//
//  SearchViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SearchViewController.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "DemandModel.h"
#import "MyTableViewCell.h"
#import "StoryboardModel.h"
#import "DemandDetailsViewController.h"
#import "SupplyDetailViewController.h"
#import "SupplyValueModel.h"
#import "CycleViewController.h"
#import "Networkservice.h"
#import "HotelModel.h"
#import <SVProgressHUD.h>
#import "DetailViewController.h"
#import "StoryboardModel.h"
#import "MySearchBar.h"
#import "DemandModel.h"
#import "DemandTableViewCell.h"
#import "ShadeView.h"
#import "MyDemandViewController.h"
#import "DemandDetailsViewController.h"
#import "SupplyValueModel.h"
#import "SupplyDetailViewController.h"

@interface SearchViewController() <UISearchBarDelegate, UITableViewDelegate>

/**
 *  搜索框
 */
@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) NSArray *demandArray;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置搜索框
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"搜索";
    [self.view addSubview:self.searchBar];
    
    [self.searchBar becomeFirstResponder];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if ([self.title isEqualToString:@"供应商查询"]) {
        [[Networkservice sharedInstance] searchWithId:@0 name:searchBar.text rows:@"10" page:@"1" success:^(NSArray *supplyValueArray) {
            
            self.demandArray = [NSArray arrayWithArray:supplyValueArray];
            [self.tableView reloadData];
            
        } failure:^(NSError *error) {
            
        }];
        
        // 退出键盘
        [self.view endEditing:YES];
        return;
        
    }
    
    [[Networkservice sharedInstance] searchWithPage:@"1" rows:@"10" startTime:@"" endTime:@"" title:searchBar.text success:^(NSArray *demandArray) {
        
        for (DemandModel *demand in demandArray) {
            DDLogVerbose(@"%@", demand);
            
        }
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
  
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 退出键盘
    [self.view endEditing:YES];
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text.length == 0) {
        return;
    }
    
    if ([self.title isEqualToString:@"供应商查询"]) {
        [[Networkservice sharedInstance] searchWithId:@0 name:searchBar.text rows:@"10" page:@"1" success:^(NSArray *supplyValueArray) {
            
            self.demandArray = [NSArray arrayWithArray:supplyValueArray];
            [self.tableView reloadData];
            
        } failure:^(NSError *error) {
            
        }];
        return;
    
    }
    
    
    
    [[Networkservice sharedInstance] searchWithPage:@"1" rows:@"10" startTime:@"" endTime:@"" title:searchBar.text success:^(NSArray *demandArray) {
        
        for (DemandModel *demand in demandArray) {
            DDLogVerbose(@"%@", demand);
            
        }
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.demandArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.title isEqualToString:@"供应商查询"]) {
        static NSString *TableSampleIdentifier = @"cell";
        //    用TableSampleIdentifier表示需要重用的单元
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
        //    如果如果没有多余单元，则需要创建新的单元
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.text = [self.demandArray[indexPath.row] title];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor colorWithRed:95 / 255.0 green:95 / 255.0 blue:95 / 255.0 alpha:1.0];
        cell.detailTextLabel.text = [self.demandArray[indexPath.row] name];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:142 / 255.0 green:142 / 255.0 blue:142 / 255.0 alpha:1.0];
        
        return cell;
    }
    
    MyTableViewCell *cell = [MyTableViewCell cellWithTableView:tableView];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.name = [self.demandArray[indexPath.row] name];
    
    cell.date = [self.demandArray[indexPath.row] time];
    
    cell.text = [self.demandArray[indexPath.row] content];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.title isEqualToString:@"需求查询"]) {
        return 80;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.title isEqualToString:@"供应商查询"]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SupplyDetailViewController_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[SupplyDetailViewController class]]) {
            SupplyDetailViewController *supplyDetailViewController = (SupplyDetailViewController *)viewController;
            supplyDetailViewController.myId = [self.demandArray[indexPath.row] myID];
            supplyDetailViewController.title = [self.demandArray[indexPath.row] name];
            [self.navigationController pushViewController:supplyDetailViewController animated:YES];
        }
        
        return;
    }
    
    
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_DEMANDDETAILSVIEWCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[DemandDetailsViewController class]]) {
        DemandDetailsViewController *demandDetailsViewController = (DemandDetailsViewController *)viewController;
        demandDetailsViewController.title = [self.demandArray[indexPath.row] name];
        demandDetailsViewController.myId = [self.demandArray[indexPath.row] myId];
        
        [self.navigationController pushViewController:demandDetailsViewController animated:YES];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBar.text = @"";
}

@end
