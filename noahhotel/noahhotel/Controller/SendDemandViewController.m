//
//  SendDemandViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SendDemandViewController.h"
#import <SVProgressHUD.h>
#import "Networkservice.h"
#import "BaseModel.h"
#import "DemandModel.h"
#import "NeedViewController.h"

@interface SendDemandViewController ()

/**
 *  留言内容
 */
@property (weak, nonatomic) IBOutlet UILabel *feedback;
/**
 *  需求描述
 */
@property (nonatomic, weak) IBOutlet UITextView *content;
/**
 *  联系方式
 */
@property (nonatomic, weak) IBOutlet UITextField *contact;
/**
 *  需求名称
 */
@property (nonatomic, weak) IBOutlet UITextField *contactName;

@end

@implementation SendDemandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)send
{
    if (self.contactName.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"需求名称不能为空"];
        return;
    }
    if (self.content.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"需求描述不能为空"];
        return;
    }
    
    if (self.contact.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"联系方式不能为空"];
        return;
    }
    
    
    DemandModel *demand = [[DemandModel alloc] init];
    demand.content = self.content.text;
    demand.phone = self.contact.text;
    demand.name = self.contactName.text;
    
    [SVProgressHUD showWithStatus:@"正在发布"];
    
    [[Networkservice sharedInstance] sendWithDemand:demand success:^(DemandModel *demand) {
        [SVProgressHUD showSuccessWithStatus:@"发布成功"];
        
        
        NeedViewController *needVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 2];
        needVC.showNew = 1;
        
        DDLogVerbose(@"%@", demand);
        
        [self back];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    self.feedback.hidden = (textView.text.length != 0);
}

@end
