//
//  SupplyDetailViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SupplyDetailViewController.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "SupplyValueModel.h"
#import "DemandDescTableViewCell.h"
#import "DetailTableViewCell.h"
#import "ProductModel.h"
#import "ProductTableViewCell.h"

@interface SupplyDetailViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) SupplyValueModel *supplyValue;

@property (nonatomic, assign) int one;

@end

@implementation SupplyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.separatorStyle = NO;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [SVProgressHUD showWithStatus:@"正在获取舞台供应商信息"];
    [[Networkservice sharedInstance] supplyDetailWithId:self.myId success:^(SupplyValueModel *supplyValue) {
        [SVProgressHUD showSuccessWithStatus:@"获取舞台供应商信息成功"];
        
        
        DDLogVerbose(@"%@", supplyValue);
        
        self.supplyValue = supplyValue;
        
        self.title = supplyValue.title;
        
        self.one = 3;
        
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.one + (self.supplyValue.product.count > 0 ? self.supplyValue.product.count + 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        DemandDescTableViewCell *cell = [DemandDescTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleString = @"    公司简介";
        
        cell.desc = self.supplyValue.companyDescription;
        
        return cell;
    } else if (indexPath.row == 1) {
        DemandDescTableViewCell *cell = [DemandDescTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleString = @"    产品简介";
        
        cell.desc = self.supplyValue.productDescription;
        
        return cell;
    } else if (indexPath.row == 2) {
        DetailTableViewCell *cell = [DetailTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.phoneNumber = self.supplyValue.contact;
        
        return cell;
    } else if (indexPath.row == 3) {
        DemandDescTableViewCell *cell = [DemandDescTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleString = @"    产品";
        
        return cell;
    }
    else {
        ProductTableViewCell *cell = [ProductTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.product = self.supplyValue.product[indexPath.row - 4];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return [self.supplyValue.companyDescription boundingRectWithSize:CGSizeMake(284, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 30 + 16;
    } else if (indexPath.row == 1) {
        return [self.supplyValue.productDescription boundingRectWithSize:CGSizeMake(284, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 30 + 16;
    } else if (indexPath.row == 2) {
        return 50;
    } else if (indexPath.row == 3) {
        return 30;
    } else {
        return 140;
    }
}

@end
