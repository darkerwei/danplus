//
//  HomeFilterViewController.m
//  noahhotel
//
//  Created by ioswei on 15/9/30.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "HomeFilterViewController.h"
#import "HomeHotelTableViewCell.h"
#import "StoryboardModel.h"
#import "AboutViewController.h"
#import "MapViewController.h"

@interface HomeFilterViewController () <HomeHotelTableViewCellDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableview;

@end

@implementation HomeFilterViewController

#pragma mark - UITableViewDataSource

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableview.tableFooterView = [[UIView alloc] init];
}

- (void)setHotelList:(NSArray *)hotelList
{
    _hotelList = hotelList;
    [self.tableview reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.hotelList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeHotelTableViewCell *cell = [HomeHotelTableViewCell cellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    cell.model = self.hotelList[indexPath.row];
    cell.addressBtn.tag = indexPath.row;
    cell.bigBtn.tag = indexPath.row;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

#pragma mark - UITabBarDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotelModel *hotel = self.hotelList[indexPath.row];
    
    
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_AboutViewController_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[AboutViewController class]]) {
        AboutViewController *detailViewController = (AboutViewController *)viewController;
        detailViewController.hotel = hotel;
        
        
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

- (void)addressBtnClick:(UIButton *)button
{
    HotelModel *hotel = self.hotelList[button.tag];
    
    MapViewController *mapVC = [[MapViewController alloc] init];
    mapVC.hotelModel = hotel;
    
    NSLog(@"%f %f", hotel.latitude.doubleValue, hotel.longitude.doubleValue);
    mapVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mapVC animated:YES];
}

@end
