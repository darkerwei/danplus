//
//  BaseViewController.h
//  noahhotel
//
//  Created by darkerwei on 15/8/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

/**
 *  返回上一级目录
 */
- (void)back;
/**
 *  直接进入首页
 */
- (void)skip;

- (void)popToRootViewController;

@end
