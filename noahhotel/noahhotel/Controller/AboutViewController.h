//
//  AboutViewController.h
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"
#import "HotelModel.h"

@interface AboutViewController : BaseViewController

@property (nonatomic, strong) HotelModel *hotel;

@end
