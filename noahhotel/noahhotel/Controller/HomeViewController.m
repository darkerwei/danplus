//
//  HomeViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/14.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "HomeViewController.h"
//#import "CycleViewController.h"
#import "Networkservice.h"
#import "HotelModel.h"
#import <SVProgressHUD.h>
//#import "HotelTableViewCell.h"
#import "DetailViewController.h"
#import "StoryboardModel.h"
#import "MySearchBar.h"
#import "AboutViewController.h"
#import <MJRefresh.h>
#import "HotelImageModel.h"
#import "HomeHotelTableViewCell.h"
#import "MapViewController.h"
#import "QFQBannerView.h"
#import "SupplyDetailViewController.h"

@interface HomeViewController () <MySearchBarDelegate, UITextFieldDelegate, HomeHotelTableViewCellDelegate, QFQBannerViewDelegate>

@property (nonatomic, weak) QFQBannerView *banner;

//@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) MySearchBar *searchBar;

@property (nonatomic, assign) int rows;

@property (nonatomic, strong) NSArray *BannerArray;

@end

@implementation HomeViewController

- (void)bannerView:(QFQBannerView *)bannerView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    QFQHomeCollecionModel *model = bannerView.data[indexPath.row];
    
    NSLog(@"%@",  model.linkUrl);
    NSLog(@"%@",  model.linkType);
    
    if ([model.linkType isEqualToString:@"2"]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SupplyDetailViewController_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[SupplyDetailViewController class]]) {
            SupplyDetailViewController *supplyDetailViewController = (SupplyDetailViewController *)viewController;
            supplyDetailViewController.myId = [NSNumber numberWithInt:model.linkUrl.intValue];
            [self.navigationController pushViewController:supplyDetailViewController animated:YES];
        }
    } else if ([model.linkType isEqualToString:@"1"]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_AboutViewController_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[AboutViewController class]]) {
            AboutViewController *detailViewController = (AboutViewController *)viewController;
//            detailViewController.hotel = hotel;
            
            
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    QFQBannerView *banner = [[QFQBannerView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 160)];
    [self.view addSubview:banner];
    self.banner = banner;
    self.banner.delegate = self;
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        
//        NSArray *array = @[
//                           @{@"imgUrl": @"http://img04.tooopen.com/images/20130701/tooopen_20083555.jpg"},
//                           @{@"imgUrl": @"http://img04.tooopen.com/images/20130630/tooopen_10444704.jpg"},
//                           
//                           ];
//        NSMutableArray *mArray = [NSMutableArray array];
//        for (NSDictionary *dict in array) {
//            QFQHomeCollecionModel *model = [QFQHomeCollecionModel new];
//            model.imgUrl = dict[@"imgUrl"];
//            [mArray addObject:model];
//        }
//        
//        banner.data = mArray;
//    });
    
    [[Networkservice sharedInstance] getHotelImagesuccess:^(NSArray *hotelImageArray) {
        
        
//        NSMutableArray *imageURL = [[NSMutableArray alloc] init];
//        
//            for (int i = 0; i < hotelImageArray.count; ++i) {
//                HotelImageModel *image = hotelImageArray[i];
//                [imageURL addObject:image.phonePicture];
//            }
        
        NSMutableArray *mArray = [NSMutableArray array];
        for (HotelImageModel *image in hotelImageArray) {
            QFQHomeCollecionModel *model = [QFQHomeCollecionModel new];
            model.imgUrl = image.phonePicture;
            model.linkUrl = image.linkId.stringValue;
            model.linkType = image.type.stringValue;
            [mArray addObject:model];
        }
        
        banner.data = mArray;
        
    } failure:^(NSError *error) {
        
    }];
    
    // 搜索框
    self.searchBar = [[MySearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 30)];
    self.navigationItem.titleView = self.searchBar;
    self.searchBar.delegate2 = self;
    self.searchBar.delegate = self;
//    self.searchBar.buttonName = @"我的需求";
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self refresh];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_DETAILVIEWCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[DetailViewController class]]) {
        DetailViewController *detailViewController = (DetailViewController *)viewController;
        
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    return NO;
}

- (void)refresh
{
    [self upRefresh];
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    // 马上进入刷新状态
    [self.tableView.header beginRefreshing];
}

- (void)loadNewData
{
    self.rows = 10;
    
     DDLogVerbose(@"%d", self.rows);
    
    [[Networkservice sharedInstance] indexWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows] success:^(NSArray *hotelList) {
        for (HotelModel *hotel in hotelList) {
            DDLogVerbose(@"%@", hotel);
            
        }
        
        if (hotelList.count == self.rows) {
            self.tableView.footer.hidden = NO;
        }
        
        self.hotelList = [NSArray arrayWithArray:hotelList];
        [self.tableView reloadData];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
    }];
}

- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.automaticallyHidden = NO;
    self.tableView.footer.hidden = YES;
}

- (void)loadMoreData
{
    self.rows += 10;
    
    DDLogVerbose(@"%d", self.rows);
    
    [[Networkservice sharedInstance] indexWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows] success:^(NSArray *hotelList) {
        
        for (HotelModel *hotel in hotelList) {
            DDLogVerbose(@"%@", hotel);
            
        }
        
        self.hotelList = [NSArray arrayWithArray:hotelList];
        [self.tableView reloadData];
        
        if (hotelList.count < self.rows) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的酒店了"];
        }
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.footer endRefreshing];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//
//    // 设置容器视图高度
//    CGFloat w = [UIScreen mainScreen].bounds.size.width;
//    CGFloat h = 110;
//    self.containerView.bounds = CGRectMake(0, 0, w, h);
//    
    self.tableView.tableHeaderView = self.banner;
//
//    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.hotelList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeHotelTableViewCell *cell = [HomeHotelTableViewCell cellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    cell.model = self.hotelList[indexPath.row];
    cell.addressBtn.tag = indexPath.row;
    cell.bigBtn.tag = indexPath.row;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

#pragma mark - UITabBarDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotelModel *hotel = self.hotelList[indexPath.row];
    
    
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_AboutViewController_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[AboutViewController class]]) {
        AboutViewController *detailViewController = (AboutViewController *)viewController;
        detailViewController.hotel = hotel;
        
        
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

#pragma mark - MySearchBarDelegate

- (void)choose
{
    [self.searchBar endEditing:YES];
    
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_DETAILVIEWCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[DetailViewController class]]) {
        DetailViewController *detailViewController = (DetailViewController *)viewController;
        
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

- (void)addressBtnClick:(UIButton *)button
{
    HotelModel *hotel = self.hotelList[button.tag];
    
    MapViewController *mapVC = [[MapViewController alloc] init];
    mapVC.hotelModel = hotel;
    
     NSLog(@"%f %f", hotel.latitude.doubleValue, hotel.longitude.doubleValue);
    mapVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mapVC animated:YES];
}

@end
