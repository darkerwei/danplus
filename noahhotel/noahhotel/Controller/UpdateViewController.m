//
//  UpdateViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/19.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "UpdateViewController.h"
#import "FormatModel.h"
#import <SVProgressHUD.h>
#import "Networkservice.h"
#import "UserModel.h"

@interface UpdateViewController ()

/**
 *  用户昵称
 */
@property (nonatomic, weak) IBOutlet UITextField *username;

@end

@implementation UpdateViewController

- (IBAction)save
{
    if (!(self.username.text.length > 0 && self.username.text.length < 16)) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式不正确", self.username.placeholder]];
        
        [self.view endEditing:YES];
        return;
    }
    
    UserModel *user = [UserModel sharedInstance];
    user.username = self.username.text;
    
    [SVProgressHUD showWithStatus:@"正在保存"];
    [[Networkservice sharedInstance] updateWithUser:user success:^(UserModel *user) {
        [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        
        DDLogVerbose(@"%@", user);
        
        [self back];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

@end
