//
//  MyDemandViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "MyDemandViewController.h"
#import "CycleViewController.h"
#import "Networkservice.h"
#import "HotelModel.h"
#import <SVProgressHUD.h>
#import "DetailViewController.h"
#import "StoryboardModel.h"
#import "MySearchBar.h"
#import "DemandModel.h"
#import "DemandTableViewCell.h"
#import "ShadeView.h"
#import "MyDemandViewController.h"
#import "DemandDetailsViewController.h"
#import "MyTableViewCell.h"
#import <MJRefresh.h>

@interface MyDemandViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *demandArray;

@property (nonatomic, assign) int rows;

@end

@implementation MyDemandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//        [[Networkservice sharedInstance] myListWithsuccess:^(NSArray *demandArray) {
//            for (DemandModel *demand in demandArray) {
//                DDLogVerbose(@"%@", demand);
//    
//            }
//    
//            self.demandArray = [NSArray arrayWithArray:demandArray];
//            [self.tableView reloadData];
//        } failure:^(NSError *error) {
//            [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
//        }];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.title = @"我发布的需求";
    
    [self refresh];
}

- (void)refresh
{
    [self upRefresh];
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    // 马上进入刷新状态
    [self.tableView.header beginRefreshing];
}

- (void)loadNewData
{
    self.rows = 10;
    
    DDLogVerbose(@"%d", self.rows);
    
    [[Networkservice sharedInstance] myListWithWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows]  success:^(NSArray *demandArray) {
        
        if (demandArray.count == self.rows) {
            self.tableView.footer.hidden = NO;
        }
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        
        [self.tableView reloadData];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
    }];
}

- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.automaticallyHidden = NO;
    self.tableView.footer.hidden = YES;
}

- (void)loadMoreData
{
    self.rows += 10;
    
    DDLogVerbose(@"%d", self.rows);
    
    [[Networkservice sharedInstance] myListWithWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows]  success:^(NSArray *demandArray) {
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        
        [self.tableView reloadData];
        
        if (demandArray.count < self.rows) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的需求了"];
        }
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.footer endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.demandArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTableViewCell *cell = [MyTableViewCell cellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.name = [self.demandArray[indexPath.row] name];
    cell.date = [self.demandArray[indexPath.row] time];
    
    return cell;
}
#pragma mark - UITabBarDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_DEMANDDETAILSVIEWCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[DemandDetailsViewController class]]) {
        DemandDetailsViewController *demandDetailsViewController = (DemandDetailsViewController *)viewController;
        demandDetailsViewController.title = [self.demandArray[indexPath.row] name];
        demandDetailsViewController.myId = [self.demandArray[indexPath.row] myId];
        [self.navigationController pushViewController:demandDetailsViewController animated:YES];
    }
}

@end
