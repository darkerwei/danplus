//
//  HomeFilterViewController.h
//  noahhotel
//
//  Created by ioswei on 15/9/30.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeFilterViewController : BaseViewController

@property (nonatomic, strong) NSArray *hotelList;

@end
