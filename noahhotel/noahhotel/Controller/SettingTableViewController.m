//
//  SettingTableViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/13.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SettingTableViewController.h"
#import "StoryboardModel.h"
#import "NewFeaturesViewController.h"
#import "NavigationController.h"
#import "FeedbackViewController.h"
#import "UserModel.h"

@interface SettingTableViewController () 

@end

@implementation SettingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:NEWFEATURES_STORYBOARD_NAME storyboardID:NEWFEATURESVIEWCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[NewFeaturesViewController class]]) {
            
            NewFeaturesViewController *newFeaturesViewController = (NewFeaturesViewController *)viewController;
            newFeaturesViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            newFeaturesViewController.firstShow = 1;
            
            [self presentViewController:(NewFeaturesViewController *)newFeaturesViewController animated:YES completion:^{
                // 隐藏状态栏
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
            }];
        }
    } else if (indexPath.row == 1) {
        if ([UserModel isLogin]) {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_FeedbackViewController_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[FeedbackViewController class]]) {
                FeedbackViewController *feedbackViewController = (FeedbackViewController *)viewController;
                
                [self.navigationController pushViewController:feedbackViewController animated:YES];
            }
        } else {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[NavigationController class]]) {
                [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
            }
        }
    }
}

@end
