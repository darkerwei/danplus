//
//  TabBarController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "TabBarController.h"
#import "StoryboardModel.h"
#import "NavigationController.h"
#import "UserModel.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>

@interface TabBarController ()

@end

@implementation TabBarController

#define SHOWLOGIN_DELAY 1.0

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 隐藏状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    
    self.tabBar.selectionIndicatorImage = [self image:[UIImage imageNamed:@"tabbar选中图片"] byScalingToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width * 0.25, 49)];
    
    if (![UserModel isLogin]) {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"skip"]) {
            
        } else {
            [self performSelector:@selector(showLogin) withObject:nil afterDelay:SHOWLOGIN_DELAY];
        }
        
    } else {
        
        UserModel *user = [UserModel getUser];
        
        [[Networkservice sharedInstance] loginWithUser:user success:^(UserModel *user) {
            
            DDLogVerbose(@"%@", user);
            [[NSUserDefaults standardUserDefaults] setObject:user.approve forKey:@"approve"];

        } failure:^(NSError *error) {
            [self performSelector:@selector(showLogin) withObject:nil afterDelay:SHOWLOGIN_DELAY];
        }];
    }
}

- (UIImage *)image:(UIImage*)image byScalingToSize:(CGSize)targetSize {
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = CGPointZero;
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage ;
}

/**
 *  显示登录界面
 */
- (void)showLogin
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[NavigationController class]]) {
        [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
    }
}

@end
