//
//  MapViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/9/14.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "MapViewController.h"
#import <MAMapKit.h>
#import "CustomAnnotationView.h"
#import "LocationModel.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "StoryboardModel.h"
#import "AboutViewController.h"
#import "HomeFilterViewController.h"

@interface MapViewController ()<MAMapViewDelegate, CustomCalloutViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITapGestureRecognizer *singleTap;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTap;

@property (nonatomic, strong) MAPointAnnotation *pointAnnotation;

/**
 *  地图
 */
@property (nonatomic, strong) MAMapView *mapView;

@property (nonatomic, assign) int show;

@end
@implementation MapViewController

+ (void)initialize
{
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    
    // 设置item颜色
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [item setTitleTextAttributes:dict forState:UIControlStateNormal];
    
    NSDictionary *disableTextAttrs = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:0.5] forKey:NSForegroundColorAttributeName];
    [item setTitleTextAttributes:disableTextAttrs forState:UIControlStateDisabled];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"酒店位置";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 注册高德地图
    [MAMapServices sharedServices].apiKey = @"712838e3ce5b25a901ab7e67bf34c8a4";
    
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    //        self.mapView.showsUserLocation = YES;
    //        self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    
    
    
    [self.view addSubview:self.mapView];
    
    //        // 添加大头针
    //        for (LocationModel *location in self.locationArray) {
    //            MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
    //            pointAnnotation.coordinate = CLLocationCoordinate2DMake(location.longitude.doubleValue, location.latitude.doubleValue);
    //            pointAnnotation.title = location.hotelName;
    //            pointAnnotation.subtitle = location.address;
    //
    //            [self.mapView addAnnotation:pointAnnotation];
    //        }
    
    if (self.hotelModel) {
        [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(self.hotelModel.latitude.doubleValue, self.hotelModel.longitude.doubleValue) animated:YES];
        
        //        [self.mapView setZoomLevel:10 animated:YES];
        
        [self performSelector: @selector(setZoom) withObject:nil afterDelay:1.0];
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        pointAnnotation.coordinate = CLLocationCoordinate2DMake(self.hotelModel.latitude.doubleValue, self.hotelModel.longitude.doubleValue);
        pointAnnotation.title = self.hotelModel.hotelName;
        pointAnnotation.subtitle = self.hotelModel.address;
        
        [self.mapView addAnnotation:pointAnnotation];
    } else {
        [self setupGestures];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"筛选" style:UIBarButtonItemStyleDone target:self action:@selector(commit)];
    }
    
}

- (void)commit {

    if (!self.pointAnnotation) {
        [SVProgressHUD showErrorWithStatus:@"请在地图上单击选点"];
        return;
    }
    
    self.selectModel.longitude = [NSString stringWithFormat:@"%f", self.pointAnnotation.coordinate.longitude];
    self.selectModel.latitude = [NSString stringWithFormat:@"%f", self.pointAnnotation.coordinate.latitude];
    
    [[Networkservice sharedInstance] indexWithPage:@"1" rows:@"10" select:self.selectModel success:^(NSArray *hotelList) {
        
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:@"HomeFilterViewController"];
        
        if ([viewController isKindOfClass:[HomeFilterViewController class]]) {
            HomeFilterViewController *detailViewController2 = (HomeFilterViewController *)viewController;
            detailViewController2.title = @"酒店筛选结果";
            detailViewController2.hotelList = hotelList;
            
            [self.navigationController pushViewController:detailViewController2 animated:YES];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)setZoom
{
    [self.mapView setZoomLevel:16 animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    
}

#pragma mark - MAMapViewDelegate

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"地理"];
        annotationView.imageURL = self.hotelModel.picture;
        
        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        annotationView.delegate2 = self;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        
        if (self.hotelModel) {
            [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.4];
        }
        
        return annotationView;
    }
    return nil;
}

- (void)setupGestures
{
    // 需要额外添加一个双击手势，以避免当执行mapView的双击动作时响应两次单击手势。
    self.doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    self.doubleTap.delegate = self;
    self.doubleTap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:self.doubleTap];
    
    self.singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    self.singleTap.delegate = self;
    [self.singleTap requireGestureRecognizerToFail:self.doubleTap];
    [self.view addGestureRecognizer:self.singleTap];
}

// todo 如果需要点击地图上的气泡进入详情可以打开注释
- (void)viewClick
{
//    if (self.show == 0) {
//        self.show = 1;
//        
//        
//        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_AboutViewController_STORYBOARD_ID];
//        
//        if ([viewController isKindOfClass:[AboutViewController class]]) {
//            AboutViewController *detailViewController = (AboutViewController *)viewController;
//            detailViewController.hotel = self.hotelModel;
//            
//            
//            [self.navigationController pushViewController:detailViewController animated:YES];
//        }
//    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.show =0;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (gestureRecognizer == self.singleTap && ([touch.view isKindOfClass:[UIControl class]] || [touch.view isKindOfClass:[MAAnnotationView class]]))
    {
        return NO;
    }
    
    if (gestureRecognizer == self.doubleTap && [touch.view isKindOfClass:[UIControl class]])
    {
        return NO;
    }
    
    return YES;
}

#pragma mark - Handle Gestures

- (void)handleSingleTap:(UITapGestureRecognizer *)theSingleTap
{
    [self.mapView removeAnnotation:self.pointAnnotation];
    
//    [self showLabelWithText:@"Tap" atPoint:[theSingleTap locationInView:self.view]];
    
    CLLocationCoordinate2D location = [self.mapView convertPoint:[theSingleTap locationInView:self.view] toCoordinateFromView:self.view];
    
    NSLog(@"%f %f", location.latitude, location.longitude);
    
    self.pointAnnotation = [[MAPointAnnotation alloc] init];
    self.pointAnnotation.coordinate = location;
    
    [self.mapView addAnnotation:self.pointAnnotation];
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)theDoubleTap
{

}

@end
