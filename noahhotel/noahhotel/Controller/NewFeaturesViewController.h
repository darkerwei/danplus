//
//  NewFeaturesViewController.h
//  noahhotel
//
//  Created by darkerwei on 15/8/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"

@interface NewFeaturesViewController : BaseViewController

@property (nonatomic, getter=isFirstShow) BOOL firstShow;

@end
