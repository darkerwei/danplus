//
//  DemandDetailsViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DemandDetailsViewController.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "DemandModel.h"
#import "DemandDescTableViewCell.h"
#import "ReplyTableViewCell.h"
#import "ReplyViewController.h"
#import "StoryboardModel.h"
#import "UserModel.h"
#import "NavigationController.h"


@interface DemandDetailsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) DemandModel *demand;

@property (nonatomic, assign) int one;

@end

@implementation DemandDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.separatorStyle = NO;
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Networkservice sharedInstance] contentWithId:self.myId success:^(DemandModel *demand) {
        
        DDLogVerbose(@"%@", demand);
        
        self.demand = demand;
        
        self.one = 3;
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.one + self.demand.reply.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        static NSString *TableSampleIdentifier = @"cell";
        //    用TableSampleIdentifier表示需要重用的单元
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
        //    如果如果没有多余单元，则需要创建新的单元
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.text = @"发布时间";
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textColor = [UIColor colorWithRed:95 / 255.0 green:95 / 255.0 blue:95 / 255.0 alpha:1.0];
        cell.detailTextLabel.text = self.demand.time;
        cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:142 / 255.0 green:142 / 255.0 blue:142 / 255.0 alpha:1.0];
        
        return cell;
    } else if (indexPath.row == 1) {
        DemandDescTableViewCell *cell = [DemandDescTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.desc = self.demand.content;
        
        return cell;
    } else if (indexPath.row == 2) {
        DemandDescTableViewCell *cell = [DemandDescTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.titleString = @"    需求回复";
        
        return cell;
    } else {
        ReplyTableViewCell *cell = [ReplyTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.reply = self.demand.reply[indexPath.row - 3];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 44;
    } else if (indexPath.row == 1) {
        return [self.demand.content boundingRectWithSize:CGSizeMake(284, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 30 + 16;
    } else if (indexPath.row == 2) {
        return 30;
    } else {
        return [[NSString stringWithFormat:@"%@ : %@", [self.demand.reply[indexPath.row - 3] name], [self.demand.reply[indexPath.row - 3] content]] boundingRectWithSize:CGSizeMake(284, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + [[NSString stringWithFormat:@"手机 : %@ %@", [self.demand.reply[indexPath.row - 3] phone], [self.demand.reply[indexPath.row - 3] time]] boundingRectWithSize:CGSizeMake(284, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 30;
    }
}

- (IBAction)reply
{
    if ([UserModel isLogin]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_REPLYVIEWCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[ReplyViewController class]]) {
            ReplyViewController *replyViewController = (ReplyViewController *)viewController;
            replyViewController.myId = self.demand.myId;
            [self.navigationController pushViewController:replyViewController animated:YES];
        }
    } else {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[NavigationController class]]) {
            [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
        }
    }
    
    
}

@end
