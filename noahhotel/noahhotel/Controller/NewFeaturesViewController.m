//
//  NewFeaturesViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "NewFeaturesViewController.h"
#import "TabBarController.h"
#import "StoryboardModel.h"

#define NEWFEATURESCOUNT 2

@interface NewFeaturesViewController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *myNewFeaturesScrollView;

@end

@implementation NewFeaturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    
    self.myNewFeaturesScrollView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * NEWFEATURESCOUNT, [UIScreen mainScreen].bounds.size.height);
    
    self.myNewFeaturesScrollView.delegate = self;
    
    for (int i = 0; i < NEWFEATURESCOUNT; ++i) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width * i, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        imageView.userInteractionEnabled = YES;
        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"NewFeatures%d.png", i + 1]];
//        imageView.backgroundColor = [UIColor colorWithRed:arc4random() % 255 / 255.0 green:arc4random() % 255 / 255.0 blue:arc4random() % 255 / 255.0 alpha:1];
        [self.myNewFeaturesScrollView addSubview:imageView];
        
        if (i == NEWFEATURESCOUNT - 1) {
            [self setupLastNewFeatureWithImageView:imageView];
        }
    }
}

- (void)setupLastNewFeatureWithImageView:(UIImageView *)imageView
{
    if (self.isFirstShow == 0) {
        UIButton *loginBtn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width / 8, [UIScreen mainScreen].bounds.size.height - [UIScreen mainScreen].bounds.size.height / 14 - [UIScreen mainScreen].bounds.size.height / 14, [UIScreen mainScreen].bounds.size.width / 2 - [UIScreen mainScreen].bounds.size.width / 8 * 1.5, [UIScreen mainScreen].bounds.size.height / 16)];
        [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
        [loginBtn setTitleColor:UIColorFromRGB(0xe2a248) forState:UIControlStateNormal];
        loginBtn.layer.borderWidth = 1;
        loginBtn.layer.borderColor = [UIColorFromRGB(0xca623e) CGColor];
        [loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
        loginBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [imageView addSubview:loginBtn];
        
        
        loginBtn.layer.cornerRadius = 4;
        loginBtn.layer.masksToBounds = YES;
        
        UIButton *skipBtn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width -[UIScreen mainScreen].bounds.size.width / 8 - ([UIScreen mainScreen].bounds.size.width / 2 - [UIScreen mainScreen].bounds.size.width / 8 * 1.5), [UIScreen mainScreen].bounds.size.height - [UIScreen mainScreen].bounds.size.height / 14 - [UIScreen mainScreen].bounds.size.height / 14, [UIScreen mainScreen].bounds.size.width / 2 - [UIScreen mainScreen].bounds.size.width / 8 * 1.5, [UIScreen mainScreen].bounds.size.height / 16)];
        [skipBtn setTitle:@"直接进入" forState:UIControlStateNormal];
        [skipBtn setTitleColor:UIColorFromRGB(0xe2a248) forState:UIControlStateNormal];
        skipBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        skipBtn.layer.borderWidth = 1;
        skipBtn.layer.borderColor = [UIColorFromRGB(0xca623e) CGColor];
        [skipBtn addTarget:self action:@selector(skip) forControlEvents:UIControlEventTouchUpInside];
        [imageView addSubview:skipBtn];
        
        skipBtn.layer.cornerRadius = 4;
        skipBtn.layer.masksToBounds = YES;
    } else if (self.firstShow == 1) {
        UIButton *skipBtn = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width / 2 - ([UIScreen mainScreen].bounds.size.width / 2 - [UIScreen mainScreen].bounds.size.width / 8 * 1.5) / 2, [UIScreen mainScreen].bounds.size.height - [UIScreen mainScreen].bounds.size.height / 14 - [UIScreen mainScreen].bounds.size.height / 14, [UIScreen mainScreen].bounds.size.width / 2 - [UIScreen mainScreen].bounds.size.width / 8 * 1.5, [UIScreen mainScreen].bounds.size.height / 16)];
        [skipBtn setTitle:@"退出" forState:UIControlStateNormal];
        [skipBtn setTitleColor:UIColorFromRGB(0xe2a248) forState:UIControlStateNormal];
        skipBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        skipBtn.layer.borderWidth = 1;
        skipBtn.layer.borderColor = [UIColorFromRGB(0xca623e) CGColor];
        [skipBtn addTarget:self action:@selector(skip) forControlEvents:UIControlEventTouchUpInside];
        [imageView addSubview:skipBtn];
        
        skipBtn.layer.cornerRadius = 4;
        skipBtn.layer.masksToBounds = YES;
    }
    
    
//    UILabel *label = [[UILabel alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    label.text = @"触摸或左划都可以进入首页";ad
//    label.font = [UIFont systemFontOfSize:20];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor colorWithRed:arc4random() % 255 / 255.0 green:arc4random() % 255 / 255.0 blue:arc4random() % 255 / 255.0 alpha:1];
//    [imageView addSubview:label];
//    
//    UIButton *button = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    [button addTarget:self action:@selector(skip) forControlEvents:UIControlEventTouchUpInside];
//    [imageView addSubview:button];
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    double page = scrollView.contentOffset.x / [UIScreen mainScreen].bounds.size.width;
//    
//    if (page > NEWFEATURESCOUNT - 1 + 0.05) {
//        [self skip];
//    }
//}

- (void)skip {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"skip" forKey:@"skip"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // 显示状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    if ( [window.rootViewController isKindOfClass:[TabBarController class]]) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    } else {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_TABBARCONTROLLER_STORYBOARD_ID];
        window.rootViewController = (TabBarController *)viewController;
        
    }
}

- (void)login {
    // 显示状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    if ( [window.rootViewController isKindOfClass:[TabBarController class]]) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    } else {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_TABBARCONTROLLER_STORYBOARD_ID];
        window.rootViewController = (TabBarController *)viewController;
        
    }
}

@end
