//
//  MapViewController.h
//  noahhotel
//
//  Created by darkerwei on 15/9/14.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"
#import "HotelModel.h"
#import "SelectModel.h"

@interface MapViewController : BaseViewController

/**
 *  位置列表
 */
@property (nonatomic, strong) NSArray *locationArray;

@property (nonatomic, strong) HotelModel *hotelModel;

@property (nonatomic, strong) SelectModel *selectModel;

@end
