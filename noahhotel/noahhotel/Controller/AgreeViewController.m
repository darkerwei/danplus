//
//  AgreeViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/9/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AgreeViewController.h"
#import "StoryboardModel.h"
#import "AgreementViewController.h"


@interface AgreeViewController ()

@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end

@implementation AgreeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"同行合作方案协议";
    
    NSURL* url = [NSURL URLWithString:@"http://115.28.94.253:8098/main/agreement/app"];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

- (IBAction)btnClick
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:@"AgreementViewController"];
    
    if ([viewController isKindOfClass:[AgreementViewController class]]) {
        AgreementViewController *replyViewController = (AgreementViewController *)viewController;
        replyViewController.myID = self.myID;
        [self.navigationController pushViewController:replyViewController animated:YES];
    }
}

@end
