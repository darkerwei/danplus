//
//  NeedViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/20.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "NeedViewController.h"
#import "CycleViewController.h"
#import "Networkservice.h"
#import "HotelModel.h"
#import <SVProgressHUD.h>
#import "DetailViewController.h"
#import "StoryboardModel.h"
#import "MySearchBar.h"
#import "DemandModel.h"
#import "DemandTableViewCell.h"
#import "ShadeView.h"
#import "MyDemandViewController.h"
#import "DemandDetailsViewController.h"
#import "SendDemandViewController.h"
#import <MJRefresh.h>
#import "MyTableViewCell.h"
#import "UserModel.h"
#import "NavigationController.h"
#import "HotelImageModel.h"
#import "SearchViewController.h"
#import "QFQBannerView.h"
#import "AboutViewController.h"
#import "SupplyDetailViewController.h"

@interface NeedViewController () <MySearchBarDelegate, DemandTableViewCellDelegate, ShadeViewDelegate, UITextFieldDelegate, QFQBannerViewDelegate>

//@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *demandArray;
@property (nonatomic, strong) MySearchBar *searchBar;
/**
 *  遮罩
 */
@property (strong, nonatomic) ShadeView *shadeView;
@property (nonatomic, strong) DemandTableViewCell * cell;

@property (nonatomic, strong) UIDatePicker *pickerView;

@property (getter = isFromTime, nonatomic) BOOL fromTime;

@property (nonatomic, assign) int rows;

@property (nonatomic, assign) int one;

@property (nonatomic, weak) QFQBannerView *banner;

@end

@implementation NeedViewController

- (void)bannerView:(QFQBannerView *)bannerView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    QFQHomeCollecionModel *model = bannerView.data[indexPath.row];
    
    NSLog(@"%@",  model.linkUrl);
    NSLog(@"%@",  model.linkType);
    
    if ([model.linkType isEqualToString:@"2"]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SupplyDetailViewController_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[SupplyDetailViewController class]]) {
            SupplyDetailViewController *supplyDetailViewController = (SupplyDetailViewController *)viewController;
            supplyDetailViewController.myId = [NSNumber numberWithInt:model.linkUrl.intValue];
            [self.navigationController pushViewController:supplyDetailViewController animated:YES];
        }
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    QFQBannerView *banner = [[QFQBannerView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 160)];
    [self.view addSubview:banner];
    self.banner = banner;
    self.banner.delegate = self;
    
    // 搜索框
    self.searchBar = [[MySearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 30)];
    self.navigationItem.titleView = self.searchBar;
    self.searchBar.delegate2 = self;
    self.searchBar.delegate = self;
    self.searchBar.buttonName = @"我的需求";
    
    [[Networkservice sharedInstance] getDemandImagesuccess:^(NSArray *hotelImageArray) {
        
        
        NSMutableArray *mArray = [NSMutableArray array];
        for (HotelImageModel *image in hotelImageArray) {
            QFQHomeCollecionModel *model = [QFQHomeCollecionModel new];
            model.imgUrl = image.phonePicture;
            model.linkUrl = image.linkId.stringValue;
            model.linkType = image.type.stringValue;
            [mArray addObject:model];
        }
        
        banner.data = mArray;
    } failure:^(NSError *error) {
        
    }];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self refresh];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SearchViewController_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[SearchViewController class]]) {
        SearchViewController *searchViewController = (SearchViewController *)viewController;
        searchViewController.title = @"需求查询";
        [self.navigationController pushViewController:searchViewController animated:YES];
    }
    return NO;
}

- (void)refresh
{
    [self upRefresh];
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    // 马上进入刷新状态
    [self.tableView.header beginRefreshing];
}

- (void)loadNewData
{
    self.rows = 10;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *endTime = [dateFormatter stringFromDate:[NSDate date]];
    
    [[Networkservice sharedInstance] searchWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows] startTime:(self.cell.fromTimeStr ? self.cell.fromTimeStr : @"2015-08-01")  endTime:endTime title:@"" success:^(NSArray *demandArray) {
        for (DemandModel *demand in demandArray) {
            DDLogVerbose(@"%@", demand);
            
        }
        
        if (demandArray.count == self.rows) {
            self.tableView.footer.hidden = NO;
        }
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        self.one = 1;
        [self.tableView reloadData];
        
        
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
    }];
}

- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.automaticallyHidden = NO;
    self.tableView.footer.hidden = YES;
}

- (void)loadMoreData
{
    self.rows += 10;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *endTime = [dateFormatter stringFromDate:[NSDate date]];
    
    [[Networkservice sharedInstance] searchWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows] startTime:(self.cell.fromTimeStr ? self.cell.fromTimeStr : @"2015-08-01") endTime:endTime title:@"" success:^(NSArray *demandArray) {
        for (DemandModel *demand in demandArray) {
            DDLogVerbose(@"%@", demand);
            
        }
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        [self.tableView reloadData];
        
        if (demandArray.count < self.rows) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的需求了"];
        }
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.footer endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.footer endRefreshing];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
//    [super viewWillAppear:animated];
//    
//    // 设置容器视图高度
//    CGFloat w = [UIScreen mainScreen].bounds.size.width;
//    CGFloat h = 110;
//    self.containerView.bounds = CGRectMake(0, 0, w, h);
//    
    self.tableView.tableHeaderView = self.banner;
//
//    if (self.showNew == 1) {
//        [self.tableView.header beginRefreshing];
//        
//        self.showNew = 0;
//    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.demandArray.count + self.one;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        self.cell = [DemandTableViewCell cellWithTableView:tableView];
        self.cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.cell.delegate = self;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *endTime = [dateFormatter stringFromDate:[NSDate date]];
        
        self.cell.toTimeStr = endTime;
        
        return self.cell;
    } else {
        MyTableViewCell *cell = [MyTableViewCell cellWithTableView:tableView];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.name = [self.demandArray[indexPath.row - 1] name];

        cell.date = [self.demandArray[indexPath.row - 1] time];
        
        cell.text = [self.demandArray[indexPath.row - 1] content];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 90;
    } else {
        return 80;
    }
}

#pragma mark - UITabBarDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_DEMANDDETAILSVIEWCONTROLLER_STORYBOARD_ID];
    
    if ([viewController isKindOfClass:[DemandDetailsViewController class]]) {
        DemandDetailsViewController *demandDetailsViewController = (DemandDetailsViewController *)viewController;
        demandDetailsViewController.title = [self.demandArray[indexPath.row - 1] name];
        demandDetailsViewController.myId = [self.demandArray[indexPath.row - 1] myId];
        
        [self.navigationController pushViewController:demandDetailsViewController animated:YES];
    }
}

#pragma mark - MySearchBarDelegate

- (void)choose
{
    [self.searchBar endEditing:YES];
    
    DDLogInfo(@"%@", self.searchBar.text);
    
    
    
    if ([UserModel isLogin]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_MYDEMANDVIEWCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[MyDemandViewController class]]) {
            MyDemandViewController *myDemandViewController = (MyDemandViewController *)viewController;
            
            [self.navigationController pushViewController:myDemandViewController animated:YES];
        }
    } else {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[NavigationController class]]) {
            [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
        }
    }
}

- (void)show
{
    self.pickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height + 30, [UIScreen mainScreen].bounds.size.width, 250 - 49)];
    self.pickerView.backgroundColor = [UIColor whiteColor];
    self.pickerView.datePickerMode = UIDatePickerModeDate;
    [self.pickerView addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    if (self.fromTime) {
        self.pickerView.date = [dateFormatter dateFromString:self.cell.fromTimeButton.titleLabel.text];
    } else {
        self.pickerView.date = [dateFormatter dateFromString:self.cell.toTimeButton.titleLabel.text];
    }
    
    [self.view addSubview:self.pickerView];
    
    [UIView animateWithDuration:ShadeAnimationTime animations:^{
        self.pickerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 250, [UIScreen mainScreen].bounds.size.width, 250 - 49);
    } completion:^(BOOL finished) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        if (self.fromTime) {
            self.cell.fromTimeStr = [dateFormatter stringFromDate:self.pickerView.date];
        } else {
            self.cell.toTimeStr = [dateFormatter stringFromDate:self.pickerView.date];
        }
    }];
}

- (void)dateChanged
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    if (self.fromTime) {
        self.cell.fromTimeStr = [dateFormatter stringFromDate:self.pickerView.date];
    } else {
        self.cell.toTimeStr = [dateFormatter stringFromDate:self.pickerView.date];
    }
}

- (void)dismiss
{
    [UIView animateWithDuration:ShadeAnimationTime animations:^{
        self.pickerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 250 - 49);
        
    } completion:^(BOOL finished) {
        [self.pickerView removeFromSuperview];
    }];
}

- (NSArray *)imageURLs {
    
    NSMutableArray *arrayM = [NSMutableArray array];
    
    for (int i = 1; i <= 3; i++) {
        NSString *fileName = [NSString stringWithFormat:@"%02d.jpg", i];
        NSURL *url = [[NSBundle mainBundle] URLForResource:fileName withExtension:nil];
        
        [arrayM addObject:url];
    }
    
    return arrayM;
}

/**
 *  发布新需求
 */
- (void)releaseDemand
{
    if ([UserModel isLogin]) {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_SENDDEMANDVIEWCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[SendDemandViewController class]]) {
            SendDemandViewController *sendDemandView = (SendDemandViewController *)viewController;
            
            [self.navigationController pushViewController:sendDemandView animated:YES];
        }
    } else {
        id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
        
        if ([viewController isKindOfClass:[NavigationController class]]) {
            [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
        }
    }
}
/**
 *  筛选
 */
- (void)chooseClick
{
//    [SVProgressHUD showWithStatus:@"正在获取需求"];
//    
//    [[Networkservice sharedInstance] searchWithPage:@"1" rows:@"10" startTime:self.cell.fromTimeButton.titleLabel.text endTime:self.cell.toTimeButton.titleLabel.text title:@"" success:^(NSArray *demandArray) {
//        [SVProgressHUD showSuccessWithStatus:@"获取需求成功"];
//        
//        for (DemandModel *demand in demandArray) {
//            DDLogVerbose(@"%@", demand);
//            
//        }
//        
//        self.demandArray = [NSArray arrayWithArray:demandArray];
//        [self.tableView reloadData];
//    } failure:^(NSError *error) {
//        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
//    }];

    self.rows = 10;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *endTime = [dateFormatter stringFromDate:[NSDate date]];
    
    [[Networkservice sharedInstance] searchWithPage:@"1" rows:[NSString stringWithFormat:@"%d", self.rows] startTime:(self.cell.fromTimeStr ? self.cell.fromTimeStr : @"2015-08-01")  endTime:endTime title:@"" success:^(NSArray *demandArray) {
        for (DemandModel *demand in demandArray) {
            DDLogVerbose(@"%@", demand);
            
        }
        
        if (demandArray.count == self.rows) {
            self.tableView.footer.hidden = NO;
        }
        
        self.demandArray = [NSArray arrayWithArray:demandArray];
        self.one = 1;
        [self.tableView reloadData];
        
        
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        
        // 拿到当前的下拉刷新控件，结束刷新状态
        [self.tableView.header endRefreshing];
    }];
}
/**
 *  从时间
 */
- (void)fromTime
{
    self.fromTime = YES;
    if (!self.shadeView) {
        self.shadeView = [[ShadeView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.shadeView.delegate = self;
    }
    
    [self.shadeView showWithView:self.view];
    
    [self.tableView reloadData];
}
/**
 *  到时间
 */
- (void)toTime
{
    self.fromTime = NO;
    
    if (!self.shadeView) {
        self.shadeView = [[ShadeView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.shadeView.delegate = self;
    }
    
    [self.shadeView showWithView:self.view];
    [self.tableView reloadData];
}

@end
