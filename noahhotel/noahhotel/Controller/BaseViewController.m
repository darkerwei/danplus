//
//  BaseViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // 点击空白退出键盘
    [self.view endEditing:YES];
}

- (IBAction)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)popToRootViewController
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)skip
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
