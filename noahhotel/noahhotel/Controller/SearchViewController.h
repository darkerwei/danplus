//
//  SearchViewController.h
//  noahhotel
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchViewController : BaseViewController

@property (nonatomic , copy) NSString *type;

@end
