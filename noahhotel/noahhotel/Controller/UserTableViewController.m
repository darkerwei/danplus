//
//  UserTableViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/13.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "UserTableViewController.h"
#import "UserModel.h"
#import "Networkservice.h"
#import <SVProgressHUD.h>
#import "UpdateViewController.h"
#import "NavigationController.h"
#import "StoryboardModel.h"
#import "PwdUpdateViewController.h"
#import "CheckViewController.h"
#import "MessageViewController.h"

@interface UserTableViewController ()

@property (weak, nonatomic) IBOutlet UITableViewCell *username;
@property (weak, nonatomic) IBOutlet UITableViewCell *message;

@end

@implementation UserTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.contentInset = UIEdgeInsetsMake(-25, 0, 0, 0);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Networkservice sharedInstance] getDetailWithsuccess:^(UserModel *user) {
        
        DDLogVerbose(@"%@", user);
        
        self.username.textLabel.text = user.name;
        self.message.detailTextLabel.text = user.replyNum.stringValue;
        
//        if (![user.replyNum.stringValue isEqualToString:@"0"]) {
//            self.navigationController.tabBarItem.badgeValue =user.replyNum.stringValue;
//        }
        
        [self.tableView reloadData];
    } failure:^(NSError *error) {
//        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        if ([UserModel isLogin]) {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_UpdateViewController_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[UpdateViewController class]]) {
                UpdateViewController *updateViewController = (UpdateViewController *)viewController;
                
                [self.navigationController pushViewController:updateViewController animated:YES];
            }
        } else {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[NavigationController class]]) {
                [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
            }
        }
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        if ([UserModel isLogin]) {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_PwdUpdateViewController_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[PwdUpdateViewController class]]) {
                PwdUpdateViewController *pwdUpdateViewController = (PwdUpdateViewController *)viewController;
                
                [self.navigationController pushViewController:pwdUpdateViewController animated:YES];
            }
        } else {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[NavigationController class]]) {
                [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
            }
        }
    } else if (indexPath.section == 2 && indexPath.row == 1) {
        if ([UserModel isLogin]) {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_CheckViewController_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[CheckViewController class]]) {
                CheckViewController *checkViewController = (CheckViewController *)viewController;
                
                [self.navigationController pushViewController:checkViewController animated:YES];
            }
        } else {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[NavigationController class]]) {
                [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
            }
        }
    }else if (indexPath.section == 1 && indexPath.row == 0) {
        if ([UserModel isLogin]) {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:MAIN_STORYBOARD_NAME storyboardID:MAIN_MessageViewController_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[MessageViewController class]]) {
                MessageViewController *messageViewController = (MessageViewController *)viewController;
                messageViewController.title = @"消息";
                [self.navigationController pushViewController:messageViewController animated:YES];
            }
        } else {
            id viewController = [StoryboardModel viewControllerWithStoryboardName:LOGIN_STORYBOARD_NAME storyboardID:LOGIN_NAVIGATIONCONTROLLER_STORYBOARD_ID];
            
            if ([viewController isKindOfClass:[NavigationController class]]) {
                [self presentViewController:(NavigationController *)viewController animated:YES completion:nil];
            }
        }
    }
}

@end
