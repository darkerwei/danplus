//
//  CheckViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/18.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "CheckViewController.h"
#import "FormatModel.h"
#import <SVProgressHUD.h>
#import "Networkservice.h"
#import "UserModel.h"
#import "IdentityModel.h"
#import "MyTableViewCell.h"

@interface CheckViewController () <UITableViewDataSource, UITableViewDelegate>

/**
 *  公司名称
 */
@property (nonatomic, weak) IBOutlet UITextField *companyName;
/**
 *  真实姓名
 */
@property (nonatomic, weak) IBOutlet UITextField *trueName;
/**
 *  职务
 */
@property (nonatomic, weak) IBOutlet UITextField *duty;
/**
 *  身份证
 */
@property (nonatomic, weak) IBOutlet UITextField *idNumber;
/**
 *  联系电话
 */
@property (nonatomic, weak) IBOutlet UITextField *telephone;

@property (nonatomic, strong) IdentityModel *identity;

@property (nonatomic, weak) IBOutlet UIButton *button;

@property (nonatomic, weak) IBOutlet UITableView *tableview;

@property (nonatomic, assign) int one;

@end

@implementation CheckViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableview.tableFooterView = [[UIView alloc] init];
    
    NSNumber *number = [[NSUserDefaults standardUserDefaults] objectForKey:@"approve"];
    
    if (number.intValue == 1) {
        
        self.button.hidden = YES;
        self.companyName.hidden = YES;
        self.trueName.hidden = YES;
        self.duty.hidden = YES;
        self.idNumber.hidden = YES;
        self.telephone.hidden = YES;
        self.tableview.hidden = NO;
        
        [[Networkservice sharedInstance] getIdentityWithsuccess:^(IdentityModel *identity) {
            
            self.identity = identity;
            self.one = 5;
            [self.tableview reloadData];
            
            self.companyName.text = identity.companyName;
            self.trueName.text = identity.trueName;
            self.duty.text = identity.duty;
            self.idNumber.text = identity.idNumber;
            self.telephone.text = identity.telephone;
            
            self.companyName.userInteractionEnabled = NO;
            self.trueName.userInteractionEnabled = NO;
            self.duty.userInteractionEnabled = NO;
            self.idNumber.userInteractionEnabled = NO;
            self.telephone.userInteractionEnabled = NO;
            
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.one;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTableViewCell *cell = [MyTableViewCell cellWithTableView:tableView];
    
    
    if (indexPath.row == 0) {
        cell.name = @"公司名称";
        cell.date = self.identity.companyName;
        
    } else if (indexPath.row == 1) {
        cell.name = @"真实姓名";
        cell.date = self.identity.trueName;
    } else if (indexPath.row == 2) {
        cell.name = @"职务";
        cell.date = self.identity.duty;
    } else if (indexPath.row == 3) {
        cell.name = @"身份证号";
        cell.date = self.identity.idNumber;
    } else if (indexPath.row == 4) {
        cell.name = @"联系电话";
        cell.date = self.identity.telephone;
    }
    
    return cell;
}

- (IBAction)check
{
    UserModel *user = [UserModel sharedInstance];
    user.companyName = self.companyName.text;
    user.trueName = self.trueName.text;
    user.duty = self.duty.text;
    user.idNumber = self.idNumber.text;
    user.telephone = self.telephone.text;
    
    [SVProgressHUD showWithStatus:@"正在提交"];
    [[Networkservice sharedInstance] checkWithUser:user success:^(UserModel *user) {
        [SVProgressHUD showSuccessWithStatus:@"提交成功"];
        
        DDLogVerbose(@"%@", user);
        
        [self back];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[BaseModel stringObjectWithResponseObject:error.userInfo key:@"msg"]];
    }];
}

@end
