//
//  PhotoViewController.h
//  noahhotel
//
//  Created by darkerwei on 15/9/28.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "BaseViewController.h"

@interface PhotoViewController : BaseViewController

@property (nonatomic, strong) NSArray *photo;

@end
