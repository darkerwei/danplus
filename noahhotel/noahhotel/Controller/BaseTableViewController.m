//
//  BaseTableViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/8/13.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController () <UITableViewDataSource, UITabBarDelegate>

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}

@end
