//
//  JWAboutOneTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/22.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutModel.h"

@protocol JWAboutOneTableViewCellDelegate <NSObject>

- (void)addressClick;
- (void)agreeBtnCLick;
- (void)photoClick;

@end

@interface JWAboutOneTableViewCell : UITableViewCell

@property (nonatomic, assign) id<JWAboutOneTableViewCellDelegate> delegate;

/**
 *  数据模型
 */
@property (nonatomic, strong) AboutModel *model;

@property (nonatomic, getter=isShowAgree) BOOL showAgree;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
