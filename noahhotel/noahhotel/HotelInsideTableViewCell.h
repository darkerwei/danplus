//
//  HotelInsideTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/28.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HotelInsideTableViewCellDelegate <NSObject>

- (void)selectSome;

@end

@interface HotelInsideTableViewCell : UITableViewCell

@property (nonatomic, copy) NSArray *array;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, getter=isShow) BOOL show;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) id<HotelInsideTableViewCellDelegate> delegate;

@end
