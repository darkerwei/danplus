//
//  JWNameValueModel.h
//  noahhotel
//
//  Created by darkerwei on 15/9/21.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface JWNameValueModel : BaseModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *value;

@end
