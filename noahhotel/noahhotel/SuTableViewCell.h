//
//  SuTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/23.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SupplyValueModel.h"

@interface SuTableViewCell : UITableViewCell

/**
 *  cell模型
 */
@property (nonatomic, strong) SupplyValueModel *model;
/**
 *  创建cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
