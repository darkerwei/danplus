//
//  CustomAnnotationView.h
//  iPolice
//
//  Created by ioswei on 15/7/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "CustomCalloutView.h"

@protocol CustomCalloutViewDelegate <NSObject>

- (void)viewClick;

@end

@interface CustomAnnotationView : MAAnnotationView

@property (nonatomic, assign) id<CustomCalloutViewDelegate> delegate2;

@property (nonatomic, readonly) CustomCalloutView *calloutView;

- (void)show;

@property (nonatomic, strong) NSString *imageURL;

@end
