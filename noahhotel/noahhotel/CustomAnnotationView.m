//
//  CustomAnnotationView.m
//  iPolice
//
//  Created by ioswei on 15/7/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CustomAnnotationView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CustomAnnotationView ()

@property (nonatomic, strong, readwrite) CustomCalloutView *calloutView;

@end

@implementation CustomAnnotationView

#define kCalloutWidth       200.0
#define kCalloutHeight      70.0

- (void)show
{
    if (self.calloutView == nil)
    {
        self.calloutView = [[CustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
        self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x,
                                              -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.calloutView.bounds];
    [imageView sd_setImageWithURL:[NSURL URLWithString:self.imageURL]];
    
    self.calloutView.image = imageView.image;
    self.calloutView.title = self.annotation.title;
    self.calloutView.subtitle = self.annotation.subtitle;
    
    [self addSubview:self.calloutView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    
}

// 重新此函数，用以实现点击calloutView判断为点击该annotationView
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL inside = [self.calloutView pointInside:[self convertPoint:point toView:self.calloutView] withEvent:event];
    if (inside == YES) {
        if (self.delegate2 && [self.delegate2 respondsToSelector:@selector(viewClick)]) {
            [self.delegate2 viewClick];
        }
    }
    
    return inside;
}

@end
