//
//  AboutOneTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutOneTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface AboutOneTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *level;
@property (nonatomic, weak) IBOutlet UILabel *lastDecorate;
@property (nonatomic, weak) IBOutlet UILabel *address;
@property (nonatomic, weak) IBOutlet UILabel *guestRoomNumber;
@property (nonatomic, weak) IBOutlet UILabel *maxFeastPerson;
@property (nonatomic, weak) IBOutlet UILabel *maxMeetingPerson;
@property (nonatomic, weak) IBOutlet UILabel *meetingRoomNumber;
@property (nonatomic, weak) IBOutlet UILabel *startBusinessTime;
@property (nonatomic, weak) IBOutlet UILabel *city;

@property (nonatomic, weak) IBOutlet UIImageView *image;

@end

@implementation AboutOneTableViewCell

- (void)setAbout:(AboutModel *)about
{
    _about = about;
    
    self.level.text = about.level;
    
//    [self.image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"", [[about.pictures firstObject] picture]]]];
    self.lastDecorate.text = about.lastDecorate.stringValue;
    self.address.text = about.address;
    self.guestRoomNumber.text = about.guestRoomNumber.stringValue;
    self.maxFeastPerson.text = about.maxFeastPerson.stringValue;
    self.maxMeetingPerson.text = about.maxMeetingPerson.stringValue;
    self.meetingRoomNumber.text = about.meetingRoomNumber.stringValue;
    self.startBusinessTime.text = about.startBusinessTime.stringValue;
    self.city.text = [NSString stringWithFormat:@"%@/%@/%@/%@", about.city, about.subway, about.zone, about.businessArea];
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"AboutOneTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    AboutOneTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

- (IBAction)cooperation
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cooperation)]) {
        [self.delegate cooperation];
    }
}

@end
