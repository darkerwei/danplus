//
//  CooperationTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "CooperationTableViewCell.h"

@implementation CooperationTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"CooperationTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    CooperationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
