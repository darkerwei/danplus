//
//  LoginTextFieldView.m
//  noahhotel
//
//  Created by darkerwei on 15/8/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "LoginTextFieldView.h"

#define LOGIN_TEXTFIELDVIEW_LINEHEIGHT 8

@implementation LoginTextFieldView

- (void)drawRect:(CGRect)rect
{
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0, self.bounds.size.height - LOGIN_TEXTFIELDVIEW_LINEHEIGHT)];
    [path addLineToPoint:CGPointMake(0, self.bounds.size.height)];
    [path addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height)];
    [path addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height - LOGIN_TEXTFIELDVIEW_LINEHEIGHT)];
    
    [[UIColor colorWithRed:149 / 255.0 green:149 / 255.0 blue:149 / 255.0 alpha:1] setStroke];
    [path stroke];
}

@end
