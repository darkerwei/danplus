//
//  DetailView2TableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/28.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailView2TableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) IBOutlet NSString *name;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
