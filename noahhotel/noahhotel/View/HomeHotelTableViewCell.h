//
//  HomeHotelTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/21.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HotelModel;

@protocol HomeHotelTableViewCellDelegate <NSObject>

/**
 *  酒店地址按钮点击
 */
- (void)addressBtnClick:(UIButton *)button;

@end

@interface HomeHotelTableViewCell : UITableViewCell

@property (nonatomic, assign) id<HomeHotelTableViewCellDelegate> delegate;

/**
 *  为了地址能点击到
 */
@property (nonatomic, weak) IBOutlet UIButton *bigBtn;

/**
 *  酒店地址
 */
@property (nonatomic, weak) IBOutlet UIButton *addressBtn;

/**
 *  cell模型
 */
@property (nonatomic, strong) HotelModel *model;
/**
 *  创建cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
