//
//  ReplyTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReplyModel;

@interface ReplyTableViewCell : UITableViewCell

/**
 *  cell数据
 */
@property (nonatomic, strong) ReplyModel *reply;

/**
 *  创建一个cell
 *
 *  @param tableView 在哪个tableView
 *
 *  @return cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
