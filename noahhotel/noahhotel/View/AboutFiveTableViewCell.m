//
//  AboutFiveTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/1.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutFiveTableViewCell.h"

@interface AboutFiveTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *name1;

@end

@implementation AboutFiveTableViewCell

- (void)setAbout:(AboutModel *)about
{
    _about = about;
    
    NSMutableString *str = [[NSMutableString alloc] init];
    for (NSString *strTemp in self.about.aroundSet) {
        [str appendString:[NSString stringWithFormat:@"%@  ", strTemp]];
    }
    
    self.name1.text = [NSString stringWithFormat:@"%@", str];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"AboutFiveTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    AboutFiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
