//
//  AboutTwoTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutTwoTableViewCell.h"
#import "NameValueModel.h"

@interface AboutTwoTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *label1;
@property (nonatomic, weak) IBOutlet UILabel *label2;
@property (nonatomic, weak) IBOutlet UILabel *label3;
@property (nonatomic, weak) IBOutlet UILabel *label4;
@property (nonatomic, weak) IBOutlet UILabel *label5;

@property (nonatomic, weak) IBOutlet UIView *view1;

@property (nonatomic, weak) IBOutlet UIView *view2;

@end

@implementation AboutTwoTableViewCell

- (void)setMeetingsModel:(MeetingsModel *)meetingsModel
{
    _meetingsModel = meetingsModel;
    
    self.label1.text = [NSString stringWithFormat:@"房型 : %@", meetingsModel.title];
    
    for (int i = 0; i < meetingsModel.content.count; ++i) {
        if (i == 0) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label2.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
            
            self.view1.hidden = YES;
            self.view2.hidden = YES;
        }
        
        if (i == 1) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label3.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
        }
        
        if (i == 2) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label4.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
            
            self.view1.hidden = NO;
            self.view2.hidden = NO;
        }
        
        if (i == 3) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label5.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
        }
    }
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"AboutTwoTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    AboutTwoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
