//
//  DetailViewTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/27.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DetailViewTableViewCell.h"

@interface DetailViewTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;

@property (nonatomic, weak) IBOutlet UILabel *namel;

@property (nonatomic, weak) IBOutlet UIView *view;

@end

@implementation DetailViewTableViewCell

- (void)setTitle:(NSString *)title
{
    _title = title;
    
    self.nameLabel.text = title;

    [self.view.layer setBorderWidth:1.0];
    [self.view.layer setBorderColor:[[UIColor colorWithRed:220 / 255.0 green:220 / 255.0 blue:220 / 255.0 alpha:1.0] CGColor]];
}

- (void)setName:(NSString *)name
{
    _name = name;
    
    self.namel.text = name;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"DetailViewTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    DetailViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    
    return cell;
}

@end
