//
//  CooperationTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CooperationTableViewCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
