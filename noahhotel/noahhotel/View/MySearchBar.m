//
//  MySearchBar.m
//  noahhotel
//
//  Created by darkerwei on 15/8/19.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "MySearchBar.h"

@interface MySearchBar()

@property (nonatomic, strong) UIButton *button;

@end

@implementation MySearchBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.background = [UIImage imageNamed:@"搜索框"];
        
        self.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30 , 30)];
        imageView.image = [UIImage imageNamed:@"搜索"];
        imageView.contentMode = UIViewContentModeCenter;
        self.leftView = imageView;
        
        self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
        [self.button setTitle:@"筛选" forState:UIControlStateNormal];
        self.button.titleLabel.font = [UIFont systemFontOfSize:15];
        self.button.backgroundColor = [UIColor colorWithRed:244 / 255.0 green:71 / 255.0 blue:75 / 255.0 alpha:1.0];
        [self.button addTarget:self action:@selector(choose) forControlEvents:UIControlEventTouchUpInside];
        self.rightView = self.button;
        
        self.textColor = [UIColor whiteColor];
        self.font = [UIFont systemFontOfSize:15];
        
        // 设置左边的view永远显示
        self.leftViewMode = UITextFieldViewModeAlways;
        self.rightViewMode = UITextFieldViewModeAlways;
        
        // 设置右边永远显示清除按钮
        self.clearButtonMode = UITextFieldViewModeAlways;
        
        
    }
    return self;
}

- (void)setButtonName:(NSString *)buttonName
{
    _buttonName = buttonName;
    
    if ([buttonName isEqualToString:@""]) {
        self.button.frame = CGRectMake(0, 0, 0, 0);
    } else {
        [self.button setTitle:buttonName forState:UIControlStateNormal];
        self.button.titleLabel.font = [UIFont systemFontOfSize:14];
        self.button.frame = CGRectMake(0, 0, 80, 30);
    }
}

- (void)choose
{
    if (self.delegate2 && [self.delegate2 respondsToSelector:@selector(choose)]) {
        [self.delegate2 choose];
    }
}

@end
