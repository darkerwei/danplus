//
//  ProductTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/25.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProductModel;

@interface ProductTableViewCell : UITableViewCell

@property (nonatomic, strong) ProductModel *product;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
