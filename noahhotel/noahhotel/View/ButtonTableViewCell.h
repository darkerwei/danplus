//
//  ButtonTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/28.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ButtonTableViewCellDelegate <NSObject>

- (void)quit;

@end

@interface ButtonTableViewCell : UITableViewCell
@property (nonatomic, assign) id<ButtonTableViewCellDelegate> delegate;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
