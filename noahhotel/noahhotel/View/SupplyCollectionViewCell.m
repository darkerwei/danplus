//
//  SupplyCollectionViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SupplyCollectionViewCell.h"
#import "SupplyValueModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define SupplyCollectionViewCellID @"SupplyCollectionViewCell"

@interface SupplyCollectionViewCell()

@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end

@implementation SupplyCollectionViewCell

- (void)setSupplyValue:(SupplyValueModel *)supplyValue
{
    _supplyValue = supplyValue;
    

    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",supplyValue.picture]]];
    
    self.name.text = supplyValue.name;
}

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath
{
    [collectionView registerNib:[UINib nibWithNibName:SupplyCollectionViewCellID bundle:nil] forCellWithReuseIdentifier:SupplyCollectionViewCellID];
    SupplyCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:SupplyCollectionViewCellID forIndexPath:indexPath];
    
    return cell;
}

@end
