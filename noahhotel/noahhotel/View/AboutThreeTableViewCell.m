//
//  AboutThreeTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/1.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutThreeTableViewCell.h"
#import "NameValueModel.h"

@interface AboutThreeTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *label1;
@property (nonatomic, weak) IBOutlet UILabel *label2;
@property (nonatomic, weak) IBOutlet UILabel *label3;
@property (nonatomic, weak) IBOutlet UILabel *label4;
@property (nonatomic, weak) IBOutlet UILabel *label5;
@property (nonatomic, weak) IBOutlet UILabel *label6;
@property (nonatomic, weak) IBOutlet UILabel *label7;
@property (nonatomic, weak) IBOutlet UILabel *label8;
@property (nonatomic, weak) IBOutlet UILabel *label9;
@property (nonatomic, weak) IBOutlet UILabel *label10;
@property (nonatomic, weak) IBOutlet UILabel *label11;

@property (nonatomic, weak) IBOutlet UIView *view1;

@property (nonatomic, weak) IBOutlet UIView *view2;
@property (nonatomic, weak) IBOutlet UIView *view3;
@property (nonatomic, weak) IBOutlet UIView *view4;
@property (nonatomic, weak) IBOutlet UIView *view5;
@property (nonatomic, weak) IBOutlet UIView *view6;
@property (nonatomic, weak) IBOutlet UIView *view7;
@property (nonatomic, weak) IBOutlet UIView *view8;

@end
@implementation AboutThreeTableViewCell

- (void)setMeetingsModel:(MeetingsModel *)meetingsModel
{
    self.label1.text = @"";
    self.label2.text = @"";
    self.label3.text = @"";
    self.label4.text = @"";
    self.label5.text = @"";
    self.label6.text = @"";
    self.label7.text = @"";
    self.label8.text = @"";
    self.label9.text = @"";
    self.label10.text = @"";
    self.label11.text = @"";
    
    _meetingsModel = meetingsModel;
    
    self.label1.text = [NSString stringWithFormat:@"%@ : %@",  self.title, meetingsModel.title];
    
    for (int i = 0; i < meetingsModel.content.count; ++i) {
        if (i == 0) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label2.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
            
            self.view1.hidden = YES;
            self.view2.hidden = YES;
            self.view3.hidden = YES;
            self.view4.hidden = YES;
            self.view5.hidden = YES;
            self.view6.hidden = YES;
            self.view7.hidden = YES;
            self.view8.hidden = YES;
            
        }
        
        if (i == 1) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label3.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
        }
        
        if (i == 2) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label4.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
            
            self.view1.hidden = NO;
            self.view2.hidden = NO;
        }
        
        if (i == 3) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label5.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
        }
        
        if (i == 4) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label6.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
            
            self.view3.hidden = NO;
            self.view4.hidden = NO;
        }
        
        if (i == 5) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label7.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
        }
        
        if (i == 6) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label8.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
            
            self.view5.hidden = NO;
            self.view6.hidden = NO;
        }
        
        if (i == 7) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label9.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
        }
        
        if (i == 8) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label10.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
            
            self.view7.hidden = NO;
            self.view6.hidden = NO;
        }
        
        if (i == 9) {
            
            NameValueModel *nameValue = meetingsModel.content[i];
            
            self.label11.text = [NSString stringWithFormat:@"%@ : %@", nameValue.name, nameValue.value];
        }
    }
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"AboutThreeTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    AboutThreeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
