//
//  ReplyTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "ReplyTableViewCell.h"
#import "ReplyModel.h"

@interface ReplyTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UILabel *phone;

@end

@implementation ReplyTableViewCell

- (void)setReply:(ReplyModel *)reply
{
    _reply = reply;
    
    self.name.text = [NSString stringWithFormat:@"%@ : %@", reply.name, reply.content];
    self.phone.text = [NSString stringWithFormat:@"手机 : %@ %@", reply.phone, reply.time];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"ReplyTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    ReplyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
