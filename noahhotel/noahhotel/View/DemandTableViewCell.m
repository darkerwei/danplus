//
//  DemandTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/20.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DemandTableViewCell.h"

@interface DemandTableViewCell()



@end

@implementation DemandTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"DemandTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    DemandTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

- (void)setFromTimeStr:(NSString *)fromTimeStr
{
    _fromTimeStr = fromTimeStr;
    
    [self.fromTimeButton setTitle:fromTimeStr forState:UIControlStateNormal];
}

- (void)setToTimeStr:(NSString *)toTimeStr
{
    _toTimeStr = toTimeStr;
    
    [self.toTimeButton setTitle:toTimeStr forState:UIControlStateNormal];
}

/**
 *  发布新需求
 */
- (IBAction)releaseDemand
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(releaseDemand)]) {
        [self.delegate releaseDemand];
    }
}
/**
 *  从时间
 */
- (IBAction)fromTime
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(fromTime)]) {
        [self.delegate fromTime];
    }
}
/**
 *  到时间
 */
- (IBAction)toTime
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(toTime)]) {
        [self.delegate toTime];
    }
}

- (IBAction)chooseClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(chooseClick)]) {
        [self.delegate chooseClick];
    }
}

@end
