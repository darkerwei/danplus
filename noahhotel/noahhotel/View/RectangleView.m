//
//  RectangleView.m
//  noahhotel
//
//  Created by darkerwei on 15/8/13.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "RectangleView.h"

@implementation RectangleView

- (void)drawRect:(CGRect)rect
{
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(0, self.bounds.size.height)];
    [path addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height)];
    [path addLineToPoint:CGPointMake(self.bounds.size.width, 0)];
    [path addLineToPoint:CGPointMake(0, 0)];
    
    path.lineWidth = 2.0;
    
    [[UIColor colorWithRed:149 / 255.0 green:149 / 255.0 blue:149 / 255.0 alpha:0.7] setStroke];
    [path stroke];
}

@end
