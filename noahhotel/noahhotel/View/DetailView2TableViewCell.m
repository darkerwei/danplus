//
//  DetailView2TableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/28.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DetailView2TableViewCell.h"

@interface DetailView2TableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;

@property (nonatomic, weak) IBOutlet UILabel *namel;

@property (nonatomic, weak) IBOutlet UIView *view;

@end

@implementation DetailView2TableViewCell

- (void)setTitle:(NSString *)title
{
    _title = title;
    
    self.nameLabel.text = title;
    
    [self.view.layer setBorderWidth:1.0];
    [self.view.layer setBorderColor:[[UIColor colorWithRed:220 / 255.0 green:220 / 255.0 blue:220 / 255.0 alpha:1.0] CGColor]];
}

- (void)setName:(NSString *)name
{
    _name = name;
    
    self.namel.text = name;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"DetailView2TableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    DetailView2TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
//    DetailView2TableViewCell *cell= (DetailView2TableViewCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"DetailView2TableViewCell" owner:self options:nil]  lastObject];
    
    return cell;
}

@end
