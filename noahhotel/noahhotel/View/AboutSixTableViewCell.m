//
//  AboutSixTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/1.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutSixTableViewCell.h"

@interface AboutSixTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *name1;
@property (nonatomic, weak) IBOutlet UILabel *name2;

@end

@implementation AboutSixTableViewCell

- (void)setAbout:(AboutModel *)about
{
    _about = about;
    
    self.name1.text = [NSString stringWithFormat:@"QQ : %@", about.qq];
    self.name2.text = [NSString stringWithFormat:@"微信 : %@", about.weixin];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"AboutSixTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    AboutSixTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
