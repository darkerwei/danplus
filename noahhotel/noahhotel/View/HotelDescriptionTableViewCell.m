//
//  HotelDescriptionTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/29.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "HotelDescriptionTableViewCell.h"


@interface HotelDescriptionTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *descLabel;

@end

@implementation HotelDescriptionTableViewCell

- (void)setDesc:(NSString *)desc
{
    _desc = desc;
    
    self.descLabel.text = desc;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"HotelDescriptionTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    
    return [tableView dequeueReusableCellWithIdentifier:identifier];
}

@end
