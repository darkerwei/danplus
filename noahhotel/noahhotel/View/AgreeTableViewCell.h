//
//  AgreeTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgreeModel.h"

@interface AgreeTableViewCell : UITableViewCell

@property (nonatomic, strong) AgreeModel *agree;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
