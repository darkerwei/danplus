//
//  DemandTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/20.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DemandTableViewCellDelegate <NSObject>

/**
 *  发布新需求
 */
- (void)releaseDemand;
/**
 *  从时间
 */
- (void)fromTime;
/**
 *  到时间
 */
- (void)toTime;
/**
 *  筛选
 */
- (void)chooseClick;

@end

@interface DemandTableViewCell : UITableViewCell

@property (nonatomic, assign) id<DemandTableViewCellDelegate> delegate;

/**
 *  发布新需求
 */
@property (weak, nonatomic) IBOutlet UIButton *releaseDemandButton;
/**
 *  从时间
 */
@property (weak, nonatomic) IBOutlet UIButton *fromTimeButton;
/**
 *  到时间
 */
@property (weak, nonatomic) IBOutlet UIButton *toTimeButton;

@property (nonatomic, copy) NSString *fromTimeStr;
@property (nonatomic, copy) NSString *toTimeStr;

/**
 *  创建一个cell
 *
 *  @param tableView 在哪个tableView
 *
 *  @return cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
