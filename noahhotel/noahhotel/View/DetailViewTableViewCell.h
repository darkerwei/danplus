//
//  DetailViewTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/27.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) IBOutlet NSString *name;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
