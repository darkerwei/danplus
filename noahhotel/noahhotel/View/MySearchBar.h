//
//  MySearchBar.h
//  noahhotel
//
//  Created by darkerwei on 15/8/19.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MySearchBarDelegate <NSObject>

/**
 *  筛选按钮事件
 */
- (void)choose;

@end

@interface MySearchBar : UITextField

@property (nonatomic, assign) id<MySearchBarDelegate> delegate2;

@property (nonatomic, copy) NSString *buttonName;

@end
