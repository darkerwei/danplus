//
//  AboutOneTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutModel.h"

@protocol AboutOneTableViewCellDelegate <NSObject>

- (void)cooperation;

@end

@interface AboutOneTableViewCell : UITableViewCell

@property (nonatomic, assign) id<AboutOneTableViewCellDelegate> delegate;

@property (nonatomic, strong) AboutModel *about;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
