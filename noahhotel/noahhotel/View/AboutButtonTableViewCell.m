//
//  AboutButtonTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/1.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutButtonTableViewCell.h"

@interface AboutButtonTableViewCell()

@property (nonatomic, weak) IBOutlet UIButton *button;
@property (nonatomic, strong) UIWebView *phoneCallWebView;

@end

@implementation AboutButtonTableViewCell

- (void)setAbout:(AboutModel *)about
{
    _about = about;
    
    [self.button setTitle:self.about.contact forState:UIControlStateNormal];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"AboutButtonTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    AboutButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

- (IBAction)tel:(UIButton *)button
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", button.titleLabel.text]];
    self.phoneCallWebView = [[UIWebView alloc] init];
    [self.phoneCallWebView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (IBAction)hotelReply
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(hotelReply)]) {
        [self.delegate hotelReply];
    }
}

@end
