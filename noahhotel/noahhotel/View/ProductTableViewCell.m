//
//  ProductTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/25.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "ProductTableViewCell.h"
#import "ProductModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ProductTableViewCell()


@property (nonatomic, weak) IBOutlet UILabel *desc;
@property (nonatomic, weak) IBOutlet UIImageView *image;

@property (nonatomic, weak) IBOutlet UILabel *label1;
@property (nonatomic, weak) IBOutlet UILabel *label2;

@end

@implementation ProductTableViewCell

- (void)setProduct:(ProductModel *)product
{
    _product = product;
    
    self.desc.text = product.myDescription;

    self.label1.text = product.name;
    self.label2.text = [NSString stringWithFormat:@"￥%@", product.price];
    
    [self.image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",product.picture]]];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"ProductTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
