//
//  DetailTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *phoneNumber;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
