//
//  AgreeTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AgreeTableViewCell.h"

@interface AgreeTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *name1;
@property (nonatomic, weak) IBOutlet UILabel *name2;
@property (nonatomic, weak) IBOutlet UILabel *name3;
@property (nonatomic, weak) IBOutlet UILabel *name4;
@property (nonatomic, weak) IBOutlet UILabel *name5;
@property (nonatomic, weak) IBOutlet UILabel *name6;
@property (nonatomic, weak) IBOutlet UILabel *name7;
@property (nonatomic, weak) IBOutlet UILabel *name8;

@end

@implementation AgreeTableViewCell

- (void)setAgree:(AgreeModel *)agree
{
    _agree = agree;
    
    self.name1.text = agree.pattern;
    self.name2.text = [NSString stringWithFormat:@"%@元（%@）", agree.meetingOffer, agree.breakfast];
    self.name3.text = [NSString stringWithFormat:@"%@折优惠", agree.discount];
    self.name4.text = [NSString stringWithFormat:@"（%@）", agree.season];
    self.name5.text = [NSString stringWithFormat:@"%@元/人/天（含两正一早餐、会议、住宿）（以上报价为基准价格，特大型会议价格可另行商议。）", agree.meetingAll];
    self.name6.text = [NSString stringWithFormat:@"%@元/桌起（%@），%@%%返点。", agree.feastOffer, agree.feastContain, agree.feastReply];
    self.name7.text = [NSString stringWithFormat:@"旅游团%@月 价格%@元/间，%@%%返点", agree.travelMonth, agree.travelOffer, agree.travelReply];
    self.name8.text = [NSString stringWithFormat:@"按总流水的%@%%返点，", agree.travelReply];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"AgreeTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    AgreeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
