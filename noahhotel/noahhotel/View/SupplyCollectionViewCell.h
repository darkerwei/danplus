//
//  SupplyCollectionViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SupplyValueModel;

@interface SupplyCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) SupplyValueModel *supplyValue;

/**
 *  快速创建一个cell
 *
 *  @param collectionView 哪个collectionView
 *  @param indexPath      collectionView的哪个cell
 *
 *  @return 一个创建好的cell
 */
+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath;

@end
