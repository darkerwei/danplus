//
//  ButtonTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/28.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "ButtonTableViewCell.h"

@implementation ButtonTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"ButtonTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    ButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

- (IBAction)quit
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(quit)]) {
        [self.delegate quit];
    }
}

@end
