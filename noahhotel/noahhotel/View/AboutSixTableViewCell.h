//
//  AboutSixTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/1.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutModel.h"

@interface AboutSixTableViewCell : UITableViewCell

@property (nonatomic, strong) AboutModel *about;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
