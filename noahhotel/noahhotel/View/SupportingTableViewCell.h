//
//  SupportingTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/29.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportingTableViewCell : UITableViewCell

/**
 *  cell模型
 */
@property (nonatomic, strong) NSArray *array;

@property (nonatomic, strong) NSString *titleStr;

/**
 *  创建cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
