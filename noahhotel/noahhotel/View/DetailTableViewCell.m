//
//  DetailTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DetailTableViewCell.h"

@interface DetailTableViewCell()

@property (nonatomic , weak) IBOutlet UIButton *telButton;

@property (nonatomic, strong) UIWebView *phoneCallWebView;

@end

@implementation DetailTableViewCell

- (void)setPhoneNumber:(NSString *)phoneNumber
{
    _phoneNumber = phoneNumber;
    
    [self.telButton setTitle:phoneNumber forState:UIControlStateNormal];
}

- (IBAction)tel:(UIButton *)button
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", button.titleLabel.text]];
    self.phoneCallWebView = [[UIWebView alloc] init];
    [self.phoneCallWebView loadRequest:[NSURLRequest requestWithURL:url]];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"DetailTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    DetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
