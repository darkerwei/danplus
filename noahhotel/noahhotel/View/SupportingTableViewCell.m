//
//  SupportingTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/29.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "SupportingTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SupportingModel.h"

@interface SupportingTableViewCell()

@property (nonatomic, strong) IBOutlet UILabel *title;

@property (nonatomic, strong) IBOutlet UILabel *label1;
@property (nonatomic, strong) IBOutlet UILabel *label2;
@property (nonatomic, strong) IBOutlet UILabel *label3;
@property (nonatomic, strong) IBOutlet UILabel *label4;
@property (nonatomic, strong) IBOutlet UILabel *label5;
@property (nonatomic, strong) IBOutlet UILabel *label6;
@property (nonatomic, strong) IBOutlet UILabel *label7;
@property (nonatomic, strong) IBOutlet UILabel *label8;
@property (nonatomic, strong) IBOutlet UILabel *label9;

@property (nonatomic, strong) IBOutlet UIImageView *imageView1;
@property (nonatomic, strong) IBOutlet UIImageView *imageView2;
@property (nonatomic, strong) IBOutlet UIImageView *imageView3;
@property (nonatomic, strong) IBOutlet UIImageView *imageView4;
@property (nonatomic, strong) IBOutlet UIImageView *imageView5;
@property (nonatomic, strong) IBOutlet UIImageView *imageView6;
@property (nonatomic, strong) IBOutlet UIImageView *imageView7;
@property (nonatomic, strong) IBOutlet UIImageView *imageView8;
@property (nonatomic, strong) IBOutlet UIImageView *imageView9;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout4;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout5;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout6;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout7;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout8;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laout9;
@end

@implementation SupportingTableViewCell

- (void)setArray:(NSArray *)array
{
    _array = array;
    
    self.label1.text = @"";
    self.label2.text = @"";
    self.label3.text = @"";
    self.label4.text = @"";
    self.label5.text = @"";
    self.label6.text = @"";
    self.label7.text = @"";
    self.label8.text = @"";
    self.label9.text = @"";
    
    self.imageView1.hidden = YES;
    self.imageView2.hidden = YES;
    self.imageView3.hidden = YES;
    self.imageView4.hidden = YES;
    self.imageView5.hidden = YES;
    self.imageView6.hidden = YES;
    self.imageView7.hidden = YES;
    self.imageView8.hidden = YES;
    self.imageView9.hidden = YES;
    
    self.laout1.constant = 0;
    self.laout2.constant = 0;
    self.laout3.constant = 0;
    self.laout4.constant = 0;
    self.laout5.constant = 0;
    self.laout6.constant = 0;
    self.laout7.constant = 0;
    self.laout8.constant = 0;
    self.laout9.constant = 0;
    
    for (int i = 0; i < array.count; ++i) {
        if (i == 0) {
            
            if ([array[i] picture].length != 0) {
                self.imageView1.hidden = NO;
                self.laout1.constant = 34;
                [self.imageView1 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label1.text = [array[i] name];
            } else {
                self.label1.text = [array[i] name];
            }
        } else if (i == 1) {
            if ([array[i] picture].length != 0) {
                self.imageView2.hidden = NO;
                self.laout2.constant = 34;
                [self.imageView2 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label2.text = [array[i] name];
            } else {
                self.label2.text = [array[i] name];
            }
        } else if (i == 2) {
            if ([array[i] picture].length != 0) {
                self.imageView3.hidden = NO;
                self.laout3.constant = 34;
                [self.imageView3 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label3.text = [array[i] name];
            } else {
                self.label3.text = [array[i] name];
            }
        } else if (i == 3) {
            if ([array[i] picture].length != 0) {
                self.imageView4.hidden = NO;
                self.laout4.constant = 34;
                [self.imageView4 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label4.text = [array[i] name];
            } else {
                self.label4.text = [array[i] name];
            }
        } else if (i == 4) {
            if ([array[i] picture].length != 0) {
                self.imageView5.hidden = NO;
                self.laout5.constant = 34;
                [self.imageView5 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label5.text = [array[i] name];
            } else {
                self.label5.text = [array[i] name];
            }
        } else if (i == 5) {
            if ([array[i] picture].length != 0) {
                self.imageView6.hidden = NO;
                self.laout6.constant = 34;
                [self.imageView6 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label6.text = [array[i] name];
            } else {
                self.label6.text = [array[i] name];
            }
        } else if (i == 6) {
            if ([array[i] picture].length != 0) {
                self.imageView7.hidden = NO;
                self.laout7.constant = 34;
                [self.imageView7 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label7.text = [array[i] name];
            } else {
                self.label3.text = [array[i] name];
            }
        } else if (i == 7) {
            if ([array[i] picture].length != 0) {
                self.imageView8.hidden = NO;
                self.laout8.constant = 34;
                [self.imageView8 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label8.text = [array[i] name];
            } else {
                self.label8.text = [array[i] name];
            }
        } else if (i == 8) {
            if ([array[i] picture].length != 0) {
                self.imageView9.hidden = NO;
                self.laout9.constant = 34;
                [self.imageView9 sd_setImageWithURL:[NSURL URLWithString:[array[i] picture]]];
                self.label9.text = [array[i] name];
            } else {
                self.label9.text = [array[i] name];
            }
        }
    }
    
    
}

- (void)setTitleStr:(NSString *)titleStr
{
    _titleStr = titleStr;
    
    self.title.text = titleStr;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"SupportingTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    
    return [tableView dequeueReusableCellWithIdentifier:identifier];
}

@end
