//
//  DemandDescTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DemandDescTableViewCell.h"

@interface DemandDescTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *descLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end

@implementation DemandDescTableViewCell

- (void)setTitleString:(NSString *)titleString
{
    _titleString = titleString;
    
    self.titleLabel.text = titleString;
}

- (void)setDesc:(NSString *)desc
{
    _desc = desc;
    
    self.descLabel.text = desc;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"DemandDescTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    DemandDescTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
