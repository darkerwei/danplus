//
//  AboutThreeTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/1.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeetingsModel.h"

@interface AboutThreeTableViewCell : UITableViewCell


@property (nonatomic, strong) MeetingsModel *meetingsModel;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (nonatomic, copy) NSString *title;

@end
