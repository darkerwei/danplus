//
//  MyTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/7.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *text;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
