//
//  MyTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/7.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "MyTableViewCell.h"

@interface MyTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *Label;
@property (nonatomic, weak) IBOutlet UILabel *Label2;

@end

@implementation MyTableViewCell

- (void)setName:(NSString *)name
{
    _name = name;
    self.nameLabel.text = name;
}

- (void)setDate:(NSString *)date
{
    _date = date;
    self.dateLabel.text = date;
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.Label.text = text;
    
    self.Label.hidden = NO;
    self.Label2.hidden = NO;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"MyTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
