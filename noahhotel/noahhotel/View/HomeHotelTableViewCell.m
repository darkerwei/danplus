//
//  HomeHotelTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/21.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "HomeHotelTableViewCell.h"
#import "HotelModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ValueTypeModel.h"
#import "JWNameValueModel.h"

@interface HomeHotelTableViewCell()

/**
 *  酒店名称
 */
@property (nonatomic, weak) IBOutlet UILabel *hotelNameLabel;

@property (nonatomic, weak) IBOutlet UILabel *levelNameLabel;
/**
 *  酒店图片
 */
@property (nonatomic, weak) IBOutlet UIImageView *pictureImageView;
/**
 *  酒店合作关系
 */
@property (nonatomic, weak) IBOutlet UILabel *consociationLable;

@property (nonatomic, weak) IBOutlet UIImageView *levelImageView1;
@property (nonatomic, weak) IBOutlet UIImageView *levelImageView2;
@property (nonatomic, weak) IBOutlet UIImageView *levelImageView3;
@property (nonatomic, weak) IBOutlet UIImageView *levelImageView4;
@property (nonatomic, weak) IBOutlet UIImageView *levelImageView5;
@property (nonatomic, weak) IBOutlet UIImageView *levelImageView6;

@property (nonatomic, weak) IBOutlet UILabel *name1Label;
@property (nonatomic, weak) IBOutlet UILabel *value1Label;
@property (nonatomic, weak) IBOutlet UILabel *name2Label;
@property (nonatomic, weak) IBOutlet UILabel *value2Label;
@property (nonatomic, weak) IBOutlet UILabel *name3Label;
@property (nonatomic, weak) IBOutlet UILabel *value3Label;

@end

@implementation HomeHotelTableViewCell

- (void)setModel:(HotelModel *)model
{
    _model = model;
    
    self.levelImageView1.hidden = YES;
    self.levelImageView2.hidden = YES;
    self.levelImageView3.hidden = YES;
    self.levelImageView4.hidden = YES;
    self.levelImageView5.hidden = YES;
    self.levelImageView6.hidden = YES;
    self.levelNameLabel.hidden = YES;
    
    self.hotelNameLabel.text = model.hotelName;
    [self.addressBtn setTitle:model.address forState:UIControlStateNormal];
    [self.pictureImageView sd_setImageWithURL:[NSURL URLWithString:model.picture]];
    
    self.consociationLable.layer.borderWidth = 1;
    self.consociationLable.layer.borderColor = [UIColorFromRGB(0xf4474b) CGColor];
    
    if ([model.consociation isEqual:@""]) {
        self.consociationLable.text = @"未合作";
    } else {
        self.consociationLable.text = @"合作";
    }
    
    if (model.level.type.intValue == 2) {
        self.levelNameLabel.hidden = NO;
        self.levelNameLabel.text = model.level.value;
    } else {
        if ([model.level.value isEqual:@"1星级"]) {
            self.levelImageView1.hidden = NO;
        } else if ([model.level.value isEqual:@"2星级"]) {
            self.levelImageView1.hidden = NO;
            self.levelImageView2.hidden = NO;
        } else if ([model.level.value isEqual:@"3星级"]) {
            self.levelImageView1.hidden = NO;
            self.levelImageView2.hidden = NO;
            self.levelImageView3.hidden = NO;
        } else if ([model.level.value isEqual:@"4星级"]) {
            self.levelImageView1.hidden = NO;
            self.levelImageView2.hidden = NO;
            self.levelImageView3.hidden = NO;
            self.levelImageView4.hidden = NO;
        } else if ([model.level.value isEqual:@"5星级"]) {
            self.levelImageView1.hidden = NO;
            self.levelImageView2.hidden = NO;
            self.levelImageView3.hidden = NO;
            self.levelImageView4.hidden = NO;
            self.levelImageView5.hidden = NO;
        } else if ([model.level.value isEqual:@"6星级"]) {
            self.levelImageView1.hidden = NO;
            self.levelImageView2.hidden = NO;
            self.levelImageView3.hidden = NO;
            self.levelImageView4.hidden = NO;
            self.levelImageView5.hidden = NO;
            self.levelImageView6.hidden = NO;
        }
    }
    
    for (int i = 0; i < model.showArray.count; ++i) {
        JWNameValueModel *nameValue = model.showArray[i];
        
        if (i == 0) {
            self.name1Label.text = nameValue.name;
            self.value1Label.text = nameValue.value;
        } else if (i == 1) {
            self.name2Label.text = nameValue.name;
            self.value2Label.text = nameValue.value;
        } else if (i == 2) {
            self.name3Label.text = nameValue.name;
            self.value3Label.text = nameValue.value;
        }
    }
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"HomeHotelTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    
    return [tableView dequeueReusableCellWithIdentifier:identifier];
}

- (IBAction)addressBtnClick:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(addressBtnClick:)]) {
        [self.delegate addressBtnClick:button];
    }
}

@end
