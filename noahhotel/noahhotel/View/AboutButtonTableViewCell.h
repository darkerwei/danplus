//
//  AboutButtonTableViewCell.h
//  noahhotel
//
//  Created by darkerwei on 15/9/1.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutModel.h"

@protocol AboutButtonTableViewCellDelegate <NSObject>

/**
 *  留言按钮事件
 */
- (void)hotelReply;

@end

@interface AboutButtonTableViewCell : UITableViewCell

@property (nonatomic, assign) id<AboutButtonTableViewCellDelegate> delegate;

@property (nonatomic, strong) AboutModel *about;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
