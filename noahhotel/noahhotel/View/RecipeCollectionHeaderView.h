//
//  RecipeCollectionHeaderView.h
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RecipeCollectionHeaderViewDelegate <NSObject>

- (void)buttonClick:(UIButton *)button;

@end

@interface RecipeCollectionHeaderView : UICollectionReusableView

@property (nonatomic, assign) id<RecipeCollectionHeaderViewDelegate> delegate;

@property (nonatomic, weak) IBOutlet UILabel *title;

@property (nonatomic, weak) IBOutlet UIButton *button;

@end
