//
//  JWNameValueModel.m
//  noahhotel
//
//  Created by darkerwei on 15/9/21.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "JWNameValueModel.h"

@implementation JWNameValueModel

- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        self.value = [BaseModel stringObjectWithResponseObject:responseObject key:@"value"];
    }
    return self;
}

@end
