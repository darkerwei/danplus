//
//  AboutMeViewController.m
//  noahhotel
//
//  Created by darkerwei on 15/9/24.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "AboutMeViewController.h"

@interface AboutMeViewController ()

@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end

@implementation AboutMeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"关于我们";
    
    NSURL* url = [NSURL URLWithString:@"http://115.28.94.253:8098/main/about/app"];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

@end
