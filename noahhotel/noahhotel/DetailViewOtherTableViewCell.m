//
//  DetailViewOtherTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/8/27.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DetailViewOtherTableViewCell.h"

@interface DetailViewOtherTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;

@property (nonatomic, weak) IBOutlet UIView *view;

@end

@implementation DetailViewOtherTableViewCell

- (void)setTitle:(NSString *)title
{
    _title = title;
    
    [self.view.layer setBorderWidth:1.0];
    [self.view.layer setBorderColor:[[UIColor colorWithRed:220 / 255.0 green:220 / 255.0 blue:220 / 255.0 alpha:1.0] CGColor]];
}

- (void)setName:(NSString *)name
{
    _name = name;
    
    self.nameLabel.text = name;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"DetailViewOtherTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    DetailViewOtherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
//    DetailViewOtherTableViewCell *cell= (DetailViewOtherTableViewCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"DetailViewOtherTableViewCell" owner:self options:nil]  lastObject];
    
    return cell;
}

- (IBAction)location
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(location)]) {
        [self.delegate location];
    }
}

@end
