//
//  ReplyModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "ReplyModel.h"

@implementation ReplyModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        NSNumber *timeTemp = [BaseModel numberObjectWithResponseObject:responseObject key:@"time"];
        
        if (timeTemp) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            self.time = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeTemp.doubleValue]];
        }
        
        
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        self.content = [BaseModel stringObjectWithResponseObject:responseObject key:@"content"];
        self.phone = [BaseModel stringObjectWithResponseObject:responseObject key:@"phone"];
        self.replyId = [BaseModel numberObjectWithResponseObject:responseObject key:@"replyId"];
        self.messageId = [BaseModel numberObjectWithResponseObject:responseObject key:@"messageId"];
    }
    
    return self;
}

- (NSString *)description
{
    NSMutableString *str = [[NSMutableString alloc] init];
    
    [str appendString:@"\n\t\t{\n"];
    
    [str appendString:self.name ? [NSString stringWithFormat:@"\t\t\t\tname : %@\n", self.name] : @""];
    [str appendString:self.time ? [NSString stringWithFormat:@"\t\t\t\ttime : %@\n", self.time] : @""];
    [str appendString:self.content ? [NSString stringWithFormat:@"\t\t\t\tcontent : %@\n", self.content] : @""];
    [str appendString:self.phone ? [NSString stringWithFormat:@"\t\t\t\tphone : %@\n", self.phone] : @""];
    [str appendString:self.replyId ? [NSString stringWithFormat:@"\t\t\t\treplyId : %@\n", self.replyId] : @""];
    [str appendString:self.messageId ? [NSString stringWithFormat:@"\t\t\t\tmessageId : %@\n", self.messageId] : @""];
    
    [str appendString:@"\t\t}"];
    
    return str;
}

@end
