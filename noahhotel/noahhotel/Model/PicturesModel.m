//
//  PicturesModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "PicturesModel.h"

@implementation PicturesModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.picture = [BaseModel stringObjectWithResponseObject:responseObject key:@"picture"];
        self.myDesc = [BaseModel stringObjectWithResponseObject:responseObject key:@"description"];
        
        self.myId = [BaseModel numberObjectWithResponseObject:responseObject key:@"id"];
        self.typeId = [BaseModel numberObjectWithResponseObject:responseObject key:@"typeId"];
        
    }
    
    return self;
}

@end
