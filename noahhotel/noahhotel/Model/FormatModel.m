//
//  FormatModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/12.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "FormatModel.h"

@implementation FormatModel

+ (BOOL)isValidFormatWithName:(NSString *)name
{
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",@"[a-zA-Z0-9]{1,15}"];
    return [passwordTest evaluateWithObject:name];
}

+ (BOOL)isValidFormatWithTelephone:(NSString *)telephone
{
    NSPredicate *telephoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",@"[1][0-9]{10}"];
    return [telephoneTest evaluateWithObject:telephone];
}

+ (BOOL)isValidFormatWithPassword:(NSString *)password
{
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",@"[a-zA-Z0-9]{6,32}"];
    return [passwordTest evaluateWithObject:password];
}

+ (BOOL)isValidFormatWithCompany:(NSString *)company
{
    NSPredicate *companyTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",@"[a-zA-Z0-9]{1,15}"];
    return [companyTest evaluateWithObject:company];
}

+ (BOOL)isValidFormatWithVerificationCode:(NSString *)VerificationCode
{
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",@"[0-9]{4}"];
    return [test evaluateWithObject:VerificationCode];
}

@end
