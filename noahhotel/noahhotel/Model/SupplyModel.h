//
//  SupplyModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface SupplyModel : BaseModel

/**
 *  分类名称
 */
@property (nonatomic, copy) NSString *name;
/**
 *  分类信息
 */
@property (nonatomic, strong) NSArray *value;

@end
