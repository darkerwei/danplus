//
//  ProductModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface ProductModel : BaseModel

@property (nonatomic, copy) NSString *myDescription;

@property (nonatomic, copy) NSString *picture;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSNumber *price;

@end
