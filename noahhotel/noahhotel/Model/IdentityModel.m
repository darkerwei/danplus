//
//  IdentityModel.m
//  noahhotel
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "IdentityModel.h"

@implementation IdentityModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.companyName = [BaseModel stringObjectWithResponseObject:responseObject key:@"companyName"];
        self.duty = [BaseModel stringObjectWithResponseObject:responseObject key:@"duty"];
        self.idNumber = [BaseModel stringObjectWithResponseObject:responseObject key:@"idNumber"];
        self.telephone = [BaseModel stringObjectWithResponseObject:responseObject key:@"telephone"];
        self.trueName = [BaseModel stringObjectWithResponseObject:responseObject key:@"trueName"];
        
    }
    
    return self;
}

@end
