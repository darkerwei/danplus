//
//  DemandModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/20.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "DemandModel.h"
#import "ReplyModel.h"

@implementation DemandModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        NSNumber *timeTemp = [BaseModel numberObjectWithResponseObject:responseObject key:@"time"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.time = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeTemp.doubleValue]];
        
        self.myId = [BaseModel numberObjectWithResponseObject:responseObject key:@"id"];
        self.demandId = [BaseModel numberObjectWithResponseObject:responseObject key:@"demandId"];
        self.status = [BaseModel numberObjectWithResponseObject:responseObject key:@"status"];
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        self.content = [BaseModel stringObjectWithResponseObject:responseObject key:@"content"];
        self.phone = [BaseModel stringObjectWithResponseObject:responseObject key:@"phone"];
        
        NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"reply"];
        
        NSMutableArray *demandList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            ReplyModel *reply = [ReplyModel modelWithResponseObject:responseObjectDict];
            
            [demandList addObject:reply];
        }
        
        self.reply = [demandList copy];
    }
    
    return self;
}

- (NSString *)description
{
    NSMutableString *str = [[NSMutableString alloc] init];
    
    [str appendString:@"\n{\n"];
    
    [str appendString:self.myId ? [NSString stringWithFormat:@"\tid : %@\n", self.myId] : @""];
    [str appendString:self.name ? [NSString stringWithFormat:@"\tname : %@\n", self.name] : @""];
    [str appendString:self.time ? [NSString stringWithFormat:@"\ttime : %@\n", self.time] : @""];
    [str appendString:self.content ? [NSString stringWithFormat:@"\tcontent : %@\n", self.content] : @""];
    [str appendString:self.demandId ? [NSString stringWithFormat:@"\tdemandId : %@\n", self.demandId] : @""];
    [str appendString:self.status ? [NSString stringWithFormat:@"\tstatus : %@\n", self.status] : @""];
    [str appendString:self.phone ? [NSString stringWithFormat:@"\tphone : %@\n", self.phone] : @""];
    [str appendString:self.reply ? [NSString stringWithFormat:@"\treply : "] : @""];
    
    for (ReplyModel *reply in self.reply) {
        [str appendString:reply ? [NSString stringWithFormat:@"%@", reply] : @""];
    }
    
    [str appendString:@"\n}"];
    
    return str;
}

@end
