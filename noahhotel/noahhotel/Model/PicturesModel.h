//
//  PicturesModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface PicturesModel : BaseModel

@property (nonatomic, copy) NSString *picture;
@property (nonatomic, copy) NSString *myDesc;

@property (nonatomic, copy) NSNumber *myId;
@property (nonatomic, copy) NSNumber *typeId;

@end
