//
//  NameValueModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "NameValueModel.h"

@implementation NameValueModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        self.value = [BaseModel stringObjectWithResponseObject:responseObject key:@"value"];
        
    }
    
    return self;
}

@end
