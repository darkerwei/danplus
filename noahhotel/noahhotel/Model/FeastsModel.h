//
//  FeastsModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"
#import "NameValueModel.h"

@interface FeastsModel : BaseModel

@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) NSArray *content;

@end
