//
//  AgreeModel.m
//  noahhotel
//
//  Created by darkerwei on 15/9/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AgreeModel.h"

@implementation AgreeModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.pattern = [BaseModel stringObjectWithResponseObject:responseObject key:@"pattern"];
        self.breakfast = [BaseModel stringObjectWithResponseObject:responseObject key:@"breakfast"];
        self.season = [BaseModel stringObjectWithResponseObject:responseObject key:@"season"];
        self.travelMonth = [BaseModel stringObjectWithResponseObject:responseObject key:@"travelMonth"];
        self.travelReply = [BaseModel numberObjectWithResponseObject:responseObject key:@"travelReply"];
        
        self.meetingOffer = [BaseModel numberObjectWithResponseObject:responseObject key:@"meetingOffer"];
        self.discount = [BaseModel numberObjectWithResponseObject:responseObject key:@"discount"];
        self.meetingReply = [BaseModel numberObjectWithResponseObject:responseObject key:@"meetingReply"];
        self.meetingAll = [BaseModel numberObjectWithResponseObject:responseObject key:@"meetingAll"];
        self.feastOffer = [BaseModel numberObjectWithResponseObject:responseObject key:@"feastOffer"];
        self.feastReply = [BaseModel numberObjectWithResponseObject:responseObject key:@"feastReply"];
        self.feastContain = [BaseModel stringObjectWithResponseObject:responseObject key:@"feastContain"];
        self.travelOffer = [BaseModel numberObjectWithResponseObject:responseObject key:@"travelOffer"];
        
    }
    
    return self;
}

@end
