//
//  MeetingsModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "MeetingsModel.h"

@implementation MeetingsModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.title = [BaseModel stringObjectWithResponseObject:responseObject key:@"title"];
        self.price = [BaseModel stringObjectWithResponseObject:responseObject key:@"price"];
        self.discount = [BaseModel stringObjectWithResponseObject:responseObject key:@"discount"];
        
        NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"content"];
        
        NSMutableArray *supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[NameValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        NameValueModel *nameValue = [[NameValueModel alloc] init];
        nameValue.name = @"门市价";
        nameValue.value = [NSString stringWithFormat:@"￥%@", self.price];
        [supplyList addObject:nameValue];
        
        NameValueModel *nameValue2 = [[NameValueModel alloc] init];
        nameValue2.name = @"折扣价";
        nameValue2.value = [NSString stringWithFormat:@"￥%@", self.discount];
        [supplyList addObject:nameValue2];
        
        self.content = [supplyList copy];
        
    }
    
    return self;
}

@end
