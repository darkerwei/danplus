//
//  AboutModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutModel.h"
#import "SupportingModel.h"

@implementation AboutModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.hotelName = [BaseModel stringObjectWithResponseObject:responseObject key:@"hotelName"];
        self.level = [BaseModel stringObjectWithResponseObject:responseObject key:@"level"];
        self.address = [BaseModel stringObjectWithResponseObject:responseObject key:@"address"];
        self.standardPrice = [BaseModel stringObjectWithResponseObject:responseObject key:@"standardPrice"];
        self.hotelServer = [BaseModel stringObjectWithResponseObject:responseObject key:@"hotelServer"];
        self.roomDescription = [BaseModel stringObjectWithResponseObject:responseObject key:@"roomDescription"];
        self.meetingDescription = [BaseModel stringObjectWithResponseObject:responseObject key:@"meetingDescription"];
        self.hotelDescription = [BaseModel stringObjectWithResponseObject:responseObject key:@"hotelDescription"];
        self.location = [BaseModel stringObjectWithResponseObject:responseObject key:@"location"];
        self.qq = [BaseModel stringObjectWithResponseObject:responseObject key:@"qq"];
        self.weixin = [BaseModel stringObjectWithResponseObject:responseObject key:@"weixin"];
        self.contact = [BaseModel stringObjectWithResponseObject:responseObject key:@"contact"];
        self.administrativeDivision = [BaseModel stringObjectWithResponseObject:responseObject key:@"administrativeDivision"];
        self.city = [BaseModel stringObjectWithResponseObject:responseObject key:@"city"];
        self.zone = [BaseModel stringObjectWithResponseObject:responseObject key:@"zone"];
        self.station = [BaseModel stringObjectWithResponseObject:responseObject key:@"station"];
        self.airport = [BaseModel stringObjectWithResponseObject:responseObject key:@"airport"];
        self.subway = [BaseModel stringObjectWithResponseObject:responseObject key:@"subway"];
        self.businessArea = [BaseModel stringObjectWithResponseObject:responseObject key:@"businessArea"];
        self.hotelPropery = [BaseModel stringObjectWithResponseObject:responseObject key:@"hotelPropery"];
        self.lastDecorate = [BaseModel numberObjectWithResponseObject:responseObject key:@"lastDecorate"];
        self.roomNumber = [BaseModel numberObjectWithResponseObject:responseObject key:@"roomNumber"];
        self.guestRoomNumber = [BaseModel numberObjectWithResponseObject:responseObject key:@"guestRoomNumber"];
        self.maxFeastPerson = [BaseModel numberObjectWithResponseObject:responseObject key:@"maxFeastPerson"];
        self.maxMeetingPerson = [BaseModel numberObjectWithResponseObject:responseObject key:@"maxMeetingPerson"];
        self.meetingRoomNumber = [BaseModel numberObjectWithResponseObject:responseObject key:@"meetingRoomNumber"];
        self.startBusinessTime = [BaseModel numberObjectWithResponseObject:responseObject key:@"startBusinessTime"];
        self.consociation = [BaseModel numberObjectWithResponseObject:responseObject key:@"consociation"];
        
        NSDictionary *dict = responseObject[@"pictures"];
        
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in dict[@"logo"]) {
            
            [arr addObject:[PicturesModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.logo = [arr copy];
        
        arr = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in dict[@"meeting"]) {
            
            [arr addObject:[PicturesModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.meeting = [arr copy];
        
        arr = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in dict[@"feast"]) {
            
            [arr addObject:[PicturesModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.feast = [arr copy];
        
        arr = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in dict[@"room"]) {
            
            [arr addObject:[PicturesModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.room = [arr copy];
        
        arr = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in dict[@"hotel"]) {
            
            [arr addObject:[PicturesModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.hotel = [arr copy];
        
        NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"feasts"];
        
        NSMutableArray *supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[FeastsModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.feasts = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"meetings"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[MeetingsModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.meetings = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rooms"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[MeetingsModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.rooms = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"innerSet"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[SupportingModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.innerSet = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"withoutSet"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[SupportingModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.withoutSet = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"aroundSet"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[SupportingModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.aroundSet = [supplyList copy];
        
    }
    
    return self;
}

@end
