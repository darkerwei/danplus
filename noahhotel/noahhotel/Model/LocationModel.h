//
//  LocationModel.h
//  noahhotel
//
//  Created by darkerwei on 15/9/14.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface LocationModel : BaseModel

/**
 *  酒店名称
 */
@property (nonatomic, copy) NSString *hotelName;

/**
 *  地址
 */
@property (nonatomic, copy) NSString *address;

/**
 *  酒店id
 */
@property (nonatomic, strong) NSNumber *hotelId;

/**
 *  经度
 */
@property (nonatomic, strong) NSNumber *longitude;

/**
 *  纬度
 */
@property (nonatomic, strong) NSNumber *latitude;

@end
