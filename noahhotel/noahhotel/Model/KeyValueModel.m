//
//  KeyValueModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/27.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "KeyValueModel.h"

@implementation KeyValueModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.key = [BaseModel numberObjectWithResponseObject:responseObject key:@"key"];
        self.value = [BaseModel stringObjectWithResponseObject:responseObject key:@"value"];
    }
    
    return self;
}

@end
