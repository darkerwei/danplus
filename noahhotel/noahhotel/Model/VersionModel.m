//
//  VersionModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "VersionModel.h"

/** 版本相关 */
#define CFBUNDLEVERSION @"CFBundleVersion"

@implementation VersionModel

+ (NSString *)currentVersion
{
    return [NSBundle mainBundle].infoDictionary[CFBUNDLEVERSION];
}

+ (void)saveCurrentVersion
{
    [[NSUserDefaults standardUserDefaults] setObject:[self currentVersion] forKey:CFBUNDLEVERSION];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isShowNewFeatures
{
    NSString *lastVersion = [[NSUserDefaults standardUserDefaults] objectForKey:CFBUNDLEVERSION];
    
    // 两次版本不同显示新特性
    return ![lastVersion isEqualToString:[self currentVersion]];
}

@end
