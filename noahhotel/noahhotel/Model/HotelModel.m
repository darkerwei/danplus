//
//  HotelModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/14.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "HotelModel.h"
#import "ValueTypeModel.h"
#import "JWNameValueModel.h"

@implementation HotelModel

- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.myId = [BaseModel numberObjectWithResponseObject:responseObject key:@"id"];
        self.hotelName = [BaseModel stringObjectWithResponseObject:responseObject key:@"hotelName"];
        self.address = [BaseModel stringObjectWithResponseObject:responseObject key:@"address"];
        self.picture = [BaseModel stringObjectWithResponseObject:responseObject key:@"picture"];
        self.latitude = [BaseModel numberObjectWithResponseObject:responseObject key:@"latitude"];
        self.longitude = [BaseModel numberObjectWithResponseObject:responseObject key:@"longitude"];
        self.consociation = [BaseModel stringObjectWithResponseObject:responseObject key:@"consociation"];
        self.level = [ValueTypeModel modelWithResponseObject:responseObject[@"level"]];
        
        NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"show"];
        
        NSMutableArray *supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[JWNameValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.showArray = [supplyList copy];
    }
    
    return self;
}

@end
