//
//  UserModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/11.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static UserModel *sharedInstance = nil;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

+ (instancetype)modelWithResponseObject:(NSDictionary *)responseObject
{
    return [[self sharedInstance] initWithResponseObject:responseObject];
}

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.username = [BaseModel stringObjectWithResponseObject:responseObject key:@"username"];
        self.userId = [BaseModel numberObjectWithResponseObject:responseObject key:@"userId"];
        self.myId = [BaseModel numberObjectWithResponseObject:responseObject key:@"id"];
        self.password = [BaseModel stringObjectWithResponseObject:responseObject key:@"password"];
        self.telephone = [BaseModel stringObjectWithResponseObject:responseObject key:@"telephone"];
        self.company = [BaseModel stringObjectWithResponseObject:responseObject key:@"company"];
        self.approve = [BaseModel numberObjectWithResponseObject:responseObject key:@"approve"];
        self.replyNum = [BaseModel numberObjectWithResponseObject:responseObject key:@"replyNum"];
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        self.oldPassword = [BaseModel stringObjectWithResponseObject:responseObject key:@"oldPassword"];
        self.myNewPassword = [BaseModel stringObjectWithResponseObject:responseObject key:@"myNewPassword"];
        self.companyName = [BaseModel stringObjectWithResponseObject:responseObject key:@"companyName"];
        self.trueName = [BaseModel stringObjectWithResponseObject:responseObject key:@"trueName"];
        self.idNumber = [BaseModel stringObjectWithResponseObject:responseObject key:@"idNumber"];
        self.telephone = [BaseModel stringObjectWithResponseObject:responseObject key:@"telephone"];
    }
    
    return self;
}

+ (void)saveUser
{
    UserModel *user = [self sharedInstance];
    
    [[NSUserDefaults standardUserDefaults] setObject:user.username forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setObject:user.password forKey:@"password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (UserModel *)getUser
{
    UserModel *user = [self sharedInstance];
    user.username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    user.password = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    
    return user;
}

+ (BOOL)isLogin
{
    [self getUser];
    if ([[self sharedInstance] username]) {
        if ([[self sharedInstance] password]) {
            return YES;
        }
    }
    return NO;
}

+ (void)deleteUser
{
    UserModel *user = [self sharedInstance];
    user.username = nil;
    user.password = nil;
    user.userId = nil;
    user.telephone = nil;
    user.company = nil;
    user.myId = nil;
    user.approve = nil;
    user.name = nil;
    user.replyNum = nil;
    user.oldPassword = nil;
    user.myNewPassword = nil;
    user.companyName = nil;
    user.trueName = nil;
    user.idNumber = nil;
    user.telephone = nil;
    user.code = nil;
}

- (NSString *)description
{
    NSMutableString *str = [[NSMutableString alloc] init];
    
    [str appendString:@"\n{\n"];
    
    [str appendString:self.username ? [NSString stringWithFormat:@"\tusername : %@\n", self.username] : @""];
    [str appendString:self.password ? [NSString stringWithFormat:@"\tpassword : %@\n", self.password] : @""];
    [str appendString:self.userId ? [NSString stringWithFormat:@"\tuserId : %@\n", self.userId] : @""];
    [str appendString:self.telephone ? [NSString stringWithFormat:@"\ttelephone : %@\n", self.telephone] : @""];
    [str appendString:self.company ? [NSString stringWithFormat:@"\tcompany : %@\n", self.company] : @""];
    [str appendString:self.myId ? [NSString stringWithFormat:@"\tid : %@\n", self.myId] : @""];
    [str appendString:self.approve ? [NSString stringWithFormat:@"\tapprove : %@\n", self.approve] : @""];
    [str appendString:self.name ? [NSString stringWithFormat:@"\tname : %@\n", self.name] : @""];
    [str appendString:self.replyNum ? [NSString stringWithFormat:@"\treplyNum : %@\n", self.replyNum] : @""];
    [str appendString:self.oldPassword ? [NSString stringWithFormat:@"\toldPassword : %@\n", self.oldPassword] : @""];
    [str appendString:self.myNewPassword ? [NSString stringWithFormat:@"\tnewPassword : %@\n", self.myNewPassword] : @""];
    [str appendString:self.companyName ? [NSString stringWithFormat:@"\tcompanyName : %@\n", self.companyName] : @""];
    [str appendString:self.trueName ? [NSString stringWithFormat:@"\ttrueName : %@\n", self.trueName] : @""];
    [str appendString:self.idNumber ? [NSString stringWithFormat:@"\tidNumber : %@\n", self.idNumber] : @""];
    [str appendString:self.telephone ? [NSString stringWithFormat:@"\ttelephone : %@\n", self.telephone] : @""];
    
    [str appendString:@"}"];
    
    return str;
}

@end
