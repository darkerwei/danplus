//
//  SupportingModel.h
//  noahhotel
//
//  Created by darkerwei on 15/9/29.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface SupportingModel : BaseModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *picture;

@end
