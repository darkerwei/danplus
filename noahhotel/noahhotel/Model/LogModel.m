//
//  LogModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "LogModel.h"

@implementation LogModel

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage
{
    NSString *logLevel = nil;
    
    switch (logMessage.flag) {
        case DDLogFlagError:
            logLevel = @"[Error]    >";
            break;
        case DDLogFlagWarning:
            logLevel = @"[Warning]  >";
            break;
        case DDLogFlagInfo:
            logLevel = @"[Info]     >";
            break;
        case DDLogFlagDebug:
            logLevel = @"[Debug]    >";
            break;
        case DDLogFlagVerbose:
            logLevel = @"[Verbose]  >";
            break;
    }
    
    return [NSString stringWithFormat:@"%@ [%@ %@][%lu]: %@", logLevel, logMessage.fileName, logMessage.function, (unsigned long)logMessage.line, logMessage.message];
}

+ (void)setupLog
{
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    [DDTTYLogger sharedInstance].logFormatter = [[self alloc] init];
    [DDTTYLogger sharedInstance].colorsEnabled = YES;

    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor whiteColor] backgroundColor:[UIColor redColor] forFlag:DDLogFlagError];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor blackColor] backgroundColor:[UIColor yellowColor] forFlag:DDLogFlagWarning];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor blackColor] backgroundColor:[UIColor greenColor] forFlag:DDLogFlagInfo];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor whiteColor] backgroundColor:[UIColor magentaColor] forFlag:DDLogFlagDebug];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor blackColor] backgroundColor:[UIColor cyanColor] forFlag:DDLogFlagVerbose];
}

@end
