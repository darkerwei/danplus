//
//  SupportingModel.m
//  noahhotel
//
//  Created by darkerwei on 15/9/29.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "SupportingModel.h"

@implementation SupportingModel

- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        self.picture = [BaseModel stringObjectWithResponseObject:responseObject key:@"picture"];
    }
    return self;
}

@end
