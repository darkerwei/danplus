//
//  HotelImageModel.m
//  noahhotel
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "HotelImageModel.h"

@implementation HotelImageModel

- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.webPicture = [BaseModel stringObjectWithResponseObject:responseObject key:@"webPicture"];
        self.phonePicture = [BaseModel stringObjectWithResponseObject:responseObject key:@"phonePicture"];
        self.type = [BaseModel numberObjectWithResponseObject:responseObject key:@"type"];
        self.linkId = [BaseModel numberObjectWithResponseObject:responseObject key:@"linkId"];
    }
    
    return self;
}

@end
