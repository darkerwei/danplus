//
//  HotelModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/14.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@class ValueTypeModel;

@interface HotelModel : BaseModel

/**
 *  酒店id
 */
@property (nonatomic, strong) NSNumber *myId;
/**
 *  酒店名称
 */
@property (nonatomic, strong) NSString *hotelName;
/**
 *  酒店地址
 */
@property (nonatomic, strong) NSString *address;
/**
 *  酒店图片
 */
@property (nonatomic, strong) NSString *picture;
/**
 *  酒店纬度
 */
@property (nonatomic, strong) NSNumber *latitude;
/**
 *  酒店经度
 */
@property (nonatomic, strong) NSNumber *longitude;
/**
 *  酒店合作关系
 */
@property (nonatomic, strong) NSString *consociation;
/**
 *  酒店星级
 */
@property (nonatomic, strong) ValueTypeModel *level;
/**
 *  附件
 */
@property (nonatomic, strong) NSArray *showArray;

@end
