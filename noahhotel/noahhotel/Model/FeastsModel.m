//
//  FeastsModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "FeastsModel.h"

@implementation FeastsModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.title = [BaseModel stringObjectWithResponseObject:responseObject key:@"title"];
        
        NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"content"];
        
        NSMutableArray *supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[NameValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.content = [supplyList copy];
        
        
        
    }
    
    return self;
}

@end
