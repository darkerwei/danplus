//
//  NameValueModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface NameValueModel : BaseModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *value;

@end
