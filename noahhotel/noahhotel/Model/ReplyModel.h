//
//  ReplyModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/21.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface ReplyModel : BaseModel

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) NSString *time;

@property (nonatomic, strong) NSNumber *replyId;

@property (nonatomic, strong) NSNumber *messageId;

@end
