//
//  UserModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/11.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface UserModel : BaseModel

/**
 *  用户名
 */
@property (nonatomic, copy) NSString *username;
/**
 *  密码
 */
@property (nonatomic, copy) NSString *oldPassword;
/**
 *  密码
 */
@property (nonatomic, copy) NSString *myNewPassword;
/**
 *  密码
 */
@property (nonatomic, copy) NSString *password;
/**
 *  登录id
 */
@property (nonatomic, strong) NSNumber *userId;
/**
 *  注册id
 */
@property (nonatomic, strong) NSNumber *myId;
/**
 *  手机号码
 */
@property (nonatomic, copy) NSString *telephone;
/**
 *  公司
 */
@property (nonatomic, copy) NSString *company;
/**
 *  用户是否实名认证 1是 0 不是
 */
@property (nonatomic, copy) NSNumber *approve;
/**
 *  用户昵称
 */
@property (nonatomic, copy) NSString *name;
/**
 *  未读信息数
 */
@property (nonatomic, copy) NSNumber *replyNum;
/**
 *  身份证
 */
@property (nonatomic, copy) NSString *idNumber;
/**
 *  真实姓名
 */
@property (nonatomic, copy) NSString *trueName;
/**
 *  公司名称
 */
@property (nonatomic, copy) NSString *companyName;
/**
 *  职务
 */
@property (nonatomic, copy) NSString *duty;

@property (nonatomic, copy) NSString *code;

/**
 *  创建用户
 *
 *  @return 用户(单例)
 */
+ (instancetype)sharedInstance;

/**
 *  保存用户
 */
+ (void)saveUser;
/**
 *  获取用户
 *
 *  @return 用户
 */
+ (UserModel *)getUser;
/**
 *  判断用户是否登录
 *
 *  @return 是否登录
 */
+ (BOOL)isLogin;

+ (void)deleteUser;

@end
