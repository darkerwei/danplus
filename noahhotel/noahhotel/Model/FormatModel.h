//
//  FormatModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/12.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface FormatModel : BaseModel

/**
 *  判断用户名格式是否有效
 *
 *  @param username 用户名
 *
 *  @return 用户名格式是否有效
 */
+ (BOOL)isValidFormatWithTelephone:(NSString *)telephone;

+ (BOOL)isValidFormatWithName:(NSString *)name;

/**
 *  判断密码格式是否有效
 *
 *  @param password 密码
 *
 *  @return 密码格式是否有效
 */
+ (BOOL)isValidFormatWithPassword:(NSString *)password;

/**
 *  判断公司名称格式是否有效
 *
 *  @param company 公司名称
 *
 *  @return 公司名称格式是否有效
 */
+ (BOOL)isValidFormatWithCompany:(NSString *)company;

+ (BOOL)isValidFormatWithVerificationCode:(NSString *)VerificationCode;

@end
