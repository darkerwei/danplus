//
//  SupplyValueModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SupplyValueModel.h"
#import "ProductModel.h"

@implementation SupplyValueModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        self.title = [BaseModel stringObjectWithResponseObject:responseObject key:@"title"];
        self.companyDescription = [BaseModel stringObjectWithResponseObject:responseObject key:@"companyDescription"];
        self.productDescription = [BaseModel stringObjectWithResponseObject:responseObject key:@"productDescription"];
        self.contact = [BaseModel stringObjectWithResponseObject:responseObject key:@"contact"];
        self.myID = [BaseModel numberObjectWithResponseObject:responseObject key:@"id"];
        self.picture = [BaseModel stringObjectWithResponseObject:responseObject key:@"picture"];
        
        NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"product"];
        
        NSMutableArray *supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            ProductModel *reply = [ProductModel modelWithResponseObject:responseObjectDict];
            
            [supplyList addObject:reply];
        }
        
        self.product = [supplyList copy];
    }
    
    return self;
}

- (NSString *)description
{
    NSMutableString *str = [[NSMutableString alloc] init];
    
    [str appendString:@"\n\t\t{\n"];
    
    [str appendString:self.name ? [NSString stringWithFormat:@"\t\t\t\tname : %@\n", self.name] : @""];
    [str appendString:self.title ? [NSString stringWithFormat:@"\t\t\t\ttitle : %@\n", self.title] : @""];
    [str appendString:self.companyDescription ? [NSString stringWithFormat:@"\t\t\t\tcompanyDescription : %@\n", self.companyDescription] : @""];
    [str appendString:self.productDescription ? [NSString stringWithFormat:@"\t\t\t\tproductDescription : %@\n", self.productDescription] : @""];
    [str appendString:self.contact ? [NSString stringWithFormat:@"\t\t\t\tcontact : %@\n", self.contact] : @""];
    [str appendString:self.myID ? [NSString stringWithFormat:@"\t\t\t\tid : %@\n", self.myID] : @""];
    
    for (ProductModel *product in self.product) {
        [str appendString:product ? [NSString stringWithFormat:@"%@", product] : @""];
    }
    
    [str appendString:@"\n\t\t}"];
    
    return str;
}

@end
