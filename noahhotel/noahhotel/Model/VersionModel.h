//
//  VersionModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VersionModel : NSObject

/**
 *  获取当前版本
 *
 *  @return 当前版本
 */
+ (NSString *)currentVersion;

/**
 *  保存当前版本
 */
+ (void)saveCurrentVersion;

/**
 *  判断是否显示新特性
 *
 *  @return 是否显示新特性
 */
+ (BOOL)isShowNewFeatures;

@end
