//
//  DemandModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/20.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface DemandModel : BaseModel

/**
 *  需求名
 */
@property (nonatomic, copy) NSString *name;
/**
 *  时间
 */
@property (nonatomic, copy) NSString *time;
/**
 *  id
 */
@property (nonatomic, strong) NSNumber *myId;
/**
 *  id
 */
@property (nonatomic, strong) NSNumber *demandId;
/**
 *  id
 */
@property (nonatomic, strong) NSNumber *status;
/**
 *  内容
 */
@property (nonatomic, copy) NSString *content;
/**
 *  电话
 */
@property (nonatomic, copy) NSString *phone;
/**
 *  回复
 */
@property (nonatomic, strong) NSArray *reply;


@end
