//
//  AgreeModel.h
//  noahhotel
//
//  Created by darkerwei on 15/9/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface AgreeModel : BaseModel

@property (nonatomic, strong) NSString *pattern;


@property (nonatomic, strong) NSString *breakfast;
@property (nonatomic, strong) NSString *season;
@property (nonatomic, strong) NSString *travelMonth;
@property (nonatomic, strong) NSNumber *travelReply;

@property (nonatomic, strong) NSNumber *meetingOffer;
@property (nonatomic, strong) NSNumber *discount;
@property (nonatomic, strong) NSNumber *meetingReply;
@property (nonatomic, strong) NSNumber *meetingAll;
@property (nonatomic, strong) NSNumber *feastOffer;
@property (nonatomic, strong) NSNumber *feastReply;
@property (nonatomic, strong) NSString *feastContain;
@property (nonatomic, strong) NSNumber *travelOffer;

@end
