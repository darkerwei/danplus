//
//  HotelImageModel.h
//  noahhotel
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface HotelImageModel : BaseModel

/**
 *  网页图片
 */
@property (nonatomic, strong) NSString *webPicture;
/**
 *  手机图片
 */
@property (nonatomic, strong) NSString *phonePicture;
/**
 *  图片类型
 */
@property (nonatomic, strong) NSNumber *type;
/**
 *  图片链接
 */
@property (nonatomic, strong) NSNumber *linkId;

@end
