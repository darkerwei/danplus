//
//  ProductModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "ProductModel.h"

@implementation ProductModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.myDescription = [BaseModel stringObjectWithResponseObject:responseObject key:@"description"];
        self.picture = [BaseModel stringObjectWithResponseObject:responseObject key:@"picture"];
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        self.price = [BaseModel numberObjectWithResponseObject:responseObject key:@"price"];
    }
    
    return self;
}

- (NSString *)description
{
    NSMutableString *str = [[NSMutableString alloc] init];
    
    [str appendString:@"\n\t\t\t\t{\n"];
    
    [str appendString:self.myDescription ? [NSString stringWithFormat:@"\t\t\t\t\t\tdescription : %@\n", self.myDescription] : @""];
    [str appendString:self.picture ? [NSString stringWithFormat:@"\t\t\t\t\t\tpicture : %@\n", self.picture] : @""];
    
    [str appendString:@"\t\t\t\t}"];
    
    return str;
}

@end
