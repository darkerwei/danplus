//
//  StoryboardModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "StoryboardModel.h"

@implementation StoryboardModel

+ (id)viewControllerWithStoryboardName:(NSString *)storyboardName storyboardID:(NSString *)storyboardID;
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:[NSBundle mainBundle]];
    return [storyboard instantiateViewControllerWithIdentifier:storyboardID];
}

@end
