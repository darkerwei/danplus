//
//  LocationModel.m
//  noahhotel
//
//  Created by darkerwei on 15/9/14.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "LocationModel.h"

@implementation LocationModel

- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.hotelName = [BaseModel stringObjectWithResponseObject:responseObject key:@"hotelName"];
        self.address = [BaseModel stringObjectWithResponseObject:responseObject key:@"address"];
        self.hotelId = [BaseModel numberObjectWithResponseObject:responseObject key:@"hotelId"];
        self.longitude = [BaseModel numberObjectWithResponseObject:responseObject key:@"longitude"];
        self.latitude = [BaseModel numberObjectWithResponseObject:responseObject key:@"latitude"];
    }
    
    return self;
}

@end
