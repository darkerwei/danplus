//
//  HotelInterfaceModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/27.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "HotelInterfaceModel.h"
#import "KeyValueModel.h"

@implementation HotelInterfaceModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        
        NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"maxMeetingPerson"];
        
        NSMutableArray *supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.maxMeetingPerson = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"meetingNumber"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.meetingNumber = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"meetingSize"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.meetingSize = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"meetingPrice"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.meetingPrice = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"feastPrice"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.feastPrice = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"feastTable"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.feastTable = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"feastPlace"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.feastPlace = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"feastStyle"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.feastStyle = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"station"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.station = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"airport"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.airport = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"division"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.division = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"city"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.city = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"aroundSet"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.aroundSet = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"businessArea"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.businessArea = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"hotelLevel"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.hotelLevel = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"hotelPropery"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.hotelPropery = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"innerSet"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.innerSet = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"subway"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.subway = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"withoutSet"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.withoutSet = [supplyList copy];
        
        responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"zone"];
        
        supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            
            [supplyList addObject:[KeyValueModel modelWithResponseObject:responseObjectDict]];
        }
        
        self.zone = [supplyList copy];
    }
    
    return self;
}

@end
