//
//  ValueTypeModel.m
//  noahhotel
//
//  Created by darkerwei on 15/9/21.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "ValueTypeModel.h"

@implementation ValueTypeModel

- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.value = [BaseModel stringObjectWithResponseObject:responseObject key:@"value"];
        self.type = [BaseModel numberObjectWithResponseObject:responseObject key:@"type"];
    }
    return self;
}

@end
