//
//  StoryboardModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/6.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface StoryboardModel : NSObject

/**
 *  从storyboard获取控制器
 *
 *  @param storyboardName storyboard文件名
 *  @param storyboardID   控制器storyboardID
 *
 *  @return 控制器
 */
+ (id)viewControllerWithStoryboardName:(NSString *)storyboardName storyboardID:(NSString *)storyboardID;

@end
