//
//  MeetingsModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"
#import "NameValueModel.h"

@interface MeetingsModel : BaseModel

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *discount;

@property (nonatomic, strong) NSArray *content;

@end
