//
//  SupplyModel.m
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "SupplyModel.h"
#import "SupplyValueModel.h"

@implementation SupplyModel

/**
 *  字典转模型
 *
 *  @param responseObject 返回参数
 *
 *  @return 模型
 */
- (instancetype)initWithResponseObject:(NSDictionary *)responseObject
{
    self = [super init];
    if (self) {
        self.name = [BaseModel stringObjectWithResponseObject:responseObject key:@"name"];
        
        NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"value"];
        
        NSMutableArray *supplyList = [[NSMutableArray alloc] init];
        for (NSDictionary *responseObjectDict in responseObjectList) {
            SupplyValueModel *reply = [SupplyValueModel modelWithResponseObject:responseObjectDict];
            
            [supplyList addObject:reply];
        }
        
        self.value = [supplyList copy];
    }
    
    return self;
}

- (NSString *)description
{
    NSMutableString *str = [[NSMutableString alloc] init];
    
    [str appendString:@"\n{\n"];
    
    [str appendString:self.name ? [NSString stringWithFormat:@"\tname : %@\n", self.name] : @""];
    
    for (SupplyValueModel *supplyValue in self.value) {
        [str appendString:supplyValue ? [NSString stringWithFormat:@"%@", supplyValue] : @""];
    }
    
    [str appendString:@"\n}"];
    
    return str;
}

@end
