//
//  LogModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogModel : NSObject <DDLogFormatter>

/**
 *  设置日志
 */
+ (void)setupLog;

@end
