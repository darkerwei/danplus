//
//  SupplyValueModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/24.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface SupplyValueModel : BaseModel

/**
 *  分类名称
 */
@property (nonatomic, strong) NSNumber *myID;
/**
 *  分类名称
 */
@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *companyDescription;

@property (nonatomic, copy) NSString *productDescription;

@property (nonatomic, copy) NSString *contact;

@property (nonatomic, strong) NSArray *product;

@property (nonatomic, copy) NSString *picture;

@end
