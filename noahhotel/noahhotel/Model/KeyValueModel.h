//
//  KeyValueModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/27.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface KeyValueModel : BaseModel

@property (nonatomic, strong) NSNumber *key;
@property (nonatomic, copy) NSString *value;

@end
