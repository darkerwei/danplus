//
//  IdentityModel.h
//  noahhotel
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface IdentityModel : BaseModel

@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, copy) NSString *duty;
@property (nonatomic, copy) NSString *idNumber;
@property (nonatomic, copy) NSString *telephone;
@property (nonatomic, copy) NSString *trueName;

@end
