//
//  SelectModel.h
//  noahhotel
//
//  Created by darkerwei on 15/9/7.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface SelectModel : BaseModel

@property (nonatomic, copy) NSString *maxMeetingPerson;
@property (nonatomic, copy) NSString *meetingNumber;
@property (nonatomic, copy) NSString *meetingSize;
@property (nonatomic, copy) NSString *meetingPrice;
@property (nonatomic, copy) NSString *feastPrice;
@property (nonatomic, copy) NSString *feastTable;
@property (nonatomic, copy) NSString *feastPlace;
@property (nonatomic, copy) NSString *feastStyle;
@property (nonatomic, copy) NSString *station;
@property (nonatomic, copy) NSString *airport;
@property (nonatomic, copy) NSString *division;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *businessArea;
@property (nonatomic, copy) NSString *hotelLevel;
@property (nonatomic, copy) NSString *hotelPropery;
@property (nonatomic, copy) NSString *subway;
@property (nonatomic, copy) NSString *zone;
@property (nonatomic, copy) NSString *hotelName;

@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;

@end
