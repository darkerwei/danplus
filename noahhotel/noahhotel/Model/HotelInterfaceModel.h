//
//  HotelInterfaceModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/27.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface HotelInterfaceModel : BaseModel

@property (nonatomic, strong) NSArray *maxMeetingPerson;
@property (nonatomic, strong) NSArray *meetingNumber;
@property (nonatomic, strong) NSArray *meetingSize;
@property (nonatomic, strong) NSArray *meetingPrice;
@property (nonatomic, strong) NSArray *feastPrice;
@property (nonatomic, strong) NSArray *feastTable;
@property (nonatomic, strong) NSArray *feastPlace;
@property (nonatomic, strong) NSArray *feastStyle;
@property (nonatomic, strong) NSArray *station;
@property (nonatomic, strong) NSArray *airport;
@property (nonatomic, strong) NSArray *division;
@property (nonatomic, strong) NSArray *city;
@property (nonatomic, strong) NSArray *aroundSet;
@property (nonatomic, strong) NSArray *businessArea;
@property (nonatomic, strong) NSArray *hotelLevel;
@property (nonatomic, strong) NSArray *hotelPropery;
@property (nonatomic, strong) NSArray *innerSet;
@property (nonatomic, strong) NSArray *subway;
@property (nonatomic, strong) NSArray *withoutSet;
@property (nonatomic, strong) NSArray *zone;

@property (nonatomic, strong) NSNumber *total;

@end
