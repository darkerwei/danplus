//
//  ValueTypeModel.h
//  noahhotel
//
//  Created by darkerwei on 15/9/21.
//  Copyright © 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"

@interface ValueTypeModel : BaseModel

/**
 *  酒店星级
 */
@property (nonatomic, strong) NSString *value;
/**
 *  酒店星级类型
 */
@property (nonatomic, strong) NSNumber *type;

@end
