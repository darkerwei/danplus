//
//  AboutModel.h
//  noahhotel
//
//  Created by darkerwei on 15/8/31.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "BaseModel.h"
#import "PicturesModel.h"
#import "FeastsModel.h"
#import "MeetingsModel.h"

@interface AboutModel : BaseModel












@property (nonatomic, copy) NSString *hotelName;
@property (nonatomic, copy) NSString *level;
@property (nonatomic, strong) NSNumber *lastDecorate;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, strong) NSNumber *roomNumber;
@property (nonatomic, strong) NSNumber *guestRoomNumber;
@property (nonatomic, strong) NSNumber *maxFeastPerson;
@property (nonatomic, strong) NSNumber *maxMeetingPerson;
@property (nonatomic, strong) NSNumber *meetingRoomNumber;
@property (nonatomic, strong) NSNumber *startBusinessTime;
@property (nonatomic, copy) NSString *standardPrice;
@property (nonatomic, copy) NSString *hotelServer;
@property (nonatomic, copy) NSString *roomDescription;
@property (nonatomic, copy) NSString *meetingDescription;
@property (nonatomic, copy) NSString *hotelDescription;
@property (nonatomic, strong) NSNumber *consociation;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *qq;
@property (nonatomic, copy) NSString *weixin;
@property (nonatomic, copy) NSString *contact;
@property (nonatomic, copy) NSString *administrativeDivision;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *zone;
@property (nonatomic, copy) NSString *station;
@property (nonatomic, copy) NSString *airport;
@property (nonatomic, copy) NSString *subway;
@property (nonatomic, copy) NSString *businessArea;
@property (nonatomic, copy) NSString *hotelPropery;

@property (nonatomic, strong) NSArray *innerSet;
@property (nonatomic, strong) NSArray *withoutSet;
@property (nonatomic, strong) NSArray *aroundSet;

@property (nonatomic, strong) NSArray *logo;
@property (nonatomic, strong) NSArray *meeting;
@property (nonatomic, strong) NSArray *feast;
@property (nonatomic, strong) NSArray *room;
@property (nonatomic, strong) NSArray *hotel;

@property (nonatomic, strong) NSArray *rooms;
@property (nonatomic, strong) NSArray *feasts;
@property (nonatomic, strong) NSArray *meetings;

@end
