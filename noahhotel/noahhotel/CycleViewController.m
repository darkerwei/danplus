#import "CycleViewController.h"
#import "QFQBannerView.h"

@interface CycleViewController() <QFQBannerViewDelegate>

@property (nonatomic, weak) QFQBannerView *banner;


@end

@implementation CycleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    QFQBannerView *banner = [[QFQBannerView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 100)];
    [self.view addSubview:banner];
    self.banner = banner;
    self.banner.delegate = self;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSArray *array = @[
                           @{@"imgUrl": @"http://img04.tooopen.com/images/20130701/tooopen_20083555.jpg"},
                           @{@"imgUrl": @"http://img04.tooopen.com/images/20130630/tooopen_10444704.jpg"},
                           
                           ];
        NSMutableArray *mArray = [NSMutableArray array];
        for (NSDictionary *dict in array) {
            QFQHomeCollecionModel *model = [QFQHomeCollecionModel new];
            model.imgUrl = dict[@"imgUrl"];
            [mArray addObject:model];
        }
        
        banner.data = mArray;
    });
    
    
}

@end
