//
//  AboutFourTableViewCell.m
//  noahhotel
//
//  Created by darkerwei on 15/9/1.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "AboutFourTableViewCell.h"

@interface AboutFourTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *name1;
@property (nonatomic, weak) IBOutlet UILabel *name2;

@end

@implementation AboutFourTableViewCell

- (void)setAbout:(AboutModel *)about
{
    _about = about;
    
    NSMutableString *str = [[NSMutableString alloc] init];
    for (NSString *strTemp in self.about.innerSet) {
        [str appendString:[NSString stringWithFormat:@"%@  ", strTemp]];
    }
    
    NSMutableString *str2 = [[NSMutableString alloc] init];
    for (NSString *strTemp in self.about.withoutSet) {
        [str2 appendString:[NSString stringWithFormat:@"%@  ", strTemp]];
    }
    
    self.name1.text = [NSString stringWithFormat:@"%@", str];
    self.name2.text = [NSString stringWithFormat:@"%@", str2];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"AboutFourTableViewCell";
    
    [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
    AboutFourTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    return cell;
}

@end
