//
//  Networkservice.m
//  noahhotel
//
//  Created by darkerwei on 15/8/11.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "Networkservice.h"
#import "HttpClient.h"
#import "UserModel.h"
#import "HotelModel.h"
#import "DemandModel.h"
#import "ReplyModel.h"
#import "SupplyModel.h"
#import "SupplyValueModel.h"
#import "HotelInterfaceModel.h"
#import "AboutModel.h"
#import "AgreeModel.h"
#import "SelectModel.h"
#import "IdentityModel.h"
#import "HotelImageModel.h"
#import "LocationModel.h"

@interface Networkservice()

/**
 *  网络请求客户端
 */
@property (nonatomic, strong) HttpClient *httpClient;

@end

@implementation Networkservice

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static Networkservice *sharedInstance = nil;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

/**
 *  判断网络请求是否成功
 *
 *  @param responseObject 返回参数
 *
 *  @return 网络请求是否成功
 */
- (BOOL)isRequestSuccessWithResponseObject:(NSDictionary *)responseObject
{
    return [[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] isEqualToNumber:@(0)];
}

/**
 *  获取请求URL
 *
 *  @param service 请求服务
 *  @param method  请求方法
 *
 *  @return 请求URL
 */
+ (NSString *)URLStringWithService:(NSString *)service method:(NSString *)method
{
    return [NSString stringWithFormat:@"http://115.28.94.253:8098/api/%@/%@", service, method];
}

- (void)loginWithUser:(UserModel *)user
              success:(void (^)(UserModel *user))success
              failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"login"];
    
    NSDictionary *parameters = @{ @"username" : user.username,
                                  @"password" : user.password };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                

                NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"channelId"];
                
                if (str) {
                    [[Networkservice sharedInstance] boundWithchannell:str success:^{
                        
                        NSLog(@"绑定成功");
                        
                    } failure:^(NSError *error) {
                        
                    }];
                }
                
                [UserModel saveUser];
                
                success([UserModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"login request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)forgetWithUser:(UserModel *)user
               success:(void (^)(UserModel *user))success
               failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"forget"];
    
    NSDictionary *parameters = @{
                                  @"password" : user.password,
                                  @"telephone" : user.telephone,
                                  @"code" : user.code};
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                [UserModel saveUser];
                
                success([UserModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"create request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)createWithUser:(UserModel *)user
               success:(void (^)(UserModel *user))success
               failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"create"];
    
    NSDictionary *parameters = @{ @"username" : user.username,
                                  @"password" : user.password,
                                  @"telephone" : user.telephone,
                                  @"company" : user.company,
                                  @"name" : user.name,
                                  @"code" : user.code};
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                [UserModel saveUser];
                
                success([UserModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"create request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)indexWithPage:(NSString *)page
                 rows:(NSString *)rows
              success:(void (^)(NSArray *hotelList))success
              failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"hotel" method:@"index"];
    
    NSDictionary *parameters = @{ @"page" : page,
                                  @"rows" : rows };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *hotelList = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    HotelModel *hotel = [HotelModel modelWithResponseObject:responseObjectDict];
                    
                    [hotelList addObject:hotel];
                }
                
                success([hotelList copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)indexWithPage:(NSString *)page
                 rows:(NSString *)rows
               select:(SelectModel *)select
              success:(void (^)(NSArray *hotelList))success
              failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"hotel" method:@"index"];
    
    NSDictionary *parameters = @{ @"page" : page,
                                  @"rows" : rows,
                                  @"maxMeetingPerson" : select.maxMeetingPerson,
                                  @"meetingNumber" : select.meetingNumber,
                                  @"meetingSize" : select.meetingSize,
                                  @"meetingPrice" : select.meetingPrice,
                                  @"feastPrice" : select.feastPrice,
                                  @"feastTable" : select.feastTable,
                                  @"feastPlace" : select.feastPlace,
                                  @"feastStyle" : select.feastStyle,
                                  @"station" : select.station,
                                  @"airport" : select.airport,
                                  @"division" : select.division,
                                  @"city" : select.city,
                                  @"businessArea" : select.businessArea,
                                  @"hotelLevel" : select.hotelLevel,
                                  @"hotelPropery" : select.hotelPropery,
                                  @"subway" : select.subway,
                                  @"zone" : select.zone,
                                  @"hotelName" : select.hotelName,
                                  @"latitude" : select.latitude,
                                  @"longitude" : select.longitude
                                  };
    
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *hotelList = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    HotelModel *hotel = [HotelModel modelWithResponseObject:responseObjectDict];
                    
                    [hotelList addObject:hotel];
                }
                
                success([hotelList copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}


- (void)getDetailWithsuccess:(void (^)(UserModel *user))success
                     failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"detail"];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([UserModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];

}

- (void)pwdUpdateWithUser:(UserModel *)user
                  success:(void (^)(UserModel *user))success
                  failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"pwdupdate"];
    
    NSDictionary *parameters = @{ @"oldPassword" : user.oldPassword,
                                  @"newPassword" : user.myNewPassword };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([UserModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"create request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)checkWithUser:(UserModel *)user
              success:(void (^)(UserModel *user))success
              failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"check"];
    
    NSDictionary *parameters = @{
                                    @"companyName" : user.companyName,
                                    @"trueName" : user.trueName,
                                    @"duty" : user.duty,
                                    @"idNumber" : user.idNumber,
                                    @"telephone" : user.telephone
                                };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([UserModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"create request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)updateWithUser:(UserModel *)user
               success:(void (^)(UserModel *user))success
               failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"update"];
    
    NSDictionary *parameters = @{
                                    @"username" : user.username
                                 };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([UserModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"create request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)suggestWithContent:(NSString *)content
                   contact:(NSString *)contact
                   success:(void (^)(NSNumber *suggestID))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"suggest"];
    
    NSDictionary *parameters = @{
                                 @"content" : content,
                                 @"contact" : contact
                                 };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([BaseModel numberObjectWithResponseObject:responseObject key:@"suggestId"]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"create request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)myListWithWithPage:(NSString *)page
                      rows:(NSString *)rows
                   success:(void (^)(NSArray *demandArray))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"demand" method:@"mylist"];
    
    NSDictionary *parameters = @{ @"page" : page,
                                  @"rows" : rows };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *demandList = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    DemandModel *hotel = [DemandModel modelWithResponseObject:responseObjectDict];
                    
                    [demandList addObject:hotel];
                }
                
                success([demandList copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
    
}

- (void)searchWithPage:(NSString *)page
                  rows:(NSString *)rows
             startTime:(NSString *)startTime
               endTime:(NSString *)endTime
                 title:(NSString *)title
               success:(void (^)(NSArray *demandArray))success
               failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"demand" method:@"search"];
    
    NSDictionary *parameters = @{ @"page" : page,
                                  @"rows" : rows,
                                  @"startTime" : startTime,
                                  @"endTime" : endTime,
                                  @"title" : title };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *demandList = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    DemandModel *hotel = [DemandModel modelWithResponseObject:responseObjectDict];
                    
                    [demandList addObject:hotel];
                }
                
                success([demandList copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)contentWithId:(NSNumber *)myId
              success:(void (^)(DemandModel *demand))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"demand" method:[NSString stringWithFormat:@"content/%@", myId]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([DemandModel modelWithResponseObject:responseObject[@"rows"]]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
    
}

- (void)agreeWithId:(NSNumber *)myId
            success:(void (^)(AgreeModel *agree))success
            failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"hotel" method:[NSString stringWithFormat:@"consociationdetail/%@", myId]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([AgreeModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
    
}

- (void)replyWithId:(NSNumber *)myId
              Reply:(ReplyModel *)reply
            success:(void (^)(ReplyModel *reply))success
            failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"demand" method:@"reply"];
    
    NSDictionary *parameters = @{ @"demandId" : myId,
                                  @"content" : reply.content,
                                  @"phone" : reply.phone };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([ReplyModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"login request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)hotelReplyWithId:(NSNumber *)myId
              Reply:(ReplyModel *)reply
            success:(void (^)(ReplyModel *reply))success
            failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"hotel" method:@"reply"];
    
    NSDictionary *parameters = @{ @"hotelId" : myId,
                                  @"content" : reply.content,
                                  @"contact" : reply.phone };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([ReplyModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"login request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)sendWithDemand:(DemandModel *)demand
               success:(void (^)(DemandModel *demand))success
               failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"demand" method:@"send"];
    
    NSDictionary *parameters = @{ @"name" : demand.name,
                                  @"content" : demand.content,
                                  @"phone" : demand.phone };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([DemandModel modelWithResponseObject:responseObject]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"login request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)logoutWithsuccess:(void (^)(void))success
                  failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:[NSString stringWithFormat:@"logout"]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success();
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)getSortWithsuccess:(void (^)(NSArray *supplyArray))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"supply" method:[NSString stringWithFormat:@"getsort"]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *supplyArray = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    SupplyModel *hotel = [SupplyModel modelWithResponseObject:responseObjectDict];
                    
                    [supplyArray addObject:hotel];
                }
                
                success([supplyArray copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)searchWithId:(NSNumber *)myId
                name:(NSString *)name
                rows:(NSString *)rows
                page:(NSString *)page
             success:(void (^)(NSArray *supplyValueArray))success
             failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"supply" method:@"search"];
    
    NSDictionary *parameters = @{ @"sortId" : myId,
                                  @"name" :  name,
                                  @"rows" : rows,
                                  @"page" : page};
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *demandList = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    SupplyValueModel *hotel = [SupplyValueModel modelWithResponseObject:responseObjectDict];
                    
                    [demandList addObject:hotel];
                }
                
                success([demandList copy]);
                
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"login request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)supplyDetailWithId:(NSNumber *)myId
                   success:(void (^)(SupplyValueModel *supplyValue))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"supply" method:[NSString stringWithFormat:@"detail/%@", myId]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([SupplyValueModel modelWithResponseObject:responseObject[@"rows"]]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)aboutWithId:(NSNumber *)myId
                   success:(void (^)(AboutModel *about))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"hotel" method:[NSString stringWithFormat:@"detail/%@", myId]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([AboutModel modelWithResponseObject:responseObject[@"rows"]]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)hotelInterfaceWithsuccess:(void (^)(HotelInterfaceModel *hotel))success
                          failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"hotel" method:[NSString stringWithFormat:@"interface"]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                HotelInterfaceModel *hotel = [HotelInterfaceModel modelWithResponseObject:responseObject[@"rows"]];
                hotel.total = [BaseModel numberObjectWithResponseObject:responseObject key:@"total"];
                
                success(hotel);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)getIdentityWithsuccess:(void (^)(IdentityModel *identity))success
                       failure:(void (^)(NSError *error))failure

{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:[NSString stringWithFormat:@"identity"]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success([IdentityModel modelWithResponseObject:responseObject]);
                
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)getCodeWithTel:(NSString *)tel
    success:(void (^)(NSString *code))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:[NSString stringWithFormat:@"getcode"]];
    
    NSDictionary *parameters = @{ @"telephone" : tel };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success(responseObject[@"code"]);
                
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)getHotelImagesuccess:(void (^)(NSArray *hotelImageArray))success
                     failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"ad" method:[NSString stringWithFormat:@"hotel"]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *supplyArray = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    HotelImageModel *hotel = [HotelImageModel modelWithResponseObject:responseObjectDict];
                    
                    [supplyArray addObject:hotel];
                }
                
                success([supplyArray copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

/**
 *  获取需求发布广告图1.28
 *
 */
- (void)getDemandImagesuccess:(void (^)(NSArray *hotelImageArray))success
                      failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"ad" method:[NSString stringWithFormat:@"demand"]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *supplyArray = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    HotelImageModel *hotel = [HotelImageModel modelWithResponseObject:responseObjectDict];
                    
                    [supplyArray addObject:hotel];
                }
                
                success([supplyArray copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

/**
 *  获取供应商广告图1.29
 *
 */
- (void)getSupplyImagesuccess:(void (^)(NSArray *hotelImageArray))success
                      failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"ad" method:[NSString stringWithFormat:@"supply"]];
    
    NSDictionary *parameters = nil;
    
    [self.httpClient requestWithMethod:@"GET" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *supplyArray = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    HotelImageModel *hotel = [HotelImageModel modelWithResponseObject:responseObjectDict];
                    
                    [supplyArray addObject:hotel];
                }
                
                success([supplyArray copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)locationsearchWithName:(NSString *)name
                       success:(void (^)(NSArray *locationArray))success
                       failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"hotel" method:[NSString stringWithFormat:@"locationsearch"]];
    
    NSDictionary *parameters = @{ @"name" : name };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *supplyArray = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    LocationModel *hotel = [LocationModel modelWithResponseObject:responseObjectDict];
                    
                    [supplyArray addObject:hotel];
                }
                
                success([supplyArray copy]);
                
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)boundWithchannell:(NSString *)channell
                  success:(void (^)(void))success
                  failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"bound"];
    
    NSDictionary *parameters = @{ @"channelId" : channell,
                                  @"type" : @"2" };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                success();
                
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"index request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
}

- (void)myMessageWithWithPage:(NSString *)page
                         rows:(NSString *)rows
                      success:(void (^)(NSArray *demandArray))success
                      failure:(void (^)(NSError *error))failure
{
    NSString *URLString = [Networkservice URLStringWithService:@"user" method:@"mymessage"];
    
    NSDictionary *parameters = @{ @"page" : page,
                                  @"rows" : rows };
    
    [self.httpClient requestWithMethod:@"POST" URLString:URLString parameters:parameters success:^(NSDictionary *responseObject) {
        if ([self isRequestSuccessWithResponseObject:responseObject]) {
            if (success) {
                
                NSArray *responseObjectList = [BaseModel arrayObjectWithResponseObject:responseObject key:@"rows"];
                
                NSMutableArray *demandList = [[NSMutableArray alloc] init];
                for (NSDictionary *responseObjectDict in responseObjectList) {
                    DemandModel *hotel = [DemandModel modelWithResponseObject:responseObjectDict];
                    
                    [demandList addObject:hotel];
                }
                
                success([demandList copy]);
            } else {
                DDLogError(@"success not found");
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"request error" code:[[BaseModel numberObjectWithResponseObject:responseObject key:@"errno"] integerValue] userInfo:@{ @"msg" : [BaseModel stringObjectWithResponseObject:responseObject key:@"errmsg"] }];
                
                failure(error);
            } else {
                DDLogError(@"failure not found");
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
    
}

#pragma mark - 懒加载

- (HttpClient *)httpClient
{
    if (!_httpClient) {
        _httpClient = [[HttpClient alloc] init];
    }
    
    return _httpClient;
}

@end
