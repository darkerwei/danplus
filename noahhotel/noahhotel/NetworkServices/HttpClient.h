//
//  HttpClient.h
//  noahhotel
//
//  Created by darkerwei on 15/8/11.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpClient : NSObject

/**
 *  发送网络请求
 *
 *  @param method     请求类型: GET/POST
 *  @param URLString  请求URL
 *  @param parameters 请求参数
 *  @param success    请求成功
 *  @param failure    请求失败
 */
- (void)requestWithMethod:(NSString *)method
                URLString:(NSString *)URLString
               parameters:(NSDictionary *)parameters
                  success:(void (^)(NSDictionary *responseObject))success
                  failure:(void (^)(NSError *error))failure;

@end
