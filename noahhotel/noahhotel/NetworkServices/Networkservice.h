//
//  Networkservice.h
//  noahhotel
//
//  Created by darkerwei on 15/8/11.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SVProgressHUD.h>

@class UserModel;
@class DemandModel;
@class ReplyModel;
@class SupplyValueModel;
@class HotelInterfaceModel;
@class AboutModel;
@class AgreeModel;
@class SelectModel;
@class IdentityModel;

@interface Networkservice : NSObject

/**
 *  创建网络服务
 *
 *  @return 网络服务(单例)
 */
+ (instancetype)sharedInstance;


/**
 *  注册1.2
 *
 *  @param user    请求参数
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)createWithUser:(UserModel *)user
               success:(void (^)(UserModel *user))success
               failure:(void (^)(NSError *error))failure;

- (void)forgetWithUser:(UserModel *)user
               success:(void (^)(UserModel *user))success
               failure:(void (^)(NSError *error))failure;

/**
 *  登录1.3
 *
 *  @param user    请求参数
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)loginWithUser:(UserModel *)user
              success:(void (^)(UserModel *user))success
              failure:(void (^)(NSError *error))failure;

/**
 *  用户身份验证1.4
 *
 *  @param user    请求参数
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)checkWithUser:(UserModel *)user
              success:(void (^)(UserModel *user))success
              failure:(void (^)(NSError *error))failure;

/**
 *  首页列表1.5
 *
 *  @param page    请求参数
 *  @param rows    请求参数
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)indexWithPage:(NSString *)page
                 rows:(NSString *)rows
               success:(void (^)(NSArray *hotelList))success
               failure:(void (^)(NSError *error))failure;

/**
 *  酒店筛选1.6
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)hotelInterfaceWithsuccess:(void (^)(HotelInterfaceModel *hotel))success
                          failure:(void (^)(NSError *error))failure;

/**
 *  1.7酒店详情
 *
 *  @param myId    <#myId description#>
 *  @param success <#success description#>
 *  @param failure <#failure description#>
 */
- (void)aboutWithId:(NSNumber *)myId
            success:(void (^)(AboutModel *about))success
            failure:(void (^)(NSError *error))failure;

/**
 *  1.8筛选返回
 */
- (void)indexWithPage:(NSString *)page
                 rows:(NSString *)rows
               select:(SelectModel *)select
              success:(void (^)(NSArray *hotelList))success
              failure:(void (^)(NSError *error))failure;

/**
 *  发布需求1.9
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)sendWithDemand:(DemandModel *)demand
              success:(void (^)(DemandModel *demand))success
              failure:(void (^)(NSError *error))failure;

/**
 *  需求查询1.10
 *
 *  @param page    当前页
 *  @param rows    每页显示总数
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)searchWithPage:(NSString *)page
                  rows:(NSString *)rows
             startTime:(NSString *)startTime
               endTime:(NSString *)endTime
                 title:(NSString *)title
               success:(void (^)(NSArray *demandArray))success
               failure:(void (^)(NSError *error))failure;

/**
 *  需求内容1.11
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)contentWithId:(NSNumber *)myId
              success:(void (^)(DemandModel *demand))success
              failure:(void (^)(NSError *error))failure;

/**
 *  需求回复1.12
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)replyWithId:(NSNumber *)myId
              Reply:(ReplyModel *)reply
            success:(void (^)(ReplyModel *reply))success
            failure:(void (^)(NSError *error))failure;

/**
 *  我的列表1.13
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)myListWithWithPage:(NSString *)page
                      rows:(NSString *)rows
                   success:(void (^)(NSArray *demandArray))success
                  failure:(void (^)(NSError *error))failure;

/**
 *  我的消息
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)myMessageWithWithPage:(NSString *)page
                      rows:(NSString *)rows
                   success:(void (^)(NSArray *demandArray))success
                   failure:(void (^)(NSError *error))failure;

/**
 *  个人详情1.14
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)getDetailWithsuccess:(void (^)(UserModel *user))success
                     failure:(void (^)(NSError *error))failure;

/**
 *  修改昵称1.15
 *
 *  @param user    请求参数
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)updateWithUser:(UserModel *)user
              success:(void (^)(UserModel *user))success
              failure:(void (^)(NSError *error))failure;

/**
 *  修改密码1.16
 *
 *  @param user    请求参数
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)pwdUpdateWithUser:(UserModel *)user
                  success:(void (^)(UserModel *user))success
                  failure:(void (^)(NSError *error))failure;

/**
 *  用户反馈1.17
 *
 *  @param content 反馈内容
 *  @param contact 联系方式
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)suggestWithContent:(NSString *)content
                   contact:(NSString *)contact
               success:(void (^)(NSNumber *suggestID))success
               failure:(void (^)(NSError *error))failure;

/**
 *  供应商分类1.19
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)getSortWithsuccess:(void (^)(NSArray *supplyArray))success
                  failure:(void (^)(NSError *error))failure;

/**
 *  供应商列表1.20
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */

- (void)searchWithId:(NSNumber *)myId
                name:(NSString *)name
                rows:(NSString *)rows
                page:(NSString *)page
             success:(void (^)(NSArray *supplyValueArray))success
             failure:(void (^)(NSError *error))failure;

/**
 *  供应商内容1.21
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)supplyDetailWithId:(NSNumber *)myId
                   success:(void (^)(SupplyValueModel *supplyValue))success
                   failure:(void (^)(NSError *error))failure;

/**
 *  同行合作方案1.23
 *
 */
- (void)agreeWithId:(NSNumber *)myId
            success:(void (^)(AgreeModel *agree))success
            failure:(void (^)(NSError *error))failure;


/**
 *  酒店留言1.24
 */
- (void)hotelReplyWithId:(NSNumber *)myId
                   Reply:(ReplyModel *)reply
                 success:(void (^)(ReplyModel *reply))success
                 failure:(void (^)(NSError *error))failure;

/**
 *  退出登录1.25
 *
 *  @param success 请求成功
 *  @param failure 请求失败
 */
- (void)logoutWithsuccess:(void (^)(void))success
                  failure:(void (^)(NSError *error))failure;


/**
 *  供应商分类1.26
 */
- (void)getIdentityWithsuccess:(void (^)(IdentityModel *identity))success
                   failure:(void (^)(NSError *error))failure;

/**
 *  获取验证码1.45
 *
 */
- (void)getCodeWithTel:(NSString *)tel
               success:(void (^)(NSString *code))success
               failure:(void (^)(NSError *error))failure;

/**
 *  获取首页广告图1.27
 *
 */
- (void)getHotelImagesuccess:(void (^)(NSArray *hotelImageArray))success
               failure:(void (^)(NSError *error))failure;

/**
 *  获取需求发布广告图1.28
 *
 */
- (void)getDemandImagesuccess:(void (^)(NSArray *hotelImageArray))success
                     failure:(void (^)(NSError *error))failure;

/**
 *  获取供应商广告图1.29
 *
 */
- (void)getSupplyImagesuccess:(void (^)(NSArray *hotelImageArray))success
                     failure:(void (^)(NSError *error))failure;

/**
 *  1.48定位查询
 */
- (void)locationsearchWithName:(NSString *)name
                       success:(void (^)(NSArray *locationArray))success
                       failure:(void (^)(NSError *error))failure;

/**
 *  绑定推送
 */
- (void)boundWithchannell:(NSString *)channell
                       success:(void (^)(void))success
                       failure:(void (^)(NSError *error))failure;



@end
