//
//  HttpClient.m
//  noahhotel
//
//  Created by darkerwei on 15/8/11.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import "HttpClient.h"
#import <AFHTTPRequestOperationManager.h>

@implementation HttpClient

- (void)requestWithMethod:(NSString *)method
                URLString:(NSString *)URLString
               parameters:(NSDictionary *)parameters
                  success:(void (^)(NSDictionary *))success
                  failure:(void (^)(NSError *))failure
{
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSURLRequest *request = [requestSerializer requestWithMethod:method URLString:URLString parameters:parameters error:nil];
    
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    requestOperation.responseSerializer = responseSerializer;
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        if (success) {
            success(responseObject);
        } else {
            DDLogError(@"success not found");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        } else {
            DDLogError(@"failure not found");
        }
    }];
    
    [requestOperation start];
    DDLogDebug(@"[%@] %@\n%@", method, URLString, parameters ? parameters : @"");
}

@end
