//
//  QFQBannerView.h
//  QFQBannerViewDemo
//
//  Created by 杨占江 on 15/9/12.
//  Copyright (c) 2015年 YZJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QFQHomeCollecionModel : NSObject
@property (nonatomic, copy) NSString *imgUrl;
@property (nonatomic, copy) NSString *linkUrl;
@property (nonatomic, copy) NSString *linkType;
@end


@class QFQBannerView;
@protocol QFQBannerViewDelegate <NSObject>
- (void)bannerView:(QFQBannerView *)bannerView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface QFQBannerView : UIView
/** data中只能存放QFQHomeCollecionModel */
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, weak) id <QFQBannerViewDelegate> delegate;
- (void)addTimer;
- (void)removeTimer;
@end
