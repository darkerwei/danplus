//
//  QFQBannerView.m
//  QFQBannerViewDemo
//
//  Created by 杨占江 on 15/9/12.
//  Copyright (c) 2015年 YZJ. All rights reserved.
//

#import "QFQBannerView.h"
#import "UIImageView+WebCache.h"

#define QFQCollectionCellID @"UICollectionViewCell"
// 共有3组图片
#define kMaxSections 3

@implementation QFQHomeCollecionModel

@end

@interface QFQHomeCollectionCell : UICollectionViewCell
@property (nonatomic, weak) QFQHomeCollecionModel *model;
@property (nonatomic, weak) UIImageView *imageView;
@end

@implementation QFQHomeCollectionCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImageView *imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:imageView];
        self.imageView = imageView;
    }
    return self;
}

- (void)setModel:(QFQHomeCollecionModel *)model
{
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.imgUrl] placeholderImage:nil];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imageView.frame = self.bounds;
}
@end

@interface QFQBannerView () <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (weak, nonatomic) UIPageControl *pageControl;
@property (nonatomic, strong) NSTimer *timer;
@end
@implementation QFQBannerView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
    }
    return self;
}
- (void)setUI
{
    [self collectionView];
    [self pageControl];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.collectionView.frame = self.bounds;
    CGPoint centerPage = CGPointMake(self.center.x, self.bounds.size.height - 16);
    self.pageControl.center = centerPage;
    
}
#pragma mark - public

- (void)reloadData
{
    [self.collectionView reloadData];
}
- (void)setData:(NSArray *)data
{
    _data = data;
    [self.collectionView reloadData];
    if (data.count > 1) {
        self.pageControl.numberOfPages = data.count;
        self.collectionView.scrollEnabled = YES;
        [self addTimer];
    } else {
        self.collectionView.scrollEnabled = NO;
    }
    
}
#pragma mark - getter
- (UIPageControl *)pageControl
{
    if (!_pageControl) {
        UIPageControl *pageControl = [[UIPageControl alloc] init];
        pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
        pageControl.pageIndicatorTintColor = [UIColor redColor];
        [self addSubview:pageControl];
        self.pageControl = pageControl;
    }
    return _pageControl;
}
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        //布局
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.minimumLineSpacing = 0;
        
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        
        collectionView.backgroundColor = [UIColor lightGrayColor];
        collectionView.pagingEnabled = YES;
        collectionView.showsHorizontalScrollIndicator = NO;
        collectionView.dataSource = self;
        collectionView.delegate = self;
        
        
        //注册cell
        [collectionView registerClass:[QFQHomeCollectionCell class] forCellWithReuseIdentifier:QFQCollectionCellID];
        self.collectionView = collectionView;
        [self addSubview:collectionView];
    }
    return _collectionView;
}

#pragma mark - 定时器的方法
/**
 *  添加定时器
 */
- (void)addTimer
{
    if (self.timer == nil && self.data.count > 1) {
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(nextPage) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        self.timer = timer;
    }
    
}

/**
 *  下一页，因为在同一组显示下一张图片，所以可以使用动画
 */
- (void)nextPage
{
    // 马上显示回最中间的那组数据
    NSIndexPath *currentIndexPathReset = [self resetIndexPath];
    //如果无数据，则返回，防止crash
    if (!currentIndexPathReset) return;
    // 计算出下一个需要展示的位置
    NSInteger nextItem = currentIndexPathReset.item + 1;
    NSInteger nextSection = currentIndexPathReset.section;
    if(nextItem == self.data.count)
    {
        // 如果滚动完了某一组，就回到下一组的起点，组数加1.
        // 这里加1下面那个方法还会变回中间那组的.
        nextItem = 0;
        nextSection++;
    }
    // 计算下一个需要展示的位置
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:nextSection];
    
    // 动画的显示中间那一组下一张图片，这里可以用动画，因为不是组间的跳转
    [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

/**
 *  不用动画快速的滚回到中间那一组的同一张图片位置
 */
- (NSIndexPath *)resetIndexPath
{
    // 获取正在展示的位置
    NSIndexPath * currentIndexPath = [[self.collectionView indexPathsForVisibleItems] lastObject];
    //如果无数据，则返回，防止crash
    if (!currentIndexPath) return nil;
    
    // 马上显示回最中间的那组数据
    NSIndexPath * currentIndexPathReset = [NSIndexPath indexPathForItem:currentIndexPath.item inSection:kMaxSections/2];
    
    // 快速地滚回到中间那一组，所以不能再用动画了。
    [self.collectionView scrollToItemAtIndexPath:currentIndexPathReset atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    
    // 显示页码
    self.pageControl.currentPage = (currentIndexPath.item) % self.data.count;
    return currentIndexPathReset;
}

/**
 *  移除定时器
 */
- (void)removeTimer
{
    // 停止定时器
    [self.timer invalidate];
    self.timer = nil;
}
#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return kMaxSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    QFQHomeCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:QFQCollectionCellID forIndexPath:indexPath];
    cell.model = self.data[indexPath.row];
    
    
    return cell;
    
}
#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.frame.size.width, self.frame.size.height);
    
}
#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(bannerView:didSelectItemAtIndexPath:)]) {
        [self.delegate bannerView:self didSelectItemAtIndexPath:indexPath];
    }
    
}

#pragma mark  - UIScrollViewDelegate
/**
 *  当用户即将开始拖拽的时候就调用
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([scrollView isKindOfClass:[UICollectionView class]]) {
        [self removeTimer];
        // 当手一松开时又循环调用中间那一组的同一张图片
        [self resetIndexPath];
    }
    
}

/**
 *  当用户停止拖拽的时候就调用
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([scrollView isKindOfClass:[UICollectionView class]]) {
        [self addTimer];
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 获取下一页，collectionView
    if ([scrollView isKindOfClass:[UICollectionView class]]) {
        if (self.data.count) {
            int page = (int)(scrollView.contentOffset.x / scrollView.bounds.size.width+0.5) % self.data.count;
            self.pageControl.currentPage = page;
        }
    }
    
}
@end
