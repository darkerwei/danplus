//
//  AppDelegate.h
//  noahhotel
//
//  Created by darkerwei on 15/8/5.
//  Copyright (c) 2015年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

