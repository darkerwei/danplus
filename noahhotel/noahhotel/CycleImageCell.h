#import <UIKit/UIKit.h>

@interface CycleImageCell : UICollectionViewCell

/**
 *  图片地址
 */
@property (nonatomic, strong) NSURL *imageURL;

@end
