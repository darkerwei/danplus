#import <UIKit/UIKit.h>

@interface CycleViewController : UICollectionViewController

@property (nonatomic, strong) NSArray *imageURLs;

@end
