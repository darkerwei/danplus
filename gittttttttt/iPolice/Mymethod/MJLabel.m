//
//  MJLabel.m
//  iPolice
//
//  Created by xwy on 15/9/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MJLabel.h"

@implementation MJLabel

- (void)setText:(NSString *)text
{
    CGSize size = [text sizeWithFont:self.font constrainedToSize:CGSizeMake(self.bounds.size.width, CGFLOAT_MAX)];
    CGAffineTransform transform = self.transform;
    self.transform = CGAffineTransformIdentity;
    CGRect frame = self.frame;
    frame.size.height = size.height;
    self.frame = frame;
    self.transform = transform;
    
    [super setText:text];
}
@end
