//
//  SetViewController.m
//  iPolice
//
//  Created by xwy on 15/6/30.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "SetViewController.h"
#import "CommonCell.h"
#import "CommonGroup.h"
#import "CommonItem.h"
#import "FunctionViewController.h"
#import "ScoreViewController.h"
#import "LoginViewController.h"
#import "WelcomePageViewController.h"

// Footer的高度
#define FooterHeight 0
// Header的高度
#define HeaderHeight 10

// cell信息
#define CommonItem1Title @"功能介绍"
#define CommonItem1TitleImageName @"附近警情"
#define CommonItem2Title @"积分规则介绍"
#define CommonItem2TitleImageName @"附近视频"
#define CommonItem3Title @"欢迎页"
#define CommonItem3TitleImageName @"附近派出所"
#define CommonItem4Title @"意见反馈"
#define CommonItem4TitleImageName @"附近的人"
#define CommonItem5Title @"一键报警"
#define CommonItem5TitleImageName @"扫一扫"
#define CommonItem6Title @"版本更新"
#define CommonItem6TitleImageName @"商城"
#define CommonItem7Title @"清缓存"
#define CommonItem7TitleImageName @"商城"

@interface SetViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
/**
 *  设置页面
 */
@property (strong, nonatomic) IBOutlet UITableView *setTableView;
/**
 *  组列表
 */
@property (nonatomic, strong) NSArray *commonGroupList;
@property (weak, nonatomic) IBOutlet UIButton *quitButton;

@end

@implementation SetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置标题
    self.title = @"设置";
    
    // 设置tableView属性
    self.setTableView.sectionFooterHeight = FooterHeight;
    self.setTableView.sectionHeaderHeight = HeaderHeight;
    
    // 调整cell位置
    self.setTableView.contentInset = UIEdgeInsetsMake(-24, 0, 0, 0);
    
    self.quitButton.layer.cornerRadius = 5;
    self.quitButton.clipsToBounds = YES;
    
}

/**
 *  退出按钮点击
 */
- (IBAction)quitBtnClick
{
    NSLog(@"在这里处理<退出程序>点击事件");
    UIAlertView *alvert = [[UIAlertView alloc] initWithTitle:@"确定退出登录?" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alvert show];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.commonGroupList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CommonGroup *theCommonGroup = self.commonGroupList[section];
    
    return theCommonGroup.commonItemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommonCell *cell = [CommonCell cellWithTableView:tableView];
    CommonGroup *theCommonGroup = self.commonGroupList[indexPath.section];
    cell.theCommonItem = theCommonGroup.commonItemList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (indexPath.row == 4) {
        UISwitch *switchView = [[UISwitch alloc] init];
        switchView.onTintColor = [UIColor orangeColor];
        [switchView addTarget:self action:@selector(updateSwitchAtIndexPath:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchView;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        NSLog(@"在这里处理<功能介绍>点击事件");
        FunctionViewController *functionVC = [[FunctionViewController alloc] init];
        [self.navigationController pushViewController:functionVC animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 1) {
        NSLog(@"在这里处理<积分规则介绍>点击事件");
        ScoreViewController *scoreVC = [[ScoreViewController alloc] init];
        [self.navigationController pushViewController:scoreVC animated:YES];
    } else if (indexPath.row == 2) {
        NSLog(@"在这里处理<欢迎页>点击事件");
        WelcomePageViewController *welVC = [[WelcomePageViewController alloc] init];
        welVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:welVC animated:YES completion:^{
        }];
    }else if (indexPath.row == 3) {
        NSLog(@"在这里处理<意见反馈>点击事件");
    }else if (indexPath.row == 4) {
        NSLog(@"在这里处理<一键报警>点击事件");
    }else if (indexPath.row == 5) {
        NSLog(@"在这里处理<版本更新>点击事件");
    }else if (indexPath.row == 6) {
        NSLog(@"在这里处理<清缓存>点击事件");
    }
}


/**
 *  switchView当前状态
 *
 *  @param switchView switchView
 */
- (void)updateSwitchAtIndexPath:(UISwitch *)switchView
{
    if ([switchView isOn]) {
        NSLog(@"switchView isOn");
        
        UIWebView *callWebview =[[UIWebView alloc] init];
        NSURL *telURL =[NSURL URLWithString:@"tel:110"];// 貌似tel:// 或者 tel: 都行
        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
        //记得添加到view上
        [self.view addSubview:callWebview];
    
        switchView.on = YES;
    
    } else {
        NSLog(@"switchView isOff");
    }
    
    
}

#pragma mark - 懒加载

- (NSArray *)commonGroupList
{
    if (_commonGroupList == nil) {
        
        // 创建组
        CommonGroup *commonGroup = [CommonGroup commonGroup];
        
        // 创建cell信息
        CommonItem *commonItem1 = [CommonItem commonItemWithTitle:CommonItem1Title titleImageName:CommonItem1TitleImageName];
        CommonItem *commonItem2 = [CommonItem commonItemWithTitle:CommonItem2Title titleImageName:CommonItem2TitleImageName];
        CommonItem *commonItem3 = [CommonItem commonItemWithTitle:CommonItem3Title titleImageName:CommonItem3TitleImageName];
        CommonItem *commonItem4 = [CommonItem commonItemWithTitle:CommonItem4Title titleImageName:CommonItem4TitleImageName];
        CommonItem *commonItem5 = [CommonItem commonItemWithTitle:CommonItem5Title titleImageName:CommonItem5TitleImageName];
        CommonItem *commonItem6 = [CommonItem commonItemWithTitle:CommonItem6Title titleImageName:CommonItem6TitleImageName description:@"v1.00"];
        CommonItem *CommonItem7 = [CommonItem commonItemWithTitle:CommonItem7Title titleImageName:CommonItem7TitleImageName description:@"0.00M"];
        commonGroup.commonItemList = @[commonItem1, commonItem2, commonItem3, commonItem4, commonItem5, commonItem6, CommonItem7];
        
        _commonGroupList = @[commonGroup];
    }
    
    return _commonGroupList;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        
        LoginViewController *login = [[LoginViewController alloc] init];
        [self.navigationController pushViewController:login animated:YES];
        
    }
}

@end
