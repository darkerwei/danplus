//
//  ScoreViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/7/3.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ScoreViewController.h"

@interface ScoreViewController ()

/**
 *  积分规则介绍页面
 */
@property(nonatomic,strong) IBOutlet UITableView *scoreTableView;

@end

@implementation ScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置标题
    self.title = @"积分规则介绍";
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    label.text = @"积分规则介绍";
    label.font = [UIFont systemFontOfSize:13];
    [view addSubview:label];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 43, Main_Screen_Width, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [view addSubview:line];
    
    self.scoreTableView.tableHeaderView = view;
    self.scoreTableView.tableFooterView = [[UIView alloc] init];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tableViewCellIdentifier = @"TableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableViewCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:tableViewCellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:13];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:13];
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"上传一张图片";
        cell.detailTextLabel.text = @"+1积分";
    } else if (indexPath.row == 1) {
        cell.textLabel.text = @"上传一段音频文件";
        cell.detailTextLabel.text = @"+5积分";
    } else if (indexPath.row == 2) {
        cell.textLabel.text = @"上传一段视频文件";
        cell.detailTextLabel.text = @"+10积分";
    } else if (indexPath.row == 3) {
        cell.textLabel.text = @"上传图片及音频文件";
        cell.detailTextLabel.text = @"+15积分";
    } else if (indexPath.row == 4) {
        cell.textLabel.text = @"上传图片及视频文件";
        cell.detailTextLabel.text = @"+20积分";
    }
    
    return cell;
}

/* 

 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
 static NSString *TableSampleIdentifier = @"TableSampleIdentifier";
 
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
 if (cell == nil) {
 cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableSampleIdentifier];
 cell.textLabel.font = [UIFont systemFontOfSize:13];
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 }
 if(indexPath.row == 0){
 cell.textLabel.text = @"上传一张图片                                     +1积分";
 }
 if(indexPath.row == 1){
 cell.textLabel.text = @"上传一段音频文件                               +5积分";
 }
 if(indexPath.row == 2){
 cell.textLabel.text = @"上传一段视频文件                             +10积分";
 }
 if(indexPath.row == 3){
 cell.textLabel.text = @"上传图片及音频文件                          +15积分";
 }
 if(indexPath.row == 4){
 cell.textLabel.text = @"上传图片及视频文件                          +20积分";
 }
 if(indexPath.row == 5){
 cell.textLabel.text = @"其他规则                                            +?积分";
 }
 return cell;
 
 
 }
 
 */

@end
