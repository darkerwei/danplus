//
//  CarIdTableViewCell.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CarIdTableViewCellDelegate <NSObject>

/**
 *  信息按钮点击
 */
- (void)InfoBtnClick;

@end

@interface CarIdTableViewCell : UITableViewCell

@property (nonatomic, assign) id<CarIdTableViewCellDelegate> delegate;

/**
 *  车架号
 */
@property (weak, nonatomic) IBOutlet UITextField *carIdTextField;

/**
 *  车架号
 */
@property (nonatomic, copy) NSString *carId;

/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
