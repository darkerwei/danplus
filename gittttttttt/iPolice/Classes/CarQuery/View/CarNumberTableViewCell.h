//
//  CarNumberTableViewCell.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarNumberTableViewCell : UITableViewCell

/**
 *  车辆省份
 */
@property (nonatomic, copy) NSString *carProvince;
/**
 *  车辆号码
 */
@property (nonatomic, copy) NSString *carNumber;
/**
 *  车辆号码
 */
@property (weak, nonatomic) IBOutlet UITextField *carNumberTextField;
/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
