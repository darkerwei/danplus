//
//  CarNumberTableViewCell.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CarNumberTableViewCell.h"

// CarNumberTableViewCell标识符
#define CarNumberTableViewCellIdentifier @"CarNumberTableViewCell"

@interface CarNumberTableViewCell()<UITextFieldDelegate>
/**
 *  车辆省份
 */
@property (weak, nonatomic) IBOutlet UILabel *carProvinceLabel;

@end

@implementation CarNumberTableViewCell

/**
 *  初始化XIB
 *
 *  @param carProvince 车辆省份
 */
- (void)setCarProvince:(NSString *)carProvince
{
    _carProvince = carProvince;
    
    self.carProvinceLabel.text = carProvince;
}

/**
 *  初始化XIB
 *
 *  @param carNumber 车辆号码
 */
- (void)setCarNumber:(NSString *)carNumber
{
    _carNumber = carNumber;
    
    self.carNumberTextField.text = carNumber;
    
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:CarNumberTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:CarNumberTableViewCellIdentifier];
    CarNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CarNumberTableViewCellIdentifier];
    
    return cell;
}

@end
