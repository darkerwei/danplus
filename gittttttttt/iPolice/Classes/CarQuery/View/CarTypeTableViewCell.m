//
//  CarTypeTableViewCell.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CarTypeTableViewCell.h"

// CarTypeTableViewCell标识符
#define CarTypeTableViewCellIdentifier @"CarTypeTableViewCell"

@interface CarTypeTableViewCell()

/**
 *  车辆种类
 */
@property (weak, nonatomic) IBOutlet UILabel *carTypeLabel;

@end

@implementation CarTypeTableViewCell

/**
 *  初始化XIB
 *
 *  @param carType 车辆种类
 */
- (void)setCarType:(NSString *)carType
{
    _carType = carType;
    
    self.carTypeLabel.text = carType;
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:CarTypeTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:CarTypeTableViewCellIdentifier];
    CarTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CarTypeTableViewCellIdentifier];
    
    return cell;
}

@end
