//
//  VolationTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/9/2.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "VolationTableViewCell.h"
#import "DriverInfo.h"

#define VolationTableViewCellIdentifier @"VolationTableViewCell"

@interface VolationTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *fines;
@property (weak, nonatomic) IBOutlet UILabel *lcredit;
@property (weak, nonatomic) IBOutlet UILabel *cstatus;
@property (weak, nonatomic) IBOutlet UILabel *hstatus;
@property (weak, nonatomic) IBOutlet UILabel *ltime;
@property (weak, nonatomic) IBOutlet UILabel *laddress;

@property (weak, nonatomic) IBOutlet UILabel *lbehavior;
@property (weak, nonatomic) IBOutlet UILabel *count;

@end

@implementation VolationTableViewCell

- (void)setCountNumber:(NSString *)countNumber{
    
    _countNumber = countNumber;
    
    self.count.text = self.countNumber;
}
- (void)setVolation:(DriverInfo *)volation{
    
    _volation = volation;
    
    self.fines.text = [NSString stringWithFormat:@"%@元", self.volation.fines];
    
    self.lcredit.text = [NSString stringWithFormat:@"%@分", self.volation.lcredit];
    
    if ([self.hstatus.text isEqualToString:@"0"]) {
        self.hstatus.text = @"未处理";
    }
    
    self.cstatus.text = self.volation.cstatus;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.volation.ltime.doubleValue / 1000.0]];
    self.ltime.text = [NSString stringWithFormat:@"%@", regStr];
    
    self.laddress.text = self.volation.laddress;
    
    self.lbehavior.text = self.volation.lbehavior;

}

// 快速创建一个Cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:VolationTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:VolationTableViewCellIdentifier];
    VolationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VolationTableViewCellIdentifier];
    
    return cell;
}

@end
