//
//  VolationTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/9/2.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DriverInfo;

@interface VolationTableViewCell : UITableViewCell

/**
 *  通缉令信息
 */
@property (nonatomic, strong) DriverInfo *volation;
@property (nonatomic, strong) NSString *countNumber;
/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
