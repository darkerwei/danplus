//
//  CarIdTableViewCell.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CarIdTableViewCell.h"

// CarIdTableViewCell标识符
#define CarIdTableViewCellIdentifier @"CarIdTableViewCell"

@interface CarIdTableViewCell()

@end

@implementation CarIdTableViewCell

/**
 *  信息按钮点击
 */
- (IBAction)InfoBtnClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(InfoBtnClick)]) {
        [self.delegate InfoBtnClick];
    }
}

/**
 *  初始化XIB
 *
 *  @param carId 车架号
 */
- (void)setCarId:(NSString *)carId
{
    _carId = carId;
    
    self.carIdTextField.text = carId;
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:CarIdTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:CarIdTableViewCellIdentifier];
    CarIdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CarIdTableViewCellIdentifier];
    
    return cell;
}

@end
