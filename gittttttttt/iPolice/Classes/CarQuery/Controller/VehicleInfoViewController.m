//
//  VehicleInfoViewController.m
//  iPolice
//
//  Created by xwy on 15/9/1.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "VehicleInfoViewController.h"
#import "DriverInfo.h"
#import "CarIdTableViewCell.h"

@interface VehicleInfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *brand;
@property (weak, nonatomic) IBOutlet UILabel *platenumber;
@property (weak, nonatomic) IBOutlet UILabel *idnumber;
@property (weak, nonatomic) IBOutlet UILabel *holder;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *color;
@property (weak, nonatomic) IBOutlet UILabel *nature;
@property (weak, nonatomic) IBOutlet UILabel *endtime;
@property (weak, nonatomic) IBOutlet UILabel *mstatus;
@property (weak, nonatomic) IBOutlet UILabel *registertime;
@property (weak, nonatomic) IBOutlet UIImageView *personImage;
@property (weak, nonatomic) IBOutlet UILabel *scrappedtime;
@property (weak, nonatomic) IBOutlet UILabel *status;
@end

@implementation VehicleInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //头像
    self.personImage.clipsToBounds = YES;
    self.personImage.layer.cornerRadius = 50;
    //商标
    self.brand.text = self.detail.brand;
    self.platenumber.text = [NSString stringWithFormat:@"鲁%@",self.detail.platenumber];
    self.idnumber.text = [NSString stringWithFormat:@"车辆识别代码:***********%@",self.code];
    self.holder.text = self.detail.holder;
    self.type.text = self.detail.type;
    self.color.text = self.detail.color;
    self.nature.text = [NSString stringWithFormat:@"%@",self.detail.nature];
    self.mstatus.text = self.detail.mstatus;
    self.status.text = self.detail.status;

    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    //有效期止
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.endtime.doubleValue / 1000.0]];
    self.endtime.text = [NSString stringWithFormat:@"%@", regStr];
    //初次登记日期
    NSString *regStr1 = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.registertime.doubleValue / 1000.0]];
    self.registertime.text = [NSString stringWithFormat:@"%@", regStr1];
    //强制报废期止
    NSString *regStr2 = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.scrappedtime.doubleValue / 1000.0]];
    self.scrappedtime.text = [NSString stringWithFormat:@"%@", regStr2];
   
}


@end
