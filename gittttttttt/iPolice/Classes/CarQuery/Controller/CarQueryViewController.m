//
//  CarQueryViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CarQueryViewController.h"
#import "CarTypeTableViewCell.h"
#import "CarNumberTableViewCell.h"
#import "ShadeView.h"
#import "CarIdTableViewCell.h"
#import "DriverInfo.h"
#import "NetworkService.h"
#import "VehicleInfoViewController.h"
#import "VolationInfoViewController.h"


// Footer的高度
#define FooterHeight 0
// Header的高度
#define HeaderHeight 10
// 车辆省份选择器的高度
#define CarNumberPickerViewHeight 160


typedef NS_ENUM(NSUInteger, CarQueryCellType) {
    CarQueryCellTypeCarType = 41,
    CarQueryCellTypeCarNumber,
    CarQueryCellTypeCarId
};

@interface CarQueryViewController ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, ShadeViewDelegate, UIPickerViewDataSource,UIPickerViewDelegate, CarIdTableViewCellDelegate, UITextFieldDelegate>
{
    CarNumberTableViewCell *carNumberTableViewCell;
    CarIdTableViewCell *carIdTableViewCell;
}

@property (nonatomic, strong) UIButton *queryButton;
/**
 *  查询页面
 */
@property (weak, nonatomic) IBOutlet UITableView *carQuerytableView;
/**
 *  车辆省份选择器
 */
@property (strong, nonatomic) UIPickerView *carNumberPickerView;
/**
 *  遮罩
 */
@property (strong, nonatomic) ShadeView *shadeView;
/**
 *  车辆省份列表
 */
@property (strong, nonatomic) NSArray *carProvincesList;
@property (nonatomic, strong) NSString *cartype;
@property (nonatomic, strong) NSString *plate;

@end

@implementation CarQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置tableView属性
    self.carQuerytableView.sectionFooterHeight = FooterHeight;
    self.carQuerytableView.sectionHeaderHeight = HeaderHeight;
    
//     调整cell位置
//    self.carQuerytableView.contentInset = UIEdgeInsetsMake(-24, 0, 0, 0);
    
    self.queryButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 450, Main_Screen_Width-40, 50)];
    [self.queryButton setTitle:@"查询" forState:UIControlStateNormal];
    self.queryButton.clipsToBounds = YES;
    self.queryButton.layer.cornerRadius = 5;
    self.queryButton.backgroundColor = [UIColor orangeColor];
    [self.queryButton addTarget:self action:@selector(clickQueryButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.queryButton];

    self.plate = @"M";
    
    //这界面要是加手势的话，会妨碍UIActionSheet控件的弹出
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClciked)];
//    [self.carQuerytableView addGestureRecognizer:tap];
}

//点击空白处隐藏键盘
- (void)tapClciked {
    [carNumberTableViewCell.carNumberTextField resignFirstResponder];
    [carIdTableViewCell.carIdTextField resignFirstResponder];
}

//点击return取消键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [carNumberTableViewCell.carNumberTextField resignFirstResponder];
    [carIdTableViewCell.carIdTextField resignFirstResponder];
    
    return YES;
}


/**
 *  初始化控制器类型
 *
 *  @param carQueryViewControllerType 控制器类型
 */
- (void)setCarQueryViewControllerType:(CarQueryViewControllerType)carQueryViewControllerType
{
    _carQueryViewControllerType = carQueryViewControllerType;
    
    if (self.carQueryViewControllerType == CarQueryViewControllerTypeCar) {
        self.title = @"机动车信息查询";
    } else if (self.carQueryViewControllerType == CarQueryViewControllerTypeTrafficOffence) {
        self.title = @"违章信息查询";
    }
}



//查询按钮点击  测试数据02 platenumber:@"MUC899" code:@"055480"];
- (void)clickQueryButton
{
    //车牌号码
    NSIndexPath *indexpath = [NSIndexPath indexPathForItem:0 inSection:1];
    CarNumberTableViewCell *cell = (CarNumberTableViewCell *)[self.carQuerytableView cellForRowAtIndexPath:indexpath];
    UITextField *textF = (UITextField *)[cell viewWithTag:1000];
    NSString *carplate = [NSString stringWithFormat:@"%@", textF.text];
    
    //  车架号
    NSIndexPath *indexpath1 = [NSIndexPath indexPathForItem:0 inSection:2];
    CarIdTableViewCell   *cell1 = (CarIdTableViewCell *)[self.carQuerytableView cellForRowAtIndexPath:indexpath1];
    UITextField *textF1 = (UITextField *)[cell1 viewWithTag:1001];
    NSString *code = [NSString stringWithFormat:@"%@", textF1.text];
    
    if (self.carQueryViewControllerType == CarQueryViewControllerTypeCar) {
        NSLog(@"在这里处理<机动车查询按钮>点击事件");
        
        DriverInfo *drive = [DriverInfo driverInfoWithType:self.cartype platenumber:[NSString stringWithFormat:@"%@%@", self.plate, carplate] code:code];
        [[NetworkService sharedInstance] getVehiclesWithVolation:drive success:^(DriverInfo *volation) {
            
            VehicleInfoViewController *vehicle = [[VehicleInfoViewController alloc] init];
            vehicle.title = @"机动车信息查询结果";
            vehicle.detail = drive;
            vehicle.code = code;
            [self.navigationController pushViewController:vehicle animated:YES];
            
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        }];
    
    } else if (self.carQueryViewControllerType == CarQueryViewControllerTypeTrafficOffence) {
        NSLog(@"在这里处理<违章查询按钮>点击事件");
        
        DriverInfo *drive = [DriverInfo driverInfoWithType:self.cartype platenumber:[NSString stringWithFormat:@"%@%@", self.plate, carplate] code:code];
        [[NetworkService sharedInstance] getVolationWithVolation:drive success:^(NSArray *volation) {
        
            VolationInfoViewController *volati = [[VolationInfoViewController alloc] init];
            volati.title = @"机动车违章信息查询结果";
            volati.recordList = volation;
            volati.code = code;
            volati.detail = drive;
            [self.navigationController pushViewController:volati animated:YES];
            
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        }];
        
        

    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CarTypeTableViewCell *cell = [CarTypeTableViewCell cellWithTableView:tableView];
        cell.tag = CarQueryCellTypeCarType;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.image = [UIImage imageNamed:@"login_important"];
        return cell;
    } else if (indexPath.section == 1) {
        carNumberTableViewCell = [CarNumberTableViewCell cellWithTableView:tableView];
        carNumberTableViewCell.tag = CarQueryCellTypeCarNumber;
        carNumberTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        carNumberTableViewCell.imageView.image = [UIImage imageNamed:@"login_important"];
        carNumberTableViewCell.carNumberTextField.delegate = self;
        return carNumberTableViewCell;
    } else {
        carIdTableViewCell = [CarIdTableViewCell cellWithTableView:tableView];
        carIdTableViewCell.tag = CarQueryCellTypeCarId;
        carIdTableViewCell.delegate = self;
        carIdTableViewCell.carIdTextField.delegate = self;
        carIdTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        carIdTableViewCell.imageView.image = [UIImage imageNamed:@"login_important"];
        return carIdTableViewCell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 50)];
        [button setTitle:@"请输入查询信息" forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"未标题-1.png"] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        return button;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 50;
    }
    return 0;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NSLog(@"在这里处理<车辆种类>点击事件");
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:@"选择车辆种类"
                                      delegate:self
                                      cancelButtonTitle:@"取消"
                                      destructiveButtonTitle:@"大型汽车"
                                      otherButtonTitles:@"小型汽车",@"普通摩托车",@"轻便摩托车",@"低速车", @"拖拉机", @"挂车",nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [actionSheet showInView:self.view];
    } else if (indexPath.section == 1) {
        NSLog(@"在这里处理<车辆号码>点击事件");
        
        self.shadeView = [[ShadeView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.shadeView.delegate = self;
        [self.shadeView showWithView:self.view];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    CarTypeTableViewCell *cell = (CarTypeTableViewCell *)[self.carQuerytableView viewWithTag:CarQueryCellTypeCarType];
    
    if (buttonIndex == 0) {
        cell.carType = @"大型汽车";
        self.cartype = [NSString stringWithFormat:@"01"];
    } else if (buttonIndex == 1) {
        cell.carType = @"小型汽车";
        self.cartype = [NSString stringWithFormat:@"02"];
    } else if (buttonIndex == 2) {
        cell.carType = @"普通摩托车";
        self.cartype = [NSString stringWithFormat:@"07"];
    } else if (buttonIndex == 3) {
        cell.carType = @"轻便摩托车";
        self.cartype = [NSString stringWithFormat:@"08"];
    } else if (buttonIndex == 4) {
        cell.carType = @"低速车";
        self.cartype = [NSString stringWithFormat:@"13"];
    } else if (buttonIndex == 5) {
        cell.carType = @"拖拉机";
        self.cartype = [NSString stringWithFormat:@"14"];
    } else if (buttonIndex == 6) {
        cell.carType = @"挂车";
        self.cartype = [NSString stringWithFormat:@"15"];
    }
    
}

#pragma mark - ShadeViewDelegate

- (void)show
{
    self.carNumberPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, Main_Screen_Height, Main_Screen_Width, CarNumberPickerViewHeight)];
    self.carNumberPickerView.backgroundColor = [UIColor whiteColor];
    self.carNumberPickerView.dataSource = self;
    self.carNumberPickerView.delegate = self;
    [self.view addSubview:_carNumberPickerView];
    
    [UIView animateWithDuration:ShadeAnimationTime animations:^{
        self.carNumberPickerView.y = Main_Screen_Height - CarNumberPickerViewHeight;
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:ShadeAnimationTime animations:^{
        self.carNumberPickerView.y = Main_Screen_Height;
    } completion:^(BOOL finished) {
        [self.carNumberPickerView removeFromSuperview];
    }];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.carProvincesList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.carProvincesList[row];
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    CarNumberTableViewCell *cell = (CarNumberTableViewCell *)[self.carQuerytableView viewWithTag:CarQueryCellTypeCarNumber];
    cell.carProvince = self.carProvincesList[row];
    self.plate = cell.carProvince;
}

#pragma mark - CarIdTableViewCellDelegate

- (void)InfoBtnClick
{
    NSLog(@"在这里处理<信息按钮>点击事件");
}

#pragma mark - 懒加载

- (NSArray *)carProvincesList
{
    if (_carProvincesList == nil) {
        NSString *carProvince1 = @"M";
        NSString *carProvince2 = @"A";
        NSString *carProvince3 = @"B";
        NSString *carProvince4 = @"C";
        NSString *carProvince5 = @"D";
        NSString *carProvince6 = @"E";
        NSString *carProvince7 = @"F";
        NSString *carProvince8 = @"G";
        NSString *carProvince9 = @"H";
        NSString *carProvince10 = @"J";
        NSString *carProvince11 = @"K";
        NSString *carProvince12 = @"L";
        NSString *carProvince13 = @"N";
        NSString *carProvince14 = @"P";
        NSString *carProvince15 = @"Q";
        NSString *carProvince16 = @"R";
        NSString *carProvince17 = @"U";
        NSString *carProvince18 = @"V";
        _carProvincesList = @[carProvince1, carProvince2, carProvince3, carProvince4, carProvince5, carProvince6, carProvince7, carProvince8, carProvince9, carProvince10, carProvince11, carProvince12, carProvince13, carProvince14, carProvince15, carProvince16, carProvince17, carProvince18];
    }
    
    return _carProvincesList;
}

@end
