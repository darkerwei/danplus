//
//  CarQueryViewController.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CarQueryViewControllerType) {
    CarQueryViewControllerTypeCar = 0, // 机动车查询
    CarQueryViewControllerTypeTrafficOffence // 违章查询
};

@interface CarQueryViewController : UIViewController

@property (nonatomic, assign) CarQueryViewControllerType carQueryViewControllerType;

@end
