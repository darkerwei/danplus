//
//  VolationInfoViewController.m
//  iPolice
//
//  Created by xwy on 15/9/1.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "VolationInfoViewController.h"
#import "DriverInfo.h"
#import "VolationTableViewCell.h"

@interface VolationInfoViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;


@property (weak, nonatomic) IBOutlet UIImageView *personImage;
@property (weak, nonatomic) IBOutlet UILabel *carType;
@property (weak, nonatomic) IBOutlet UILabel *platenumber;
@property (weak, nonatomic) IBOutlet UILabel *idnumber;
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UIButton *selectedButton;

@end

@implementation VolationInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //头像
    self.personImage.clipsToBounds = YES;
    self.personImage.layer.cornerRadius = 40;
    
    [self.selectedButton setImage:[UIImage imageNamed:@"选择框.png"] forState:UIControlStateNormal];
    [self.selectedButton setImage:[UIImage imageNamed:@"选择框-1.png"] forState:UIControlStateSelected];
    [self.selectedButton addTarget:self action:@selector(clickSelectedButton) forControlEvents:UIControlEventTouchUpInside];

    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 240, Main_Screen_Width, Main_Screen_Height-200)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    // 调整Cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self getInformation];
}

- (void)getInformation{
    
    self.carType.text = @"五菱牌";
    self.platenumber.text = [NSString stringWithFormat:@"鲁%@",self.detail.platenumber];
    self.idnumber.text = [NSString stringWithFormat:@"车辆识别代码:***********%@",self.code];
    self.count.text = [NSString stringWithFormat:@"共有%ld条违法记录", self.recordList.count];
}

//未处理的
- (void)clickSelectedButton{
    
    self.selectedButton.selected = !self.selectedButton.selected;
    
//    NSString *creditprice = [[NSString alloc] init];
//    if (self.selectedButton.selected == NO) {
//        creditprice = @"";
//    } else {
//        creditprice = @"250";
//    }
//    
//    ShopModel *shop = [ShopModel shopWithStart:@"0" limit:@"10" keywords:@"" typeID:@"" creditprice:creditprice shopprice:@""];
//    
//    [[NetworkService sharedInstance] getGoodsWithShop:shop success:^(NSArray *shopList) {
//        [SVProgressHUD showWithStatus:@"加载中..."];
//        self.goodsList = shopList;
//        [self.collectionView reloadData];
//        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
//    } failure:^(NSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
//        
//    }];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VolationTableViewCell *cell = [VolationTableViewCell cellWithTableView:tableView];
    cell.volation = self.recordList[indexPath.row];
    cell.countNumber = [NSString stringWithFormat:@"%ld", indexPath.row+1];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}
@end
