//
//  VolationInfoViewController.h
//  iPolice
//
//  Created by xwy on 15/9/1.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DriverInfo;

@interface VolationInfoViewController : UIViewController


@property(nonatomic, strong) DriverInfo *detail;

@property (nonatomic, strong) NSArray *recordList;
@property (nonatomic, strong) NSString *code;

@end
