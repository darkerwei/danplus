//
//  PersonCell.m
//  iPolice
//
//  Created by xwy on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PersonCell.h"
#import "CommonItem.h"

//PersonCell标识符
#define PersonCellIdentifier @"PersonCell"

@interface PersonCell()

/**
 *  标题
 */
@property (weak, nonatomic) IBOutlet UILabel *title;

/**
 *  描述
 */
@property (weak, nonatomic) IBOutlet UILabel *desc;

@end

@implementation PersonCell

/**
 *  初始化Xib
 *
 *  @param theCommonItem cell信息
 */
- (void)setTheCommonItem:(CommonItem *)theCommonItem
{
    _theCommonItem = theCommonItem;
    self.title.text = theCommonItem.title;
    self.desc.text = theCommonItem.desc;

}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:PersonCellIdentifier bundle:nil] forCellReuseIdentifier:PersonCellIdentifier];
    PersonCell *cell = [tableView dequeueReusableCellWithIdentifier:PersonCellIdentifier];
    return cell;
}


@end
