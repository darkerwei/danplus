//
//  ComplainTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/9/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ComplainTableViewCell.h"
#import "ComplaintModel.h"
#import "ComplainAttachment.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define ComplainTableViewCellIdentifier @"ComplainTableViewCell"

@interface ComplainTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UIImageView *thumnail;
@property (weak, nonatomic) IBOutlet UILabel *time;

@end


@implementation ComplainTableViewCell

- (void)setComplain:(ComplaintModel *)complain{

    _complain = complain;
    
    self.message.text = [NSString stringWithFormat:@"%@", self.complain.message];
//    投诉状态表，0-未处理，1-处理中，2-已处理。
    if ([self.complain.status isEqualToString:@"0"]) {
        self.status.text = @"未处理";
    } else if ([self.complain.status isEqualToString:@"1"]) {
        self.status.text = @"处理中";
    } else if ([self.complain.status isEqualToString:@"2"]) {
        self.status.text = @"已处理";
    }

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *issueTime = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.complain.dateline.doubleValue /1000.0]];
    self.time.text = [NSString stringWithFormat:@"%@", issueTime];
    self.time.font = [UIFont systemFontOfSize:12];
    
    
    ComplainAttachment *attach = [self.complain.attachments firstObject];
    [self.thumnail sd_setImageWithURL:[NSURL URLWithString:attach.thumbnail] placeholderImage:nil];
}


// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:ComplainTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:ComplainTableViewCellIdentifier];
    ComplainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ComplainTableViewCellIdentifier];
    
    return cell;
}

@end
