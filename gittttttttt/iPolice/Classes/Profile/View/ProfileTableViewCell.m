//
//  ProfileTableViewCell.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ProfileTableViewCell.h"
#import "CreditsModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UserInfo.h"

// CommonCell标识符
#define ProfileTableViewCellIdentifier @"ProfileTableViewCell"
@interface ProfileTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *creditsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *personImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *nickName;

@end
@implementation ProfileTableViewCell

- (void)setCredits:(CreditsModel *)credits{
    
    _credits = credits;
    
    if (credits.credit.integerValue > 1000) {
        self.creditsLabel.text = @"1000+";
    } else {
        self.creditsLabel.text = [NSString stringWithFormat:@"%d",credits.credit.intValue];
    }
    
    
}

//头像
- (void)setUserInforA:(UserInfo *)userInforA{
    
    _userInforA = userInforA;
    
    self.personImage.layer.cornerRadius = 30;
    self.personImage.clipsToBounds = YES;
    [self.personImage sd_setImageWithURL:[NSURL URLWithString:self.userInforA.avatar] placeholderImage:[UIImage imageNamed:@"默认头像.png"]];
}

//名称+签名
- (void)setUserInfor:(UserInfo *)userInfor{
    
    _userInfor = userInfor;
    
    if ([self.userInfor.nickname isEqualToString:@""]) {
        return;
    } else {
        self.userName.text = self.userInfor.nickname;
    }
    if ([self.userInfor.signature isEqualToString:@""]) {
        return;
    } else {
        self.nickName.text = self.userInfor.signature;
    }
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:ProfileTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:ProfileTableViewCellIdentifier];
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ProfileTableViewCellIdentifier];
    
    return cell;
}

// 我的积分按钮点击
- (IBAction)myIntegralBtnClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(myIntegralBtnClick)]) {
        [self.delegate myIntegralBtnClick];
    }
}

// 我的消息按钮点击
- (IBAction)myOrdersBtnClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(myOrdersBtnClick)]) {
        [self.delegate myOrdersBtnClick];
    }
}

// 我的朋友按钮点击
- (IBAction)shoppingCartBtnClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(shoppingCartBtnClick)]) {
        [self.delegate shoppingCartBtnClick];
    }
}

@end
