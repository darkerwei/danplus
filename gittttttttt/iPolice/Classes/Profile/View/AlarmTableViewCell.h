//
//  AlarmTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/9/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PostAlarm;

@interface AlarmTableViewCell : UITableViewCell

@property (nonatomic, strong) PostAlarm *alarm;

/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
