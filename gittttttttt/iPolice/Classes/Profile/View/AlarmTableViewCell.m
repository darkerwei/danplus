//
//  AlarmTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/9/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "AlarmTableViewCell.h"
#import "PostAlarm.h"
#import "PostAlarmAttachments.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define AlarmTableViewCellIdentifier @"AlarmTableViewCell"

@interface AlarmTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *typeImage;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UIImageView *alarmImage;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *time;

@end


@implementation AlarmTableViewCell

- (void)setAlarm:(PostAlarm *)alarm{
    
    _alarm = alarm;
    
//    //报警类型：0-智能报警；1-110报警；2-专线电话报警
//    if ([alarm.type isEqualToString:@"0"]) {
//        [self.typeImage setImage:[UIImage imageNamed:@"智能报警.png"]];
//        self.typeLabel.text = @"智能报警";
//    } else if ([alarm.type isEqualToString:@"1"]) {
//        [self.typeImage setImage:[UIImage imageNamed:@"110报警.png"]];
//        self.typeLabel.text = @"110报警.png";
//    } else if ([alarm.type isEqualToString:@"2"]) {
//        [self.typeImage setImage:[UIImage imageNamed:@"附近警局报警.png"]];
//        self.typeLabel.text = @"附近警局报警";
//    }
    
    //0-未处理，1-处理中，2-已处理，3-已结案
    if ([self.alarm.status isEqualToString:@"0"]) {
        self.status.text = @"未处理";
    } else if ([self.alarm.status isEqualToString:@"1"]) {
        self.status.text = @"处理中";
    } else if ([self.alarm.status isEqualToString:@"2"]) {
        self.status.text = @"已处理";
    } else if ([self.alarm.status isEqualToString:@"3"]) {
        self.status.text = @"已结案";
    }
    
    self.message.text = [NSString stringWithFormat:@"%@",self.alarm.message];
    
    PostAlarmAttachments *attach = [self.alarm.attachments firstObject];
    [self.alarmImage sd_setImageWithURL:[NSURL URLWithString:attach.thumbnail] placeholderImage:nil];
    
    self.address.text = self.alarm.location;
    
    //发布时间
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.alarm.dateline.doubleValue / 1000.0]];
    self.time.text = [NSString stringWithFormat:@"%@", regStr];

}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:AlarmTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:AlarmTableViewCellIdentifier];
    AlarmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AlarmTableViewCellIdentifier];
    
    return cell;
}
@end
