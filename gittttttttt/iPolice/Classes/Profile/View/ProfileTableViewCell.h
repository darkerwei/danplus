//
//  ProfileTableViewCell.h
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CreditsModel;
@class UserInfo;

@protocol ProfileTableViewCellDelegate <NSObject>

/**
 *  我的积分按钮点击
 */
- (void)myIntegralBtnClick;
/**
 *  我的订单按钮点击
 */
- (void)myOrdersBtnClick;
/**
 *  购物车按钮点击
 */
- (void)shoppingCartBtnClick;

@end

@interface ProfileTableViewCell : UITableViewCell

@property (nonatomic, assign) id<ProfileTableViewCellDelegate> delegate;

@property(nonatomic, strong) CreditsModel *credits;
@property (nonatomic, strong) UserInfo *userInfor;
@property (nonatomic, strong) UserInfo *userInforA;

/**
 *  快速创建一个cell
 *
 *  @param tableView 哪个tableView
 *
 *  @return 一个创建好的cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
