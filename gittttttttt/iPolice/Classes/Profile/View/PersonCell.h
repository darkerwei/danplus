//
//  PersonCell.h
//  iPolice
//
//  Created by xwy on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CommonItem;

@interface PersonCell : UITableViewCell

/**
 *  cell信息
 */
@property (nonatomic, strong) CommonItem *theCommonItem;

/**
 *  快速创建一个cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
