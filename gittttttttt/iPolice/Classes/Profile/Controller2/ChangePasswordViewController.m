//
//  ChangePasswordViewController.m
//  iPolice
//
//  Created by xwy on 15/9/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "UserInfo.h"
#import "RegularExpression.h"

@interface ChangePasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *oldPassword;
@property (weak, nonatomic) IBOutlet UITextField *newsPassword;
@property (weak, nonatomic) IBOutlet UITextField *surePassword;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"修改密码";
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    //右导航
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(clickRight)];
    self.navigationItem.rightBarButtonItem = right;
}

- (void)clickRight{
    
    if ([self.oldPassword.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"请输入旧密码"];
        return;
    } else if (![self.oldPassword.text isEqualToString:self.password]) {
        [SVProgressHUD showErrorWithStatus:@"旧密码错误"];
        return;
    }
    
    // 检测密码格式的正确性
    if (![RegularExpression isPassword:self.newsPassword.text]) {
        [SVProgressHUD showErrorWithStatus:@"新密码输入错误"];
        return;
    } else if ([self.newsPassword.text isEqualToString:self.password]){
        [SVProgressHUD showErrorWithStatus:@"与旧密码相同"];
        return;
    }
    
    // 检测密码和确认密码的一致性
    if (![self.newsPassword.text isEqualToString:self.surePassword.text]) {
        [SVProgressHUD showErrorWithStatus:@"两次密码输入不相同"];
        return;
    }
    
    UserInfo *user = [UserInfo userInfoWithNewPassword1:self.newsPassword.text newPassword2:self.surePassword.text];
    [[NetworkService sharedInstance] updatePasswordWithUserInfo:user success:^{
     
        [SVProgressHUD showSuccessWithStatus:@"密码修改成功"];
        [self.navigationController popViewControllerAnimated:YES];
     
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.oldPassword resignFirstResponder];
    [self.newsPassword resignFirstResponder];
    [self.surePassword resignFirstResponder];
}

@end
