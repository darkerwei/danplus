//
//  NicknameViewController.h
//  iPolice
//
//  Created by xwy on 15/9/16.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserInfo;
//typedef void(^configNameBlock)(NSString *name);
@interface NicknameViewController : UIViewController

@property (nonatomic, strong) UserInfo *nick;
//@property(copy, nonatomic) configNameBlock configNameBlock;

@end
