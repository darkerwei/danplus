//
//  SignViewController.h
//  iPolice
//
//  Created by xwy on 15/9/16.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserInfo;

typedef void(^configSignBlock)(NSString *sign);

@interface SignViewController : UIViewController
@property (nonatomic, strong) UserInfo *sign;

@property(copy, nonatomic) configSignBlock configSignBlock;

@end
