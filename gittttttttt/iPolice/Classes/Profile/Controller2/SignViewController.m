//
//  SignViewController.m
//  iPolice
//
//  Created by xwy on 15/9/16.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "SignViewController.h"
#import "UserInfo.h"

#define textMessage @"修改签名"
#define TextCountMax 200

@interface SignViewController ()<UITextViewDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) UITextView *textView;
@end

@implementation SignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"个性签名";
    
    //让scrollView自动适应屏幕
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 70, Main_Screen_Width-20, 200)];
    self.textView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.textView.text = textMessage;
    self.textView.textAlignment = NSTextAlignmentNatural;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    
    //为导航栏添加右侧按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    // 只显示<最大允许输入字数>个内容
    if (textView.text.length > TextCountMax) {
        textView.text = [textView.text substringToIndex:TextCountMax];
    }
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.textView resignFirstResponder];
}

//输入框代理
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:textMessage]) {
        textView.text = @"";
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = textMessage;
    }
    
    [textView resignFirstResponder];
}


- (void)doneClick{
    
    
    if (self.textView.text.length == 0 || [self.textView.text isEqualToString:textMessage]) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请修改正确签名" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        [alvert show];
        
    } else {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"系统提示!" message:@"确认修改?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        [alvert show];
        
        
    }
    
}


//UIAlvertView的响应事件
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        
    } else if (buttonIndex == 1){
        
        [SVProgressHUD showWithStatus:@"正在修改签名"];
        UserInfo *user = [UserInfo userInfoWithNickname:self.sign.nickname mobile:self.sign.mobile signature:self.textView.text];
        [[NetworkService sharedInstance] updateUserWithUserInfo:user success:^{
            
            [SVProgressHUD showSuccessWithStatus:@"签名修改成功"];
            
            NSDictionary *dic = @{@"nickname":@"", @"sign":self.textView.text};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateUserData" object:nil userInfo:dic];
            [self.navigationController popViewControllerAnimated:YES];
            
            NSLog(@"签名修改");
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        }];
        
        
        
    }
}


@end
