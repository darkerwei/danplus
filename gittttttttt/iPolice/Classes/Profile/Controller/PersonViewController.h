//
//  PersonViewController.h
//  iPolice
//
//  Created by xwy on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserInfo;

//typedef void(^configUserBlock)(NSString *user);
@interface PersonViewController : UIViewController

@property (nonatomic, strong) UserInfo *person1;
@property (nonatomic, strong) UserInfo *person2;

//@property (nonatomic, copy) configUserBlock configUserBlock;

@end
