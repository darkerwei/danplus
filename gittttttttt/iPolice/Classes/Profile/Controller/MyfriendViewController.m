//
//  MyfriendViewController.m
//  iPolice
//
//  Created by xwy on 15/9/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MyfriendViewController.h"

@interface MyfriendViewController ()

@end

@implementation MyfriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"我的好友";
    
    //设置背景图
    UIImage *image = [UIImage imageNamed:@"敬请期待.png"];
    self.view.layer.contents = (id) image.CGImage;
}


@end
