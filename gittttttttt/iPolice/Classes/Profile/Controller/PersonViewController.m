//
//  PersonViewController.m
//  iPolice
//
//  Created by xwy on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PersonViewController.h"
#import "PersonCell.h"
#import "CommonItem.h"
#import "CommonGroup.h"
#import "ChangePasswordViewController.h"
#import "UserInfo.h"
#import "NicknameViewController.h"
#import "SignViewController.h"

#import "toolsManager.h"

// Footer的高度
#define FooterHeight 0
// Header的高度
#define HeaderHeight 0

//cell信息
#define CommonItem1Title @"头像"
#define CommonItem2Title @"用户名"
#define CommonItem3Title @"昵称"
#define CommonItem4Title @"个性签名"
#define CommonItem5Title @"修改密码"

#import "PostImageView.h"

@interface PersonViewController ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) UITableView *tableView;
/**
 *  组列表
 */
@property (nonatomic, strong) NSArray *commonGroupList;
@property (nonatomic, strong) UIImageView *personImage;
@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UILabel *nickName;
@property (nonatomic, strong) UILabel *sign;

@end

@implementation PersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"个人信息";
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = YES;

    
    // 设置tableView属性
    self.tableView.sectionFooterHeight = FooterHeight;
    self.tableView.sectionHeaderHeight = HeaderHeight;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, Main_Screen_Width, Main_Screen_Height-100)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
    
    // 调整cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(-24, 0, 0, 0);
 
    //设置通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserData:) name:@"updateUserData" object:nil];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.commonGroupList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CommonGroup *theCommonGroup = self.commonGroupList[section];
    
    return theCommonGroup.commonItemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PersonCell *cell = [PersonCell cellWithTableView:tableView];
    CommonGroup *theCommonGroup = self.commonGroupList[indexPath.section];
    cell.theCommonItem = theCommonGroup.commonItemList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 1 && indexPath.row == 0) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        self.personImage = [[UIImageView alloc] initWithFrame:CGRectMake(Main_Screen_Width-80, 10, 50, 50)];
        self.personImage.clipsToBounds = YES;
        self.personImage.layer.cornerRadius = 25;
        [self.personImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", self.person1.avatar]]];
        cell.textLabel.text = @"头像";
        [cell addSubview:self.personImage];
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        self.userName = [[UILabel alloc] initWithFrame:CGRectMake(Main_Screen_Width-120, 10, 80, 30)];
        self.userName.text = [NSString stringWithFormat:@"%@", self.person2.username];
        self.userName.textAlignment = NSTextAlignmentRight;
        [cell addSubview:self.userName];
    } else if (indexPath.section == 1 && indexPath.row == 1) {
        self.nickName = [[UILabel alloc] initWithFrame:CGRectMake(Main_Screen_Width-120, 10, 80, 30)];
        self.nickName.text = [NSString stringWithFormat:@"%@", self.person2.nickname];
        self.nickName.textAlignment = NSTextAlignmentRight;
        [cell addSubview:self.nickName];
    } else if (indexPath.section == 1 && indexPath.row == 2) {
        self.sign = [[UILabel alloc] initWithFrame:CGRectMake(Main_Screen_Width-120, 10, 80, 30)];
        self.sign.text = [NSString stringWithFormat:@"%@", self.person2.signature];
        self.sign.textAlignment = NSTextAlignmentRight;
        [cell addSubview:self.sign];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        return 70;
    } else {
        return 50;
    }
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        NSLog(@"在这里处理<头像>点击事件");
        
        [[PostImageView sharedPostImageView] createActionSheetViewWithViewController:self title:@"选择一张图片作为头像" andMethod:^(UIImage *image, NSString *imagePath, NSData *imageData){
            [self rightOnClick:image imageData:imageData andImagePath:imagePath];
        }];

    } else if (indexPath.section == 1 && indexPath.row == 0) {
        NSLog(@"在这里处理<用户名>点击事件");
    } else if (indexPath.section == 1 && indexPath.row == 1) {
        NSLog(@"在这里处理<昵称>点击事件");
        NicknameViewController *nick = [[NicknameViewController alloc] init];
        nick.nick = self.person2;

        [self.navigationController pushViewController:nick animated:YES];
    }else if (indexPath.section == 1 && indexPath.row == 2) {
        NSLog(@"在这里处理<个性签名>点击事件");
        SignViewController *sign = [[SignViewController alloc] init];
        sign.sign = self.person2;
        
        [self.navigationController pushViewController:sign animated:YES];
    }else if (indexPath.section == 1 && indexPath.row == 3) {
        NSLog(@"在这里处理<修改密码>点击事件");
        ChangePasswordViewController *change = [[ChangePasswordViewController alloc] init];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        NSString *password = [defaults objectForKey:@"password"];
        change.password = password;
        [self.navigationController pushViewController:change animated:YES];
    }
}

- (void)updateUserData:(NSNotification *)noti {
  
    if ([noti.userInfo[@"nickname"] isEqualToString:@""]) {
        
        if ([noti.userInfo[@"sign"] isEqualToString:@""]) {
            return;
        } else {
            self.sign.text = noti.userInfo[@"sign"];
        }
    } else {
        self.nickName.text = noti.userInfo[@"nickname"];
    }
    
}

#pragma mark 上传头像
- (void)rightOnClick:(UIImage *)item imageData:(NSData *)imageData andImagePath:(NSString *)imagePath
{
    self.personImage.image = item;
    /**
     *  把图片上传到 阿里云服务器
     */
    [[toolsManager share] saveImageWithPath:imageData url:imagePath callback:^{
        
    }];
    /**
     *  获得 阿里云生成的图片的 uuid
     */
    NSString *uuid = [[toolsManager share] getUUID];
    
    //调用上传头像接口
    NSString *url = SERVER_URL(@"UCenter", @"setAvatar");
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSDictionary *params = @{@"aid":uuid,@"name":uuid,@"type":@"1", @"uri":[NSString stringWithFormat:@"http://oss-cn-beijing.aliyuncs.com/%@",uuid] ,@"description":@"设置头像",@"size":[NSNumber numberWithInt:0]};
    UserInfo *user = [UserInfo userInfoWithAttachment:params];
    NSDictionary *dic = @{@"token":[NSString stringWithFormat:@"%@",token], @"uid":[NSString stringWithFormat:@"%@",uid], @"attachment":[NSString stringWithFormat:@"%@", user.attachment]};
    NSLog(@"%@", dic);
    NSDictionary *params2 = @{@"condition":[NSString stringWithFormat:@"%@", dic]};
    NSLog(@"URL  =  %@   params = %@",url, params2);
    
    [[HttpClient sharedHTTPManager] POST:url parameters:params2 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@", dic);
        if ([dic[@"code"] intValue] == 0) {
            [SVProgressHUD showSuccessWithStatus:@"上传成功"];
        } else {
            [SVProgressHUD showErrorWithStatus:@"上传失败"];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == -1001) {
            [SVProgressHUD showErrorWithStatus:@"请求超时"];
        } else {
            [SVProgressHUD showErrorWithStatus:@"上传失败"];
        }
    }];

}

#pragma mark - 懒加载

- (NSArray *)commonGroupList
{
    if (_commonGroupList == nil) {
        
        // 创建组
        CommonGroup *commonGroup1 = [CommonGroup commonGroup];
        CommonGroup *commonGroup2 = [CommonGroup commonGroup];
        
        
        // 创建cell信息
        CommonItem *CommonItem1 = [CommonItem commonItemWithTitle:nil];
        commonGroup1.commonItemList = @[CommonItem1];
        
        CommonItem *CommonItem2 = [CommonItem commonItemWithTitle:CommonItem2Title];
        CommonItem *CommonItem3 = [CommonItem commonItemWithTitle:CommonItem3Title];
        CommonItem *CommonItem4 = [CommonItem commonItemWithTitle:CommonItem4Title];
        CommonItem *CommonItem5 = [CommonItem commonItemWithTitle:CommonItem5Title];
        commonGroup2.commonItemList = @[CommonItem2, CommonItem3, CommonItem4, CommonItem5];
        
        
        _commonGroupList = @[commonGroup1, commonGroup2];
    }
    
    return _commonGroupList;
}
@end
