//
//  MyAlarmViewController.m
//  iPolice
//
//  Created by xwy on 15/9/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MyAlarmViewController.h"
#import "PostAlarm.h"
#import "AlarmTableViewCell.h"

@interface MyAlarmViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
/**
 *  视频列表
 */
@property (nonatomic, strong) NSArray *alarmsList;

/**
 *  服务器一次返回视频数量
 */
@property (nonatomic, assign) int limit;
@end

@implementation MyAlarmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"报警记录";
   
    self.automaticallyAdjustsScrollViewInsets = NO;
    // 去除多余cell
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, Main_Screen_Height-64)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];

}
/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    
    [SVProgressHUD showInfoWithStatus:@"正在获取报警列表"];
    PostAlarm *alarms = [PostAlarm postAlarmWithStatus:@"" start:@"" limit:[NSString stringWithFormat:@"%d", self.limit]];
    [[NetworkService sharedInstance] getAlarmsWithPostAlarm:alarms success:^(NSArray *alarmList) {
        [SVProgressHUD showSuccessWithStatus:@"获取报警列表成功"];
        
        //满10个才显示上拉刷新
        if (alarmList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (alarmList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        
        self.alarmsList = alarmList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
        
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
    
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    [SVProgressHUD showInfoWithStatus:@"正在获取报警列表"];
    PostAlarm *alarms = [PostAlarm postAlarmWithStatus:@"" start:@"" limit:@""];
    [[NetworkService sharedInstance] getAlarmsWithPostAlarm:alarms success:^(NSArray *alarmList) {
        [SVProgressHUD showSuccessWithStatus:@"获取报警列表成功"];
        
        //满10个才显示上拉刷新
        if (alarmList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取报警列表成功"];
        } else if (alarmList.count < self.limit-10) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的记录了"];
        }
        
        self.alarmsList = alarmList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
        
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.alarmsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlarmTableViewCell *cell = [AlarmTableViewCell cellWithTableView:tableView];
    cell.alarm = self.alarmsList[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    VideoInfo *theVideoInfo = [VideoInfo videoInfo];
//    theVideoInfo.vid = [self.videoInfoList[indexPath.row] vid];
//    
//    [[NetworkService sharedInstance] getVideoDetailWithVideoInfo:theVideoInfo success:^(VideoInfo *videoInfo) {
//        
//        NSLog(@"%@", videoInfo);
//        
//    } failure:^(NSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
//    }];
//    
//    VideoViewController *video = [[VideoViewController alloc] init];
//    [self.navigationController pushViewController:video animated:YES];
//    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}
@end
