//
//  ProfileViewController.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ProfileViewController.h"
#import "ProfileTableViewCell.h"
#import "CommonCell.h"
#import "CommonItem.h"
#import "CommonGroup.h"
#import "SetViewController.h"
#import "PersonViewController.h"
#import "CreditsModel.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import "UserInfo.h"
#import "MyCreditViewController.h"
#import "MyAlarmViewController.h"
#import "MyComplainViewController.h"
#import "MymessageViewController.h"
#import "MyfriendViewController.h"

// 头像cell高度
#define ProfileTableViewCellHeight 180
// cell高度
#define CellHeight 44
// Footer的高度
#define FooterHeight 0
// Header的高度
#define HeaderHeight 2

// cell信息
//#define CommonItem1Title @"我的地址"
//#define CommonItem1TitleImageName @"我的地址图标"
//#define CommonItem1Description @"地址管理"

#define CommonItem2Title @"我的报警"
#define CommonItem2TitleImageName @"我的报警"
#define CommonItem2Description @"历史报警记录"
#define CommonItem3Title @"我的投诉"
#define CommonItem3TitleImageName @"我要投诉"
#define CommonItem3Description @"我的投诉记录"
#define CommonItem4Title @"我的设置"
#define CommonItem4TitleImageName @"我的设置图标"
#define CommonItem4Description @"功能介绍、意见反馈"

@interface ProfileViewController ()<UITableViewDataSource, UITableViewDelegate, ProfileTableViewCellDelegate>

/**
 *  整个页面
 */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/**
 *  组列表
 */
@property (nonatomic, strong) NSArray *commonGroupList;

@property (nonatomic, strong) CreditsModel *creditsM;
@property (nonatomic, strong) UserInfo *userInfo;
@property (nonatomic, strong) UserInfo *userInfoM;

@end

@implementation ProfileViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self getAvatar];
    [self getUserInfor];
    [self getCredits];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置tableView属性
    self.tableView.sectionFooterHeight = FooterHeight;
    self.tableView.sectionHeaderHeight = HeaderHeight;
    
    // 调整cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
    self.tableView.bounces = NO;

}

//获取用户头像
- (void)getAvatar{
    
    UserInfo *userInfo = [UserInfo userInfoWithSize:@"small"];
    [[NetworkService sharedInstance] getAvatarUserInfo:userInfo success:^(UserInfo *userInfo) {
        
        self.userInfo = userInfo;
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

//获取用户信息
- (void)getUserInfor{
    
    UserInfo *userInfo = [UserInfo userInfo];
    
    [[NetworkService sharedInstance] getUserWithUserInfo:userInfo success:^(UserInfo *userInfo) {
        
        self.userInfoM = userInfo;
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

//我的积分
- (void)getCredits{
    
    CreditsModel *credits = [CreditsModel credits];
    [[NetworkService sharedInstance] getCreditsWithMyCredits:credits success:^(CreditsModel *credits) {
        
        self.creditsM = credits;
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.commonGroupList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CommonGroup *theCommonGroup = self.commonGroupList[section];
    
    return theCommonGroup.commonItemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && indexPath.section == 0) {
        ProfileTableViewCell *cell = [ProfileTableViewCell cellWithTableView:tableView];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.credits = self.creditsM;
        
        //页面传值
        cell.userInfor = self.userInfoM;
        
        //头像传值
        cell.userInforA = self.userInfo;
        
        return cell;
    } else {
        CommonCell *cell = [CommonCell cellWithTableView:tableView];
        CommonGroup *theCommonGroup = self.commonGroupList[indexPath.section];
        cell.theCommonItem = theCommonGroup.commonItemList[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        NSLog(@"在这里处理<个人中心>点击事件");
        PersonViewController *person = [[PersonViewController alloc] init];
        person.person1 = self.userInfo;
        person.person2 = self.userInfoM;
        [self.navigationController pushViewController:person animated:YES];
    }
    
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        NSLog(@"在这里处理<我的报警>点击事件");
        MyAlarmViewController *alarms = [[MyAlarmViewController alloc] init];
        [self.navigationController pushViewController:alarms animated:YES];
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        NSLog(@"在这里处理<我的投诉>点击事件");
        MyComplainViewController *complain = [[MyComplainViewController alloc] init];
        [self.navigationController pushViewController:complain animated:YES];
    } else if (indexPath.section == 3 && indexPath.row == 0) {
        NSLog(@"在这里处理<我的设置>点击事件");
        SetViewController *setVC = [[SetViewController alloc] init];
        [self.navigationController pushViewController:setVC animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && indexPath.section == 0) {
        return ProfileTableViewCellHeight;
    }
    return CellHeight;
}

#pragma mark - ProfileTableViewCellDelegate

- (void)myIntegralBtnClick
{
    NSLog(@"在这里处理<我的积分>点击事件");
    MyCreditViewController *credit = [[MyCreditViewController alloc] init];
    credit.title = @"积分商城";
    [self.navigationController pushViewController:credit animated:YES];
}

- (void)myOrdersBtnClick{
    
    NSLog(@"在这里处理<我的消息>点击事件");
    MymessageViewController *message = [[MymessageViewController alloc] init];
    [self.navigationController pushViewController:message animated:YES];
    
}

- (void)shoppingCartBtnClick{
    
    NSLog(@"在这里处理<我的好友>点击事件");
    MyfriendViewController *friend = [[MyfriendViewController alloc] init];
    [self.navigationController pushViewController:friend animated:YES];
}


#pragma mark - 懒加载

- (NSArray *)commonGroupList
{
    if (_commonGroupList == nil) {
        
        // 创建组
        CommonGroup *commonGroup1 = [CommonGroup commonGroup];
        //        CommonGroup *commonGroup2 = [CommonGroup commonGroup];
        CommonGroup *commonGroup3 = [CommonGroup commonGroup];
        CommonGroup *commonGroup4 = [CommonGroup commonGroup];
        CommonGroup *commonGroup5 = [CommonGroup commonGroup];
        
        // 创建cell信息
        CommonItem *CommonItem1 = [[CommonItem alloc] init];
        commonGroup1.commonItemList = @[CommonItem1];
        
        //        CommonItem *CommonItem2 = [CommonItem commonItemWithTitle:CommonItem1Title titleImageName:CommonItem1TitleImageName description:CommonItem1Description];
        //        commonGroup2.commonItemList = @[CommonItem2];
        
        CommonItem *CommonItem3 = [CommonItem commonItemWithTitle:CommonItem2Title titleImageName:CommonItem2TitleImageName description:CommonItem2Description];
        commonGroup3.commonItemList = @[CommonItem3];
        
        CommonItem *CommonItem4 = [CommonItem commonItemWithTitle:CommonItem3Title titleImageName:CommonItem3TitleImageName description:CommonItem3Description];
        commonGroup4.commonItemList = @[CommonItem4];
        
        CommonItem *CommonItem5 = [CommonItem commonItemWithTitle:CommonItem4Title titleImageName:CommonItem4TitleImageName description:CommonItem4Description];
        commonGroup5.commonItemList = @[CommonItem5];
        
        _commonGroupList = @[commonGroup1, commonGroup3, commonGroup4, commonGroup5];
    }
    return _commonGroupList;
}

@end
