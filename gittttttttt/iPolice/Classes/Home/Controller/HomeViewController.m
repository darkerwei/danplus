//
//  HomeViewController.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "HomeViewController.h"
#import "ServiceCollectionViewCell.h"
#import "Service.h"
#import "QueryServiceCollectionViewCell.h"
#import "QueryService.h"
#import "ClueViewController.h"
#import "MailboxViewController.h"
#import "ConsultingViewController.h"
#import "PoliceViewController.h"
#import "NearVideoViewController.h"
#import "CarQueryViewController.h"
#import "DriverQueryViewController.h"
#import "DuplicationNameQueryViewController.h"
#import <MAMapKit.h>
#import "CycleImageCell.h"
#import "NetworkService.h"
#import "Threads.h"
#import <SVProgressHUD.h>
#import "SnapShotViewController.h"
#import "NewsViewController.h"
#import "SecureViewController.h"
#import "ClueViewController.h"
#import "MyTagViewController.h"
#import "InformViewController.h"
#import "ComplainViewController.h"
#import "ShopViewController.h"
#import "CreditsModel.h"
#import "ThreadDetailViewController.h"
#import "AdsInfo.h"
#import "NewsDetailViewController.h"

// 图片轮播高度
#define PageViewHeight ((Main_Screen_Height - 129) * 0.25)
// 服务Cell高度
#define ServiceCollectionViewCellHeight ((PageViewHeight  + 20) * 0.5)
// 服务Cell宽度
#define ServiceCollectionViewCellWidth (Main_Screen_Width * 0.2 + 8)
// 搜索服务Cell高度
#define QueryServiceCollectionViewCellHeight ((PageViewHeight * 2 - 78) *0.25)
// 搜索服务Cell宽度
#define QueryServiceCollectionViewCellWidth (Main_Screen_Width * 0.5 - 5)
// 第一条线的Y值
#define FirstLineY (Main_Screen_Height - 9 - 2 *PageViewHeight)

// 服务cell信息
#define Service1Title @"新闻资讯"
#define Service1ImageName @"新闻资讯"
#define Service2Title @"周边视频"
#define Service2ImageName @"周边视频"
#define Service3Title @"我要报警"
#define Service3ImageName @"我要报警"
#define Service4Title @"业务咨询"
#define Service4ImageName @"业务咨询"
#define Service5Title @"治安指数"
#define Service5ImageName @"治安指数"
#define Service6Title @"不文明曝光"
#define Service6ImageName @"一键抓拍"
#define Service7Title @"网上通缉"
#define Service7ImageName @"曝光台"
#define Service8Title @"线索举报"
#define Service8ImageName @"我有线索"
#define Service9Title @"协助挪车"
#define Service9ImageName @"协助挪车"
#define Service10Title @"身份证辨别真伪"
#define Service10ImageName @"身份证辨别真伪"

// 热门服务cell信息
#define queryService1Title @"我的标签"
#define queryService1TitleImageName @"智慧生活"
#define queryService1Description @"随时了解电动车位置"
#define queryService2Title @"积分商城"
#define queryService2TitleImageName @"积分商城"
#define queryService2Description @"积分好礼换不停"
#define queryService3Title @"局长信箱"
#define queryService3TitleImageName @"局长信箱"
#define queryService3Description @"连线局长举报投诉随时报"
#define queryService4Title @"我要投诉"
#define queryService4TitleImageName @"我要投诉"
#define queryService4Description @"投诉执法部门的快速通道"
#define queryService5Title @"驾驶人查询"
#define queryService5TitleImageName @"驾驶人查询"
#define queryService5Description @"您的驾驶信息一目了然"
#define queryService6Title @"机动车查询"
#define queryService6TitleImageName @"机动车查询"
#define queryService6Description @"机动车信息快速查询"
#define queryService8Title @"违章查询"
#define queryService8TitleImageName @"违章查询"
#define queryService8Description @"机动车违章查询"
#define queryService7Title @"重名查询"
#define queryService7TitleImageName @"重名查询"
#define queryService7Description @"新生儿起名重名查询"

#define queryService9Title @"协助挪车"
#define queryService9TitleImageName @"协助挪车"
#define queryService9Description @"让人挪车点这里"
#define queryService10Title @"身份证辨别真伪"
#define queryService10TitleImageName @"身份证辨别真伪"
#define queryService10Description @"真假身份证，一辩便知"

typedef NS_ENUM(NSUInteger, CollectionViewType) {
    CollectionViewTypeService = 11, // 服务栏tag
    CollectionViewTypeQueryService,  // 热门服务栏tag
    CollectionViewTypeCycle // 轮播tag
};

@interface HomeViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MAMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondBottomViewConstraintWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstBottonViewConstraintWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstBottonViewConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondBottonViewConstraintHeight;
@property (weak, nonatomic) IBOutlet UILabel *remenLabel;

/**
 *  服务栏
 */
@property (weak, nonatomic) IBOutlet UICollectionView *serviceCollectionView;
/**
 *  服务列表
 */
@property (nonatomic, strong) NSArray *servicesList;
/**
 *  热门服务栏
 */
@property (weak, nonatomic) IBOutlet UICollectionView *queryServiceCollectionView;
/**
 *  热门服务列表
 */
@property (nonatomic, strong) NSArray *queryServicesList;
/**
 *  地图
 */
@property (nonatomic, strong) MAMapView *mapView;

@property (nonatomic, assign) CGFloat mapx;
@property (nonatomic, assign) CGFloat mapy;

/**
 *  轮播
 */
@property (strong, nonatomic) UICollectionView *cycleCollectionView;
@property (strong, nonatomic) UICollectionViewFlowLayout *cycleCollectionViewFlowLayout;
/**
 *  索引
 */
@property (nonatomic, assign) NSUInteger currentIndex;
@property (nonatomic, strong) NSArray *imageURLsList;
@property (nonatomic, strong) NSArray *subjectsList;

@property (nonatomic, strong) NSArray *photoArray1;
@property (nonatomic, strong) NSArray *photoArray2;


@property (nonatomic, strong) NSTimer *timer;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (nonatomic, assign) BOOL isFirstCurrentPage;

@property (nonatomic, strong) NSMutableArray *imageList;
@property (nonatomic, strong) NSMutableArray *subjectList;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.firstBottonViewConstraintWidth.constant = Main_Screen_Width;
    self.secondBottomViewConstraintWidth.constant = Main_Screen_Width;
    
    if (iPhone4) {
        self.firstBottonViewConstraintHeight.constant = 240;
        self.secondBottonViewConstraintHeight.constant = 210;
    }
    if (iPhone5) {
        self.firstBottonViewConstraintHeight.constant = 260;
        self.secondBottonViewConstraintHeight.constant = 260;
    }
    if (iPhone6P) {
        self.firstBottonViewConstraintHeight.constant = 380;
        self.secondBottonViewConstraintHeight.constant = 380;
    }

    self.scrollView.contentSize = CGSizeMake(0, self.firstBottonViewConstraintHeight.constant+self.secondBottonViewConstraintHeight.constant);
    
    
    self.cycleCollectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    // 设置界面布局
    self.cycleCollectionViewFlowLayout.itemSize = CGSizeMake(Main_Screen_Width, PageViewHeight);
    self.cycleCollectionViewFlowLayout.minimumInteritemSpacing = 0;
    self.cycleCollectionViewFlowLayout.minimumLineSpacing = 0;
    self.cycleCollectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.cycleCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, PageViewHeight) collectionViewLayout:self.cycleCollectionViewFlowLayout];
    self.cycleCollectionView.delegate = self;
    self.cycleCollectionView.dataSource = self;
    self.cycleCollectionView.tag = CollectionViewTypeCycle;
    self.cycleCollectionView.showsHorizontalScrollIndicator = NO;
    self.cycleCollectionView.pagingEnabled = YES;
    [self.scrollView addSubview:self.cycleCollectionView];
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(Main_Screen_Width - 70, CGRectGetMaxY(self.cycleCollectionView.frame) - 30, 70, 30)];
    // 设置pageControl属性
    self.pageControl.numberOfPages = 7;
    self.pageControl.userInteractionEnabled = NO;
    self.pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    self.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.pageControl];
    
    self.currentIndex = 0;
    
    [self getThreads];
    
    self.isFirstCurrentPage = YES;

    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    
    
    
    // 首页分割线
    UIView *line1= [[UIView alloc] initWithFrame:CGRectMake(0, self.firstBottonViewConstraintHeight.constant, Main_Screen_Width, 1)];
    line1.backgroundColor = [UIColor lightGrayColor];
    [self.scrollView addSubview:line1];

    UIView *line= [[UIView alloc] initWithFrame:CGRectMake(0, self.firstBottonViewConstraintHeight.constant+CGRectGetMaxY(self.remenLabel.frame), Main_Screen_Width, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self.scrollView addSubview:line];
//
    UIView *line2= [[UIView alloc] initWithFrame:CGRectMake(0, self.firstBottonViewConstraintHeight.constant+CGRectGetMaxY(self.remenLabel.frame) + (QueryServiceCollectionViewCellHeight+10), Main_Screen_Width, 1)];
    line2.backgroundColor = [UIColor lightGrayColor];
    [self.scrollView addSubview:line2];
//
    UIView *line3= [[UIView alloc] initWithFrame:CGRectMake(0, self.firstBottonViewConstraintHeight.constant+CGRectGetMaxY(self.remenLabel.frame) + (QueryServiceCollectionViewCellHeight+10)*2, Main_Screen_Width, 1)];
    line3.backgroundColor = [UIColor lightGrayColor];
    [self.scrollView addSubview:line3];
//
    UIView *line4= [[UIView alloc] initWithFrame:CGRectMake(0, self.firstBottonViewConstraintHeight.constant+CGRectGetMaxY(self.remenLabel.frame) + (QueryServiceCollectionViewCellHeight+10)*3, Main_Screen_Width, 1)];
    line4.backgroundColor = [UIColor lightGrayColor];
    [self.scrollView addSubview:line4];
    
    UIView *line5= [[UIView alloc] initWithFrame:CGRectMake(0, self.firstBottonViewConstraintHeight.constant+CGRectGetMaxY(self.remenLabel.frame) + (QueryServiceCollectionViewCellHeight+10)*4, Main_Screen_Width, 1)];
    line5.backgroundColor = [UIColor lightGrayColor];
    [self.scrollView addSubview:line5];
}

- (void)getThreads
{
    Threads *theThreads = [Threads threadsWithCid:@"" status:@"0" style:@"1" type:@"" start:@"0" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {

        self.imageList = [NSMutableArray array];
        self.subjectList = [NSMutableArray array];
        
        //遍历前5个新闻页面
        for (Threads *theThreads in threadList) {
            if (theThreads.thumbnail && theThreads.subject) {
                if (self.imageList.count == 5) {
                    break;
                }
                self.photoArray1 = threadList;
                
                [self.imageList addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@@1200w_800h_90q.jpg", theThreads.thumbnail]]];
                [self.subjectList addObject:theThreads.subject];
            }
        }
        
        AdsInfo *ads = [AdsInfo adsInfoWithPlaceid:@"" available:@"" start:@"" limit:@""];
        [[NetworkService alloc] getAdsWithAds:ads success:^(NSArray *adsList) {
            
            //遍历前2个广告页面
            for (AdsInfo *ads in adsList) {
                if (ads.thumbnail && ads.subject) {
                    if (self.imageList.count == 2) {
                        break;
                    }
                    self.photoArray2 = adsList;
                    
                    [self.imageList addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@", ads.thumbnail]]];
                    [self.subjectList addObject:ads.subject];
                }
            }

            self.imageURLsList = [NSArray arrayWithArray:self.imageList];
            self.subjectsList = [NSArray arrayWithArray:self.subjectList];
            [self.cycleCollectionView reloadData];
            
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        }];

    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self startTimer];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - UICollectionViewDatasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == CollectionViewTypeService) {
        return self.servicesList.count;
    } else if (collectionView.tag == CollectionViewTypeCycle) {
        return 7;
    } else {
        return self.queryServicesList.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == CollectionViewTypeService) {
        Service *theService = self.servicesList[indexPath.row];
        ServiceCollectionViewCell* cell = [ServiceCollectionViewCell cellWithCollectionView:collectionView indexPath:indexPath];
        cell.service = theService;
        
        return cell;
    } else if (collectionView.tag == CollectionViewTypeCycle) {
        CycleImageCell *cell = [CycleImageCell cellWithCollectionView:collectionView indexPath:indexPath];
        cell.imageURL = self.imageURLsList[[self indexWithOffset:indexPath.item]];
        cell.subject = self.subjectsList[[self indexWithOffset:indexPath.item]];
        self.pageControl.currentPage = [self indexWithOffset:indexPath.item];
        return cell;
    } else {
        QueryService *theQueryService = self.queryServicesList[indexPath.row];
        QueryServiceCollectionViewCell* cell = [QueryServiceCollectionViewCell cellWithCollectionView:collectionView indexPath:indexPath];
        cell.queryService = theQueryService;
        
        return cell;
    }
}

- (NSUInteger)indexWithOffset:(NSUInteger)offset {
    return (self.currentIndex + offset - 1 + 7) % 7;
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView.tag == CollectionViewTypeQueryService) {
        return UIEdgeInsetsMake(5, 0, 10, 0);
    } else {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == CollectionViewTypeService) {
        return CGSizeMake(ServiceCollectionViewCellWidth, ServiceCollectionViewCellHeight);
    } else if (collectionView.tag == CollectionViewTypeCycle) {
        return CGSizeMake(Main_Screen_Width, PageViewHeight);
    } else {
        return CGSizeMake(QueryServiceCollectionViewCellWidth, QueryServiceCollectionViewCellHeight);
    }
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == CollectionViewTypeCycle){
        NSLog(@"在这里处理<新闻轮播>");
        
        // 图片轮播进详情 有bug
        if (self.pageControl.currentPage < 5) {
            ThreadDetailViewController *threadDetailVC = [[ThreadDetailViewController alloc] init];
            threadDetailVC.theThreads = self.photoArray1[[self indexWithOffset:indexPath.item]];
            [self.navigationController pushViewController:threadDetailVC animated:YES];
  
        } else if (self.pageControl.currentPage >= 5 && self.pageControl.currentPage <= 6) {
            NewsDetailViewController *newsDetailVC = [[NewsDetailViewController alloc] init];
            newsDetailVC.detail = self.photoArray2[[self indexWithOffset:indexPath.item-5]];
            [self.navigationController pushViewController:newsDetailVC animated:YES];
    
        }
        
    }
    
    if (collectionView.tag == CollectionViewTypeService) {
        if (indexPath.row == 0) {
            NSLog(@"在这里处理<新闻资讯>点击事件");
            
            NewsViewController *newsVC = [[NewsViewController alloc] init];
            [self.navigationController pushViewController:newsVC animated:YES];
        } else if (indexPath.row == 1) {
            NSLog(@"在这里处理<周边视频>点击事件");
            
            NearVideoViewController *nearVideoVC= [[NearVideoViewController alloc] init];
            nearVideoVC.mapx = self.mapx;
            nearVideoVC.mapy = self.mapy;
            [self.navigationController pushViewController:nearVideoVC animated:YES];
        } else if (indexPath.row == 2) {
            NSLog(@"在这里处理<我要报警>点击事件");
            
            PoliceViewController *callPoliceVC = [[PoliceViewController alloc] initWithNibName:@"PoliceViewController" bundle:nil];
            [self.navigationController pushViewController:callPoliceVC animated:YES];
        } else if (indexPath.row == 3) {
            NSLog(@"在这里处理<业务咨询>点击事件");
            
            ConsultingViewController *consultingVC = [[ConsultingViewController alloc] init];
            [self.navigationController pushViewController:consultingVC animated:YES];
        } else if (indexPath.row == 4) {
            NSLog(@"在这里处理<治安指数>点击事件");
            
            SecureViewController *secure = [[SecureViewController alloc] init];
            [self.navigationController pushViewController:secure animated:YES];
            
        } else if (indexPath.row == 5) {
            NSLog(@"在这里处理<不文明曝光>点击事件");
            
            SnapShotViewController *snapShotVC = [[SnapShotViewController alloc] init];
            [self.navigationController pushViewController:snapShotVC animated:YES];
        } else if (indexPath.row == 6) {
            NSLog(@"在这里处理<网上通缉>点击事件");
            ClueViewController *clue = [[ClueViewController alloc] init];
            [self.navigationController pushViewController:clue animated:YES];
            
        } else if (indexPath.row == 7) {
            NSLog(@"在这里处理<线索举报>点击事件");
            
            InformViewController *inform = [[InformViewController alloc] init];
            [self.navigationController pushViewController:inform animated:YES];
        }
    } else if (collectionView.tag == CollectionViewTypeQueryService){
        if (indexPath.row == 0) {
            NSLog(@"在这里处理<我的标签>点击事件");
            
            MyTagViewController *myTag = [[MyTagViewController alloc] init];
            [self.navigationController pushViewController:myTag animated:YES];
        } else if (indexPath.row == 1) {
            NSLog(@"在这里处理<积分商城>点击事件");
            
            ShopViewController *shopVC = [[ShopViewController alloc] init];
            shopVC.title = @"积分商城";
            [self.navigationController pushViewController:shopVC animated:YES];
        } else if (indexPath.row == 2) {
            NSLog(@"在这里处理<局长信箱>点击事件");
            
           MailboxViewController *mailBoxVC = [[MailboxViewController alloc] init];
           mailBoxVC.title = @"局长信箱";
           [self.navigationController pushViewController:mailBoxVC animated:YES];
       } else if (indexPath.row == 3) {
           NSLog(@"在这里处理<我要投诉>点击事件");
           
           ComplainViewController *complainVC = [[ComplainViewController alloc] init];
           complainVC.title = @"我要投诉";
           [self.navigationController pushViewController:complainVC animated:YES];
       } else if (indexPath.row == 4) {
           NSLog(@"在这里处理<驾驶人查询>点击事件");
           
           DriverQueryViewController *driverQueryVC = [[DriverQueryViewController alloc] init];
           [self.navigationController pushViewController:driverQueryVC animated:YES];
       } else if (indexPath.row == 5) {
           NSLog(@"在这里处理<机动车查询>点击事件");
           
           CarQueryViewController *carQueryVC = [[CarQueryViewController alloc] init];
           carQueryVC.carQueryViewControllerType = CarQueryViewControllerTypeCar;
           [self.navigationController pushViewController:carQueryVC animated:YES];
       } else if (indexPath.row == 7) {
           NSLog(@"在这里处理<违章查询>点击事件");
           
           CarQueryViewController *trafficOffenceQueryVC = [[CarQueryViewController alloc] init];
           trafficOffenceQueryVC.carQueryViewControllerType = CarQueryViewControllerTypeTrafficOffence;
           [self.navigationController pushViewController:trafficOffenceQueryVC animated:YES];
       } else if (indexPath.row == 6) {
           NSLog(@"在这里处理<重名查询>点击事件");
           
           DuplicationNameQueryViewController *duplicationNameQueryVC = [[DuplicationNameQueryViewController alloc] init];
           [self.navigationController pushViewController:duplicationNameQueryVC animated:YES];
       }
   }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.tag == CollectionViewTypeCycle) {
        NSUInteger offset = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        if (self.isFirstCurrentPage == NO) {
            if (offset == 0) { // 向左滚动
                if (self.pageControl.currentPage == 0) { // 处理左边界
                    self.pageControl.currentPage = 7 - 1;
                } else {
                    self.pageControl.currentPage--;
                }
            } else { // 向右滚动
                if (self.pageControl.currentPage == 7 - 1) { // 处理右边界
                    self.pageControl.currentPage = 0;
                } else {
                    self.pageControl.currentPage++;
                }
            }
        } else {
            self.isFirstCurrentPage = NO;
        }
        
        self.currentIndex = [self indexWithOffset:offset];
        
        // 滚动到第一张图片
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
        [self.cycleCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        
        [UIView setAnimationsEnabled:NO];
        [self.cycleCollectionView reloadItemsAtIndexPaths:@[indexPath]];
        [UIView setAnimationsEnabled:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView.tag == CollectionViewTypeCycle) {
        [self stopTimer];
    }
}

- (void)stopTimer {
    if (self.timer == nil) {
        return;
    }
    [self.timer invalidate];
    self.timer = nil;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView.tag == CollectionViewTypeCycle) {
        [self startTimer];
    }
}

- (void)startTimer {
    if (self.timer) {
        return;
    }
    self.timer = [NSTimer timerWithTimeInterval:3.0 target:self selector:@selector(roll) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)roll {
    // 取出当前显示cell
    NSIndexPath *indexPath = [self.cycleCollectionView indexPathsForVisibleItems].lastObject;
    
    // 显示下一张
    [self.cycleCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:(indexPath.item + 1) inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self scrollViewDidEndDecelerating:self.cycleCollectionView];
    });
}

#pragma mark - MAMapViewDelegate

-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
//        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
        self.mapx = userLocation.coordinate.latitude;
        self.mapy = userLocation.coordinate.longitude;
        
        [self performSelector:@selector(close) withObject:nil afterDelay:5.0f];
    }
}

- (void)close
{
    self.mapView.showsUserLocation = NO;
}

#pragma mark - 懒加载

- (NSArray *)servicesList
{
    if (_servicesList == nil) {
        Service *Service1 = [Service serviceWithServiceTitle:Service1Title serviceImageName:Service1ImageName];
        Service *Service2 = [Service serviceWithServiceTitle:Service2Title serviceImageName:Service2ImageName];
        Service *Service3 = [Service serviceWithServiceTitle:Service3Title serviceImageName:Service3ImageName];
        Service *Service4 = [Service serviceWithServiceTitle:Service4Title serviceImageName:Service4ImageName];
        Service *Service5 = [Service serviceWithServiceTitle:Service5Title serviceImageName:Service5ImageName];
        Service *Service6 = [Service serviceWithServiceTitle:Service6Title serviceImageName:Service6ImageName];
        Service *Service7 = [Service serviceWithServiceTitle:Service7Title serviceImageName:Service7ImageName];
        Service *Service8 = [Service serviceWithServiceTitle:Service8Title serviceImageName:Service8ImageName];
        Service *Service9 = [Service serviceWithServiceTitle:Service9Title serviceImageName:Service9ImageName];
        Service *Service10 = [Service serviceWithServiceTitle:Service10Title serviceImageName:Service10ImageName];
        
        _servicesList = @[Service1, Service2, Service3, Service4, Service5, Service6, Service7, Service8, Service9,Service10];
        
    }
    return _servicesList;
}

- (NSArray *)queryServicesList
{
    if (_queryServicesList == nil) {
        QueryService *queryService1 = [QueryService queryServiceWithqueryServiceTitle:queryService1Title queryServiceDesc:queryService1Description queryServiceImageName:queryService1TitleImageName];
        QueryService *queryService2 = [QueryService queryServiceWithqueryServiceTitle:queryService2Title queryServiceDesc:queryService2Description queryServiceImageName:queryService2TitleImageName];
        QueryService *queryService3 = [QueryService queryServiceWithqueryServiceTitle:queryService3Title queryServiceDesc:queryService3Description queryServiceImageName:queryService3TitleImageName];
        QueryService *queryService4 = [QueryService queryServiceWithqueryServiceTitle:queryService4Title queryServiceDesc:queryService4Description queryServiceImageName:queryService4TitleImageName];
        QueryService *queryService5 = [QueryService queryServiceWithqueryServiceTitle:queryService5Title queryServiceDesc:queryService5Description queryServiceImageName:queryService5TitleImageName];
        QueryService *queryService6 = [QueryService queryServiceWithqueryServiceTitle:queryService6Title queryServiceDesc:queryService6Description queryServiceImageName:queryService6TitleImageName];
        QueryService *queryService7 = [QueryService queryServiceWithqueryServiceTitle:queryService7Title queryServiceDesc:queryService7Description queryServiceImageName:queryService7TitleImageName];
        QueryService *queryService8 = [QueryService queryServiceWithqueryServiceTitle:queryService8Title queryServiceDesc:queryService8Description queryServiceImageName:queryService8TitleImageName];
        QueryService *queryService9 = [QueryService queryServiceWithqueryServiceTitle:queryService9Title queryServiceDesc:queryService8Description queryServiceImageName:queryService9TitleImageName];
        QueryService *queryService10 = [QueryService queryServiceWithqueryServiceTitle:queryService10Title queryServiceDesc:queryService8Description queryServiceImageName:queryService10TitleImageName];
        
        _queryServicesList = @[queryService1, queryService2, queryService3, queryService4, queryService5, queryService6, queryService7, queryService8, queryService9, queryService10];
    }
    return _queryServicesList;
}

@end
