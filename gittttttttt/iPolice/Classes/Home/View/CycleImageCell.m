//
//  CycleImageCell.m
//  iPolice
//
//  Created by ioswei on 15/7/9.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CycleImageCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define CycleImageCellIdentifier @"CycleImageCell"

@interface CycleImageCell()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
/**
 *  标题
 */
@property (nonatomic, weak) IBOutlet UILabel *subjectLabel;

@end

@implementation CycleImageCell

- (void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    
    [self.imageView sd_setImageWithURL:self.imageURL placeholderImage:[UIImage imageNamed:@"默认图.png"]];
}

- (void)setSubject:(NSString *)subject
{
    _subject = subject;
    
    self.subjectLabel.text = subject;
}

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath
{
    [collectionView registerNib:[UINib nibWithNibName:CycleImageCellIdentifier bundle:nil] forCellWithReuseIdentifier:CycleImageCellIdentifier];
    CycleImageCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:CycleImageCellIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
