//
//  QueryServiceCollectionViewCell.m
//  iPolice
//
//  Created by ioswei on 15/6/11.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "QueryServiceCollectionViewCell.h"
#import "QueryService.h"

// QueryServiceCollectionViewCell标识符
#define QueryServiceCollectionViewCellIdentifier @"QueryServiceCollectionViewCell"

@interface QueryServiceCollectionViewCell()

/**
 *  热门服务名称
 */
@property (nonatomic, weak) IBOutlet UILabel *queryServiceTitle;
/**
 *  热门服务介绍
 */
@property (nonatomic, weak) IBOutlet UILabel *queryServiceDesc;
/**
 *  热门服务图片
 */
@property (nonatomic, weak) IBOutlet UIImageView *queryServiceImage;

@end

@implementation QueryServiceCollectionViewCell

/**
 *  初始化Xib
 *
 *  @param service 热门服务信息
 */
- (void)setQueryService:(QueryService *)queryService
{
    _queryService = queryService;
    
    self.queryServiceImage.image = [UIImage imageNamed:queryService.queryServiceImageName];
    self.queryServiceTitle.text = queryService.queryServiceTitle;
    self.queryServiceDesc.text = queryService.queryServiceDesc;
}

// 快速创建一个cell
+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath
{
    [collectionView registerNib:[UINib nibWithNibName:QueryServiceCollectionViewCellIdentifier bundle:nil] forCellWithReuseIdentifier:QueryServiceCollectionViewCellIdentifier];
    QueryServiceCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:QueryServiceCollectionViewCellIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
