//
//  CycleImageCell.h
//  iPolice
//
//  Created by ioswei on 15/7/9.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CycleImageCell : UICollectionViewCell

@property (nonatomic, strong) NSURL *imageURL;
/**
 *  标题
 */
@property (nonatomic, strong) NSString *subject;

/**
 *  快速创建一个cell
 *
 *  @param collectionView 哪个collectionView
 *  @param indexPath      collectionView的哪个cell
 *
 *  @return 一个创建好的cell
 */
+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath;

@end
