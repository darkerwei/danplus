//
//  QueryServiceCollectionViewCell.h
//  iPolice
//
//  Created by ioswei on 15/6/11.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QueryService;

@interface QueryServiceCollectionViewCell : UICollectionViewCell

/**
 *  热门服务信息
 */
@property (nonatomic, strong) QueryService *queryService;

/**
 *  快速创建一个cell
 *
 *  @param collectionView 哪个collectionView
 *  @param indexPath      collectionView的哪个cell
 *
 *  @return 一个创建好的cell
 */
+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath;

@end
