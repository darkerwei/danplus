//
//  Attachment.m
//  iPolice
//
//  Created by ioswei on 15/7/16.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Attachment.h"

@implementation Attachment

/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)attachment
{
    return [[self alloc] init];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)attachmentWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.desc = dict[@"description"];
        self.thumbnail = dict[@"thumbnail"];
        self.name = dict[@"name"];
        self.aid = dict[@"aid"];
        self.type = dict[@"type"];
        self.uri = dict[@"uri"];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\"description\":\"%@\", \"thumbnail\":\"%@\", \"name\":\"%@\", \"aid\":\"%@\", \"type\":\"%@\", \"uri\":\"%@\"}", self.desc, self.thumbnail, self.name, self.aid, self.type, self.uri];
}

@end
