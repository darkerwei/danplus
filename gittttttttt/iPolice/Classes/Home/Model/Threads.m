//
//  Threads.m
//  iPolice
//
//  Created by ioswei on 15/7/9.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Threads.h"
#import "Attachment.h"

@implementation Threads

/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)threads
{
    return [[self alloc] init];
}

/**
 *  快速构造一个模型
 *
 *  @param token    Token
 *  @param cid      频道id
 *  @param uid      用户id
 *  @param status   状态 0-可见* 1不可见 2待审核
 *  @param style    主题样式 0-列表主题* 1-图片主题
 *  @param type     分类id 空-查询所有分类*
 *  @param start    内容开始
 *  @param limit    内容个数
 *  @param hasImage 是否带图片 1-带图片 0/空-不带图片*
 *
 *  @return 创建好的模型
 */
+ (instancetype)threadsWithCid:(NSString *)cid
                        status:(NSString *)status
                         style:(NSString *)style
                          type:(NSString *)type
                         start:(NSString *)start
                         limit:(NSString *)limit
                      hasImage:(NSString *)hasImage
                     hasDetail:(NSString *)hasDetail
{
    Threads *theThreads = [self threads];
    
    theThreads.cid = cid;
    theThreads.status = status;
    theThreads.style = style;
    theThreads.type = type;
    theThreads.start = start;
    theThreads.limit = limit;
    theThreads.hasImage = hasImage;
    theThreads.hasDetail = hasDetail;
    
    return theThreads;
}

//2.16
+ (instancetype)threadsWithTid:(NSString *)tid
                       message:(NSString *)message
                     extfield1:(NSString *)extfield1
                     extfield2:(NSString *)extfield2
                     extfield3:(NSString *)extfield3
                     extfield4:(NSString *)extfield4
                     extfield5:(NSString *)extfield5{
    
    Threads *theThreads = [self threads];
    
    theThreads.tid = tid;
    theThreads.message = message;
    theThreads.extfield1 = extfield1;
    theThreads.extfield2 = extfield2;
    theThreads.extfield3 = extfield3;
    theThreads.extfield4 = extfield4;
    theThreads.extfield5 = extfield5;

    return theThreads;
}
/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)threadsInfoWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.token = dict[@"token"];
        self.cid = dict[@"cid"];
        self.uid = dict[@"uid"];
        self.status = dict[@"status"];
        self.style = dict[@"style"];
        self.type = dict[@"type"];
        self.start = dict[@"start"];
        self.limit = dict[@"limit"];
        self.hasImage = dict[@"hasImage"];
        self.tid = dict[@"tid"];
        self.subject = dict[@"subject"];
        self.abstracts = dict[@"abstracts"];
        self.dateline = dict[@"dateline"];
        self.replies = dict[@"replies"];
        self.allowreply = dict[@"allowreply"];
        self.views = dict[@"views"];
        self.favtimes = dict[@"favtimes"];
        self.sharetimes = dict[@"sharetimes"];
        self.theTypename = dict[@"typename"];
        self.sticky = dict[@"sticky"];
        self.username = dict[@"username"];
        self.nickname = dict[@"nickname"];
        self.mapx = dict[@"mapx"];
        self.mapy = dict[@"mapy"];
        self.location = dict[@"location"];
        self.thumbnail = dict[@"thumbnail"];
        self.message = dict[@"message"];
        self.extfield1 = dict[@"extfield1"];
        self.extfield2 = dict[@"extfield2"];
        self.extfield3 = dict[@"extfield3"];
        self.extfield4 = dict[@"extfield4"];
        self.extfield5 = dict[@"extfield5"];
        self.avatar = dict[@"avatar"];
      
        
        NSArray *threadsList = dict[@"attachments"];
        NSMutableArray *tempThreadsList = [NSMutableArray array];
        for (NSDictionary *dict in threadsList) {
            [tempThreadsList addObject:[Attachment attachmentWithDict:dict]];
        }
        self.attachments = tempThreadsList;
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\"token\":\"%@\", \"cid\":\"%@\", \"uid\":\"%@\", \"status\":\"%@\", \"style\":\"%@\", \"type\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\", \"hasImage\":\"%@\", \"tid\":\"%@\", \"subject\":\"%@\", \"abstracts\":\"%@\", \"dateline\":\"%@\", \"replies\":\"%@\", \"allowreply\":\"%@\", \"views\":\"%@\", \"favtimes\":\"%@\", \"sharetimes\":\"%@\", \"typename\":\"%@\", \"sticky\":\"%@\", \"username\":\"%@\", \"nickname\":\"%@\", \"mapx\":\"%@\", \"mapy\":\"%@\", \"location\":\"%@\", \"thumbnail\":\"%@\", \"message\":\"%@\", \"extfield1\":\"%@\", \"extfield2\":\"%@\", \"extfield3\":\"%@\", \"extfield4\":\"%@\", \"extfield5\":\"%@\"}", self.token, self.cid, self.uid, self.status, self.style, self.type, self.start, self.limit, self.hasImage, self.tid, self.subject, self.abstracts, self.dateline, self.replies, self.allowreply, self.views, self.favtimes, self.sharetimes, self.theTypename, self.sticky, self.username, self.nickname, self.mapx, self.mapy, self.location, self.thumbnail, self.message, self.extfield1, self.extfield2, self.extfield3, self.extfield4, self.extfield5];
}

@end
