//
//  QueryService.h
//  iPolice
//
//  Created by ioswei on 15/6/11.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QueryService : NSObject

/**
 *  热门服务名称
 */
@property (nonatomic, copy) NSString *queryServiceTitle;
/**
 *  热门服务介绍
 */
@property (nonatomic, copy) NSString *queryServiceDesc;
/**
 *  热门服务图片名称
 */
@property (nonatomic, copy) NSString *queryServiceImageName;

/**
 *  快速创建一个热门服务
 *
 *  @param queryServiceTitle      热门服务名称
 *  @param queryServiceDesc     热门服务介绍
 *  @param queryServiceImageName 热门服务图片名称
 *
 *  @return 一个创建好的热门服务
 */
+ (instancetype)queryServiceWithqueryServiceTitle:(NSString *)queryServiceTitle queryServiceDesc:(NSString *)queryServiceDesc queryServiceImageName:(NSString *)queryServiceImageName;

@end
