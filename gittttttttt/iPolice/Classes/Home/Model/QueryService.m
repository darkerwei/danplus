//
//  QueryService.m
//  iPolice
//
//  Created by ioswei on 15/6/11.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "QueryService.h"

@implementation QueryService

// 快速创建一个热门服务
+ (instancetype)queryServiceWithqueryServiceTitle:(NSString *)queryServiceTitle queryServiceDesc:(NSString *)queryServiceDesc queryServiceImageName:(NSString *)queryServiceImageName
{
    QueryService *theQueryService = [[self alloc] init];
    theQueryService.queryServiceTitle = queryServiceTitle;
    theQueryService.queryServiceDesc = queryServiceDesc;
    theQueryService.queryServiceImageName = queryServiceImageName;
    
    return theQueryService;
}

@end
