//
//  Utilities.m
//  iPolice
//
//  Created by Bert on 6/12/15.
//  Copyright (c) 2015 Bert. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+(instancetype)shared{
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

+(float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    NSAttributedString *valuestr = [[NSAttributedString alloc] initWithString:value attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
    CGSize textsize = [valuestr boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
    return textsize.height;
}
+(float) widthForString:(NSString *)value fontSize:(float)fontSize andHeight:(float)height
{
    NSAttributedString *valuestr = [[NSAttributedString alloc] initWithString:value attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
    CGSize textsize = [valuestr boundingRectWithSize:CGSizeMake(MAXFLOAT, height) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
    return textsize.width;
}
+(void) saveToken:(NSString *)token uid:(NSString *)uid{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] setObject:uid forKey:@"uid"];
}
+(UILabel*)titleForNavigation:(NSString*)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textColor = [UIColor whiteColor];
    [label sizeToFit];
    return label;
}
@end
