//
//  Utilities.h
//  iPolice
//
//  Created by Bert on 6/12/15.
//  Copyright (c) 2015 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject

+(instancetype) shared;

/**
 * @brief 获取字符串高度.
 *
 * @param  value 需要计算高度的字符串.
 *
 * @param  fontSize 字符串尺寸.
 *
 * @param  width 字符串宽度.
 *
 * @return 无返回值.
 */
+(float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width;



/**
 * @brief 获取字符串高度.
 *
 * @param  value 需要计算高度的字符串.
 *
 * @param  fontSize 字符串尺寸.
 *
 * @param  width 字符串宽度.
 *
 * @return 无返回值.
 */
+(float) widthForString:(NSString *)value fontSize:(float)fontSize andHeight:(float)height;
/**
 *  保存token 和 uid
 *
 *  @param token token
 *  @param uid   uid
 */
+(void) saveToken:(NSString *)token uid:(NSString *)uid;
/**
 *  设置title
 *
 *  @param title titlename
 *
 *  @return uilabel
 */
+(UILabel*)titleForNavigation:(NSString*)title;

@end
