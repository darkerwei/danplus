//
//  PreNetAPI.h
//  iPolice
//
//  Created by Bert on 6/11/15.
//  Copyright (c) 2015 Bert. All rights reserved.
//

#ifndef iPolice_PreNetAPI_h
#define iPolice_PreNetAPI_h

typedef enum ERROR_CODE{
    SUCCESS = 200,
    ERROR_SERVER = 505,
    ERROR_BADGATEWAY = 502,
    ERROR_NOTFIND = 404,
}NET_CODE;

//cms:9090 		http://123.57.229.250:9090/services/cms/方法名
//ads:9091 		http://123.57.229.250:9091/services/ads/方法名
//businessService:9092 	http://123.57.229.250:9092/services/businessService/方法名
//videoService:9093	http://123.57.229.250:9093/services/videoService/方法名
//user:9094		http://123.57.229.250:9094/UCenter/user/方法名
//http://123.57.229.250:9999/api/getData？module=value&method=value
#define SERVER_CMS(name) [NSString stringWithFormat:@"http://123.57.229.250:9090/services/cms/%@",name];
#define SERVER_BUSINESS(name)[NSString stringWithFormat:@"http://123.57.229.250:9092/services/businessService/%@",name];
#define SERVER_VIDEO(name)[NSString stringWithFormat:@"http://123.57.229.250:9093/services/videoService/",name];
#define SERVER_USERCENTER(name)[NSString stringWithFormat:@"http://123.57.229.250:9094/UCenter/%@",name];
#define SERVER_CHANNEL(name)[NSString stringWithFormat:@"http://123.57.229.250:9090/services/getChannels/%@",name];
#define SERVER_ADS(name)[NSString stringWithFormat:@"http://123.57.229.250:9091/services/ads/%@",name];

#define SERVER_URL(module,method)[NSString stringWithFormat:@"http://123.57.229.250:9999/api/getData?module=%@&method=%@",module,method];
#endif
