//
//  WantedTableViewCell.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "WantedTableViewCell.h"
#import "Threads.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "InformViewController.h"

// WantedTableViewCell标识符
#define WantedTableViewCellIdentifier @"WantedTableViewCell"

@interface WantedTableViewCell()

/**
 *  通缉令图片
 */
@property (weak, nonatomic) IBOutlet UIImageView *wantedImage;
/**
 *  通缉令描述
 */
@property (weak, nonatomic) IBOutlet UITextView *wantedDesc;
@property (weak, nonatomic) IBOutlet MJLabel *introduceLabel;
@property (weak, nonatomic) IBOutlet UIButton *reportButton;

@end

@implementation WantedTableViewCell

// 举报按钮点击
- (IBAction)reportBtnClick
{
//    if ([self.delegate respondsToSelector:@selector(reportBtnClick)]) {
//        [self.delegate reportBtnClick];

        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:self.reportButton,@"Field" ,nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"mvc" object:self userInfo:dic];
//    }
}


/**
 *  初始化Xib
 *
 *  @param theWanted Cell信息
 */
- (void)setThreads:(Threads *)threads{
    
    _threads = threads;
    
    //通缉头像
    [self.wantedImage sd_setImageWithURL:[NSURL URLWithString:threads.thumbnail] placeholderImage:[UIImage imageNamed:@"曝光台.png"]];
    
    //通缉号
    self.wantedDesc.text = [NSString stringWithFormat:@"%@", threads.subject];
    
    //通缉详情
    self.introduceLabel.numberOfLines = 3;
    self.introduceLabel.text = [NSString stringWithFormat:@"%@", threads.abstracts];
    
    //举报按钮
    [self.reportButton addTarget:self action:@selector(reportBtnClick) forControlEvents:UIControlEventTouchUpInside];
}

// 快速创建一个Cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:WantedTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:WantedTableViewCellIdentifier];
    WantedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:WantedTableViewCellIdentifier];
    
    return cell;
}
@end
