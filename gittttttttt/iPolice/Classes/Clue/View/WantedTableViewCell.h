//
//  WantedTableViewCell.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Threads;

@protocol WantedTableViewCellDelegate <NSObject>

@end

@interface WantedTableViewCell : UITableViewCell

@property (nonatomic, assign) id<WantedTableViewCellDelegate> delegate;

/**
 *  通缉令信息
 */
@property (nonatomic, strong) Threads *threads;

/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
