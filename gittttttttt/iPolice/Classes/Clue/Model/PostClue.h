//
//  PostClue.h
//  iPolice
//
//  Created by xwy on 15/9/19.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostClue : NSObject

/**
 *  Token
 */
@property (nonatomic, copy) NSString *token;
/**
 *  用户id
 */
@property (nonatomic, copy) NSString *uid;
/**
 *  关联id（举报通缉令是所需, 只有在举报通缉令时有效，通缉令id（主题发布中有个频道是通缉令频道））
 */
@property (nonatomic, copy) NSString *relatedid;
/**
 *  标题
 */
@property (nonatomic, copy) NSString *subject;
/**
 *  报警内容
 */
@property (nonatomic, copy) NSString *message;
/**
 *  附件 0无附件 1有附件
 */
@property (nonatomic, copy) NSString *attachment;
/**
 *  X
 */
@property (nonatomic, copy) NSString *mapx;
/**
 *  Y
 */
@property (nonatomic, copy) NSString *mapy;
/**
 *  位置
 */
@property (nonatomic, copy) NSString *location;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield1;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield2;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield3;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield4;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield5;
//附件
@property (nonatomic, copy) NSArray *attachments;


+ (instancetype)postClue;

//11 线索举报服务
+ (instancetype)postClueWithRelatedid:(NSString *)relatedid
                              subject:(NSString *)subject
                              message:(NSString *)message
                           attachment:(NSString *)attachment
                                 mapx:(NSString *)mapx
                                 mapy:(NSString *)mapy
                             location:(NSString *)location
                            extfield1:(NSString *)extfield1
                            extfield2:(NSString *)extfield2
                            extfield3:(NSString *)extfield3
                            extfield4:(NSString *)extfield4
                            extfield5:(NSString *)extfield5
                          attachments:(NSArray *)attachments;

+ (instancetype)postClueWithDict:(NSDictionary *)dict;


- (instancetype)initPostClueWithDict:(NSDictionary *)dict;


@end
