//
//  PostClue.m
//  iPolice
//
//  Created by xwy on 15/9/19.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PostClue.h"

@implementation PostClue

/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)postClue
{
    return [[self alloc] init];
}

//11 线索举报服务
+ (instancetype)postClueWithRelatedid:(NSString *)relatedid
                              subject:(NSString *)subject
                              message:(NSString *)message
                           attachment:(NSString *)attachment
                                 mapx:(NSString *)mapx
                                 mapy:(NSString *)mapy
                             location:(NSString *)location
                            extfield1:(NSString *)extfield1
                            extfield2:(NSString *)extfield2
                            extfield3:(NSString *)extfield3
                            extfield4:(NSString *)extfield4
                            extfield5:(NSString *)extfield5
                          attachments:(NSArray *)attachments{
    
    PostClue *clue = [PostClue postClue];
    
    clue.relatedid = relatedid;
    clue.subject = subject;
    clue.message = message;
    clue.attachment = attachment;
    clue.mapx = mapx;
    clue.mapy = mapy;
    clue.location = location;
    clue.extfield1 = extfield1;
    clue.extfield2 = extfield2;
    clue.extfield3 = extfield3;
    clue.extfield4 = extfield4;
    clue.extfield5 = extfield5;
    clue.attachments = attachments;
    
    return clue;
}

+ (instancetype)postClueWithDict:(NSDictionary *)dict{
    
    return [[self alloc] initPostClueWithDict:dict];
}

- (instancetype)initPostClueWithDict:(NSDictionary *)dict{
    
    self = [super init];
    if (self) {
        

        self.token = dict[@"token"];
        self.uid = dict[@"uid"];
        self.relatedid = dict[@"relatedid"];
        self.subject = dict[@"subject"];
        self.message = dict[@"message"];
        self.attachment = dict[@"attachment"];
        self.mapx = dict[@"mapx"];
        self.mapy = dict[@"mapy"];
        self.location = dict[@"location"];
        self.extfield1 = dict[@"extfield1"];
        self.extfield2 = dict[@"extfield2"];
        self.extfield3 = dict[@"extfield3"];
        self.extfield4 = dict[@"extfield4"];
        self.extfield5 = dict[@"extfield5"];
        
        //
        self.attachments = dict[@""];
    }
    return self;
}



@end
