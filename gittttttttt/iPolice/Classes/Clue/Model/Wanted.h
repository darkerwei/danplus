//
//  Wanted.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Wanted : NSObject

/**
 *  通缉令图片名称
 */
@property (nonatomic, copy) NSString *wantedImageName;
/**
 *  通缉令描述
 */
@property (nonatomic, copy) NSString *wantedDesc;

/**
 *  快速创建一个通缉令
 *
 *  @param wantedImageName 通缉令图片名称
 *  @param wantedDesc      通缉令描述
 *
 *  @return 一个创建好的通缉令
 */
+ (instancetype)wantedWithWantedImageName:(NSString *)wantedImageName WantedDesc:(NSString *)wantedDesc;

@end
