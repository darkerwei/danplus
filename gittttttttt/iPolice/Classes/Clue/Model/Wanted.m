//
//  Wanted.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Wanted.h"

@implementation Wanted

// 快速创建一个通缉令
+ (instancetype)wantedWithWantedImageName:(NSString *)wantedImageName WantedDesc:(NSString *)wantedDesc
{
    Wanted *theWanted = [[self alloc] init];
    theWanted.wantedImageName = wantedImageName;
    theWanted.wantedDesc = wantedDesc;
    
    return theWanted;
}

@end
