//
//  InformViewController.m
//  iPolice
//
//  Created by xwy on 15/7/16.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "InformViewController.h"
#import "ClueViewController.h"
#import "GYLAddImage.h"
#import "NetworkService.h"
#import <MAMapKit.h>
#import <AMapSearchAPI.h>
#import "PostClue.h"

#define textMessage @"请提供举报线索(100字以内)"
@interface InformViewController ()<UITextViewDelegate, UIAlertViewDelegate, MAMapViewDelegate, AMapSearchDelegate>
{
    BOOL alvertBOOL;
    float myLatitude;
    float myLongitude;
    AMapSearchAPI *_search;
}

@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UILabel *countLabel;
@property (nonatomic, strong) GYLAddImage *addImageView;
@property (nonatomic, strong) UIButton *phone;

@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) UILabel *addressLable;
@end

#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width
#define Main_Screen_Height      [[UIScreen mainScreen] bounds].size.height
#define TextCountMax 100

@implementation InformViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.userInteractionEnabled = YES;
//     _search = [[AMapSearchAPI alloc] initWithSearchKey:MAPAPI_Key Delegate:self];
    _search = [[AMapSearchAPI alloc]init];
    _search.delegate = self;
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:15 animated:YES];

    
    self.title = @"线索举报";
    
    //让scrollView自动适应屏幕
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 70, Main_Screen_Width-20, 100)];
    self.textView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.textView.text = textMessage;
    self.textView.textAlignment = NSTextAlignmentNatural;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    
    self.phone = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.phone setFrame:CGRectMake(20, Main_Screen_Height-50, Main_Screen_Width-40, 50)];
    self.phone.backgroundColor = [UIColor grayColor];
    [self.phone setTitle:@"  电话举报" forState:UIControlStateNormal];
    [self.phone setImage:[UIImage imageNamed:@"举报电话-图标.jpg"] forState:UIControlStateNormal];
    [self.phone addTarget:self action:@selector(clickPhone110) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.phone];
    
    //不显示文字限制
    self.countLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.textView.frame.size.width-70, self.textView.frame.size.height-30, 80, 30)];
    self.countLabel.text = @"0/100";
//    [self.textView addSubview:self.countLabel];
    
    
    //为导航栏左侧添加系统自定义按钮
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];

    //为导航栏添加右侧按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    
    
    _addImageView = [[GYLAddImage alloc] initWithFrame:CGRectMake(15, 200, 50, 50)];
    __weak typeof(self) weakSelf = self;
    _addImageView.block = ^(UIViewController *pickerVC){
        [weakSelf presentViewController:pickerVC animated:YES completion:nil];
    };
    [self.view addSubview:_addImageView];
    
    
    //地址背景
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 330, Main_Screen_Width-20, 30)];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:view];
    
    //地址图标
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(30, 335, 20, 20)];
    image.image = [UIImage imageNamed:@"我的地址图标.png"];
    [self.view addSubview:image];

    //地址标签
    self.addressLable = [[MJLabel alloc] initWithFrame:CGRectMake(50, 340, Main_Screen_Width-20, 60)];
    self.addressLable.font = [UIFont systemFontOfSize:12];
    self.addressLable.numberOfLines = 0;
    [self.view addSubview:self.addressLable];

}

- (void)clickPhone110{
    
    NSLog(@"电话举报按钮");
    UIWebView *callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:@"tel:110"];// 貌似tel:// 或者 tel: 都行
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    //记得添加到view上
    [self.view addSubview:callWebview];
    
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    // 只显示<最大允许输入字数>个内容
    if (textView.text.length > TextCountMax) {
        textView.text = [textView.text substringToIndex:TextCountMax];
    }
    
    // 实时更新输入字数统计
    self.countLabel.text = [NSString stringWithFormat:@"%ld/%d", textView.text.length, TextCountMax];
}

//点击空白处取消键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.textView resignFirstResponder];
}

//点击return隐藏键盘
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

//输入框代理
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:textMessage]) {
        textView.text = @"";
    }
    
//    [self animateTextView:textView up: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = textMessage;
    }
    
//    [self animateTextView:textView up:NO];
    [textView resignFirstResponder];
}

- (void)backClick{
    
    UIAlertView *alvert = [[UIAlertView alloc] initWithTitle:@"确认取消?" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认",nil];
    alvertBOOL = YES;
     [alvert show];
}


-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        //        NSLog(@"当前位置：latitude : %f,longitude: %f",userLocation.location.coordinate.latitude,userLocation.coordinate.longitude);
        
        myLatitude = userLocation.coordinate.latitude;
        myLongitude = userLocation.coordinate.longitude;
        
        
        //构造AMapReGeocodeSearchRequest对象，location为必选项，radius为可选项
        AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
//        regeoRequest.searchType = AMapSearchType_ReGeocode;
        regeoRequest.location = [AMapGeoPoint locationWithLatitude:myLatitude longitude:myLongitude];
        regeoRequest.radius = 10000;
        regeoRequest.requireExtension = YES;
        
        //发起逆地理编码
        [_search AMapReGoecodeSearch: regeoRequest];
    }
}


//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        //通过AMapReGeocodeSearchResponse对象处理搜索结果
        self.addressLable.text = [NSString stringWithFormat:@"%@%@%@%@%@", response.regeocode.addressComponent.province, response.regeocode.addressComponent.district, response.regeocode.addressComponent.township, response.regeocode.addressComponent.streetNumber.street, response.regeocode.addressComponent.streetNumber.number];
    }

}

- (void)doneClick{
    
    NSLog(@"在这里处理<提交按钮>");
    
    if (self.textView.text.length == 0 || [self.textView.text isEqualToString:textMessage]) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请添加信息或文件~" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        alvertBOOL = NO;
        [alvert show];
        
    } else {
 
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"系统提示!" message:@"确认发布?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        [alvert show];
    }
    
}


//- (void)animateTextView: (UITextView *) textView up: (BOOL) up
//{
//    const int movementDistance = 258; // tweak as needed
//    const float movementDuration = 0.2f; // tweak as needed
//    
//    int movement = (up ? -movementDistance : movementDistance);
//    
//    [UIView beginAnimations: nil context: nil];
//    [UIView setAnimationBeginsFromCurrentState: YES];
//    [UIView setAnimationDuration: movementDuration];
//    
////    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
//    self.phone.frame = CGRectOffset(self.phone.frame, 0, movement);
//
//    [UIView commitAnimations];
//    
//}

//UIAlvertView的响应事件
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alvertBOOL == NO) {
        if (buttonIndex == 0) {
            
        } else if (buttonIndex == 1){
            
            [SVProgressHUD showSuccessWithStatus:@"信息发布成功"];
            
//            //调用接口
//            PostThreads *postThreads = [PostThreads postThreadsWithCid:@"channel_uncivilized" subject:@"" message:self.textView.text];
//            
//            [[NetworkService sharedInstance] postThreadsWithPostThreads:postThreads success:^{
//                [SVProgressHUD showSuccessWithStatus:@"信息发布成功"];
//                
//            } failure:^(NSError *error) {
//                [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
//            }];
            [self.navigationController popViewControllerAnimated:YES];

        }
    } else if (alvertBOOL == YES){
        if (buttonIndex == 1){
           
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
}

@end
