//
//  ClueViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ClueViewController.h"
#import "WantedTableViewCell.h"
#import "Wanted.h"
#import "Threads.h"
#import "InformViewController.h"
#import "NetworkService.h"

@interface ClueViewController ()<UITableViewDataSource, UITableViewDelegate, WantedTableViewCellDelegate>

/**
 *  我有线索页面
 */
@property (weak, nonatomic) IBOutlet UITableView *clueTableView;
/**
 *  通缉令列表
 */
@property (nonatomic, strong) NSArray *wantedsList;

@end

@implementation ClueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"网上通缉";
    
    // 调整Cell位置
    self.clueTableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    
    self.clueTableView.tableFooterView = [[UIView alloc] init];
    
    [self getInformation];
}


#pragma mark - WantedTableViewCellDelegate

/**
 *  举报按钮点击
 */
- (void)reportBtnClick
{
    NSLog(@"在这里处理<举报>点击事件");
    InformViewController *info = [[InformViewController alloc] init];
    [self.navigationController pushViewController:info animated:YES];
    
}

- (void)getInformation
{
    Threads *theThreads = [Threads threadsWithCid:@"channel_wanted" status:@"" style:@"" type:@"" start:@"" limit:@"" hasImage:@"1" hasDetail:@"0"];
    
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        
        for (Threads *theThreads in threadList) {
            NSLog(@"%@", theThreads);
        }
        
        self.wantedsList = threadList;
        [self.clueTableView reloadData];
        
    } failure:^(NSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.wantedsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WantedTableViewCell *cell = [WantedTableViewCell cellWithTableView:tableView];
    cell.threads = self.wantedsList[indexPath.row];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"在这里处理<第%ld个Cell>点击事件", indexPath.row);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

@end
