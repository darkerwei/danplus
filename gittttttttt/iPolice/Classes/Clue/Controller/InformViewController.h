//
//  InformViewController.h
//  iPolice
//
//  Created by xwy on 15/7/16.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MapModel;
@interface InformViewController : UIViewController

/**
 *  频道ID
 */
@property (nonatomic, copy) NSString *cid;

@property(nonatomic, strong) MapModel *map;
@end
