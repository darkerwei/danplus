//
//  DriverTableViewCell.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "DriverTableViewCell.h"
#import "DriverInfo.h"
#import "DriverQueryViewController.h"

// DriverTableViewCell标识符
#define DriverTableViewCellIdentifier @"DriverTableViewCell"

@interface DriverTableViewCell()

/**
 *  驾驶人信息标题
 */
@property (weak, nonatomic) IBOutlet UILabel *driverInfoTitleLabel;

@end

@implementation DriverTableViewCell

/**
 *  初始化XIB
 *
 *  @param driverInfo 驾驶人信息
 */
- (void)setDriverInfo:(DriverInfo *)driverInfo
{
    _driverInfo = driverInfo;
    
    self.driverInfoTitleLabel.text = driverInfo.driverInfoTitle;
    self.driverInfoTextTextField.placeholder = driverInfo.driverInfoPlaceholder;
    
    DriverQueryViewController *driver = [[DriverQueryViewController alloc] init];
    driver.driverInfo = self.driverInfoTextTextField.text;

}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:DriverTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:DriverTableViewCellIdentifier];
    DriverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DriverTableViewCellIdentifier];
    
    return cell;
}

@end
