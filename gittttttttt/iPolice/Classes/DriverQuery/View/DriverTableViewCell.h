//
//  DriverTableViewCell.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DriverInfo;

@interface DriverTableViewCell : UITableViewCell

/**
 *  驾驶人信息
 */
@property (nonatomic, strong) DriverInfo *driverInfo;

/**
 *  驾驶人信息内容
 */
@property (weak, nonatomic) IBOutlet UITextField *driverInfoTextTextField;

/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
