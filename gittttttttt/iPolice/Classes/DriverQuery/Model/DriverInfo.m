//
//  DriverInfo.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "DriverInfo.h"

@implementation DriverInfo

+ (instancetype)driverInfo{
    
    return [[self alloc] init];
}

//14.1 查询驾驶人接口
+ (instancetype)driverInfoWithIdnumber:(NSString *)idnumber
                             filenumber:(NSString *)filenumber{
    
    DriverInfo *drive = [self driverInfo];
    
    drive.idnumber = idnumber;
    drive.filenumber = filenumber;
    
    return drive;
}

//14.2 机动车查询
+ (instancetype)driverInfoWithType:(NSString *)type
                       platenumber:(NSString *)platenumber
                              code:(NSString *)code{
   
    DriverInfo *drive = [self driverInfo];
    
    drive.type = type;
    drive.platenumber = platenumber;
    drive.code = code;
    
    return drive;
}

+ (instancetype)driverInfoWithDic:(NSDictionary *)dict{
    
    return [[self alloc] initDriverInfoWithDic:dict];
}

- (instancetype)initDriverInfoWithDic:(NSDictionary *)dic{
    
    self = [super init];
    if (self) {
        
        self.token = dic[@"token"];
        self.uid = dic[@"uid"];
        self.name = dic[@"name"];
        self.idnumber = dic[@"idnumber"];
        self.filenumber = dic[@"filenumber"];
        self.name = dic[@"name"];
        self.birthday = dic[@"birthday"];
        self.mobile = dic[@"mobile"];
        self.ftime = dic[@"ftime"];
        self.atime = dic[@"atime"];
        self.status = dic[@"status"];
        self.credit = dic[@"credit"];
        self.dtype = dic[@"dtype"];
        self.starttime = dic[@"starttime"];
        self.endtime = dic[@"endtime"];
        self.dperiod = dic[@"dperiod"];
        self.type = dic[@"type"];
        self.platenumber = dic[@"platenumber"];
        self.code = dic[@"code"];
        self.ltime = dic[@"ltime"];
        self.laddress = dic[@"laddress"];
        self.fines = dic[@"fines"];
        self.hstatus = dic[@"hstatus"];
        self.htime = dic[@"htime"];
        self.cstatus = dic[@"cstatus"];
        self.lcredit = dic[@"lcredit"];
        self.lbehavior = dic[@"lbehavior"];
        self.holder = dic[@"holder"];
        self.color = dic[@"color"];
        self.brand = dic[@"brand"];
        self.nature = dic[@"nature"];
        self.scrappedtime = dic[@"scrappedtime"];
        self.mstatus = dic[@"mstatus"];
        self.registertime = dic[@"registertime"];
        

    }
    return self;
}

/**
 *  快速创建一个驾驶人信息
 *
 *  @param driverInfoTitle       驾驶人信息题目
 *  @param driverInfoPlaceholder 驾驶人信息提示
 *  @param driverInfoText        驾驶人信息内容
 *
 *  @return 一个创建好的驾驶人信息
 */
+ (instancetype)driverInfoWithDriverInfoTitle:(NSString *)driverInfoTitle DriverInfoPlaceholder:(NSString *)driverInfoPlaceholder DriverInfoText:(NSString *)driverInfoText
{
    DriverInfo *theDriverInfo = [[self alloc] init];
    theDriverInfo.driverInfoTitle = driverInfoTitle;
    theDriverInfo.driverInfoPlaceholder = driverInfoPlaceholder;
    theDriverInfo.driverInfoText = driverInfoText;
    
    return theDriverInfo;
}

@end
