//
//  DriverInfo.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DriverInfo : NSObject


//Token
@property (nonatomic, copy) NSString *token;
//用户id
@property (nonatomic, copy) NSString *uid;
//身份证号码
@property (nonatomic, copy) NSString *idnumber;
//档案编号
@property (nonatomic, copy) NSString *filenumber;

//姓名
@property (nonatomic, copy) NSString *name;
//出生日期
@property (nonatomic, copy) NSString *birthday;
//手机号码
@property (nonatomic, copy) NSString *mobile;
//初次领证日期
@property (nonatomic, copy) NSString *ftime;
//审验有效期止
@property (nonatomic, copy) NSString *atime;
//状态
@property (nonatomic, copy) NSString *status;
//累计扣分
@property (nonatomic, copy) NSString *credit;
//准驾车型
@property (nonatomic, copy) NSString *dtype;
//有效期始
@property (nonatomic, copy) NSString *starttime;
//有效期止
@property (nonatomic, copy) NSString *endtime;
//驾驶证期限
@property (nonatomic, copy) NSString *dperiod;
//号牌类型
@property (nonatomic, copy) NSString *type;
//号牌号码
@property (nonatomic, copy) NSString *platenumber ;
//识别号后六位
@property (nonatomic, copy) NSString *code;
//违法时间
@property (nonatomic, copy) NSString *ltime;
//违法地址
@property (nonatomic, copy) NSString *laddress;
//罚款金额
@property (nonatomic, copy) NSString *fines;
//处理状态
@property (nonatomic, copy) NSString *hstatus;
//处理时间
@property (nonatomic, copy) NSString *htime;
//交款状态
@property (nonatomic, copy) NSString *cstatus;
//违法扣分数
@property (nonatomic, copy) NSString *lcredit;
//违法行为
@property (nonatomic, copy) NSString *lbehavior;
//机动车所有人
@property (nonatomic, copy) NSString *holder;
//车身颜色
@property (nonatomic, copy) NSString *color;
//车辆品牌
@property (nonatomic, copy) NSString *brand;
//使用性质
@property (nonatomic, copy) NSString *nature;
//强制报废期止
@property (nonatomic, copy) NSString *scrappedtime;
//抵押状态
@property (nonatomic, copy) NSString *mstatus;
//初次登记日期
@property (nonatomic, copy) NSString *registertime;


/**
 *  驾驶人信息题目
 */
@property (nonatomic, copy) NSString *driverInfoTitle;
/**
 *  驾驶人信息提示
 */
@property (nonatomic, copy) NSString *driverInfoPlaceholder;
/**
 *  驾驶人信息内容
 */
@property (nonatomic, copy) NSString *driverInfoText;

+ (instancetype)driverInfo;

//14.1 查询驾驶人接口
+ (instancetype)driverInfoWithIdnumber:(NSString *)idnumber
                             filenumber:(NSString *)filenumber;

//14.2 机动车查询
+ (instancetype)driverInfoWithType:(NSString *)type
                       platenumber:(NSString *)platenumber
                              code:(NSString *)code;

+ (instancetype)driverInfoWithDic:(NSDictionary *)dict;

- (instancetype)initDriverInfoWithDic:(NSDictionary *)dic;

/**
 *  快速创建一个驾驶人信息
 *
 *  @param driverInfoTitle       驾驶人信息题目
 *  @param driverInfoPlaceholder 驾驶人信息提示
 *  @param driverInfoText        驾驶人信息内容
 *
 *  @return 一个创建好的驾驶人信息
 */
+ (instancetype)driverInfoWithDriverInfoTitle:(NSString *)driverInfoTitle
                        DriverInfoPlaceholder:(NSString *)driverInfoPlaceholder
                               DriverInfoText:(NSString *)driverInfoText;


@end
