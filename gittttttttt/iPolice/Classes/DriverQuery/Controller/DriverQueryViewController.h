//
//  DriverQueryViewController.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverQueryViewController : UIViewController

@property(nonatomic, strong) NSString *driverInfo;
@end
