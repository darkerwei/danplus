//
//  DriverInfoViewController.m
//  iPolice
//
//  Created by xwy on 15/8/31.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "DriverInfoViewController.h"
#import "DriverInfo.h"

@interface DriverInfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *birthday;
@property (weak, nonatomic) IBOutlet UILabel *mobile;
@property (weak, nonatomic) IBOutlet UILabel *ftime;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *credit;
@property (weak, nonatomic) IBOutlet UILabel *starttime;
@property (weak, nonatomic) IBOutlet UILabel *endtime;
@property (weak, nonatomic) IBOutlet UILabel *dtype;
@property (weak, nonatomic) IBOutlet UILabel *atime;
@property (weak, nonatomic) IBOutlet UILabel *dperiod;
@property (weak, nonatomic) IBOutlet UILabel *inumber;
@property (weak, nonatomic) IBOutlet UIImageView *personImage;

@end

@implementation DriverInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //头像
    self.personImage.clipsToBounds = YES;
    self.personImage.layer.cornerRadius = 50;
    [self.personImage setImage:[UIImage imageNamed:@"驾驶人信息查询结果－默认图.png"]];
    //名字
    self.name.text = self.detail.name;
    //身份证号
    self.inumber.text = [NSString stringWithFormat:@"驾驶证号:%@", self.IDnumber];
    self.inumber.font = [UIFont systemFontOfSize:12];
    
    self.mobile.text = self.detail.mobile;
    self.status.text = self.detail.status;
    self.dtype.text = self.detail.dtype;
    self.credit.text = [NSString stringWithFormat:@"%@", self.detail.credit];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    
    
    //出生日期
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.birthday.doubleValue / 1000.0]];
    self.birthday.text = [NSString stringWithFormat:@"%@", regStr];
    //初次领证日期
    NSString *regStr1 = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.ftime.doubleValue / 1000.0]];
    self.ftime.text = [NSString stringWithFormat:@"%@", regStr1];
    //驾驶证期限
    NSString *regStr2 = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.dperiod.doubleValue / 1000.0]];
    self.dperiod.text = [NSString stringWithFormat:@"%@", regStr2];
    //有限期始
    NSString *regStr3 = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.starttime.doubleValue / 1000.0]];
    self.starttime.text = [NSString stringWithFormat:@"%@", regStr3];
    //有效期止
    NSString *regStr4 = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.endtime.doubleValue / 1000.0]];
    self.endtime.text = [NSString stringWithFormat:@"%@", regStr4];
    //审验有效期
    NSString *regStr5 = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.detail.atime.doubleValue / 1000.0]];
    self.atime.text = [NSString stringWithFormat:@"%@", regStr5];
    
}

@end
