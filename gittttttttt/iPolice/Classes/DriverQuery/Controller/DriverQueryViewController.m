//
//  DriverQueryViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "DriverQueryViewController.h"
#import "DriverTableViewCell.h"
#import "CommonGroup.h"
#import "DriverInfo.h"
#import "DriverTableViewCell.h"
#import "DriverInfoViewController.h"
#import "NetworkService.h"

// Footer的高度
#define FooterHeight 0
// Header的高度
#define HeaderHeight 10

// cell信息
#define DriverInfo1Title @"身份证号:"
#define DriverInfo1Placeholder @"请输入本人身份证号"
#define DriverInfo2Title @"档案编号:"
#define DriverInfo2Placeholder @"请输入档案编号后6位"

@interface DriverQueryViewController ()<UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate>
{
    DriverTableViewCell *driverTableViewCellID;
    DriverTableViewCell *driverTableViewCellNumber;
}
/**
 *  查询页面
 */
@property (weak, nonatomic) IBOutlet UITableView *driverQuerytableView;
/**
 *  组列表
 */
@property (nonatomic, strong) NSArray *commonGroupList;

@property (weak, nonatomic) IBOutlet UIButton *queryButton;

@end

@implementation DriverQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"驾驶人信息查询";
    
    // 设置tableView属性
    self.driverQuerytableView.sectionFooterHeight = FooterHeight;
    self.driverQuerytableView.sectionHeaderHeight = HeaderHeight;
    
    // 调整cell位置
    self.driverQuerytableView.contentInset = UIEdgeInsetsMake(-24, 0, 0, 0);
    self.driverQuerytableView.bounces = NO;
    self.queryButton.clipsToBounds = YES;
    self.queryButton.layer.cornerRadius = 5;
    
    MJLabel *label = [[MJLabel alloc] initWithFrame:CGRectMake(20, 200, Main_Screen_Width-40, 200)];
    label.numberOfLines = 0;
    label.text = @"请输入您驾驶证副证上的本人身份证号，以及档案编号最后6位数字;\n如果信息输入正确，就可以查询您的准驾车型、驾驶证状态、累积记分、审验日期、初次领证日期、换证日期等信息。";
    label.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:label];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClicked)];
    [self.driverQuerytableView addGestureRecognizer:tap];
}

//点击空白处隐藏键盘
- (void)tapClicked {
    
    [driverTableViewCellID.driverInfoTextTextField resignFirstResponder];
    [driverTableViewCellNumber.driverInfoTextTextField resignFirstResponder];
}

//点击return取消键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [driverTableViewCellID.driverInfoTextTextField resignFirstResponder];
    [driverTableViewCellNumber.driverInfoTextTextField resignFirstResponder];
    
    return YES;
}
/**
 *  查询按钮点击
 */
- (IBAction)QueryBtnClick
{
    NSLog(@"在这里处理<查询按钮>点击事件");
    
    //测试数据：Idnumber:@"372321198507264014" filenumber:@"877327"
    DriverInfo *driver = [DriverInfo driverInfoWithIdnumber:driverTableViewCellID.driverInfoTextTextField.text filenumber:driverTableViewCellNumber.driverInfoTextTextField.text];
    [SVProgressHUD showWithStatus:@"正在加载驾驶人信息"];
    [[NetworkService sharedInstance] getDriverWithDriver:driver success:^(DriverInfo *driver) {
        [SVProgressHUD showSuccessWithStatus:@"驾驶人信息查询完成"];
        
        DriverInfoViewController *info = [[DriverInfoViewController alloc] init];
        info.detail = driver;
        info.IDnumber = driverTableViewCellID.driverInfoTextTextField.text;
        info.title = @"驾驶人信息查询";
        [self.navigationController pushViewController:info animated:NO];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        driverTableViewCellID = [DriverTableViewCell cellWithTableView:tableView];
        driverTableViewCellID.selectionStyle = UITableViewCellSelectionStyleNone;
        CommonGroup *theCommonGroup = self.commonGroupList[indexPath.section];
        driverTableViewCellID.driverInfo = theCommonGroup.commonItemList[indexPath.row];
        driverTableViewCellID.driverInfoTextTextField.delegate = self;
        return driverTableViewCellID;
    } else if(indexPath.section == 1){
        driverTableViewCellNumber = [DriverTableViewCell cellWithTableView:tableView];
        driverTableViewCellNumber.selectionStyle = UITableViewCellSelectionStyleNone;
        driverTableViewCellNumber.driverInfoTextTextField.delegate = self;
        CommonGroup *theCommonGroup = self.commonGroupList[indexPath.section];
        driverTableViewCellNumber.driverInfo = theCommonGroup.commonItemList[indexPath.row];
        return driverTableViewCellNumber;
    }
    return nil;
}

#pragma mark - 懒加载
- (NSArray *)commonGroupList
{
    if (_commonGroupList == nil) {
        
        // 创建组
        CommonGroup *commonGroup1 = [CommonGroup commonGroup];
        CommonGroup *commonGroup2 = [CommonGroup commonGroup];
        
        // 创建cell信息
        DriverInfo *driverInfo1 = [DriverInfo driverInfoWithDriverInfoTitle:DriverInfo1Title DriverInfoPlaceholder:DriverInfo1Placeholder DriverInfoText:nil];
        commonGroup1.commonItemList = @[driverInfo1];
        
        DriverInfo *driverInfo2 = [DriverInfo driverInfoWithDriverInfoTitle:DriverInfo2Title DriverInfoPlaceholder:DriverInfo2Placeholder DriverInfoText:nil];
        commonGroup2.commonItemList = @[driverInfo2];
   
        
        _commonGroupList = @[commonGroup1, commonGroup2];
    }
    return _commonGroupList;
}


@end
