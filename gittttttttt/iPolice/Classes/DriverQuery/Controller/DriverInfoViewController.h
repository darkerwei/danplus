//
//  DriverInfoViewController.h
//  iPolice
//
//  Created by xwy on 15/8/31.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DriverInfo;

@interface DriverInfoViewController : UIViewController

@property(nonatomic, strong) DriverInfo *detail;

@property (nonatomic, strong) NSString *IDnumber;
@end
