//
//  AddressTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopModel;

@protocol AddressTableCellDelegate <NSObject>

- (void)editClick:(UIButton *)sender;
- (void)delectClick:(UIButton *)sender;

@end

@interface AddressTableViewCell : UITableViewCell

@property (nonatomic, assign) id<AddressTableCellDelegate> delegate;

@property (nonatomic, strong) ShopModel *shopM;

@property (weak, nonatomic) IBOutlet UIButton *defauleButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *delectButton;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
