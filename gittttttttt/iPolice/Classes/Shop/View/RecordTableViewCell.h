//
//  RecordTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/8/25.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopModel;

@interface RecordTableViewCell : UITableViewCell

@property (nonatomic, strong) ShopModel *record;

/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
