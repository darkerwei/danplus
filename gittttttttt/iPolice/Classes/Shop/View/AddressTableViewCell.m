//
//  AddressTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "AddressTableViewCell.h"
#import "ShopModel.h"

#define AddressTableViewCellIdentifier @"AddressTableViewCell"

@interface AddressTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *consignee;
@property (weak, nonatomic) IBOutlet UILabel *mobile;
@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation AddressTableViewCell

- (IBAction)editClick:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(editClick:)]) {
        [self.delegate editClick:sender];
    }
}

- (IBAction)delectClick:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(delectClick:)]) {
        [self.delegate delectClick:sender];
    }
}



- (void)setShopM:(ShopModel *)shopM{
    
    _shopM = shopM;
    
    self.consignee.text = self.shopM.consignee;
    self.mobile.text = self.shopM.mobile;
    self.address.text = self.shopM.address;
    
    //最下方的灰色背景
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 115, Main_Screen_Width, 5)];
    label2.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:label2];

}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:AddressTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:AddressTableViewCellIdentifier];
    AddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AddressTableViewCellIdentifier];
    return cell;
}
@end
