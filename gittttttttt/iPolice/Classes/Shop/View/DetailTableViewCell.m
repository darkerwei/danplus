//
//  DetailTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/8/27.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "DetailTableViewCell.h"
#import "CreditsModel.h"

#define DetailTableViewCellIdentifier @"DetailTableViewCell"

@interface DetailTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *creditrule;
@property (weak, nonatomic) IBOutlet UILabel *credittime;
@property (weak, nonatomic) IBOutlet UILabel *experience;

@end
@implementation DetailTableViewCell

- (void)setCreditsM:(CreditsModel *)creditsM{
    
    _creditsM = creditsM;
    self.backgroundColor = [UIColor whiteColor];
    
    self.creditrule.text = self.creditsM.creditrule;
    
    self.experience.text = [NSString stringWithFormat:@"%@积分", self.creditsM.experience];
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.creditsM.credittime.doubleValue / 1000.0]];
//    self.credittime.text = [NSString stringWithFormat:@"%@", regStr];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:self.creditsM.credittime.doubleValue/1000.0];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];

    self.credittime.text = confromTimespStr;
    
//    int a=[self intervalSinceNow:confromTimespStr];

}

- (int)intervalSinceNow: (NSString *) theDate
{
    
    NSDateFormatter *date=[[NSDateFormatter alloc] init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *d=[date dateFromString:theDate];
    
    NSLog(@"sssss%@",d);
    
    NSTimeInterval late=[d timeIntervalSince1970]*1;
    
    
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSLog(@"dddddd%@",dat);
    NSTimeInterval now=[dat timeIntervalSince1970]*1;
    NSString *timeString= @"";
    
    NSTimeInterval cha = now-late;
    
    
    if (cha/86400>1)
    {
        timeString = [NSString stringWithFormat:@"%f", cha/86400];
        timeString = [timeString substringToIndex:timeString.length-7];
        return [timeString intValue];
    }else{
        if (cha/3600>1&&cha/86400<1){
            timeString = [NSString stringWithFormat:@"%f", cha/3600];
            timeString = [timeString substringToIndex:timeString.length-6];
            return [timeString intValue];
        }else{
            if (cha/3600<1) {
                timeString = [NSString stringWithFormat:@"%f", cha/60];
                timeString = [timeString substringToIndex:timeString.length-3];
                return [timeString intValue];
            }
        }
        
    }
   
    return -1;
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:DetailTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:DetailTableViewCellIdentifier];
    DetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DetailTableViewCellIdentifier];
    return cell;
}

@end
