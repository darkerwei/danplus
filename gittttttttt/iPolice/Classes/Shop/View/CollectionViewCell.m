//
//  CollectionViewCell.m
//  iPolice
//
//  Created by xwy on 15/8/19.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CollectionViewCell.h"
#import "ShopModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define CollectionViewCellIdentifier @"CollectionViewCell"

@interface CollectionViewCell()

@property(nonatomic ,strong)UILabel *companyName;
@property(nonatomic ,strong)UILabel *goodsName;
@property(nonatomic ,strong)UILabel *price;
@property(nonatomic ,strong)UIImageView *priceImage;
@property(nonatomic ,strong)UIImageView *imagView;

@end

@implementation CollectionViewCell

- (void)setShop:(ShopModel *)shop{
    
    _shop = shop;
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.companyName.textColor = [UIColor grayColor];
    self.companyName.font = [UIFont systemFontOfSize:9];
    self.companyName.text = [NSString stringWithFormat:@"%@", shop.brand];
    
    self.goodsName.textColor = [UIColor blackColor];
    self.goodsName.text = [NSString stringWithFormat:@"%@", shop.subject];
    self.goodsName.font = [UIFont systemFontOfSize:11.5];
    
    
    self.price.textColor = [UIColor orangeColor];
    self.price.textAlignment = NSTextAlignmentLeft;
    self.price.text = [NSString stringWithFormat:@"%d", shop.creditprice.intValue];
    self.price.font = [UIFont systemFontOfSize:10.5];
    
    
    [self.priceImage setImage:[UIImage imageNamed:@"小积分图标"]];
    
    [self.imagView sd_setImageWithURL:[NSURL URLWithString:shop.thumbnail] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:CollectionViewCellIdentifier bundle:nil] forCellReuseIdentifier:CollectionViewCellIdentifier];
    CollectionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CollectionViewCellIdentifier];
    return cell;
}


//懒加载
- (UILabel *)companyName{
    
    if (_companyName == nil) {
        self.companyName = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 20)];
        [self addSubview:self.companyName];
    }
    return _companyName;
}

- (UILabel *)goodsName{
    if (_goodsName == nil) {
        self.goodsName = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, 250, 20)];
        [self addSubview:self.goodsName];
    }
    return _goodsName;
}

- (UILabel *)price{
    if (_price == nil) {
        self.price = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 35, 20)];
        [self addSubview:self.price];
       
    }
    return _price;
}

- (UIImageView *)priceImage{
    if (_priceImage == nil) {
        self.priceImage = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.price.frame), 50, 20, 20)];
        [self addSubview:self.priceImage];
    }
    return _priceImage;
}
- (UIImageView *)imagView{
    if (_imagView == nil) {
        self.imagView = [[UIImageView alloc]initWithFrame:CGRectMake(70, 80, 70, 70)];
        [self addSubview:self.imagView];
    }
    return _imagView;
}
@end
