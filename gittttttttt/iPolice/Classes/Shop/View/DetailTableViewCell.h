//
//  DetailTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/8/27.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CreditsModel;

@interface DetailTableViewCell : UITableViewCell

@property (nonatomic, strong) CreditsModel *creditsM;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
