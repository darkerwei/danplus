//
//  CollectionViewCell.h
//  iPolice
//
//  Created by xwy on 15/8/19.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopModel;

@interface CollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) ShopModel *shop;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
