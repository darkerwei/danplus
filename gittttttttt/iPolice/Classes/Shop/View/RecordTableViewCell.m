//
//  RecordTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/8/25.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "RecordTableViewCell.h"
#import "ShopModel.h"
#import <SDWebImage/UIImageView+WebCache.h>


#define RecordTableViewCellIdentifier @"RecordTableViewCell"

@interface RecordTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *listLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumnailImage;
@property (weak, nonatomic) IBOutlet UILabel *subject;
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UILabel *dateline;
@property (weak, nonatomic) IBOutlet UILabel *credits;
@property (weak, nonatomic) IBOutlet UIButton *sureButton;

@end

@implementation RecordTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setRecord:(ShopModel *)record{
    
    _record = record;
    
    self.listLabel.text = self.record.ordersn;
    
    [self.thumnailImage sd_setImageWithURL:[NSURL URLWithString:self.record.thumbnail] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    self.subject.text = self.record.subject;
    self.count.text = [NSString stringWithFormat:@"数量:%d", self.record.count.intValue];
   
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.record.dateline.doubleValue / 1000.0]];
    self.dateline.text = [NSString stringWithFormat:@"%@", regStr];

    self.credits.text = [NSString stringWithFormat:@"%d",self.record.credits.intValue];
    
    //状态判断
    NSString *status = [NSString stringWithFormat:@"%@", self.record.status];
    if ([status isEqualToString:@"0"]) {
        status = @"交易进行中";
    } else if([status isEqualToString:@"1"]) {
        status = @"交易已完成";
    } else if ([status isEqualToString:@"2"]) {
        status = @"交易已取消";
    }
    [self.sureButton setTitle:status forState:UIControlStateNormal];

}


// 快速创建一个Cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:RecordTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:RecordTableViewCellIdentifier];
    RecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RecordTableViewCellIdentifier];
    
    return cell;
}
@end
