//
//  GoodsDetailViewController.h
//  iPolice
//
//  Created by xwy on 15/8/21.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopModel;

@interface GoodsDetailViewController : UIViewController

@property (nonatomic, strong) ShopModel *shopDetail;

@property(nonatomic, strong) UIImageView *goodsImage;
@property(nonatomic, strong) UIButton *exangeButton;
@property(nonatomic, strong) UILabel *grade;
@property(nonatomic, strong) UILabel *message;
@end
