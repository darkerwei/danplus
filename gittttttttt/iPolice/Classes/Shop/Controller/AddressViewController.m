//
//  AddressViewController.m
//  iPolice
//
//  Created by xwy on 15/8/26.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "AddressViewController.h"
#import "AddAddressViewController.h"
#import "ShopModel.h"
#import "NetworkService.h"
#import "AddressTableViewCell.h"
#import "ExangeViewController.h"
#import "EditViewController.h"

@interface AddressViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, AddressTableCellDelegate>
{
    int number;
}
@property (weak, nonatomic) IBOutlet UIButton *addAddress;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *addressList;

@end

@implementation AddressViewController

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    //刷新
    [self refresh];
    [self upRefresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.addAddress addTarget:self action:@selector(clickAddAddress) forControlEvents:UIControlEventTouchUpInside];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, Main_Screen_Height-64-60)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    // 调整Cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    self.tableView.tableFooterView = [[UIView alloc] init];

}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    ShopModel *shopModel = [ShopModel shopWithStart:@"0" limit:@"10"];
    
    [SVProgressHUD showWithStatus:@"正在获取地址信息"];
    
    [[NetworkService sharedInstance] getAddressesWithAddresses:shopModel success:^(NSArray *addresses) {
        
        [SVProgressHUD showSuccessWithStatus:@"获取地址信息成功"];
        
        
        // 视频满10个才显示上拉刷新
        if (addresses.count == 10) {
            self.tableView.footer.hidden = NO;
        } else if (addresses.count < 10) {
            self.tableView.footer.hidden = YES;
        }
        
        self.addressList = addresses;

        
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    ShopModel *shopModel = [ShopModel shopWithStart:@"0" limit:@"10"];
    
    [SVProgressHUD showWithStatus:@"正在获取地址信息"];
    
    [[NetworkService sharedInstance] getAddressesWithAddresses:shopModel success:^(NSArray *addresses) {
        
        // 没有更多视频时隐藏上拉刷新
        if (addresses.count == 10) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取地址信息成功"];
        } else if (addresses.count <= 0) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的内容了"];
        }
        
        self.addressList = addresses;
        [self.tableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"获取地址信息成功"];
        [self.tableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];

}


- (void)clickAddAddress{
    
    AddAddressViewController *addAddress = [[AddAddressViewController alloc] init];
    addAddress.title = @"添加收货地址";
    [self.navigationController pushViewController:addAddress animated:NO];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.addressList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    AddressTableViewCell *cell = [AddressTableViewCell cellWithTableView:tableView];
    cell.shopM = self.addressList[indexPath.row];
    cell.delegate = self;
    cell.editButton.tag = indexPath.row;
    cell.delectButton.tag = indexPath.row;
    if (indexPath.row == 0 && indexPath.section == 0) {
        [cell.defauleButton setImage:[UIImage imageNamed:@"选择框-1"] forState:UIControlStateNormal];
    } else {
        [cell.defauleButton setImage:[UIImage imageNamed:@"选择框"] forState:UIControlStateNormal];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    number = (int)indexPath.row;
    
    UIAlertView *alvert = [[UIAlertView alloc] initWithTitle:@"系统提示!" message:@"确认设为默认地址?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
    [alvert show];
    
}

- (void)editClick:(UIButton *)btn{
    
    NSLog(@"编辑");
    EditViewController *edit = [[EditViewController alloc] init];
    edit.title = @"修改收货地址";
    edit.address = self.addressList[btn.tag];
    [self.navigationController pushViewController:edit animated:YES];
}

- (void)delectClick:(UIButton *)btn{
    
    NSLog(@"删除");
    ShopModel *shop = [ShopModel shop];
    shop.addressid = [self.addressList[btn.tag] addressid];
    [[NetworkService sharedInstance] delAddressWithUpateAddress:shop success:^{
        [SVProgressHUD showSuccessWithStatus:@"删除完成"];
        [self refresh];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}



//UIAlvertView的响应事件
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1){

        ShopModel *shop = self.addressList[number];
        shop.isdefault = @"1";
        [[NetworkService sharedInstance] updateAddressWithUpateAddress:shop success:^{

            //返回上一个页面
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        }];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
@end
