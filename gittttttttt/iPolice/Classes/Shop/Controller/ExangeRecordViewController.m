//
//  ExangeRecordViewController.m
//  iPolice
//
//  Created by xwy on 15/8/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ExangeRecordViewController.h"
#import "ShopModel.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import "RecordTableViewCell.h"
#import "GoodsDetailViewController.h"

@interface ExangeRecordViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) ShopModel *shopM;
/**
 *  通缉令列表
 */
@property (nonatomic, strong) NSArray *recordsList;
@property (nonatomic, assign) int limit;

@end

@implementation ExangeRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, Main_Screen_Height-64)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    // 调整Cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];
}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    ShopModel *shopModel = [ShopModel shopWithStart:@"0" limit:@"10"];
    [SVProgressHUD showWithStatus:@"正在获取兑换信息"];
    
    [[NetworkService sharedInstance] getOrdersWithMyOrders:shopModel success:^(NSArray *myOrderList) {
        
        [SVProgressHUD showSuccessWithStatus:@"获取兑换信息成功"];
        
        // 视频满10个才显示上拉刷新
        if (myOrderList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (myOrderList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        
        self.shopM = shopModel;
        
        self.recordsList = myOrderList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    
    self.limit += 10;
    ShopModel *shopModel = [ShopModel shopWithStart:@"0" limit:@"10"];
    [SVProgressHUD showWithStatus:@"正在获取兑换信息"];
    
    [[NetworkService sharedInstance] getOrdersWithMyOrders:shopModel success:^(NSArray *myOrderList) {
        
        // 没有更多视频时隐藏上拉刷新
        if (myOrderList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取兑换信息成功"];
        } else if (myOrderList.count <= self.limit -10) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的内容了"];
        }
        
        self.shopM = shopModel;
        
        self.recordsList = myOrderList;
        [self.tableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"获取兑换信息成功"];
        [self.tableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];
    
}


- (void)getInformation{
    
    ShopModel *shopModel = [ShopModel shopWithStart:@"0" limit:@""];
    [[NetworkService sharedInstance] getOrdersWithMyOrders:shopModel success:^(NSArray *myOrderList) {
       
        self.shopM = shopModel;
        
        self.recordsList = myOrderList;
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RecordTableViewCell *cell = [RecordTableViewCell cellWithTableView:tableView];
    cell.record = self.recordsList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

#pragma mark - UITableViewDelegate
//订单记录跳到商品详情
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[NetworkService sharedInstance] getGoodsDetailWithGoodsDetail:self.recordsList[indexPath.row] success:^(ShopModel *goodsDetail) {
        
        GoodsDetailViewController *detail = [[GoodsDetailViewController alloc] init];
        detail.title = @"商品详情";
        detail.shopDetail = self.recordsList[indexPath.row];
        [self.navigationController pushViewController:detail animated:YES];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}
@end
