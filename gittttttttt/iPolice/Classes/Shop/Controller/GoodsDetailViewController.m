//
//  GoodsDetailViewController.m
//  iPolice
//
//  Created by xwy on 15/8/21.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "GoodsDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ExangeViewController.h"
#import "ShopModel.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>

#define Height 64
@interface GoodsDetailViewController ()

@end

@implementation GoodsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.goodsImage = [[UIImageView alloc] initWithFrame:CGRectMake(50, Height, Main_Screen_Width-100, 150)];
    [self.goodsImage sd_setImageWithURL:[NSURL URLWithString:self.shopDetail.thumbnail] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    [self.view addSubview:self.goodsImage];
    [self.goodsImage setContentMode:UIViewContentModeScaleToFill];
    self.goodsImage.layer.masksToBounds = YES;
    
    self.grade = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.goodsImage.frame), 50, 40)];
    self.grade.textColor = [UIColor orangeColor];
    self.grade.font = [UIFont systemFontOfSize:17];
    self.grade.textAlignment = NSTextAlignmentRight;
    self.grade.text = [NSString stringWithFormat:@"%d", self.shopDetail.creditprice.intValue];
    [self.view addSubview:self.grade];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.grade.frame), CGRectGetMaxY(self.goodsImage.frame)+10, 20, 20)];
    image.image = [UIImage imageNamed:@"积分图标.png"];
    [self.view addSubview:image];
    
    self.exangeButton = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width-120, CGRectGetMaxY(self.goodsImage.frame)+5, 100, 30)];
    self.exangeButton.backgroundColor = [UIColor orangeColor];
    [self.exangeButton setTitle:@"立即兑换" forState:UIControlStateNormal];
    [self.exangeButton addTarget:self action:@selector(clickExangeButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.exangeButton];
   
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.exangeButton.frame)+5, Main_Screen_Width, 2)];
    label.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:label];
    
    //发布详情
//    UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.grade.frame)+2, Main_Screen_Width-20, Main_Screen_Height-CGRectGetMaxY(self.grade.frame)-2)];
//    [webview loadHTMLString:self.shopDetail.message baseURL:nil];
//    webview.backgroundColor = [UIColor redColor];
//    webview.scrollView.bounces = NO;
//    [self.view addSubview:webview];
//    
    self.message = [[MJLabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.grade.frame)+10, Main_Screen_Width-20, Main_Screen_Height-CGRectGetMaxY(self.grade.frame)-2)];
    self.message.text = self.shopDetail.message;
    self.message.numberOfLines = 0;
    self.message.textAlignment = NSTextAlignmentLeft;
    self.message.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.view addSubview:self.message];
    
}


//立即兑换
- (void)clickExangeButton{
    
    ShopModel *shopModel = [ShopModel shopWithGid:self.shopDetail.gid];

    [[NetworkService sharedInstance] getGoodsDetailWithGoodsDetail:shopModel success:^(ShopModel *goodsDetail) {

        ExangeViewController *exange = [[ExangeViewController alloc] init];
        exange.title = @"立即兑换";
        exange.exange = shopModel;
        [self.navigationController pushViewController:exange animated:NO];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
 
}

@end
