//
//  MyCreditViewController.m
//  iPolice
//
//  Created by xwy on 15/8/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MyCreditViewController.h"
#import "ShopViewController.h"
#import "CreditsModel.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import "DetailTableViewCell.h"
#import "ScoreViewController.h"

@interface MyCreditViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UIButton *ruleButton;
@property (weak, nonatomic) IBOutlet UILabel *creditsLabel;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *detailList;
@property (nonatomic, assign) int limit;

@end

@implementation MyCreditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.shopButton addTarget:self action:@selector(clickShopButton) forControlEvents:UIControlEventTouchUpInside];
    
    [self.ruleButton addTarget:self action:@selector(clickRuleButton) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 230, Main_Screen_Width-20, 2)];
    label.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:label];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(Main_Screen_Width/2-30, 220, 60, 20)];
    label2.text = @"积分明细";
    label2.backgroundColor = [UIColor whiteColor];
    label2.font = [UIFont systemFontOfSize:13];
    label2.textColor = [UIColor grayColor];
    [self.view addSubview:label2];

    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 240, Main_Screen_Width, Main_Screen_Height-240)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    // 调整Cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self getCredits];
    [self refresh];
    [self upRefresh];
}

- (void)clickShopButton{
    
    ShopViewController *shop = [[ShopViewController alloc] init];
    shop.title = @"积分商城";
    [self.navigationController pushViewController:shop animated:YES];
}

- (void)getCredits{
    
    CreditsModel *credits = [CreditsModel credits];
    [[NetworkService sharedInstance] getCreditsWithMyCredits:credits success:^(CreditsModel *credits) {
        
        if (credits.credit.integerValue >= 100000000) {
            self.creditsLabel.text = [NSString stringWithFormat:@"%.2f亿",credits.credit.doubleValue/100000000];
        } else if (credits.credit.integerValue >= 10000 && credits.credit.intValue < 100000000) {
            self.creditsLabel.text = [NSString stringWithFormat:@"%.2f万",(float)(credits.credit.intValue/10000)];
        } else {
            self.creditsLabel.text = [NSString stringWithFormat:@"%d",credits.credit.intValue];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

//积分兑换规则
- (void)clickRuleButton{
    ScoreViewController *score = [[ScoreViewController alloc] init];
    score.title = @"积分规则";
    [self.navigationController pushViewController:score animated:YES];
    
}


- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

- (void)loadNewData
{
    self.limit = 10;
    CreditsModel *credits = [CreditsModel creditsWithStart:@"0" limit:@"1000"];
    [SVProgressHUD showInfoWithStatus:@"正在获取积分列表"];
    [[NetworkService sharedInstance] getCreditLogsWithCreditLogs:credits success:^(NSArray *creditLogsList) {
        [SVProgressHUD showSuccessWithStatus:@"获取积分列表成功"];
        
        if (creditLogsList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (creditLogsList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        
        self.detailList = creditLogsList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
    
}

- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    CreditsModel *credits = [CreditsModel creditsWithStart:@"" limit:@"1000"];
    [SVProgressHUD showInfoWithStatus:@"正在获取积分列表"];
    [[NetworkService sharedInstance] getCreditLogsWithCreditLogs:credits success:^(NSArray *creditLogsList) {
        [SVProgressHUD showSuccessWithStatus:@"获取积分列表成功"];
       
        // 没有更多视频时隐藏上拉刷新
        if (creditLogsList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取积分列表成功"];
        } else if (creditLogsList.count < self.limit-10) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多信息了"];
        }
        self.detailList = creditLogsList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.detailList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailTableViewCell *cell = [DetailTableViewCell cellWithTableView:tableView];
    cell.creditsM = self.detailList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

@end
