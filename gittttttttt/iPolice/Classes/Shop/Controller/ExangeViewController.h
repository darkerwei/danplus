//
//  ExangeViewController.h
//  iPolice
//
//  Created by xwy on 15/8/21.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopModel;

@interface ExangeViewController : UIViewController

@property (nonatomic, strong) ShopModel *exange;

@end
