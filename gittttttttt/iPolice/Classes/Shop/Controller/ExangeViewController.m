//
//  ExangeViewController.m
//  iPolice
//
//  Created by xwy on 15/8/21.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ExangeViewController.h"
#import "ShopModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CreditsModel.h"
#import "NetworkService.h"
#import "AddressViewController.h"
#import "ExangeRecordViewController.h"

@interface ExangeViewController ()<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIImageView *thumbnail;
@property (nonatomic, strong) UILabel *subject;
@property (nonatomic, strong) UILabel *creditprice;
@property (nonatomic, strong) UILabel *number;

@property (nonatomic, strong) UITextView *count;
@property (nonatomic, strong) UILabel *retCredits;

@property (nonatomic, strong) UILabel *addressMobile;
@property (nonatomic, strong) UILabel *addressConsignee;
@property (nonatomic, strong) UILabel *addressAddress;

@property (weak, nonatomic) IBOutlet UILabel *creditsTotal;
@property (weak, nonatomic) IBOutlet UIButton *exangeButton;

@property (nonatomic, strong) CreditsModel *model;

@property (nonatomic, strong) ShopModel *shop;

@end

@implementation ExangeViewController


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 80, Main_Screen_Width, Main_Screen_Height-80-50)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
    
    // 设置tableView属性
    self.tableView.sectionFooterHeight = 0;
    self.tableView.sectionHeaderHeight = 0;
    
    // 调整cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(-24, 0, 0, 0);
    [self.exangeButton addTarget:self action:@selector(clickExange) forControlEvents:UIControlEventTouchUpInside];
    
    [self getCredits];
    [self getAddress];
    
    self.creditsTotal.text = [NSString stringWithFormat:@"订单总额%d积分", self.exange.creditprice.intValue];
}

-  (void)clickExange{
    
    ShopModel *shopModel = [ShopModel shopWithGid:self.exange.gid addressid:self.shop.addressid count:self.count.text remark:self.exange.remark];
    [[NetworkService sharedInstance] postOrderWithPostOrder:shopModel success:^(ShopModel *postOrder) {
 
        [SVProgressHUD showSuccessWithStatus:@"兑换成功"];
        ExangeRecordViewController *record = [[ExangeRecordViewController alloc] init];
        [self.navigationController pushViewController:record animated:YES];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
   
}

- (void)getCredits{
    
    CreditsModel *credits = [CreditsModel credits];
    [[NetworkService sharedInstance] getCreditsWithMyCredits:credits success:^(CreditsModel *credits) {
        
        self.model = credits;
        self.count.text = @"1";
        
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (void)getAddress{

    ShopModel *shop = [ShopModel shop];
    [[NetworkService sharedInstance] getDefaultAddressWithDefaultAddress:shop success:^(ShopModel *defaultAddressList) {
        
        [SVProgressHUD showInfoWithStatus:@"正在加载"];
        
        self.addressMobile.text = defaultAddressList.mobile;
        self.addressConsignee.text = defaultAddressList.consignee;
        self.addressAddress.text = defaultAddressList.address;
        
        self.shop = shop;
        
        [self.tableView reloadData];
        
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    static NSString *reuseIdetify = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdetify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdetify];
    }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.section == 0 && indexPath.row == 0) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            self.thumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 110, 110)];
            [self.thumbnail sd_setImageWithURL:[NSURL URLWithString:self.exange.thumbnail] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
            [cell addSubview:self.thumbnail];
            
            self.subject = [[UILabel alloc] initWithFrame:CGRectMake(150, 20, 150, 40)];
            self.subject.text = self.exange.subject;
            self.subject.textColor = [UIColor blackColor];
            [cell addSubview:self.subject];
            
            self.creditprice = [[UILabel alloc] initWithFrame:CGRectMake(150, 90, 50, 50)];
            self.creditprice.textAlignment = NSTextAlignmentLeft;
             self.creditprice.textColor = [UIColor orangeColor];
            self.creditprice.text = [NSString stringWithFormat:@"%d", self.exange.creditprice.intValue];
            [cell addSubview:self.creditprice];

            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.creditprice.frame), 105, 20, 20)];
            [imageView setImage:[UIImage imageNamed:@"积分图标.png"]];
            [cell addSubview:imageView];
            
            self.number = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame)+10, 90, 150, 50)];
            self.number.text = [NSString stringWithFormat:@"库存%d件", self.exange.number.intValue];
            self.number.font = [UIFont systemFontOfSize:13];
            [cell addSubview:self.number];
            
        }else if (indexPath.section == 1 && indexPath.row == 0) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
            label1.text = @"兑换数量";
            [cell addSubview:label1];
            
            UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(Main_Screen_Width-100, 10, 20, 30)];
            label2.text = @"X";
            [cell addSubview:label2];
            
            self.count = [[UITextView alloc] initWithFrame:CGRectMake(Main_Screen_Width-80, 10, 60, 30)];
            self.count.layer.borderColor = [UIColor grayColor].CGColor;
            self.count.layer.borderWidth = 1.0;//该属性显示外边框
            self.count.layer.cornerRadius = 5;//通过该值来设置textView边角的弧度
            self.count.layer.masksToBounds = YES;
            self.count.tag = 1;
            self.count.text = @"1";
            self.count.scrollEnabled = NO;
            self.count.font = [UIFont systemFontOfSize:18];
            self.count.delegate = self;
            [cell addSubview:self.count];
            
        } else if (indexPath.section == 1 && indexPath.row == 1) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            self.retCredits = [[UILabel alloc] initWithFrame:CGRectMake(Main_Screen_Width-310, 10, 300, 30)];
            self.retCredits.textAlignment = NSTextAlignmentRight;
            [cell addSubview:self.retCredits];
            
        } else if (indexPath.section == 2 && indexPath.row == 0) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            for (int i = 0; i < 3; i++) {
                NSArray *array = @[@"联系人手机号", @"收件人", @"配货地址"];
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, i*50, 120, 50)];
                label.text = array[i];
                label.font = [UIFont systemFontOfSize:15];
                label.textAlignment = NSTextAlignmentLeft;
                [cell addSubview:label];
            }
            
            //联系手机号
            self.addressMobile = [[UILabel alloc] initWithFrame:CGRectMake(120, 10, 180, 30)];
            self.addressMobile.font = [UIFont systemFontOfSize:15];
            self.addressMobile.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:self.addressMobile];
            
            //收件人
            self.addressConsignee = [[UILabel alloc] initWithFrame:CGRectMake(120, 60, 180, 30)];
            self.addressConsignee.font = [UIFont systemFontOfSize:15];
            self.addressConsignee.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:self.addressConsignee];
            
            //配货地址
            self.addressAddress = [[UILabel alloc] initWithFrame:CGRectMake(120, 110, 180, 30)];
            self.addressAddress.font = [UIFont systemFontOfSize:15];
            self.addressAddress.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:self.addressAddress];
            
    }
     return cell;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    if (self.count.text.intValue <= self.exange.number.intValue && textView.tag == 1) {
        self.count.text = textView.text;
    } else if (self.count.text.intValue > self.exange.number.intValue && textView.tag == 1){
        //假如超出库存
        self.count.text = [NSString stringWithFormat:@"%@",self.exange.number];
    }
    
    //实时变化
    self.retCredits.text = [NSString stringWithFormat:@"兑换后积分%d分", self.model.credit.intValue - self.exange.creditprice.intValue*(self.count.text.intValue)];
    self.creditsTotal.text = [NSString stringWithFormat:@"订单总额%d积分", self.exange.creditprice.intValue*(self.count.text.intValue)];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text

{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        return NO;
    }
    return YES;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2 && indexPath.row == 0) {
        
        AddressViewController *address = [[AddressViewController alloc] init];
        address.title = @"地址管理";
        [self.navigationController pushViewController:address animated:NO];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 150;
    }else if (indexPath.section == 1) {
        return 50;
    }else if (indexPath.section == 2 && indexPath.row == 0) {
        return 150;
    }
    return 50;
}

@end
