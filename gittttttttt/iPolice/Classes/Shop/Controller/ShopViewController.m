//
//  ShopViewController.m
//  iPolice
//
//  Created by xwy on 15/8/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ShopViewController.h"
#import "CollectionViewCell.h"
#import "NetworkService.h"
#import "ShopModel.h"
#import "GoodsDetailViewController.h"
#import "MyCreditViewController.h"
#import "ExangeRecordViewController.h"
#import "CreditsModel.h"


@interface ShopViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate>

@property (nonatomic, strong) UIButton *creditsButton;
@property (nonatomic, strong) UIButton *exangeButton;
@property (nonatomic, strong) UILabel *creditsLabel;
@property (nonatomic, strong) UIButton *selectedButton;

@property (nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *goodsList;

@property (nonatomic,strong) CreditsModel *creditsM;

//搜索栏
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, assign) int limit;
@end

@implementation ShopViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //我的积分
    [self getCredits];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.creditsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width/2, 50)];
    [self.creditsButton setImage:[UIImage imageNamed:@"我有积分.png"] forState:UIControlStateNormal];
    [self.creditsButton setTitle:@"  我有积分" forState:UIControlStateNormal];
    [self.creditsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.creditsButton addTarget:self action:@selector(clickCreditsButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.creditsButton];
    
    
    self.exangeButton = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width/2, 64, Main_Screen_Width/2, 50)];
    [self.exangeButton setTitle:@"  兑换记录" forState:UIControlStateNormal];
    [self.exangeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.exangeButton setImage:[UIImage imageNamed:@"兑换纪录.png"] forState:UIControlStateNormal];
    [self.exangeButton addTarget:self action:@selector(clickExangeButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.exangeButton];
    
    self.creditsLabel = [[UILabel alloc] initWithFrame:CGRectMake(Main_Screen_Width/2-30, 64, 70, 50)];
    self.creditsLabel.textColor = [UIColor orangeColor];
    self.creditsLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:self.creditsLabel];
    
    // 设置搜索框
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 64+50, Main_Screen_Width, 50)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"请输入关键字";
    self.searchBar.backgroundColor = [UIColor purpleColor];
    [self.view addSubview:self.searchBar];
    
    UILabel *list = [[UILabel alloc]initWithFrame:CGRectMake(10, 64+50+50, 150, 50)];
    list.textAlignment = NSTextAlignmentLeft;
    list.text = @"商品列表";
    [self.view addSubview:list];
    
    UILabel *exange = [[UILabel alloc] initWithFrame:CGRectMake(Main_Screen_Width-200, 64+50+50, 150, 50)];
    exange.textAlignment = NSTextAlignmentRight;
    exange.text = @"只看我能兑换的";
    [self.view addSubview:exange];
    
    self.selectedButton = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width-50, 64+50+50, 50, 50)];
    [self.selectedButton setImage:[UIImage imageNamed:@"选择框.png"] forState:UIControlStateNormal];
    [self.selectedButton setImage:[UIImage imageNamed:@"选择框-1.png"] forState:UIControlStateSelected];
    [self.selectedButton addTarget:self action:@selector(clickSelectedButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.selectedButton];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 64+50+50+48, Main_Screen_Width, 2)];
    label.backgroundColor = [UIColor grayColor];
    [self.view addSubview:label];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64+50+50+50, Main_Screen_Width, Main_Screen_Height-64-150) collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:self.collectionView];

    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];

}

//我的积分
- (void)getCredits{
    
    CreditsModel *credits = [CreditsModel credits];
    [[NetworkService sharedInstance] getCreditsWithMyCredits:credits success:^(CreditsModel *credits) {
     
        if (credits.credit.integerValue >= 100000000) {
            self.creditsLabel.text = [NSString stringWithFormat:@"%.2f亿",credits.credit.doubleValue/100000000];
        } else if (credits.credit.integerValue >= 10000 && credits.credit.intValue < 100000000) {
            self.creditsLabel.text = [NSString stringWithFormat:@"%.2f万",(float)(credits.credit.intValue/10000)];
        } else {
            self.creditsLabel.text = [NSString stringWithFormat:@"%d",credits.credit.intValue];
        }
        //传到模型
        self.creditsM = credits;
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

//我有积分
- (void)clickCreditsButton{
    
    //页面传值
    MyCreditViewController *myCredit = [[MyCreditViewController alloc] init];
    myCredit.title = @"我的积分";
    [self.navigationController pushViewController:myCredit animated:YES];
   
}

//兑换记录
- (void)clickExangeButton{
    
    ExangeRecordViewController *exange = [[ExangeRecordViewController alloc] init];
    exange.title = @"兑换记录";
    [self.navigationController pushViewController:exange animated:YES];
}


//能兑换的
- (void)clickSelectedButton{
    
    self.selectedButton.selected = !self.selectedButton.selected;
    
    NSString *creditprice = [[NSString alloc] init];
    if (self.selectedButton.selected == NO) {
       creditprice = @"";
    } else {
        creditprice = @"250";
    }
    
    ShopModel *shop = [ShopModel shopWithStart:@"0" limit:@"10" keywords:@"" typeID:@"" creditprice:creditprice shopprice:@""];
   
    [[NetworkService sharedInstance] getGoodsWithShop:shop success:^(NSArray *shopList) {
        [SVProgressHUD showWithStatus:@"加载中..."];
        self.goodsList = shopList;
        [self.collectionView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        
    }];
    
}

- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.collectionView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.collectionView.header beginRefreshing];
}

- (void)loadNewData
{
    self.limit = 10;
    ShopModel *shop = [ShopModel shopWithStart:@"0" limit:@"10" keywords:@"" typeID:@"" creditprice:@"" shopprice:@""];
    [SVProgressHUD showInfoWithStatus:@"正在获取商品列表"];
    [[NetworkService sharedInstance] getGoodsWithShop:shop success:^(NSArray *shopList) {
        [SVProgressHUD showSuccessWithStatus:@"获取商品列表成功"];
        
        // 视频满10个才显示上拉刷新
        if (shopList.count == self.limit) {
            self.collectionView.footer.hidden = NO;
        } else if (shopList.count < self.limit) {
            self.collectionView.footer.hidden = YES;
        }
        
        self.goodsList = shopList;
        [self.collectionView reloadData];
        
        [self.collectionView.header endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.collectionView.header endRefreshing];
    }];

}

- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.collectionView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.collectionView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    ShopModel *shop = [ShopModel shopWithStart:@"0" limit:@"" keywords:@"" typeID:@"" creditprice:@"" shopprice:@""];
     [SVProgressHUD showWithStatus:@"正在获取商品列表"];
    [[NetworkService sharedInstance] getGoodsWithShop:shop success:^(NSArray *shopList) {
        [SVProgressHUD showSuccessWithStatus:@"获取商品列表成功"];
        self.goodsList = shopList;
        
        // 没有更多视频时隐藏上拉刷新
        if (shopList.count == self.limit) {
            self.collectionView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取商品列表成功"];
        } else if (shopList.count < self.limit-10) {
            self.collectionView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的商品了"];
        }
        
        [self.collectionView reloadData];
        [self.collectionView.footer endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
         [self.collectionView.footer endRefreshing];
    }];

}

- (void)getInformation
{
    ShopModel *shop = [ShopModel shopWithStart:@"0" limit:@"10" keywords:@"" typeID:@"" creditprice:@"" shopprice:@""];
    
    [[NetworkService sharedInstance] getGoodsWithShop:shop success:^(NSArray *shopList) {
      
        self.goodsList = shopList;
        [self.collectionView reloadData];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];

    }];
   
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.goodsList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.shop =  self.goodsList[indexPath.row];

    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width)/2, (Main_Screen_Width)/2);
}

//每个cell的大小 // top, left, bottom, right
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, -2, 0, 0);
    
}

//水平
-(CGFloat )collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0;
}
//竖直
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 2;
}

//点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   
    ShopModel *shopModel = self.goodsList[indexPath.row];
    
    [[NetworkService sharedInstance] getGoodsDetailWithGoodsDetail:shopModel success:^(ShopModel *goodsDetail) {
      
        GoodsDetailViewController *detail = [[GoodsDetailViewController alloc] init];
        detail.title = @"商品详情";
        detail.shopDetail = shopModel;
        [self.navigationController pushViewController:detail animated:YES];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    ShopModel *shop = [ShopModel shopWithStart:@"0" limit:@"10" keywords:self.searchBar.text typeID:@"" creditprice:@"" shopprice:@""];
    
    [[NetworkService sharedInstance] getGoodsWithShop:shop success:^(NSArray *shopList) {
        [SVProgressHUD showWithStatus:@"加载中..."];
        self.goodsList = shopList;
        [self.collectionView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        
    }];
    
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    ShopModel *shop = [ShopModel shopWithStart:@"0" limit:@"10" keywords:self.searchBar.text typeID:@"" creditprice:@"" shopprice:@""];
    
    [[NetworkService sharedInstance] getGoodsWithShop:shop success:^(NSArray *shopList) {
        [SVProgressHUD showWithStatus:@"加载中..."];
        self.goodsList = shopList;
        [self.collectionView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        
    }];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBar.text = @"";
}

@end
