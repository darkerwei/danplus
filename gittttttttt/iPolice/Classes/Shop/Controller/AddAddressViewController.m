//
//  AddAddressViewController.m
//  iPolice
//
//  Created by xwy on 15/8/26.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "AddAddressViewController.h"
#import "ShopModel.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>

#define Height 50
@interface AddAddressViewController ()<UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) UITextView *textView0;
@property(nonatomic, strong) UITextView *textView1;
@property(nonatomic, strong) UITextView *textView2;
@property(nonatomic, strong) UITextView *textView3;

@end

@implementation AddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //为导航栏添加右侧按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, 500)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.bounces = NO;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *reuseIdetify = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdetify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdetify];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (indexPath.section == 0 && indexPath.row == 0) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 80, Height)];
            label.text = @"收货人姓名";
            label.font = [UIFont systemFontOfSize:15];
            label.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:label];
            self.textView0 = [[UITextView alloc] initWithFrame:CGRectMake(90, 10, 150, Height-20)];
            self.textView0.font = [UIFont systemFontOfSize:15];
            self.textView0.textAlignment = NSTextAlignmentLeft;
            self.textView0.delegate = self;
            [cell addSubview:self.textView0];
        } else if (indexPath.section == 0 && indexPath.row == 1) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 80, Height)];
            label.text = @"联系方式";
            label.font = [UIFont systemFontOfSize:15];
            label.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:label];
            self.textView1 = [[UITextView alloc] initWithFrame:CGRectMake(90, 10, 180, Height)];
            self.textView1.font = [UIFont systemFontOfSize:15];
            self.textView1.textAlignment = NSTextAlignmentLeft;
            self.textView1.delegate = self;
            [cell addSubview:self.textView1];
        } else if (indexPath.section == 0 && indexPath.row == 2) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 80, Height)];
            label.text = @"所在地区";
            label.font = [UIFont systemFontOfSize:15];
            label.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:label];
            self.textView2 = [[UITextView alloc] initWithFrame:CGRectMake(90, 10, 180, Height)];
            self.textView2.font = [UIFont systemFontOfSize:15];
            self.textView2.textAlignment = NSTextAlignmentLeft;
            self.textView2.text = @"滨州市";
            self.textView2.userInteractionEnabled = NO;
            [cell addSubview:self.textView2];
        } else if (indexPath.section == 0 && indexPath.row == 3) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 80, Height)];
            label.text = @"详细地址";
            label.font = [UIFont systemFontOfSize:15];
            label.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:label];
            self.textView3 = [[UITextView alloc] initWithFrame:CGRectMake(90, 10, 180, Height)];
            self.textView3.font = [UIFont systemFontOfSize:15];
            self.textView3.textAlignment = NSTextAlignmentLeft;
            self.textView3.delegate = self;
            [cell addSubview:self.textView3];
        }
    }
    return cell;
}


//点击return收回键盘
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text

{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        return NO;
    }
    return YES;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return Height;
}

- (void)doneClick{
    
    NSLog(@"在这里处理<确定按钮>");
    
    
    if (self.textView0.text.length == 0 || self.textView1.text.length == 0 || self.textView3.text.length == 0) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请添加收货信息" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        
        [alvert show];
        
    } else {

        ShopModel *shop = [ShopModel shopWithConsignee:self.textView0.text address:self.textView3.text mobile:self.textView1.text email:@"" zipcode:@"" tel:@"" besttime:@"" signbuilding:@"" isdefault:@"0"];
        
        [[NetworkService sharedInstance] addAddressWithAddAddress:shop success:^{
            [SVProgressHUD showSuccessWithStatus:@"添加地址成功"];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"添加地址失败"];
        }];
        
    }
}

@end
