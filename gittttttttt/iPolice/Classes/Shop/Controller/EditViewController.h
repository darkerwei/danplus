//
//  EditViewController.h
//  iPolice
//
//  Created by xwy on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopModel;

@interface EditViewController : UIViewController

@property (nonatomic, strong) ShopModel *address;
@end
