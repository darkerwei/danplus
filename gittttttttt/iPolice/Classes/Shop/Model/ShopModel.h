//
//  ShopModel.h
//  iPolice
//
//  Created by xwy on 15/8/21.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopModel : NSObject

//Token
@property (nonatomic, copy) NSString *token;
//用户id
@property (nonatomic, copy) NSString *uid;
//内容开始
@property (nonatomic, copy) NSString *start;
//内容个数
@property (nonatomic, copy) NSString *limit;
//关键词
@property (nonatomic, copy) NSString *keywords;
//商品分类
@property (nonatomic, copy) NSString *typeID;
//商品ID
@property (nonatomic, copy) NSString *gid;
//商品唯一编码
@property (nonatomic, copy) NSString *sn;
//品牌
@property (nonatomic, copy) NSString *brand;
//商品名称
@property (nonatomic, copy) NSString *subject;
//摘要
@property (nonatomic, copy) NSString *abstracts;
//是否是实物 1是 0否
@property (nonatomic, copy) NSString *isreal;
//是否精品 0否 1是
@property (nonatomic, copy) NSString *isbest;
//是否新品
@property (nonatomic, copy) NSString *isnew;
//是否促销
@property (nonatomic, copy) NSString *ishot;
//是否特价促销
@property (nonatomic, copy) NSString *ispromote;
//缩略图
@property (nonatomic, copy) NSString *thumbnail;

//库存量
@property (nonatomic, copy) NSString *number;
//积分价格
@property (nonatomic, copy) NSString *creditprice;
//市场售价
@property (nonatomic, copy) NSString *marketprice;
//本店售价
@property (nonatomic, copy) NSString *shopprice;
//促销价格
@property (nonatomic, copy) NSString *promoteprice;
//促销价格开始日期
@property (nonatomic, copy) NSString *promotestartdate;
//促销价格结束日期
@property (nonatomic, copy) NSString *promoteenddate;
//浏览数
@property (nonatomic, copy) NSString *views;
//评分
@property (nonatomic, copy) NSString *score;

//商品信息
@property (nonatomic, copy) NSString *message;
//供货商名称
@property (nonatomic, copy) NSString *provider;

//收货地址id
@property (nonatomic, copy) NSString *addressid;
//购买数量
@property (nonatomic, copy) NSString *count;
//备注
@property (nonatomic, copy) NSString *remark;
//订单id
@property (nonatomic, copy) NSString *orderid;
//订单号
@property (nonatomic, copy) NSString *ordersn;
//订单生成时间
@property (nonatomic, copy) NSString *dateline;
//状态 0进行中 1完成
@property (nonatomic, copy) NSString *status;
//实付积分
@property (nonatomic, copy) NSString *credits;
//收货地址ID
@property (nonatomic, copy) NSString *consignee;

//收货人详细地址
@property (nonatomic, copy) NSString *address;
//收货人手机号码
@property (nonatomic, copy) NSString *mobile;
//收货人邮箱
@property (nonatomic, copy) NSString *email;
//收货人邮编
@property (nonatomic, copy) NSString *zipcode;
//收货人电话
@property (nonatomic, copy) NSString *tel;
//最佳收货时间
@property (nonatomic, copy) NSString *besttime;
//标志性建筑
@property (nonatomic, copy) NSString *signbuilding;
//是否默认（0否1是）
@property (nonatomic, copy) NSString *isdefault;

+ (instancetype)shop;

//13.1 商品列表
+ (instancetype)shopWithStart:(NSString *)start
                        limit:(NSString *)limit
                     keywords:(NSString *)keywords
                       typeID:(NSString *)typeID
                  creditprice:(NSString *)creditprice
                    shopprice:(NSString *)shopprice;

//13.2 商品详情
+ (instancetype)shopWithGid:(NSString *)gid;

//13.3 兑换详情
+ (instancetype)shopWithGid:(NSString *)gid
                  addressid:(NSString *)addressid
                      count:(NSString *)count
                     remark:(NSString *)remark;
//13.4 订单接口
+ (instancetype)shopWithStart:(NSString *)start
                        limit:(NSString *)limit;

//13.7
+ (instancetype)shopWithConsignee:(NSString *)consignee
                          address:(NSString *)address
                           mobile:(NSString *)mobile
                            email:(NSString *)email
                          zipcode:(NSString *)zipcode
                              tel:(NSString *)tel
                         besttime:(NSString *)besttime
                     signbuilding:(NSString *)signbuilding
                        isdefault:(NSString *)isdefault;
//13.8
+ (instancetype)shopWithAddressid:(NSString *)addressid
                        Consignee:(NSString *)consignee
                          address:(NSString *)address
                           mobile:(NSString *)mobile
                            email:(NSString *)email
                          zipcode:(NSString *)zipcode
                              tel:(NSString *)tel
                         besttime:(NSString *)besttime
                     signbuilding:(NSString *)signbuilding
                        isdefault:(NSString *)isdefault;

+ (instancetype)shopInfoWithDict:(NSDictionary *)dict;
- (instancetype)initShopWithDict:(NSDictionary *)dict;
@end
