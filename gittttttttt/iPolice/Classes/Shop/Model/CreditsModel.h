//
//  CreditsModel.h
//  iPolice
//
//  Created by xwy on 15/8/25.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditsModel : NSObject

//Token
@property (nonatomic, copy) NSString *token;
//用户id
@property (nonatomic, copy) NSString *uid;
//积分
@property (nonatomic, copy) NSString *credit;
//经验
@property (nonatomic, copy) NSString *experience;
//起始索引
@property (nonatomic, copy) NSString *start;
//返回个数
@property (nonatomic, copy) NSString *limit;

//日志id
@property (nonatomic, copy) NSString *logid;
//产生积分的动作
@property (nonatomic, copy) NSString *creditrule;
//日志id
@property (nonatomic, copy) NSString *creditruleid;
//类型
@property (nonatomic, copy) NSString *type;
//时间
@property (nonatomic, copy) NSString *credittime;
//积分变化值
@property (nonatomic, copy) NSString *credits;
//扩展积分变化值
@property (nonatomic, copy) NSString *extcredits1;
@property (nonatomic, copy) NSString *extcredits2;
@property (nonatomic, copy) NSString *extcredits3;
@property (nonatomic, copy) NSString *extcredits4;
@property (nonatomic, copy) NSString *extcredits5;
//标签
@property (nonatomic, copy) NSString *tag;
//描述
@property (nonatomic, copy) NSString *message;


+ (instancetype)credits;

//8.5 积分日志
+ (instancetype)creditsWithStart:(NSString *)start
                           limit:(NSString *)limit;

+ (instancetype)creditsInfoWithDict:(NSDictionary *)dict;
- (instancetype)initCreditsWithDict:(NSDictionary *)dict;

@end
