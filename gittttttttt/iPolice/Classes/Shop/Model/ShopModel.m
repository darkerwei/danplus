//
//  ShopModel.m
//  iPolice
//
//  Created by xwy on 15/8/21.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ShopModel.h"

@implementation ShopModel

+ (instancetype)shop{
    
    return [[self alloc] init];
}

//13.1
+ (instancetype)shopWithStart:(NSString *)start
                        limit:(NSString *)limit
                     keywords:(NSString *)keywords
                       typeID:(NSString *)typeID
                  creditprice:(NSString *)creditprice
                    shopprice:(NSString *)shopprice{
    
    ShopModel *shop = [self shop];
    
    shop.start = start;
    shop.limit = limit;
    shop.keywords = keywords;
    shop.typeID = typeID;
    shop.creditprice = creditprice;
    shop.shopprice = shopprice;
    
    return shop;
}

//13.3 商品详情
+ (instancetype)shopWithGid:(NSString *)gid{
    
    ShopModel *shop = [self shop];
    
    shop.gid = gid;
    
    return shop;
}

//13.3 商品详情
+ (instancetype)shopWithGid:(NSString *)gid
                  addressid:(NSString *)addressid
                      count:(NSString *)count
                     remark:(NSString *)remark{
    ShopModel *shop = [self shop];
    
    shop.gid = gid;
    shop.addressid = addressid;
    shop.count = count;
    shop.remark = remark;
    
    
    return shop;
}

//13.4 订单接口
+ (instancetype)shopWithStart:(NSString *)start
                        limit:(NSString *)limit{
    ShopModel *shop = [self shop];
    
    shop.start = start;
    shop.limit = limit;
    
    return shop;
}

//13.7
+ (instancetype)shopWithConsignee:(NSString *)consignee
                          address:(NSString *)address
                           mobile:(NSString *)mobile
                            email:(NSString *)email
                          zipcode:(NSString *)zipcode
                              tel:(NSString *)tel
                         besttime:(NSString *)besttime
                     signbuilding:(NSString *)signbuilding
                        isdefault:(NSString *)isdefault{

    ShopModel *shop = [self shop];
    
    shop.consignee = consignee;
    shop.address = address;
    shop.mobile = mobile;
    shop.email = email;
    shop.zipcode = zipcode;
    shop.tel = tel;
    shop.besttime = besttime;
    shop.signbuilding = signbuilding;
    shop.isdefault = isdefault;
    
    return shop;
}

//13.8
+ (instancetype)shopWithAddressid:(NSString *)addressid
                        Consignee:(NSString *)consignee
                          address:(NSString *)address
                           mobile:(NSString *)mobile
                            email:(NSString *)email
                          zipcode:(NSString *)zipcode
                              tel:(NSString *)tel
                         besttime:(NSString *)besttime
                     signbuilding:(NSString *)signbuilding
                        isdefault:(NSString *)isdefault{
    
    ShopModel *shop = [self shop];
    
    shop.addressid = addressid;
    shop.consignee = consignee;
    shop.address = address;
    shop.mobile = mobile;
    shop.email = email;
    shop.zipcode = zipcode;
    shop.tel = tel;
    shop.besttime = besttime;
    shop.signbuilding = signbuilding;
    shop.isdefault = isdefault;
    
    return shop;

    
}


+ (instancetype)shopInfoWithDict:(NSDictionary *)dict{
    
    return [[self alloc] initShopWithDict:dict];
}

- (instancetype)initShopWithDict:(NSDictionary *)dict{
    
    self = [super init];
    if (self) {
        
        self.token = dict[@"token"];
        self.uid = dict[@"uid"];
        self.start = dict[@"start"];
        self.limit = dict[@"limit"];
        self.keywords = dict[@"keywords"];
        self.typeID = dict[@"typeid"];
        self.creditprice = dict[@"creditprice"];
        self.shopprice = dict[@"shopprice"];
        self.gid = dict[@"gid"];
        self.sn = dict[@"sn"];
        self.brand = dict[@"brand"];
        self.subject = dict[@"subject"];
        self.abstracts = dict[@"abstracts"];
        self.number = dict[@"number"];
        self.isreal = dict[@"isreal"];
        self.creditprice = dict[@"creditprice"];
        self.marketprice = dict[@"marketprice"];
        self.shopprice = dict[@"shopprice"];
        self.promoteprice = dict[@"promoteprice"];
        self.promotestartdate = dict[@"promotestartdate"];
        self.promoteenddate = dict[@"promoteenddate"];
        self.isbest = dict[@"isbest"];
        self.isnew = dict[@"isnew"];
        self.ishot = dict[@"ishot"];
        self.ispromote = dict[@"ispromote"];
        self.thumbnail = dict[@"thumbnail"];
        self.views = dict[@"views"];
        self.score = dict[@"score"];
        
        self.message = dict[@"message"];
        self.provider = dict[@"provider"];
        
        self.addressid = dict[@"addressid"];
        self.count = dict[@"count"];
        self.remark = dict[@"remark"];
        self.orderid = dict[@"orderid"];
        self.ordersn = dict[@"ordersn"];
        
        self.dateline = dict[@"dateline"];
        self.credits = dict[@"credits"];
        self.status = dict[@"status"];
        
        self.consignee = dict[@"consignee"];
        self.address = dict[@"address"];
        self.mobile = dict[@"mobile"];
        self.email = dict[@"email"];
        self.zipcode = dict[@"zipcode"];
        self.tel = dict[@"tel"];
        self.besttime = dict[@"besttime"];
        self.signbuilding = dict[@"signbuilding"];
        self.isdefault = dict[@"isdefault"];
        
    }
    return self;
}

@end
