//
//  CreditsModel.m
//  iPolice
//
//  Created by xwy on 15/8/25.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CreditsModel.h"

@implementation CreditsModel

+ (instancetype)credits{
    
    return [[self alloc] init];
}

//8.5 积分日志
+ (instancetype)creditsWithStart:(NSString *)start
                           limit:(NSString *)limit{

    CreditsModel *credits = [self credits];
    
    credits.start = start;
    credits.limit = limit;
    
    return credits;
    
}

+ (instancetype)creditsInfoWithDict:(NSDictionary *)dict{
    
    return [[self alloc] initCreditsWithDict:dict];
}

- (instancetype)initCreditsWithDict:(NSDictionary *)dict{
    
    self = [super init];
    if (self) {
        
        self.token = dict[@"token"];
        self.uid = dict[@"uid"];
        self.credit = dict[@"credit"];
        self.experience = dict[@"experience"];
        self.start = dict[@"start"];
        self.limit = dict[@"limit"];
        self.logid = dict[@"id"];
        self.creditrule = dict[@"creditrule"];
        self.creditruleid = dict[@"creditruleid"];
        self.credits = dict[@"credits"];
        self.extcredits1 = dict[@"extcredits1"];
        self.extcredits2 = dict[@"extcredits2"];
        self.extcredits3 = dict[@"extcredits3"];
        self.extcredits4 = dict[@"extcredits4"];
        self.extcredits5 = dict[@"extcredits5"];
        self.tag = dict[@"tag"];
        self.message = dict[@"message"];
        self.type = dict[@"type"];
        self.credittime = dict[@"credittime"];
        
    }
    return self;
}

@end
