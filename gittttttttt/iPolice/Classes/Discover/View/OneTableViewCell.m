//
//  OneTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "OneTableViewCell.h"
#import "AdsInfo.h"

#define OneTableViewCellIdentifier @"OneTableViewCell"

@interface OneTableViewCell()

@property (weak, nonatomic) IBOutlet MJLabel *subject;
@property (weak, nonatomic) IBOutlet UILabel *person;
@property (weak, nonatomic) IBOutlet UILabel *time;

@end

@implementation OneTableViewCell

- (void)setThreads:(AdsInfo *)threads{
    
    _threads = threads;
    
    //标题
    self.subject.text = [NSString stringWithFormat:@"%@",self.threads.subject];
    self.subject.numberOfLines = 2;
    //发布者
    self.person.text = [NSString stringWithFormat:@"发布者:%@", self.threads.author];
    //时间
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.threads.dateline.doubleValue / 1000.0]];
    self.time.text = regStr;

}


+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:OneTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:OneTableViewCellIdentifier];
    OneTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:OneTableViewCellIdentifier];
    
    return cell;
}
@end
