//
//  CommonCell.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CommonCell.h"
#import "CommonItem.h"

// CommonCell标识符
#define CommonCellIdentifier @"CommonCell"

@interface CommonCell()

/**
 *  标题
 */
@property (weak, nonatomic) IBOutlet UILabel *title;
/**
 *  标题图片
 */
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
/**
 *  描述
 */
@property (weak, nonatomic) IBOutlet UILabel *desc;

@end

@implementation CommonCell

/**
 *  初始化Xib
 *
 *  @param theCommonItem cell信息
 */
- (void)setTheCommonItem:(CommonItem *)theCommonItem
{
    _theCommonItem = theCommonItem;
    
    self.title.text = theCommonItem.title;
    self.titleImage.image = [UIImage imageNamed:theCommonItem.titleImageName];
    self.desc.text = theCommonItem.desc;
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:CommonCellIdentifier bundle:nil] forCellReuseIdentifier:CommonCellIdentifier];
    CommonCell *cell = [tableView dequeueReusableCellWithIdentifier:CommonCellIdentifier];

    return cell;
}

@end
