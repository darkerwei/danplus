//
//  TwoTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TwoTableViewCell.h"
#import "AdsInfo.h"

#define TwoTableViewCellIdentifier @"TwoTableViewCell"

@interface TwoTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet MJLabel *message;

@end

@implementation TwoTableViewCell

- (void)setThreads:(AdsInfo *)threads{
    
    _threads = threads;
    
    [self.image sd_setImageWithURL:[NSURL URLWithString:self.threads.thumbnail] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    self.message.text = self.threads.message;
    self.message.numberOfLines = 0;
    self.message.font = [UIFont systemFontOfSize:15];
}


+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:TwoTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:TwoTableViewCellIdentifier];
    TwoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TwoTableViewCellIdentifier];
    
    return cell;
}
@end
