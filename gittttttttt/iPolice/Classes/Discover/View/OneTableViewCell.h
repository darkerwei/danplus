//
//  OneTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AdsInfo;

@interface OneTableViewCell : UITableViewCell

/**
 *  threads model -- 频道详情
 */
@property (nonatomic, strong) AdsInfo *threads;

/**
 *  create cell -- 创建cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
