//
//  CommonCell.h
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CommonItem;

@interface CommonCell : UITableViewCell

/**
 *  cell信息
 */
@property (nonatomic, strong) CommonItem *theCommonItem;

/**
 *  快速创建一个cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
