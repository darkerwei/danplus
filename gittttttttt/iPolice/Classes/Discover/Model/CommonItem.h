//
//  CommonItem.h
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonItem : NSObject

/** 标题 */
@property (nonatomic, copy) NSString *title;
/** 标题图片名称 */
@property (nonatomic, copy) NSString *titleImageName;
/** 描述 */
@property (nonatomic, copy) NSString *desc;

/**
 *  快速创建一个cell信息
 *
 *  @param title          标题
 *
 *  @return 一个创建好的cell信息
 */
+ (instancetype)commonItemWithTitle:(NSString *)title;

/**
 *  快速创建一个cell信息
 *
 *  @param title          标题
 *  @param titleImageName 标题图片名称
 *
 *  @return 一个创建好的cell信息
 */
+ (instancetype)commonItemWithTitle:(NSString *)title titleImageName:(NSString *)titleImageName;

/**
 *  快速创建一个cell信息
 *
 *  @param title          标题
 *  @param titleImageName 标题图片名称
 *  @param description    描述
 *
 *  @return 一个创建好的cell信息
 */
+ (instancetype)commonItemWithTitle:(NSString *)title titleImageName:(NSString *)titleImageName description:(NSString *)description;

@end
