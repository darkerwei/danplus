//
//  AdsInfo.m
//  iPolice
//
//  Created by xwy on 15/9/12.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "AdsInfo.h"

@implementation AdsInfo

+ (instancetype)adsInfo{
    
    return [[self alloc] init];
}


//3.5 获取广告列表接口
+ (instancetype)adsInfoWithPlaceid:(NSString *)placeid
                         available:(NSString *)available
                             start:(NSString *)start
                             limit:(NSString *)limit{
    AdsInfo *ads = [self adsInfo];
    
    ads.placeid = placeid;
    ads.available = available;
    ads.start = start;
    ads.limit = limit;
    
    return ads;
    
}

//3.7	查看广告详情接口
+ (instancetype)adsInfoWithAdvid:(NSString *)advid{
    
    AdsInfo *ads = [self adsInfo];
    
    ads.advid = advid;
    
    return ads;
}

+ (instancetype)adsInfoWithDic:(NSDictionary *)dict{
    
    return [[self alloc] initAdsInfoWithDic:dict];
}

- (instancetype)initAdsInfoWithDic:(NSDictionary *)dic{
    
    self = [super init];
    if (self) {
        
        self.token = dic[@"token"];
        self.uid = dic[@"uid"];
        self.placeid = dic[@"placeid"];
        self.available = dic[@"available"];
        self.start = dic[@"start"];
        self.limit = dic[@"limit"];
    
        self.advid = dic[@"advid"];
        self.subject = dic[@"subject"];
        self.url = dic[@"url"];
        self.thumbnail = dic[@"thumbnail"];
        self.views = dic[@"views"];
        self.authorid = dic[@"authorid"];
        self.author = dic[@"author"];
        self.dateline = dic[@"dateline"];
        
        self.starttime = dic[@"starttime"];
        self.endtime = dic[@"endtime"];
        self.message = dic[@"message"];
        self.extfield1 = dic[@"extfield1"];
        self.extfield2 = dic[@"extfield2"];
        self.extfield3 = dic[@"extfield3"];
        self.extfield4 = dic[@"extfield4"];
        self.extfield5 = dic[@"extfield5"];
        
    }
    return self;
}


@end
