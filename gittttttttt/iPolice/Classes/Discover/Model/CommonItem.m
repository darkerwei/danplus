//
//  CommonItem.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CommonItem.h"

@implementation CommonItem

// 快速创建一个cell信息
+ (instancetype)commonItemWithTitle:(NSString *)title
{
    CommonItem *theCommonItem = [[self alloc] init];
    theCommonItem.title = title;
    
    return theCommonItem;
}

// 快速创建一个cell信息
+ (instancetype)commonItemWithTitle:(NSString *)title titleImageName:(NSString *)titleImageName
{
    CommonItem *theCommonItem = [[self alloc] init];
    theCommonItem.title = title;
    theCommonItem.titleImageName = titleImageName;
    
    return theCommonItem;
}

// 快速创建一个cell信息
+ (instancetype)commonItemWithTitle:(NSString *)title titleImageName:(NSString *)titleImageName description:(NSString *)description
{
    CommonItem *theCommonItem = [self commonItemWithTitle:title titleImageName:titleImageName];
    theCommonItem.desc = description;
    
    return theCommonItem;
}

@end
