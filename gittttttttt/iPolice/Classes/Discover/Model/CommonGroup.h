//
//  CommonGroup.h
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonGroup : NSObject

/**
 *  cell信息列表
 */
@property (nonatomic, strong) NSArray *commonItemList;

/**
 *  快速创建一个组
 *
 *  @return 一个创建好的组
 */
+ (instancetype)commonGroup;

@end
