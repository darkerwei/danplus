//
//  AdsInfo.h
//  iPolice
//
//  Created by xwy on 15/9/12.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdsInfo : NSObject

//Token
@property (nonatomic, copy) NSString *token;
//用户id
@property (nonatomic, copy) NSString *uid;
//投放点
@property (nonatomic, copy) NSString *placeid;
//是否启用
@property (nonatomic, copy) NSString *available;
//内容开始
@property (nonatomic, copy) NSString *start;
//内容个数
@property (nonatomic, copy) NSString *limit;

//广告id
@property (nonatomic, copy) NSString *advid;
//广告标题
@property (nonatomic, copy) NSString *subject;
//广告链接
@property (nonatomic, copy) NSString *url;
//缩略图url地址
@property (nonatomic, copy) NSString *thumbnail;
//查看数
@property (nonatomic, copy) NSString *views;
//发布人id
@property (nonatomic, copy) NSString *authorid;
//发布人
@property (nonatomic, copy) NSString *author;
//发布时间
@property (nonatomic, copy) NSString *dateline;
//广告开始时间
@property (nonatomic, copy) NSString *starttime;
//广告结束时间
@property (nonatomic, copy) NSString *endtime;
//广告内容
@property (nonatomic, copy) NSString *message;
//扩展字段
@property (nonatomic, copy) NSString *extfield1;
//扩展字段
@property (nonatomic, copy) NSString *extfield2;
//扩展字段
@property (nonatomic, copy) NSString *extfield3;
//扩展字段
@property (nonatomic, copy) NSString *extfield4;
//扩展字段
@property (nonatomic, copy) NSString *extfield5;


+ (instancetype)adsInfo;

//3.5 获取广告列表接口
+ (instancetype)adsInfoWithPlaceid:(NSString *)placeid
                         available:(NSString *)available
                             start:(NSString *)start
                             limit:(NSString *)limit;

//3.7	查看广告详情接口
+ (instancetype)adsInfoWithAdvid:(NSString *)advid;


+ (instancetype)adsInfoWithDic:(NSDictionary *)dict;

- (instancetype)initAdsInfoWithDic:(NSDictionary *)dic;

@end
