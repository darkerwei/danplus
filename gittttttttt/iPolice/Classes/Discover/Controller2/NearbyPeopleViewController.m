//
//  NearbyPeopleViewController.m
//  iPolice
//
//  Created by xwy on 15/9/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NearbyPeopleViewController.h"
#import <MAMapKit.h>
#import "UserInfo.h"
#import "CustomAnnotationView.h"

@interface NearbyPeopleViewController ()<MAMapViewDelegate>
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) NSString *personImage;

@end

@implementation NearbyPeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"附近的人";
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 注册高德地图
//    [MAMapServices sharedServices].apiKey = MAPAPI_Key;
    
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:15 animated:YES];
    
    [self.view addSubview:self.mapView];
    
    
     NSLog(@"%f %f %@", [[NSUserDefaults standardUserDefaults] doubleForKey:@"lat"], [[NSUserDefaults standardUserDefaults] doubleForKey:@"lon"],[[NSUserDefaults standardUserDefaults] objectForKey:@"address"]);
    
    UserInfo *user = [UserInfo userInfoWithmapx:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] doubleForKey:@"lon"]] mapy:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] doubleForKey:@"lat"]] start:@"" limit:@""];
    [[NetworkService sharedInstance] getNearPersonWithUserInfo:user success:^(NSArray *personList) {
        
        self.personInfoList = personList;
        
        //添加大头针
        for (UserInfo *userInfo in self.personInfoList) {
            
            MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
            pointAnnotation.coordinate = CLLocationCoordinate2DMake(userInfo.mapy.doubleValue,userInfo.mapx.doubleValue);
            pointAnnotation.title = userInfo.username;
            pointAnnotation.subtitle = [NSString stringWithFormat:@"距离:%d米",userInfo.distance.intValue];
            self.personImage = userInfo.avatar;
            [self.mapView addAnnotation:pointAnnotation];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
  
}

#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]]){
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
        
        //添加大头针
        for (UserInfo *userInfo in self.personInfoList) {
            if (annotation.coordinate.longitude == userInfo.mapx.doubleValue && annotation.coordinate.latitude == userInfo.mapy.doubleValue) {
                annotationView.imageURL = userInfo.avatar;
            }
        }
        
        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        //将大头针的show关闭，开启气泡的自动显示
        [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.5f];
        
        return annotationView;
    }
    return nil;
}


@end
