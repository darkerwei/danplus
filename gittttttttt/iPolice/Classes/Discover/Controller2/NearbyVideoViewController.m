//
//  NearbyVideoViewController.m
//  iPolice
//
//  Created by xwy on 15/9/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NearbyVideoViewController.h"
#import <MAMapKit.h>
#import "VideoInfo.h"
#import "CustomAnnotationView.h"
#import "NearVideoViewController.h"

@interface NearbyVideoViewController ()<MAMapViewDelegate>
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) NSString *personImage;
@property (nonatomic, strong) NSArray *videoInfoList;

@end

@implementation NearbyVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"附近视频";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStyleDone target:self action:@selector(videoMap)];
    //防止图片渲染
    UIImage *image = [[UIImage imageNamed:@"video_map.jpg"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem.image = image;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:15 animated:YES];
    
    [self.view addSubview:self.mapView];
    
    VideoInfo *theVideoInfo = [VideoInfo videoInfoWithMapx:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] doubleForKey:@"lon"]] mapy:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] doubleForKey:@"lat"]] start:@"0" limit:@""];
    [[NetworkService sharedInstance] getVideosWithVideoInfo:theVideoInfo success:^(NSArray *videoInfoList) {
        
        self.videoInfoList = videoInfoList;
        
        //添加大头针
        for (VideoInfo *videoInfo in self.videoInfoList) {
            
            MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
            pointAnnotation.coordinate = CLLocationCoordinate2DMake(videoInfo.mapy.doubleValue, videoInfo.mapx.doubleValue);
            pointAnnotation.title = videoInfo.name;
            pointAnnotation.subtitle = videoInfo.location;
            self.personImage = videoInfo.thumbnail;
            [self.mapView addAnnotation:pointAnnotation];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]]){
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
        
        //添加大头针
        for (VideoInfo *userInfo in self.videoInfoList) {
            if (annotation.coordinate.longitude == userInfo.mapx.doubleValue && annotation.coordinate.latitude == userInfo.mapy.doubleValue) {
                annotationView.imageURL = userInfo.thumbnail;
            }
        }
        
        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        //将大头针的show关闭，开启气泡的自动显示
        [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.5f];
        
        return annotationView;
    }
    return nil;
}

- (void)videoMap{
    
    NearVideoViewController *near = [[NearVideoViewController alloc] init];
    [self.navigationController pushViewController:near animated:YES];
}


@end
