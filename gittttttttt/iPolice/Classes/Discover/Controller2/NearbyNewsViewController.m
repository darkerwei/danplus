//
//  NearbyNewsViewController.m
//  iPolice
//
//  Created by xwy on 15/9/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NearbyNewsViewController.h"
#import "NewsTableViewCell.h"
#import "Threads.h"
#import "ThreadDetailViewController.h"
#import "AdsInfo.h"
#import "CycleImageCell.h"
#import "NewsDetailViewController.h"

// 图片轮播图片Url
#define ImageUrl1 @"http://www.zxy00804.com/87fb4a4e0f0d472ba66935fa5c288d69.jpg"
#define ImageUrl2 @"http://www.zxy00804.com/banner.jpg"
#define ImageUrl3 @"http://img.dapixie.com/uploads/allimg/111119/1-111119120A5.jpg"
#define ImageUrl4 @"http://img3.3lian.com/2013/c4/98/d/48.jpg"
#define ImageUrl5 @"http://tupian.enterdesk.com/2013/mxy/10/12/7/2.jpg"
#define ImageUrl6 @"http://dmimg.5054399.com/allimg/xztuku/130924005.jpg"

// 图片轮播高度
#define PageViewHeight 116


@interface NearbyNewsViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataList;
@property (nonatomic, assign) int limit;


@property (strong, nonatomic) UICollectionView *cycleCollectionView;
@property (strong, nonatomic) UICollectionViewFlowLayout *cycleCollectionViewFlowLayout;

@property (nonatomic, assign) NSUInteger currentIndex;
@property (nonatomic, strong) NSArray *imagesList;
//图片数组
@property (nonatomic, strong) NSArray *imageURLsList;
@property (nonatomic, strong) NSTimer *timer;
//标题数组
@property (nonatomic, strong) NSArray *subjectsList;
//指示页
@property (nonatomic, strong) UIPageControl *pageControl;

@end

@implementation NearbyNewsViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    [self startTimer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"附近警情";
    
    self.cycleCollectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    // 设置界面布局
    self.cycleCollectionViewFlowLayout.itemSize = CGSizeMake(Main_Screen_Width, PageViewHeight);
    self.cycleCollectionViewFlowLayout.minimumInteritemSpacing = 0;
    self.cycleCollectionViewFlowLayout.minimumLineSpacing = 0;
    self.cycleCollectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.cycleCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, PageViewHeight) collectionViewLayout:self.cycleCollectionViewFlowLayout];
    self.cycleCollectionView.delegate = self;
    self.cycleCollectionView.dataSource = self;
    self.cycleCollectionView.showsHorizontalScrollIndicator = NO;
    self.cycleCollectionView.pagingEnabled = YES;
    [self.view addSubview:self.cycleCollectionView];
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(Main_Screen_Width - 80, CGRectGetMaxY(self.cycleCollectionView.frame) - 30, 60, 30)];
    // 设置pageControl属性
    self.pageControl.numberOfPages = self.imageURLsList.count;
    self.pageControl.userInteractionEnabled = NO;
    self.pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    self.pageControl.pageIndicatorTintColor = [UIColor darkGrayColor];
    [self.view addSubview:self.pageControl];
    
    self.currentIndex = 0;
    // 滚动到第一张图片
    [self.cycleCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    // 去除多余cell
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 180, Main_Screen_Width, Main_Screen_Height-180)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    [self getAds];
    
    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - UICollectionViewDatasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return self.imageURLsList.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  
    CycleImageCell *cell = [CycleImageCell cellWithCollectionView:collectionView indexPath:indexPath];
    cell.imageURL = self.imageURLsList[[self indexWithOffset:indexPath.item]];
    cell.subject = self.subjectsList[[self indexWithOffset:indexPath.item]];
    self.pageControl.currentPage = [self indexWithOffset:indexPath.item];
    return cell;
    
}


- (NSUInteger)indexWithOffset:(NSUInteger)offset {
    return (self.currentIndex + offset - 1 + self.imageURLsList.count) % self.imageURLsList.count;
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSUInteger offset = scrollView.contentOffset.x / scrollView.bounds.size.width;
    if (offset == 0) { // 向左滚动
        if (self.pageControl.currentPage == 0) { // 处理左边界
            self.pageControl.currentPage = self.imageURLsList.count - 1;
        } else {
            self.pageControl.currentPage--;
        }
    } else { // 向右滚动
        if (self.pageControl.currentPage == self.imageURLsList.count - 1) { // 处理右边界
            self.pageControl.currentPage = 0;
        } else {
            self.pageControl.currentPage++;
        }
    }
    
    self.currentIndex = [self indexWithOffset:offset];
    
    // 滚动到第一张图片
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    [self.cycleCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [UIView setAnimationsEnabled:NO];
//    [self.cycleCollectionView reloadItemsAtIndexPaths:@[indexPath]];
    [UIView setAnimationsEnabled:YES];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [self stopTimer];
    
}

- (void)stopTimer {
    if (self.timer == nil) {
        return;
    }
    [self.timer invalidate];
    self.timer = nil;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    [self startTimer];
    
}

- (void)startTimer {
    if (self.timer) {
        return;
    }
    self.timer = [NSTimer timerWithTimeInterval:3.0 target:self selector:@selector(roll) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)roll {
    // 取出当前显示cell
    NSIndexPath *indexPath = [self.cycleCollectionView indexPathsForVisibleItems].lastObject;
    
    // 显示下一张
    [self.cycleCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:(indexPath.item + 1) inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self scrollViewDidEndDecelerating:self.cycleCollectionView];
    });
}

- (NSArray *)imageURLsList
{
    if (_imageURLsList == nil) {
        NSURL *url1 = [NSURL URLWithString:ImageUrl1];
        NSURL *url2 = [NSURL URLWithString:ImageUrl2];
        NSURL *url3 = [NSURL URLWithString:ImageUrl3];
        NSURL *url4 = [NSURL URLWithString:ImageUrl4];
        NSURL *url5 = [NSURL URLWithString:ImageUrl5];
//        NSURL *url6 = [NSURL URLWithString:ImageUrl6];
        
        _imageURLsList = @[url1, url2, url3, url4, url5];

    }
    
    return _imageURLsList;
}

- (NSArray *)subjectsList{
    
    if (_subjectsList == nil) {
        NSString *subject1 = [NSString stringWithFormat:@"网传DIY美容大法：美容还是毁容?"];
        _subjectsList = @[subject1];
    }
    return _subjectsList;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AdsInfo *ads = [AdsInfo adsInfoWithAdvid:[self.imagesList[[self indexWithOffset:indexPath.item]] advid]];
    [[NetworkService sharedInstance] getAdDetailWithAds:ads success:^(AdsInfo *adsList) {

        NewsDetailViewController *detail = [[NewsDetailViewController alloc] init];
        detail.detail = ads;
        detail.title = ads.subject;
        [self.navigationController pushViewController:detail animated:YES];

    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (void)getAds{
    
    AdsInfo *ads = [AdsInfo adsInfoWithPlaceid:@"" available:@"" start:@"" limit:@""];
    [[NetworkService alloc] getAdsWithAds:ads success:^(NSArray *adsList) {
        
        self.imagesList = adsList;
    
        NSMutableArray *imageURLsList = [NSMutableArray array];
        NSMutableArray *subjectsList = [NSMutableArray array];
        for (AdsInfo *ads in adsList) {
            if (ads.thumbnail && ads.subject) {
                
                //截取前五个
                if (imageURLsList.count == 5) {
                    break;
                }
                
                [imageURLsList addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@", ads.thumbnail]]];
                [subjectsList addObject:ads.subject];
            }
        }
        
        self.imageURLsList = [NSArray arrayWithArray:imageURLsList];
        self.subjectsList = [NSArray arrayWithArray:subjectsList];
        
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    [SVProgressHUD showInfoWithStatus:@"正在获取新闻列表"];
    Threads *theThreads = [Threads threadsWithCid:@"" status:@"" style:@"" type:@"" start:@"0" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        [SVProgressHUD showSuccessWithStatus:@"获取新闻列表成功"];
        
        //满10个才显示上拉刷新
        if (threadList.count == self.limit) {
            self.tableView.header.hidden = NO;
        } else if (threadList.count < self.limit) {
            self.tableView.header.hidden = YES;
        }
        
        self.dataList = threadList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    [SVProgressHUD showInfoWithStatus:@"正在获取新闻列表"];
    Threads *theThreads = [Threads threadsWithCid:@"" status:@"0" style:@"1" type:@"" start:@"0" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        [SVProgressHUD showSuccessWithStatus:@"获取新闻列表成功"];
        
        //满10个才显示上拉刷新
        if (threadList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取新闻列表成功"];
        } else if (threadList.count < self.limit) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的记录了"];
        }
        
        self.dataList = threadList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];

}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsTableViewCell *cell = [NewsTableViewCell cellWithTableView:tableView];
    cell.theThreads = self.dataList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Threads *theThreads = self.dataList[indexPath.row];
    [[NetworkService sharedInstance] getThreadDetailWithThreads:theThreads success:^(Threads *threads) {
     
        ThreadDetailViewController *threadDetailVC = [[ThreadDetailViewController alloc] init];
        threadDetailVC.theThreads = threads;
        [self.navigationController pushViewController:threadDetailVC animated:YES];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

@end
