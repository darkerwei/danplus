//
//  NewsDetailViewController.m
//  iPolice
//
//  Created by xwy on 15/9/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "AdsInfo.h"
#import "OneTableViewCell.h"
#import "TwoTableViewCell.h"

@interface NewsDetailViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation NewsDetailViewController

- (void)setDetail:(AdsInfo *)detail{
    
    _detail = detail;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = self.detail.subject;
    
    UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height)];
    if (![self.detail.url isEqualToString:@""]) {
        [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.detail.url]]];
    }
    
    webview.scrollView.bounces = NO;
    [self.view addSubview:webview];
    
    if ([self.detail.url isEqualToString:@""]) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, Main_Screen_Height-64)];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.tableFooterView = [[UIView alloc] init];
        //没有分割线
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.tableView];
    }

}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        OneTableViewCell *cell = [OneTableViewCell cellWithTableView:tableView];
        cell.threads = self.detail;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        TwoTableViewCell *cell = [TwoTableViewCell cellWithTableView:tableView];
        cell.threads = self.detail;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return 0;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 70;
    } else {
        return 250;
    }
}

@end
