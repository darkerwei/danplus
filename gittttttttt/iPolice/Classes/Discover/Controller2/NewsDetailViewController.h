//
//  NewsDetailViewController.h
//  iPolice
//
//  Created by xwy on 15/9/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AdsInfo;

@interface NewsDetailViewController : UIViewController

@property (nonatomic, strong) AdsInfo *detail;

@end
