//
//  NearbyPoliceViewController.m
//  iPolice
//
//  Created by xwy on 15/9/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NearbyPoliceViewController.h"
#import <MAMapKit.h>
#import "CustomAnnotationView.h"
#import <AMapSearchKit/AMapSearchAPI.h>


@interface NearbyPoliceViewController()<MAMapViewDelegate, AMapSearchDelegate>
{
    float myLatitude;
    float myLongitude;
    AMapPOI *poi;
    AMapSearchAPI *_search;
    NSMutableArray *poiAnnotations;
}
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) NSArray *dateArray;

@end

@implementation NearbyPoliceViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 注册高德地图
//    [MAMapServices sharedServices].apiKey = MAPAPI_Key;
    
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:13 animated:YES];
    [self.view addSubview:self.mapView];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"附近派出所";
    
    //初始化检索对象
    _search = [[AMapSearchAPI alloc] init];
    _search.delegate = self;
    
    //构造AMapPlaceSearchRequest对象，配置关键字搜索参数
    AMapPOIAroundSearchRequest *poiRequest = [[AMapPOIAroundSearchRequest alloc] init];
//    poiRequest.searchType = AMapSearchType_PlaceAround;
    poiRequest.location = [AMapGeoPoint locationWithLatitude:[[NSUserDefaults standardUserDefaults] doubleForKey:@"lat"] longitude:[[NSUserDefaults standardUserDefaults] doubleForKey:@"lon"]];
    poiRequest.keywords = @"派出所";
    poiRequest.radius = 3000;
    poiRequest.requireExtension = YES;
    //发起POI搜索
    [_search AMapPOIAroundSearch: poiRequest];
    
}

//实现POI搜索对应的回调函数
-(void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response{
    if(response.pois.count == 0)
    {
        return;
    }
    
    poiAnnotations = [NSMutableArray arrayWithCapacity:response.pois.count];
    
    [response.pois enumerateObjectsUsingBlock:^(AMapPOI *obj, NSUInteger idx, BOOL *stop) {
        
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        pointAnnotation.coordinate = CLLocationCoordinate2DMake(poi.location.latitude, poi.location.longitude);
        pointAnnotation.title = [NSString stringWithFormat:@"%@",poi.name];
        pointAnnotation.subtitle = [NSString stringWithFormat:@"%@", poi.address];
        poi = obj;
        [poiAnnotations addObject:pointAnnotation];
        
    }];
    
    /* 将结果以annotation的形式加载到地图上. */
    [self.mapView addAnnotations:poiAnnotations];
}
//- (void)onPlaceSearchDone:(AMapPlaceSearchRequest *)request response:(AMapPlaceSearchResponse *)response
//{
//    
//}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]]){

        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
        

        annotationView.imageURL = @"http://img01.tooopen.com/downs/images/2010/12/8/sy_20101208200157718113.jpg";
        
        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        //将大头针的show关闭，开启气泡的自动显示
        [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.5f];
        
        return annotationView;
        
//        static NSString *annotationName = @"annocation";
//        MAPinAnnotationView *annotationView = (MAPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationName];
//        if(annotationView == nil)
//        {
//            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationName];
//        }
//        // 开启下落动画
//        annotationView.animatesDrop = YES;
//        // 颜色
//        annotationView.pinColor = MAPinAnnotationColorGreen;
//        
//        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
//        // 开启标注点信息
//        annotationView.canShowCallout = YES;
//        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
//        annotationView.centerOffset = CGPointMake(0, -18);

    }
    return nil;
}


-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
//        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
        myLatitude = userLocation.coordinate.latitude;
        myLongitude = userLocation.coordinate.longitude;
    }
}
@end
