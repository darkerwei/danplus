//
//  NearbyPeopleViewController.h
//  iPolice
//
//  Created by xwy on 15/9/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserInfo;

@interface NearbyPeopleViewController : UIViewController

@property (nonatomic, strong) NSArray *personInfoList;

@end
