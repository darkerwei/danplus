//
//  DiscoverViewController.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "DiscoverViewController.h"
#import "CommonCell.h"
#import "CommonItem.h"
#import "CommonGroup.h"
#import "ShopViewController.h"
#import "NearbyNewsViewController.h"
#import "NearbyVideoViewController.h"
#import "NearbyPoliceViewController.h"
#import "NearbyPeopleViewController.h"
#import "ZCZBarViewController.h"

// Footer的高度
#define FooterHeight 0
// Header的高度
#define HeaderHeight 10
#define IOS7 [[[UIDevice currentDevice] systemVersion]floatValue]>=7

// cell信息
#define CommonItem1Title @"附近警情"
#define CommonItem1TitleImageName @"附近警情"
#define CommonItem2Title @"附近视频"
#define CommonItem2TitleImageName @"附近视频"
#define CommonItem3Title @"附近派出所"
#define CommonItem3TitleImageName @"附近派出所"
#define CommonItem4Title @"附近的人"
#define CommonItem4TitleImageName @"附近的人"
#define CommonItem5Title @"扫一扫"
#define CommonItem5TitleImageName @"扫一扫"
#define CommonItem6Title @"积分商城"
#define CommonItem6TitleImageName @"商城"

@interface DiscoverViewController ()<UITableViewDataSource, UITableViewDelegate>

/**
 *  整个页面
 */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/**
 *  组列表
 */
@property (nonatomic, strong) NSArray *commonGroupList;

@end

@implementation DiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置tableView属性
    self.tableView.sectionFooterHeight = FooterHeight;
    self.tableView.sectionHeaderHeight = HeaderHeight;
    
    // 调整cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(-24, 0, 0, 0);

}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.commonGroupList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CommonGroup *theCommonGroup = self.commonGroupList[section];
    
    return theCommonGroup.commonItemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommonCell *cell = [CommonCell cellWithTableView:tableView];
    CommonGroup *theCommonGroup = self.commonGroupList[indexPath.section];
    cell.theCommonItem = theCommonGroup.commonItemList[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        NSLog(@"在这里处理<周边警情>点击事件");
        NearbyNewsViewController *news = [[NearbyNewsViewController alloc] init];
        [self.navigationController pushViewController:news animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 1) {
        NSLog(@"在这里处理<周边视频>点击事件");
        NearbyVideoViewController *video = [[NearbyVideoViewController alloc] init];
        [self.navigationController pushViewController:video animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 2) {
        NSLog(@"在这里处理<周边派出所>点击事件");
        NearbyPoliceViewController *police = [[NearbyPoliceViewController alloc] init];
        [self.navigationController pushViewController:police animated:YES];
    }else if (indexPath.section == 0 && indexPath.row == 3) {
        NSLog(@"在这里处理<周边的人>点击事件");
        NearbyPeopleViewController *people = [[NearbyPeopleViewController alloc] init];
        [self.navigationController pushViewController:people animated:YES];
    }else if (indexPath.section == 1 && indexPath.row == 0) {
        NSLog(@"在这里处理<扫一扫>点击事件");
        
        ZCZBarViewController *vc=[[ZCZBarViewController alloc] initWithBlock:^(NSString *result, BOOL isFinish) {
            if (isFinish) {
                NSLog(@"最后的结果%@",result);
                //跳转到应用页面
                NSString *str = [NSString stringWithFormat:@"%@",result];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
            }
        }];
        [self presentViewController:vc animated:YES completion:nil];
        
    }else if (indexPath.section == 2 && indexPath.row == 0) {
        NSLog(@"在这里处理<商城>点击事件");
        ShopViewController *shop = [[ShopViewController alloc] init];
        shop.title = @"积分商场";
        [self.navigationController pushViewController:shop animated:YES];
        
    }
}

#pragma mark - 懒加载

- (NSArray *)commonGroupList
{
    if (_commonGroupList == nil) {
        
        // 创建组
        CommonGroup *commonGroup1 = [CommonGroup commonGroup];
        CommonGroup *commonGroup2 = [CommonGroup commonGroup];
        CommonGroup *commonGroup3 = [CommonGroup commonGroup];

        // 创建cell信息
        CommonItem *CommonItem1 = [CommonItem commonItemWithTitle:CommonItem1Title titleImageName:CommonItem1TitleImageName];
        CommonItem *CommonItem2 = [CommonItem commonItemWithTitle:CommonItem2Title titleImageName:CommonItem2TitleImageName];
        CommonItem *CommonItem3 = [CommonItem commonItemWithTitle:CommonItem3Title titleImageName:CommonItem3TitleImageName];
        CommonItem *CommonItem4 = [CommonItem commonItemWithTitle:CommonItem4Title titleImageName:CommonItem4TitleImageName];
        commonGroup1.commonItemList = @[CommonItem1, CommonItem2, CommonItem3, CommonItem4];
        
        CommonItem *CommonItem5 = [CommonItem commonItemWithTitle:CommonItem5Title titleImageName:CommonItem5TitleImageName];
        commonGroup2.commonItemList = @[CommonItem5];
        
        CommonItem *CommonItem6 = [CommonItem commonItemWithTitle:CommonItem6Title titleImageName:CommonItem6TitleImageName];
        commonGroup3.commonItemList = @[CommonItem6];
        
        _commonGroupList = @[commonGroup1, commonGroup2, commonGroup3];
    }
    
    return _commonGroupList;
}

@end
