//
//  ConsultInfo.h
//  iPolice
//
//  Created by xwy on 15/7/30.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConsultInfo : NSObject

//指南id
@property (nonatomic, copy) NSString *idid;
//办事指南id
@property (nonatomic, copy) NSString *personid;
//部门id
@property (nonatomic, copy) NSString *devid;
//内容开始
@property (nonatomic, copy) NSString *start;
//内容个数
@property (nonatomic, copy) NSString *limit;
//发布人id
@property (nonatomic, copy) NSString *uid;
//发布人
@property (nonatomic, copy) NSString *username;
//标题
@property (nonatomic, copy) NSString *subject;
//发布时间
@property (nonatomic, strong) NSNumber *dateline;
//是否有附件
@property (nonatomic, copy) NSArray *attachments;
//指南消息
@property (nonatomic, copy) NSString *message;
//扩展信息1
@property (nonatomic, copy) NSString *extfield1;
//扩展信息2
@property (nonatomic, copy) NSString *extfield2;
//扩展信息3
@property (nonatomic, copy) NSString *extfield3;
//扩展信息4
@property (nonatomic, copy) NSString *extfield4;
//扩展信息5
@property (nonatomic, copy) NSString *extfield5;

+ (instancetype)consultInfo;


//7.6 查看指南详情接
+ (instancetype)consultInfoWithId:(NSString *)Id;

+ (instancetype)consultInfoWithDict:(NSDictionary *)dict;
- (instancetype)initWithConsultDict:(NSDictionary *)dict;

@end
