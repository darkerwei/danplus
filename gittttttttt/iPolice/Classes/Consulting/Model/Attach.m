//
//  Attach.m
//  iPolice
//
//  Created by xwy on 15/8/5.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Attach.h"

@implementation Attach
/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)attach
{
    return [[self alloc] init];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)attachWithDict:(NSDictionary *)dict
{
    return [[self alloc] initAttachWithDict:dict];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initAttachWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.desc = dict[@"description"];
        self.thumbnail = dict[@"thumbnail"];
        self.name = dict[@"name"];
        self.aid = dict[@"aid"];
        self.type = dict[@"type"];
        self.uri = dict[@"uri"];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\"description\":\"%@\", \"thumbnail\":\"%@\", \"name\":\"%@\", \"aid\":\"%@\", \"type\":\"%@\", \"uri\":\"%@\"}", self.desc, self.thumbnail, self.name, self.aid, self.type, self.uri];
}

@end
