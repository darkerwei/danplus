//
//  Consult.h
//  iPolice
//
//  Created by xwy on 15/8/5.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Consult : NSObject

//图标url
@property (nonatomic, copy) NSString *icon;
//名称
@property (nonatomic, copy) NSString *name;
//描述
@property (nonatomic, copy) NSString *desc;

//办事指南id
@property (nonatomic, copy) NSString *personid;
//部门id
@property (nonatomic, copy) NSString *devid;
//内容开始
@property (nonatomic, copy) NSString *start;
//内容个数
@property (nonatomic, copy) NSString *limit;
//发布人id
@property (nonatomic, copy) NSString *uid;
//发布人
@property (nonatomic, copy) NSString *username;
//发布人
@property (nonatomic, copy) NSString *message;
//标题
@property (nonatomic, copy) NSString *subject;
//关键字
@property (nonatomic, copy) NSString *keyword;
//发布时间
@property (nonatomic, copy) NSString *dateline;
//是否有附件
@property (nonatomic, copy) NSArray *attachments;

+ (instancetype)consult;

//7.5 获取业务指南列表接口
+ (instancetype)consultWithDevid:(NSString *)devid
                           start:(NSString *)start
                           limit:(NSString *)limit;

//7.7 搜索指南接口
+ (instancetype)consultWithDevid:(NSString *)devid
                         keyword:(NSString *)keyword
                           start:(NSString *)start
                           limit:(NSString *)limit;

+ (instancetype)consultWithDict:(NSDictionary *)dict;
- (instancetype)initConsultWithDict:(NSDictionary *)dict;
@end
