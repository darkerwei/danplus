//
//  ConsultInfo.m
//  iPolice
//
//  Created by xwy on 15/7/30.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ConsultInfo.h"
#import "Attach.h"

@implementation ConsultInfo

+ (instancetype)consultInfo{
    
    return [[self alloc] init];
}

//7.6 查看指南详情接
+ (instancetype)consultInfoWithId:(NSString *)Id{
    
    ConsultInfo *consultInfo = [self consultInfo];
    consultInfo.idid = Id;
    return consultInfo;
}

+ (instancetype)consultInfoWithDict:(NSDictionary *)dict{
    
    return [[self alloc] initWithConsultDict:dict];
}

-(instancetype)initWithConsultDict:(NSDictionary *)dict{
    
    self = [super init];
    if (self) {
        self.personid = dict[@"id"];
        self.devid = dict[@"devid"];
        self.uid = dict[@"uid"];
        self.idid = dict[@"id"];
        self.username = dict[@"username"];
        self.subject = dict[@"subject"];
        self.dateline = dict[@"dateline"];
        self.message = dict[@"message"];
        self.extfield1 = dict[@"extfield1"];
        self.extfield2 = dict[@"extfield2"];
        self.extfield3 = dict[@"extfield3"];
        self.extfield4 = dict[@"extfield4"];
        self.extfield5 = dict[@"extfield5"];
        
        
        NSArray *consultList = dict[@"attachments"];
        NSMutableArray *tempConsultList = [NSMutableArray array];
        for (NSDictionary *dict in consultList) {
            [tempConsultList addObject:[Attach attachWithDict:dict]];
        }
        self.attachments = tempConsultList;
    }
    return self;
}
@end
