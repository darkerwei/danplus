//
//  Consult.m
//  iPolice
//
//  Created by xwy on 15/8/5.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Consult.h"

@implementation Consult

+ (instancetype)consult{
    
    return [[self alloc] init];
}


//7.5接口
+ (instancetype)consultWithDevid:(NSString *)devid
                           start:(NSString *)start
                           limit:(NSString *)limit{
    
    Consult *consult = [self consult];
    consult.devid = devid;
    consult.start = start;
    consult.limit = limit;
    
    return consult;
    
}


//7.7 搜索指南接口
+ (instancetype)consultWithDevid:(NSString *)devid
                         keyword:(NSString *)keyword
                           start:(NSString *)start
                           limit:(NSString *)limit{
    
    Consult *consult = [self consult];
    
    consult.devid = devid;
    consult.keyword = keyword;
    consult.start = start;
    consult.limit = limit;
    
    return consult;
}

+ (instancetype)consultWithDict:(NSDictionary *)dict{
    
    return [[self alloc] initConsultWithDict:dict];
}

- (instancetype)initConsultWithDict:(NSDictionary *)dict{
    
    self = [super init];
    if (self) {
        self.name = dict[@"name"];
        self.icon = dict[@"icon"];
        self.desc = dict[@"description"];
        
        self.personid = dict[@"id"];
        self.devid = dict[@"devid"];
        self.uid = dict[@"uid"];
        self.username = dict[@"username"];
        self.message = dict[@"message"];
        self.subject = dict[@"subject"];
        self.dateline = dict[@"dateline"];
        self.attachments = dict[@"attachments"];
        self.keyword = dict[@"keyword"];
    }
    return self;
}
@end
