//
//  Attach.h
//  iPolice
//
//  Created by xwy on 15/8/5.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Attach : NSObject
//附件ID
@property (nonatomic, copy) NSString *aid;
//附件名称
@property (nonatomic, copy) NSString *name;
//附件类型
@property (nonatomic, copy) NSString *type;
//附件路径
@property (nonatomic, copy) NSString *uri;
//缩略图
@property (nonatomic, copy) NSString *thumbnail;
//说明
@property (nonatomic, copy) NSString *desc;
/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)attach;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)attachWithDict:(NSDictionary *)dict;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initAttachWithDict:(NSDictionary *)dict;
@end
