//
//  TableviewDetailViewController.m
//  iPolice
//
//  Created by xwy on 15/8/3.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TableviewDetailViewController.h"
#import "NetworkService.h"
#import "ConsultInfo.h"
#import <SVProgressHUD.h>
#import "Attach.h"
#import "Consult.h"
#import "CommentTableViewCell.h"

@interface TableviewDetailViewController ()<UITableViewDelegate>

@property(nonatomic, strong) UILabel *timeLabel;
@property(nonatomic, strong) UILabel *personLabel;


@end

@implementation TableviewDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//     Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = self.consultInfo.subject;
    
    //发布时间
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, 300, 30)];
    self.timeLabel.text = @"";
    self.timeLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:self.timeLabel];
    
    //发布人
    self.personLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 300, 30)];
    self.personLabel.text = @"";
    self.personLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:self.personLabel];
    
    //发布人
    self.personLabel.text = [NSString stringWithFormat:@"发布人: %@",self.consultInfo.username];
    
    //发布时间
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *issueTime = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.consultInfo.dateline.doubleValue /1000.0]];
    self.timeLabel.text = [NSString stringWithFormat:@"发布时间: %@", issueTime];
    
    //发布详情
    UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(10, 130, Main_Screen_Width-20, Main_Screen_Height-130)];
    [webview loadHTMLString:self.consultInfo.message baseURL:nil];
    webview.scrollView.bounces = NO;
    [self.view addSubview:webview];

}

@end
