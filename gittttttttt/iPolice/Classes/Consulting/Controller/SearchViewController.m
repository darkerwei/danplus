//
//  SearchViewController.m
//  iPolice
//
//  Created by xwy on 15/10/12.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "SearchViewController.h"
#import "Consult.h"
#import "CommentTableViewCell.h"
#import "TableviewDetailViewController.h"

// 搜索框高度
#define SearchBarHeight 44
@interface SearchViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

//tableview的标题
@property (strong, nonatomic)UITableView *tableView;
@property (nonatomic, strong) NSArray *subjectList;
@property (nonatomic, assign) int limit;
/**
 *  搜索框
 */
@property (nonatomic, strong) UISearchBar *searchBar;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // 设置搜索框
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, SearchBarHeight)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"请输入关键字";
    [self.view addSubview:self.searchBar];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64+SearchBarHeight, Main_Screen_Width, Main_Screen_Height-64-SearchBarHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];
    
    [self refresh];
    [self upRefresh];
}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    Consult *consult = [Consult consultWithDevid:@"" start:@"0" limit:@"10"];
    [SVProgressHUD showInfoWithStatus:@"正在获取列表"];
    //获得tableview的标题
    [[NetworkService sharedInstance] getGuidesWithConsult:consult success:^(NSArray *consultList) {
        [SVProgressHUD showSuccessWithStatus:@"获取列表成功"];
        
        // 视频满10个才显示上拉刷新
        if (consultList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (consultList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        
        self.subjectList = consultList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
    
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    Consult *consult = [Consult consultWithDevid:@"" start:@"" limit:@"10000"];
    [SVProgressHUD showInfoWithStatus:@"正在获取列表"];
    //获得tableview的标题
    [[NetworkService sharedInstance] getGuidesWithConsult:consult success:^(NSArray *consultList) {
        [SVProgressHUD showSuccessWithStatus:@"获取列表成功"];
        
        // 视频满10个才显示上拉刷新
        if (consultList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取列表成功"];
        } else if (consultList.count < self.limit-5) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的信息了"];
        }
        
        self.subjectList = consultList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];
    
}



#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.subjectList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [CommentTableViewCell cellWithTableView:tableView];
    cell.consultInfo = self.subjectList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [CommentTableViewCell cellWithTableView:tableView];
    cell.consultInfo = self.subjectList[indexPath.row];
    
    TableviewDetailViewController *tableviewDetail = [[TableviewDetailViewController alloc] init];
    tableviewDetail.consultInfo = cell.consultInfo;
    [self.navigationController pushViewController:tableviewDetail animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    Consult *consult = [Consult consultWithDevid:@"" keyword:searchBar.text start:@"" limit:@""];
    //获得tableview的标题
    [[NetworkService sharedInstance] searchGuidesWithConsult:consult success:^(NSArray *consultList) {
        
        self.tableView.footer.hidden = YES;
        
        self.subjectList = consultList;
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        
    }];
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    Consult *consult = [Consult consultWithDevid:@"" keyword:searchBar.text start:@"" limit:@""];
    //获得tableview的标题
    [[NetworkService sharedInstance] searchGuidesWithConsult:consult success:^(NSArray *consultList) {
        
        self.tableView.footer.hidden = YES;
        
        self.subjectList = consultList;
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        
    }];
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBar.text = @"";
}

@end
