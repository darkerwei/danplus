//
//  ButtonDetailViewController.h
//  iPolice
//
//  Created by xwy on 15/8/3.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConsultInfo;

@interface ButtonDetailViewController : UIViewController

@property (nonatomic, strong) ConsultInfo *consultInfo;
@end
