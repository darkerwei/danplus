//
//  ConsultingViewController.m
//  iPolice
//
//  Created by xwy on 15/7/30.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ConsultingViewController.h"
#import "NetworkService.h"
#import "ConsultInfo.h"
#import <SVProgressHUD.h>
#import "ButtonDetailViewController.h"
#import "CommentTableViewCell.h"
#import "NetworkService.h"
#import "TableviewDetailViewController.h"
#import <UIButton+WebCache.h>
#import "Attach.h"
#import "Consult.h"
#import "SearchViewController.h"

#define backgroundHeight ([UIScreen mainScreen].bounds.size.height-64)/4
#define buttonWeight 30
@interface ConsultingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIButton *onlineButton;
@property (nonatomic, strong) UIButton *phoneButton;
@property (nonatomic, strong) UIView *onlineLabel;
@property (nonatomic, strong) UIView *phoneLabel;

//部门按钮默认图片数组
@property (nonatomic, strong) NSArray *buttonArray;
//部门标签
@property (nonatomic, strong) UILabel *departmentLabel;
//部门按钮数组
@property (nonatomic, strong) NSArray *buttonList;
//部门标签数组
@property (nonatomic, strong) NSArray *labelArray;

//业务咨询页的tableview
@property (nonatomic, strong) UITableView *tableView;
//tableview的标题
@property (nonatomic, strong) NSArray *subjectList;
@property (nonatomic, assign) int limit;

@end

@implementation ConsultingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"业务咨询";
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //背景图
    UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, backgroundHeight)];
    UIImage *image = [UIImage imageNamed:@"业务咨询背景图"];
    background.layer.contents = (id)image.CGImage;
    [self.view addSubview:background];
    
    //在线咨询按钮
    self.onlineButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonWeight, 64, 120, 100)];
    [self.onlineButton setImage:[UIImage imageNamed:@"在线咨询按钮"] forState:UIControlStateNormal];
    [self.onlineButton addTarget:self action:@selector(clickOnline) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.onlineButton];
    
    //在线咨询文字
    self.onlineLabel = [[UIView alloc] initWithFrame:CGRectMake(buttonWeight+20, 164, 80, 20)];
    UIImage *image1 = [UIImage imageNamed:@"在线咨询文字"];
    self.onlineLabel.layer.contents = (id)image1.CGImage;
    [self.view addSubview:self.onlineLabel];
    
    //电话咨询按钮
    self.phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width-buttonWeight-120, 64, 120, 100)];
    [self.phoneButton setImage:[UIImage imageNamed:@"电话咨询按钮"] forState:UIControlStateNormal];
    [self.phoneButton addTarget:self action:@selector(clickPhone) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.phoneButton];
    
    //电话咨询文字
    self.phoneLabel = [[UIView alloc] initWithFrame:CGRectMake(Main_Screen_Width-buttonWeight-100, 164, 80, 20)];
    UIImage *image2 = [UIImage imageNamed:@"电话咨询文字"];
    self.phoneLabel.layer.contents = (id)image2.CGImage;
    [self.view addSubview:self.phoneLabel];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStyleDone target:self action:@selector(searchButton)];
    //防止图片渲染
    UIImage *searchImage = [[UIImage imageNamed:@"搜索.jpg"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem.image = searchImage;
    
    //请求button
    [self getButtonList];
    
    //设置tableview界面
    [self setTableView];
    
    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];
}

- (void)searchButton{
    
    SearchViewController *search = [[SearchViewController alloc] init];
    search.title = @"关键字搜索";
    [self.navigationController pushViewController:search animated:YES];
}

#pragma marks -- 四个button按钮
- (void)setButton{
    
    CGFloat w1 = Main_Screen_Width*0.08;
    CGFloat w2 = Main_Screen_Width*0.12;
    CGFloat w3 = Main_Screen_Width*0.24;
    
    for (int i = 0; i < self.buttonList.count; i++) {
        
        self.buttonArray = [NSArray arrayWithObjects:@"1.png", @"2.png", @"3.png", @"4.png", nil];
        self.labelArray = [NSArray arrayWithObjects:@"出入境业务指南",@"户籍业务指南",@"治安业务指南",@"其他业务指南", nil];
        //赋值
        Consult *consult = self.buttonList[i];
        
        //部门按钮
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(w1+i*2*w2, 64+backgroundHeight+10, w2, w2);
        [button sd_setImageWithURL:[NSURL URLWithString:consult.icon] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:self.buttonArray[i]]];
        button.tag = i;
        [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        
        //部门标签
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(i*w3+10, 64+backgroundHeight+20+w2, w3-2, 13)];
        label.font = [UIFont systemFontOfSize:11];
        label.text = consult.name;
        label.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:label];
    }
}

- (void)getButtonList{
    
    [[NetworkService sharedInstance] getDepartmentsWithConsultInfo:^(NSArray *consultList) {
     
        self.buttonList = consultList;
        
        //设置button按钮视图
        [self setButton];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}

    
    
//部门的点击事件
- (void)clickButton:(UIButton *)button{
    
    ButtonDetailViewController *buttonDetail = [[ButtonDetailViewController alloc] init];
    buttonDetail.title = self.labelArray[button.tag];
    [self.navigationController pushViewController:buttonDetail animated:NO];

}

#pragma marks -- tableview页面
- (void)setTableView{
    
    UIView *v =[[UIView alloc] initWithFrame:CGRectMake(0, 64+backgroundHeight+80+1, Main_Screen_Width, 3)];
    v.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:v];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64+backgroundHeight+80+3, Main_Screen_Width, Main_Screen_Height-64-backgroundHeight-100)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];
    
    self.subjectList = [[NSArray alloc] init];
    
}

//在线咨询按钮触发
- (void)clickOnline{
    NSLog(@"在这里处理<在线咨询>");
}
//电话咨询按钮触发
- (void)clickPhone{
    NSLog(@"在这里处理<电话咨询>");
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.subjectList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [CommentTableViewCell cellWithTableView:tableView];
    cell.consultInfo = self.subjectList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [CommentTableViewCell cellWithTableView:tableView];
    cell.consultInfo = self.subjectList[indexPath.row];
    
    TableviewDetailViewController *tableviewDetail = [[TableviewDetailViewController alloc] init];
    tableviewDetail.consultInfo = cell.consultInfo;
    [self.navigationController pushViewController:tableviewDetail animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    Consult *consult = [Consult consultWithDevid:@"" start:@"0" limit:@"10"];
    [SVProgressHUD showInfoWithStatus:@"正在获取列表"];
    //获得tableview的标题
    [[NetworkService sharedInstance] getGuidesWithConsult:consult success:^(NSArray *consultList) {
        [SVProgressHUD showSuccessWithStatus:@"获取列表成功"];
        
        // 视频满10个才显示上拉刷新
        if (consultList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (consultList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }

        self.subjectList = consultList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];

}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    Consult *consult = [Consult consultWithDevid:@"" start:@"" limit:@"10000"];
    [SVProgressHUD showInfoWithStatus:@"正在获取列表"];
    //获得tableview的标题
    [[NetworkService sharedInstance] getGuidesWithConsult:consult success:^(NSArray *consultList) {
        [SVProgressHUD showSuccessWithStatus:@"获取列表成功"];
        
        // 视频满10个才显示上拉刷新
        if (consultList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取列表成功"];
        } else if (consultList.count < self.limit-5) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的信息了"];
        }
        
        self.subjectList = consultList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];

}


@end
