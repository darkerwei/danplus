//
//  ButtonDetailViewController.m
//  iPolice
//
//  Created by xwy on 15/8/3.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ButtonDetailViewController.h"
#import "CommentTableViewCell.h"
#import "TableviewDetailViewController.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import "ConsultInfo.h"
#import "Consult.h"

@interface ButtonDetailViewController ()<UITableViewDelegate, UITableViewDataSource>
//业务咨询页的tableview
@property (nonatomic, strong) UITableView *tableView;
//tableview的标题
@property (nonatomic, strong) NSArray *subjectList;

@end

@implementation ButtonDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setTableView];
    
    [self getTableviewList];
    
    [self getConsult];
}

#pragma marks -- tableview页面
- (void)setTableView{
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height)];
    self.tableView.bounces = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];
    
    self.subjectList = [[NSArray alloc] init];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.subjectList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [CommentTableViewCell cellWithTableView:tableView];
    cell.consultInfo = self.subjectList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [CommentTableViewCell cellWithTableView:tableView];
    cell.consultInfo = self.subjectList[indexPath.row];
    
    TableviewDetailViewController *detail = [[TableviewDetailViewController alloc] init];
    detail.consultInfo = cell.consultInfo;
    detail.title = cell.consultInfo.subject;
    [self.navigationController pushViewController:detail animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

#pragma mark -- getTableviewList
- (void)getTableviewList{
    
    NSString *devid;
    if ([self.title isEqualToString:@"出入境业务指南"]) {
        devid = [NSString stringWithFormat:@"bd806b9306584836bc5ee0911f5a8289"];
    } else if ([self.title isEqualToString:@"户籍业务指南"]) {
        devid = [NSString stringWithFormat:@"e09e424a70634692914e0452a4966d21"];
    } else if ([self.title isEqualToString:@"治安业务指南"]) {
        devid = [NSString stringWithFormat:@"85fd86e6ff0b4d528957d1f9ab74d302"];
    } else if ([self.title isEqualToString:@"其他业务指南"]) {
        devid = [NSString stringWithFormat:@"456043f662a1496b846d9c5fd167ae6a"];
    }
    
    Consult *consult = [Consult consultWithDevid:devid start:@"" limit:@""];
    //获得tableview的标题
    [[NetworkService sharedInstance] getGuidesWithConsult:consult success:^(NSArray *consultList) {
     
        self.subjectList = consultList;
        [self.tableView reloadData];
    
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];

}

- (void)getConsult{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, Main_Screen_Height-100, Main_Screen_Height, 100)];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:view];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 150, 40)];
    label.text = @"没有您的问题吗?";
    label.textAlignment = NSTextAlignmentLeft;
    [view addSubview:label];
    
    UIButton *button1 = [[UIButton alloc] initWithFrame:CGRectMake(20, 40, Main_Screen_Width/2-40, 60)];
    [button1 setImage:[UIImage imageNamed:@"联系在线客服.png"] forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(clickButton1) forControlEvents:UIControlEventTouchUpInside];
    [button1 setTitle:[NSString stringWithFormat:@"联系在线咨询"] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont systemFontOfSize:15];
    [button1 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [view addSubview:button1];
    
    UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width/2+20, 40, Main_Screen_Width/2-40, 60)];
    [button2 setImage:[UIImage imageNamed:@"联系电话客服.png"] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(clickButton2) forControlEvents:UIControlEventTouchUpInside];
    [button2 setTitle:[NSString stringWithFormat:@"联系电话咨询"] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont systemFontOfSize:15];
    [button2 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [view addSubview:button2];
}

- (void)clickButton1{
    
}
- (void)clickButton2{
    
}
@end
