//
//  CommentTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/8/3.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "ConsultInfo.h"

#define CommentTableViewCellIdentifier @"CommentTableViewCell"

@interface CommentTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;

@end
@implementation CommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setConsultInfo:(ConsultInfo *)consultInfo{
    
    _consultInfo = consultInfo;
    self.subjectLabel.text = [NSString stringWithFormat:@"%@", consultInfo.subject];
    self.subjectLabel.font = [UIFont systemFontOfSize:13];
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:CommentTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:CommentTableViewCellIdentifier];
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CommentTableViewCellIdentifier];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
@end
