//
//  HttpClient.h
//  iPolice
//
//  Created by ioswei on 15/6/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

@interface HttpClient : NSObject

/**
 *  发送网络请求
 *
 *  @param url     请求URL
 *  @param method  请求方式(GET/POST)
 *  @param params  请求参数
 *  @param success 请求成功时的回调
 *  @param failure 请求失败时的回调
 */
- (void)sendRequestWithURL:(NSString *)url
                    method:(NSString *)method
                    params:(NSString *)params
                   success:(void (^)(NSDictionary *responseObject))success
                   failure:(void (^)(NSError *error))failure;


/**
 *  上传图片
 */
+ (AFHTTPRequestOperationManager *)sharedHTTPManager;
@end
