//
//  NetworkService.h
//  iPolice
//
//  Created by ioswei on 15/6/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserInfo;
@class VideoInfo;
@class Threads;
@class ConsultInfo;
@class Consult;
@class Police;
@class PostThreads;
@class PostAlarm;
@class TagModel;
@class ComplaintModel;
@class ShopModel;
@class CreditsModel;
@class DriverInfo;
@class DoubleName;
@class AdsInfo;

@interface NetworkService : NSObject

/**
 *  创建网络服务
 *
 *  @return 网络服务(单例)
 */
+(instancetype)sharedInstance;

/**
 *  获取频道接口2.4
 *
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getChannel:(void (^)(NSArray *channelList))success
          failure:(void (^)(NSError *error))failure;

/**
 *  发布主题接口 2.9
 *
 *  @param postThreads 发布主题接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)postThreadsWithPostThreads:(PostThreads *)postThreads
                          success:(void (^)(void))success
                          failure:(void (^)(NSError *error))failure;
/**
 *  获取主题列表接口2.12
 *
 *  @param thread  主题请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getThreadsWithThreads:(Threads *)threads
                      success:(void (^)(NSArray *threadList))success
                      failure:(void (^)(NSError *error))failure;

/**
 *  获取主题详情接口2.14
 *
 *  @param thread  主题详情请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getThreadDetailWithThreads:(Threads *)threads
                     success:(void (^)(Threads *threads))success
                     failure:(void (^)(NSError *error))failure;


/**
 *  发布帖子接口 2.16
 *  @param postPost 发布帖子接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)postPostWithPost:(Threads *)postPost
                success:(void (^)(void))success
                failure:(void (^)(NSError *error))failure;


/**
 *  获取跟帖接口2.19
 *
 *  @param threads 跟帖请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getPostsWithThreads:(Threads *)threads
                      success:(void (^)(NSArray *postsList))success
                      failure:(void (^)(NSError *error))failure;

/**
 *  获取警情统计接口2.21
 *
 *  @param police 警情请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getAlarmStatisticsWithPolice:(Police *)thePolice
                            success:(void (^)(NSArray *polistList))success
                            failure:(void (^)(NSError *error))failure;


/**
 *  3.5	获取广告列表接口
 *
 *  @param getAds 广告请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getAdsWithAds:(AdsInfo *)ads
             success:(void (^)(NSArray *adsList))success
             failure:(void (^)(NSError *error))failure;

/**
 *  3.7	查看广告详情接口
 *
 *  @param getAds 广告请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getAdDetailWithAds:(AdsInfo *)ads
                  success:(void (^)(AdsInfo *adsList))success
                  failure:(void (^)(NSError *error))failure;

/**
 *  注册接口4.1
 *
 *  @param userInfo 注册的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)registerWithUserInfo:(UserInfo *)userInfo
            success:(void (^)(UserInfo *userInfo))success
            failure:(void (^)(NSError *error))failure;

/**
 *  登陆接口4.2
 *
 *  @param userInfo 登陆的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)loginWithUserInfo:(UserInfo *)userInfo
                    success:(void (^)(UserInfo *userInfo))success
                    failure:(void (^)(NSError *error))failure;

/**
 *  4.3	获取用户信息接口
 *
 *  @param userInfo 登陆的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)getUserWithUserInfo:(UserInfo *)userInfo
                 success:(void (^)(UserInfo *userInfo))success
                 failure:(void (^)(NSError *error))failure;

/**
 *  4.4	修改用户信息接口
 *
 *  @param userInfo 修改密码的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)updateUserWithUserInfo:(UserInfo *)userInfo
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure;

/**
 *  4.10 修改密码接口
 *
 *  @param userInfo 修改密码的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)updatePasswordWithUserInfo:(UserInfo *)userInfo
                          success:(void (^)(void))success
                          failure:(void (^)(NSError *error))failure;

/**
 *  重置密码接口4.11
 *
 *  @param userInfo 重置密码的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)resetPasswordWithUserInfo:(UserInfo *)userInfo
                 success:(void (^)(void))success
                 failure:(void (^)(NSError *error))failure;

/**
 *  4.12 设置用户头像接口
 *
 *  @param userInfo 修改密码的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)setAvatarWithUserInfo:(UserInfo *)userInfo
                     success:(void (^)(void))success
                     failure:(void (^)(NSError *error))failure;

/**
 *  4.13 获取用户头像接口
 *
 *  @param userInfo 登陆的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)getAvatarUserInfo:(UserInfo *)userInfo
                 success:(void (^)(UserInfo *userInfo))success
                 failure:(void (^)(NSError *error))failure;

/**
 *  4.24  获取附近的人接口
 *
 *  @param nearPerson 警情请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getNearPersonWithUserInfo:(UserInfo *)userInfo
                         success:(void (^)(NSArray *personList))success
                         failure:(void (^)(NSError *error))failure;

/**
 *  4.28 签到接口
 *
 *  @param userSign 签到积分的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)userSignWithUserSign:(UserInfo *)userSign
                    success:(void (^)(void))success
                    failure:(void (^)(NSError *error))failure;

/**
 *  发布报警接口 5.1
 *
 *  @param postThreads 发布报警接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)postAlarmWithPostAlarms:(PostAlarm *)postAlarm
                       success:(void (^)(void))success
                       failure:(void (^)(NSError *error))failure;

/**
 *  5.5	获取报警列表接口
 *
 *  @param Alarms 获取报警信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getAlarmsWithPostAlarm:(PostAlarm *)alarm
                      success:(void (^)(NSArray *videoInfoList))success
                      failure:(void (^)(NSError *error))failure;

/**
 *  获取视频列表接口6.4
 *
 *  @param VideoInfo 获取视频列表的视频信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getVideosWithVideoInfo:(VideoInfo *)videoInfo
                      success:(void (^)(NSArray *videoInfoList))success
                      failure:(void (^)(NSError *error))failure;

/**
 *  视频搜索接口6.5
 *
 *  @param VideoInfo 视频搜索的视频信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)searchVideosWithVideoInfo:(VideoInfo *)videoInfo
                      success:(void (^)(NSArray *videoInfoList))success
                      failure:(void (^)(NSError *error))failure;

/**
 *  获取视频详情接口6.6
 *
 *  @param VideoInfo 获取视频详情的视频信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getVideoDetailWithVideoInfo:(VideoInfo *)videoInfo
                         success:(void (^)(VideoInfo *videoInfo))success
                         failure:(void (^)(NSError *error))failure;
/**
 *  获取部门接口7.4
 *
 *  @param VideoInfo 获取部门信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getDepartmentsWithConsultInfo:(void (^)(NSArray *consultList))success
                             failure:(void (^)(NSError *error))failure;

/**
 *  获取业务指南列表接口7.5
 *
 *  @param ConsultInfo 获取视频详情的视频信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getGuidesWithConsult:(Consult *)consult
                    success:(void (^)(NSArray *consultList))success
                    failure:(void (^)(NSError *error))failure;

/**
 *  获取查看指南详情接口7.6
 *
 *  @param ConsultInfo 查看指南详情接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getGuideDetailWithConsultInfo:(ConsultInfo *)consultInfo
                             success:(void (^)(ConsultInfo *consultInfoList))success
                             failure:(void (^)(NSError *error))failure;


/**
 *  7.7	搜索指南接口
 *
 *  @param ConsultInfo 获取视频详情的视频信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)searchGuidesWithConsult:(Consult *)consult
                    success:(void (^)(NSArray *consultList))success
                    failure:(void (^)(NSError *error))failure;

/**
 *  8.2	查询积分接口
 *
 *  @param myCredits 获取积分的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getCreditsWithMyCredits:(CreditsModel *)myCredits
                       success:(void (^)(CreditsModel *credits))success
                       failure:(void (^)(NSError *error))failure;


/**
 *  8.4	查询积分日志接口
 *
 *  @param CreditLogs   获取积分日志接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getCreditLogsWithCreditLogs:(CreditsModel *)creditLogs
                     success:(void (^)(NSArray *creditLogsList))success
                     failure:(void (^)(NSError *error))failure;


/**
 *  添加标签接口 9.1
 *
 *  @param device  添加标签信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)addDeviceWithDevice:(TagModel *)device
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *error))failure;

/**
 *  9.2	删除标签接口
 *
 *  @param device  添加标签信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)delDeviceWithDevice:(TagModel *)device
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *error))failure;


/**
 *  9.3	更新标签接口
 *
 *  @param device  添加标签信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)updateDeviceWithDevice:(TagModel *)device
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *error))failure;

/**
 *  9.4	获取查询标签接口
 *
 *  @param myTag  标签接口请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getDevicesWithMytag:(TagModel *)myTag
                   success:(void (^)(NSArray *myTagList))success
                   failure:(void (^)(NSError *error))failure;

/**
 *  9.6	查询轨迹接口
 *
 *  @param myTag  标签接口请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getTrackWithMytag:(TagModel *)myTag
                   success:(void (^)(NSArray *myTagList))success
                   failure:(void (^)(NSError *error))failure;

/**
 *  9.5	查询标签位置接口
 *
 *  @param myTag  标签接口请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getLocationWithMytag:(TagModel *)myTag
                    success:(void (^)(NSArray *myTagList))success
                    failure:(void (^)(NSError *error))failure;

/**
 *  发布投诉接口 10.1
 *
 *  @param ComplainModel 发布主题接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)postComplaintWithComplain:(ComplaintModel *)complain
                         success:(void (^)(void))success
                         failure:(void (^)(NSError *error))failure;

/**
 *  10.4 投诉接口列表
 *
 *  @param shop  商品列表接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getComplaintsWithComplaints:(ComplaintModel *)complaints
                           success:(void (^)(NSArray *complaintsList))success
                           failure:(void (^)(NSError *error))failure;

/**
 *  13.1 获取商品列表接口
 *
 *  @param shop  商品列表接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getGoodsWithShop:(ShopModel *)shop
                success:(void (^)(NSArray *shopList))success
                failure:(void (^)(NSError *error))failure;

/**
 *  13.2 获取商品详情接口
 *
 *  @param ShopModel 获取商品详情的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getGoodsDetailWithGoodsDetail:(ShopModel *)goodsDetail
                           success:(void (^)(ShopModel *goodsDetail))success
                           failure:(void (^)(NSError *error))failure;

/**
 *  13.3 获取兑换详情接口
 *
 *  @param ShopModel 获取兑换详情的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)postOrderWithPostOrder:(ShopModel *)postOrder
                             success:(void (^)(ShopModel *postOrder))success
                             failure:(void (^)(NSError *error))failure;

/**
 *  13.4  获取订单列表接口
 *
 *  @param myOrders   获取订单列表接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getOrdersWithMyOrders:(ShopModel *)myOrders
                     success:(void (^)(NSArray *myOrderList))success
                     failure:(void (^)(NSError *error))failure;

/**
 *  13.7 添加收货地址接口
 *
 *  @param addAddress 联系地址接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)addAddressWithAddAddress:(ShopModel *)addAddress
                        success:(void (^)(void))success
                        failure:(void (^)(NSError *error))failure;

/**
 *  13.8  修改收货地址接口
 *
 *  @param addAddress 联系地址接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)updateAddressWithUpateAddress:(ShopModel *)updateAddress
                             success:(void (^)(void))success
                             failure:(void (^)(NSError *error))failure;

/**
 *  13.9  删除收货地址接口
 *
 *  @param delAddress 联系地址接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)delAddressWithUpateAddress:(ShopModel *)updateAddress
                             success:(void (^)(void))success
                             failure:(void (^)(NSError *error))failure;

/**
 *  13.11 获取收货地址列表接口
 *
 *  @param getAddresses 获取地址详情的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getAddressesWithAddresses:(ShopModel *)addresses
                         success:(void (^)(NSArray *addresses))success
                         failure:(void (^)(NSError *error))failure;

/**
 *  13.12	获取默认地址接口
 *
 *  @param defaultAddress 获取默认地址的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getDefaultAddressWithDefaultAddress:(ShopModel *)defaultAddress
                                   success:(void (^)(ShopModel *defaultAddressList))success
                                   failure:(void (^)(NSError *error))failure;

/**
 *  14.1 驾驶人信息查询接口
 *
 *  @param defaultAddress 获取默认地址的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getDriverWithDriver:(DriverInfo *)driver
                   success:(void (^)(DriverInfo *driver))success
                   failure:(void (^)(NSError *error))failure;

/**
 *  14.2 机动车违章查询
 *
 *  @param getVolation 获取机动车信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getVolationWithVolation:(DriverInfo *)volation
                       success:(void (^)(NSArray *volation))success
                       failure:(void (^)(NSError *error))failure;

/**
 *  14.3 机动车查询
 *
 *  @param getVolation 获取机动车信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getVehiclesWithVolation:(DriverInfo *)vehicles
                       success:(void (^)(DriverInfo *vehicles))success
                       failure:(void (^)(NSError *error))failure;

/**
 *  14.4 重名查询
 *
 *  @param repeatName 获取重名信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)checkRepeatNameWithRepeatName:(DoubleName *)repeatName
                             success:(void (^)(DoubleName *repeatName))success
                             failure:(void (^)(NSError *error))failure;

/**
 *  14.5 局长信箱
 *
 *  @param sendMail 发布接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)sendMailWithMail:(PostThreads *)mail
                success:(void (^)(void))success
                failure:(void (^)(NSError *error))failure;
@end
