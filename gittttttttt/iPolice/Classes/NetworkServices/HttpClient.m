//
//  HttpClient.m
//  iPolice
//
//  Created by ioswei on 15/6/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "HttpClient.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPRequestOperationManager.h"

@implementation HttpClient

/**
 *  发送网络请求
 *
 *  @param url     请求URL
 *  @param method  请求方式(GET/POST)
 *  @param params  请求参数
 *  @param success 请求成功时的回调
 *  @param failure 请求失败时的回调
 */
- (void)sendRequestWithURL:(NSString *)url
                    method:(NSString *)method
                    params:(NSString *)params
                   success:(void (^)(NSDictionary *responseObject))success
                   failure:(void (^)(NSError *error))failure
{
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    theRequest.HTTPMethod = method;
    theRequest.HTTPBody = [NSData dataWithData:[params dataUsingEncoding:NSUTF8StringEncoding]];
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    NSURLRequest *request = [requestSerializer requestBySerializingRequest:theRequest withParameters:nil error:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setResponseSerializer:responseSerializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    [operation start];
    NSLog(@"[%@]\t%@\n%@", method, url, params);
}

/**
 *  上传图片
 */
+ (AFHTTPRequestOperationManager *)sharedHTTPManager {
    static AFHTTPRequestOperationManager * manager;
    if (!manager) {
        manager = [AFHTTPRequestOperationManager manager];
        /**
         *  调用的是AFHTTP方法, 处理的是数据流  一般上传图片用
         */
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil]];
        
    }
    
    return manager;
}
@end
