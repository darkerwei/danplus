//
//  NetworkService.m
//  iPolice
//
//  Created by ioswei on 15/6/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NetworkService.h"
#import "HttpClient.h"
#import "PreNetAPI.h"
#import "UserInfo.h"
#import "VideoInfo.h"
#import "Threads.h"
#import "ChannelModel.h"
#import "Posts.h"
#import "ConsultInfo.h"
#import "Consult.h"
#import "Police.h"
#import "PostThreads.h"
#import "PostAlarm.h"
#import "TagModel.h"
#import "ComplaintModel.h"
#import "ShopModel.h"
#import "CreditsModel.h"
#import "DriverInfo.h"
#import "DoubleName.h"
#import "AdsInfo.h"

@interface NetworkService()

/** 网络请求客户端 */
@property (nonatomic, strong) HttpClient *restClient;

@end

@implementation NetworkService

/** 创建网络服务 */
+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static NetworkService *sharedInstance = nil;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

/**
 *  判断返回结果
 *
 *  @param responseObject 返回数据
 *
 *  @return 返回结果
 */
- (BOOL)successful:(NSDictionary *)responseObject
{
    return [responseObject[@"code"] isEqualToNumber:@(0)];
}

/**
 *  获取频道接口2.4
 *
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getChannel:(void (^)(NSArray *channelList))success
          failure:(void (^)(NSError *error))failure;
{
    NSString *url = SERVER_URL(@"cms", @"getChannels");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\"}", token, uid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *tempListDict = responseObject[@"data"];
                
                NSMutableArray *channelList = [NSMutableArray array];
                for (NSDictionary *dict in tempListDict) {
                    [channelList addObject:[ChannelModel channelWithDict:dict]];
                }
                
                success(channelList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  发布主题接口 2.9
 *
 *  @param postThreads 发布主题接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)postThreadsWithPostThreads:(PostThreads *)postThreads
                          success:(void (^)(void))success
                          failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"UCenter", @"postThread");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"cid\":\"%@\", \"subject\":\"%@\", \"message\":\"%@\"}", token, uid,postThreads.cid, postThreads.subject, postThreads.message];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

}

/**
 *  获取主题列表接口2.12
 *
 *  @param thread  主题请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getThreadsWithThreads:(Threads *)threads
                     success:(void (^)(NSArray *threadList))success
                     failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"cms", @"getThreads");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"cid\":\"%@\", \"status\":\"%@\", \"style\":\"%@\", \"type\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\", \"hasImage\":\"%@\", \"hasDetail\":\"%@\"}", token, uid, threads.cid, threads.status, threads.style, threads.type, threads.start, threads.limit, threads.hasImage, threads.hasDetail];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *threadsListDict = responseObject[@"data"];
                
                NSMutableArray *threadsList = [NSMutableArray array];
                for (NSDictionary *threadsDict in threadsListDict) {
                    [threadsList addObject:[Threads threadsInfoWithDict:threadsDict]];
                }
                
                success(threadsList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  获取主题详情接口2.14
 *
 *  @param thread  主题详情请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getThreadDetailWithThreads:(Threads *)threads
                          success:(void (^)(Threads *threads))success
                          failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"cms", @"getThreadDetail");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"tid\":\"%@\", \"cid\":\"%@\"}", token, uid, threads.tid, threads.cid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([threads initWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  发布帖子接口 2.16
 *  @param postPost 发布帖子接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)postPostWithPost:(Threads *)postPost
                success:(void (^)(void))success
                failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"cms", @"postPost");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"tid\":\"%@\", \"message\":\"%@\", \"extfield1\":\"%@\", \"extfield2\":\"%@\", \"extfield3\":\"%@\", \"extfield4\":\"%@\", \"extfield5\":\"%@\"}", token, uid,postPost.tid, postPost.message, postPost.extfield1,postPost.extfield2,postPost.extfield3,postPost.extfield4,postPost.extfield5];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

}

/**
 *  获取跟帖接口2.19
 *
 *  @param threads 跟帖请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getPostsWithThreads:(Threads *)threads
                   success:(void (^)(NSArray *postsList))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"cms", @"getPosts");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"tid\":\"%@\", \"cid\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, threads.tid, threads.cid, threads.start, threads.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *postsListDict = responseObject[@"data"];
                
                NSMutableArray *postsList = [NSMutableArray array];
                for (NSDictionary *postsDict in postsListDict) {
                    [postsList addObject:[Posts postsWithDict:postsDict]];
                }
                
                success(postsList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  获取警情统计接口2.21
 *
 *  @param police 警情请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getAlarmStatisticsWithPolice:(Police *)thePolice
                            success:(void (^)(NSArray *polistList))success
                            failure:(void (^)(NSError *error))failure{
    NSString *url = SERVER_URL(@"cms", @"getAlarmStatistics");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"cycletype\":\"%@\", \"content\":\"%@\", \"sort\":\"%@\", \"mapx\":\"%@\", \"mapy\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, thePolice.cycletype,thePolice.content, thePolice.sort, thePolice.mapx, thePolice.mapy, thePolice.start, thePolice.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *policeListDict = responseObject[@"data"];
                
                NSMutableArray *policeList = [NSMutableArray array];
                for (NSDictionary *policeDict in policeListDict) {
                    [policeList addObject:[Police policeWithDict:policeDict]];
                }
                
                success(policeList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

}

/**
 *  3.5	获取广告列表接口
 *
 *  @param getAds 广告请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getAdsWithAds:(AdsInfo *)ads
             success:(void (^)(NSArray *adsList))success
             failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"ads", @"getAds");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"placeid\":\"%@\", \"available\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, ads.placeid, ads.available, ads.start, ads.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *policeListDict = responseObject[@"data"];
                
                NSMutableArray *policeList = [NSMutableArray array];
                for (NSDictionary *policeDict in policeListDict) {
                    [policeList addObject:[AdsInfo adsInfoWithDic:policeDict]];
                }
                
                success(policeList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  3.7	查看广告详情接口
 *
 *  @param getAds 广告请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getAdDetailWithAds:(AdsInfo *)ads
                  success:(void (^)(AdsInfo *adsList))success
                  failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"ads", @"getAdDetail");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"advid\":\"%@\"}", token, uid, ads.advid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([ads initAdsInfoWithDic:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  注册接口4.1 (调用此接口在成功回调中写入下面代码 存储token 和 uid)
 
 
 // 存储token和uid
 NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
 [defaults setObject:userInfo.token forKey:@"token"];
 [defaults setObject:userInfo.uid forKey:@"uid"];
 [defaults synchronize];

 *
 *  @param userInfo 注册的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)registerWithUserInfo:(UserInfo *)userInfo
                    success:(void (^)(UserInfo *userInfo))success
                    failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"UCenter", @"register");
    
    NSString *params = [NSString stringWithFormat:@"condition={\"username\":\"%@\", \"password\":\"%@\", \"mobile\":\"%@\", \"nickname\":\"%@\", \"invitecode\":\"%@\"}", userInfo.username, userInfo.password, userInfo.mobile, userInfo.nickname, userInfo.invitecode];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                
                success([userInfo initWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  登陆接口4.2 (调用此接口在成功回调中写入下面代码 存储token 和 uid)
 

 // 存储token和uid
 NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
 [defaults setObject:userInfo.token forKey:@"token"];
 [defaults setObject:userInfo.uid forKey:@"uid"];
 [defaults synchronize];
 
 
 
 *  @param userInfo 登陆的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)loginWithUserInfo:(UserInfo *)userInfo
                 success:(void (^)(UserInfo *userInfo))success
                 failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"UCenter", @"login");
    
    NSString *params = [NSString stringWithFormat:@"condition={\"username\":\"%@\", \"password\":\"%@\"}", userInfo.username, userInfo.password];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([userInfo initWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  4.3	获取用户信息接口
 *
 *  @param userInfo 登陆的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)getUserWithUserInfo:(UserInfo *)userInfo
                   success:(void (^)(UserInfo *userInfo))success
                   failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"UCenter", @"getUser");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\"}", token, uid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([userInfo initWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  4.4	修改用户信息接口
 *
 *  @param userInfo 修改密码的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)updateUserWithUserInfo:(UserInfo *)userInfo
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"UCenter", @"updateUser");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"nickname\":\"%@\", \"mobile\":\"%@\", \"signature\":\"%@\"}", token, uid,userInfo.nickname, userInfo.mobile, userInfo.signature];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  4.10 修改密码接口
 *
 *  @param userInfo 修改密码的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)updatePasswordWithUserInfo:(UserInfo *)userInfo
                          success:(void (^)(void))success
                          failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"UCenter", @"updatePassword");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"newPassword1\":\"%@\", \"newPassword2\":\"%@\"}", token, uid,userInfo.theNewPassword1, userInfo.theNewPassword2];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}
/**
 *  重置密码接口4.11
 *
 *  @param userInfo 重置密码的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)resetPasswordWithUserInfo:(UserInfo *)userInfo
                         success:(void (^)(void))success
                         failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"UCenter", @"resetPassword");
    
    NSString *params = [NSString stringWithFormat:@"condition={\"newPassword1\":\"%@\", \"newPassword2\":\"%@\", \"mobile\":\"%@\"}", userInfo.theNewPassword1, userInfo.theNewPassword2, userInfo.mobile];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  4.12 设置用户头像接口
 *
 *  @param userInfo 修改密码的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)setAvatarWithUserInfo:(UserInfo *)userInfo
                     success:(void (^)(void))success
                     failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"UCenter", @"setAvatar");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"attachment\":\"%@\"}", token, uid, userInfo.attachment];
    NSLog(@"params = %@",params);
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  4.13 获取用户头像接口
 *
 *  @param userInfo 登陆的用户信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)getAvatarUserInfo:(UserInfo *)userInfo
                 success:(void (^)(UserInfo *userInfo))success
                 failure:(void (^)(NSError *error))failure{

    NSString *url = SERVER_URL(@"UCenter", @"getAvatar");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"size\":\"%@\"}", token, uid,userInfo.size];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([userInfo initWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  4.24  获取附近的人接口
 *
 *  @param nearPerson 警情请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getNearPersonWithUserInfo:(UserInfo *)userInfo
                         success:(void (^)(NSArray *personList))success
                         failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"UCenter", @"getNearPerson");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"mapx\":\"%@\", \"mapy\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, userInfo.mapx, userInfo.mapy, userInfo.start, userInfo.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *videoInfoListDict = responseObject[@"data"];
                
                NSMutableArray *videoInfoList = [NSMutableArray array];
                for (NSDictionary *videoInfoDict in videoInfoListDict) {
                    [videoInfoList addObject:[UserInfo userInfoWithDict:videoInfoDict]];
                }
                
                success(videoInfoList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  4.28 签到接口
 *
 *  @param userSign 签到积分的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)userSignWithUserSign:(UserInfo *)userSign
                    success:(void (^)(void))success
                    failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"UCenter", @"userSign");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\",\"mapx\":\"%@\", \"mapy\":\"%@\",\"location\":\"%@\"}", token, uid, userSign.mapx, userSign.mapy, userSign.location];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  发布报警接口 5.1
 *
 *  @param postThreads 发布报警接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)postAlarmWithPostAlarms:(PostAlarm *)postAlarm
                       success:(void (^)(void))success
                       failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"alarmService", @"postAlarm");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"mapx\":\"%@\", \"mapy\":\"%@\", \"location\":\"%@\", \"attachment\":\"%@\", \"message\":\"%@\", \"attachments\":\"%@\"}", token, uid, postAlarm.mapx, postAlarm.mapy, postAlarm.location, postAlarm.attachment, postAlarm.message, postAlarm.attachments];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  5.5	获取报警列表接口
 *
 *  @param Alarms 获取报警信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getAlarmsWithPostAlarm:(PostAlarm *)alarm
                      success:(void (^)(NSArray *alarmList))success
                      failure:(void (^)(NSError *error))failure{
   
    NSString *url = SERVER_URL(@"alarmService", @"getAlarms");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"status\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, alarm.status, alarm.start, alarm.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *videoInfoListDict = responseObject[@"data"];
                
                NSMutableArray *videoInfoList = [NSMutableArray array];
                for (NSDictionary *videoInfoDict in videoInfoListDict) {
                    [videoInfoList addObject:[PostAlarm postAlarmsWithDict:videoInfoDict]];
                }
                
                success(videoInfoList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  获取视频列表接口6.4
 *
 *  @param VideoInfo 获取视频列表的视频信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getVideosWithVideoInfo:(VideoInfo *)videoInfo
                      success:(void (^)(NSArray *videoInfoList))success
                      failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"videoService", @"getVideos");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"mapx\":\"%@\", \"mapy\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, videoInfo.mapx, videoInfo.mapy, videoInfo.start, videoInfo.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *videoInfoListDict = responseObject[@"data"];
                
                NSMutableArray *videoInfoList = [NSMutableArray array];
                for (NSDictionary *videoInfoDict in videoInfoListDict) {
                    [videoInfoList addObject:[VideoInfo videoInfoWithDict:videoInfoDict]];
                }
    
                success(videoInfoList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  视频搜索接口6.5
 *
 *  @param VideoInfo 视频搜索的视频信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)searchVideosWithVideoInfo:(VideoInfo *)videoInfo
                         success:(void (^)(NSArray *videoInfoList))success
                         failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"videoService", @"searchVideos");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"keyword\":\"%@\", \"mapx\":\"%@\", \"mapy\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, videoInfo.keyword, videoInfo.mapx, videoInfo.mapy, videoInfo.start, videoInfo.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *videoInfoListDict = responseObject[@"data"];
                
                NSMutableArray *videoInfoList = [NSMutableArray array];
                for (NSDictionary *videoInfoDict in videoInfoListDict) {
                    [videoInfoList addObject:[VideoInfo videoInfoWithDict:videoInfoDict]];
                }
                
                success(videoInfoList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  获取视频详情接口6.6
 *
 *  @param VideoInfo 获取视频详情的视频信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getVideoDetailWithVideoInfo:(VideoInfo *)videoInfo
                           success:(void (^)(VideoInfo *videoInfo))success
                           failure:(void (^)(NSError *error))failure
{
    NSString *url = SERVER_URL(@"videoService", @"getVideoDetail");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"vid\":\"%@\"}", token, uid, videoInfo.vid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([videoInfo initWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  获取部门接口7.4
 *
 *  @param VideoInfo 获取部门信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getDepartmentsWithConsultInfo:(void (^)(NSArray *consultList))success
                             failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"businessService", @"getDepartments");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\"}", token, uid];
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *tempListDict = responseObject[@"data"];
                
                NSMutableArray *consult = [NSMutableArray array];
                for (NSDictionary *dict in tempListDict) {
                    [consult addObject:[Consult consultWithDict:dict]];
                }
                
                success(consult);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  获取业务指南列表接口7.5
 *
 *  @param ConsultInfo 获取业务指南列表信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getGuidesWithConsult:(Consult *)consult
                    success:(void (^)(NSArray *consultList))success
                    failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"businessService", @"getGuides");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"devid\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, consult.devid, consult.start, consult.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *tempListDict = responseObject[@"data"];
                
                NSMutableArray *tableviewList = [NSMutableArray array];
                for (NSDictionary *dict in tempListDict) {
                    [tableviewList addObject:[Consult consultWithDict:dict]];
                }
                
                success(tableviewList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

}

/**
 *  获取查看指南详情接口7.6
 *
 *  @param ConsultInfo 查看指南详情接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getGuideDetailWithConsultInfo:(ConsultInfo *)consultInfo
                             success:(void (^)(ConsultInfo *consultInfoList))success
                             failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"businessService", @"getGuideDetail");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"id\":\"%@\"}", token, uid, consultInfo.idid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([ConsultInfo consultInfoWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

}

/**
 *  7.7	搜索指南接口
 *
 *  @param ConsultInfo 获取视频详情的视频信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)searchGuidesWithConsult:(Consult *)consult
                       success:(void (^)(NSArray *consultList))success
                       failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"businessService", @"searchGuides");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"devid\":\"%@\", \"keyword\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, consult.devid, consult.keyword, consult.start, consult.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *tempListDict = responseObject[@"data"];
                
                NSMutableArray *tableviewList = [NSMutableArray array];
                for (NSDictionary *dict in tempListDict) {
                    [tableviewList addObject:[Consult consultWithDict:dict]];
                }
                
                success(tableviewList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  8.2	查询积分接口
 *
 *  @param myCredits 获取积分的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getCreditsWithMyCredits:(CreditsModel *)myCredits
                       success:(void (^)(CreditsModel *myCredits))success
                       failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"creditService", @"getCredits");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\"}", token, uid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([myCredits initCreditsWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  8.4	查询积分日志接口
 *
 *  @param CreditLogs   获取积分日志接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getCreditLogsWithCreditLogs:(CreditsModel *)creditLogs
                           success:(void (^)(NSArray *creditLogsList))success
                           failure:(void (^)(NSError *error))failure{
    NSString *url = SERVER_URL(@"creditService", @"getCreditLogs");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, creditLogs.start, creditLogs.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *myTagListDict = responseObject[@"data"];
                
                NSMutableArray *tagList = [NSMutableArray array];
                for (NSDictionary *threadsDict in myTagListDict) {
                    [tagList addObject:[CreditsModel creditsInfoWithDict:threadsDict]];
                }
                
                success(tagList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}


/**
 *  添加标签接口 9.1
 *
 *  @param device  添加标签信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)addDeviceWithDevice:(TagModel *)device
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"rfidService", @"addDevice");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\",\"devid\":\"%@\", \"name\":\"%@\", \"description\":\"%@\", \"model\":\"%@\", \"oem\":\"%@\", \"icon\":\"%@\"}", token, uid, device.devid, device.name, device.desc, device.model, device.oem, device.icon];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}


/**
 *  9.2	删除标签接口
 *
 *  @param device  添加标签信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)delDeviceWithDevice:(TagModel *)device
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"rfidService", @"delDevice");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\",\"devid\":\"%@\"}", token, uid, device.devid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  9.3	更新标签接口
 *
 *  @param device  添加标签信息
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)updateDeviceWithDevice:(TagModel *)device
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"rfidService", @"updateDevice");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\",\"devid\":\"%@\", \"name\":\"%@\", \"description\":\"%@\", \"model\":\"%@\", \"oem\":\"%@\", \"icon\":\"%@\"}", token, uid, device.devid, device.name, device.desc, device.model, device.oem, device.icon];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  9.4	获取查询标签接口
 *
 *  @param myTag  标签接口请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getDevicesWithMytag:(TagModel *)myTag
                   success:(void (^)(NSArray *myTagList))success
                   failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"rfidService", @"getDevices");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, myTag.start, myTag.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *myTagListDict = responseObject[@"data"];
                
                NSMutableArray *tagList = [NSMutableArray array];
                for (NSDictionary *threadsDict in myTagListDict) {
                    [tagList addObject:[TagModel myTagWithDic:threadsDict]];
                }
                
                success(tagList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

}


/**
 *  9.5	查询标签位置接口
 *
 *  @param myTag  标签接口请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getLocationWithMytag:(TagModel *)myTag
                    success:(void (^)(NSArray *myTagList))success
                    failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"rfidService", @"getLocation");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    //参数拼接
    NSMutableString *params = [NSMutableString string];
    [params appendString:[NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\",\"devids\":[", token, uid]];
    
    for (NSString *str in myTag.devids) {
        [params appendString:[NSString stringWithFormat:@"\"%@\",", str]];
    }
    
    [params appendString:@"]}"];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:[params copy] success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *myTagListDict = responseObject[@"data"];
                
                NSMutableArray *tagList = [NSMutableArray array];
                for (NSDictionary *threadsDict in myTagListDict) {
                    [tagList addObject:[TagModel myTagWithDic:threadsDict]];
                }
                
                success(tagList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  9.6	查询轨迹接口
 *
 *  @param myTag  标签接口请求信息
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getTrackWithMytag:(TagModel *)myTag
                 success:(void (^)(NSArray *myTagList))success
                 failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"rfidService", @"getTrack");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\",\"devid\":\"%@\", \"starttime\":\"%@\",\"endtime\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid,myTag.devid,myTag.starttime,myTag.endtime, myTag.start, myTag.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *myTagListDict = responseObject[@"data"];
                
                NSMutableArray *tagList = [NSMutableArray array];
                for (NSDictionary *threadsDict in myTagListDict) {
                    [tagList addObject:[TagModel myTagWithDic:threadsDict]];
                }
                
                success(tagList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}
/**
 *  发布投诉接口 10.1
 *
 *  @param ComplainModel 发布主题接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)postComplaintWithComplain:(ComplaintModel *)complain
                         success:(void (^)(void))success
                         failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"complaintService", @"postComplaint");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"subject\":\"%@\", \"message\":\"%@\", \"attachment\":\"%@\", \"extfield1\":\"%@\", \"extfield2\":\"%@\", \"extfield3\":\"%@\", \"extfield4\":\"%@\", \"extfield5\":\"%@\", \"attachments\":\"%@\"}", token, uid, complain.subject, complain.message, complain.attachment, complain.extfield1, complain.extfield2, complain.extfield3, complain.extfield4, complain.extfield5, complain.attachments];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}


/**
 *  10.4 投诉接口列表
 *
 *  @param shop  商品列表接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getComplaintsWithComplaints:(ComplaintModel *)complaints
                           success:(void (^)(NSArray *complaintsList))success
                           failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"complaintService", @"getComplaints");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, complaints.start, complaints.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *shopListDict = responseObject[@"data"];
                
                NSMutableArray *shopList = [NSMutableArray array];
                for (NSDictionary *shopDict in shopListDict) {
                    [shopList addObject:[ComplaintModel complainInfoWithDict:shopDict]];
                }
                
                success(shopList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

    
}

/**
 *  13.1 获取商品列表接口
 *
 *  @param shop  商品列表接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getGoodsWithShop:(ShopModel *)shop
                success:(void (^)(NSArray *shopList))success
                failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"getGoods");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];

    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\", \"keywords\":\"%@\", \"typeid\":\"%@\", \"creditprice\":\"%@\", \"shopprice\":\"%@\"}", token, uid, shop.start, shop.limit, shop.keywords, shop.typeID, shop.creditprice, shop.shopprice];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *shopListDict = responseObject[@"data"];
                
                NSMutableArray *shopList = [NSMutableArray array];
                for (NSDictionary *shopDict in shopListDict) {
                    [shopList addObject:[ShopModel shopInfoWithDict:shopDict]];
                }
                
                success(shopList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  13.2 获取视频详情接口
 *
 *  @param ShopModel 获取商品详情的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getGoodsDetailWithGoodsDetail:(ShopModel *)goodsDetail
                             success:(void (^)(ShopModel *goodsDetail))success
                             failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"getGoodsDetail");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"gid\":\"%@\"}", token, uid, goodsDetail.gid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([goodsDetail initShopWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
/**
 *  13.3 获取兑换详情接口
 *
 *  @param ShopModel 获取兑换详情的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)postOrderWithPostOrder:(ShopModel *)postOrder
                      success:(void (^)(ShopModel *postOrder))success
                      failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"postOrder");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"gid\":\"%@\", \"addressid\":\"%@\", \"count\":\"%@\", \"remark\":\"%@\"}", token, uid, postOrder.gid, postOrder.addressid, postOrder.count, postOrder.remark];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([postOrder initShopWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  13.4  获取订单列表接口
 *
 *  @param myOrders   获取订单列表接口
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
-(void)getOrdersWithMyOrders:(ShopModel *)myOrders
                     success:(void (^)(NSArray *myOrderList))success
                     failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"getOrders");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];

    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, myOrders.start, myOrders.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *myTagListDict = responseObject[@"data"];
                
                NSMutableArray *tagList = [NSMutableArray array];
                for (NSDictionary *threadsDict in myTagListDict) {
                    [tagList addObject:[ShopModel shopInfoWithDict:threadsDict]];
                }
                
                success(tagList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  13.7 添加收货地址接口
 *
 *  @param addAddress 联系地址接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)addAddressWithAddAddress:(ShopModel *)addAddress
                        success:(void (^)(void))success
                        failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"addAddress");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"consignee\":\"%@\", \"address\":\"%@\", \"mobile\":\"%@\", \"email\":\"%@\", \"zipcode\":\"%@\", \"tel\":\"%@\", \"besttime\":\"%@\", \"signbuilding\":\"%@\", \"isdefault\":\"%@\"}", token, uid, addAddress.consignee, addAddress.address, addAddress.mobile, addAddress.email, addAddress.zipcode, addAddress.tel, addAddress.besttime, addAddress.signbuilding, addAddress.isdefault];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

}

/**
 *  13.8  修改收货地址接口
 *
 *  @param addAddress 联系地址接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)updateAddressWithUpateAddress:(ShopModel *)updateAddress
                             success:(void (^)(void))success
                             failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"updateAddress");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"addressid\":\"%@\", \"consignee\":\"%@\", \"address\":\"%@\", \"mobile\":\"%@\", \"email\":\"%@\", \"zipcode\":\"%@\", \"tel\":\"%@\", \"besttime\":\"%@\", \"signbuilding\":\"%@\", \"isdefault\":\"%@\"}", token, uid, updateAddress.addressid, updateAddress.consignee, updateAddress.address, updateAddress.mobile, updateAddress.email, updateAddress.zipcode, updateAddress.tel, updateAddress.besttime, updateAddress.signbuilding, updateAddress.isdefault];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  13.9  删除收货地址接口
 *
 *  @param delAddress 联系地址接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)delAddressWithUpateAddress:(ShopModel *)updateAddress
                          success:(void (^)(void))success
                          failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"delAddress");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"addressid\":\"%@\"}", token, uid, updateAddress.addressid];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}
/**
 *  13.11 获取收货地址列表接口
 *
 *  @param getAddresses 获取地址详情的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getAddressesWithAddresses:(ShopModel *)addresses
                         success:(void (^)(NSArray *addresses))success
                         failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"getAddresses");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\"}", token, uid, addresses.start, addresses.limit];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *shopListDict = responseObject[@"data"];
                
                NSMutableArray *shopList = [NSMutableArray array];
                for (NSDictionary *shopDict in shopListDict) {
                    [shopList addObject:[ShopModel shopInfoWithDict:shopDict]];
                }
                
                success(shopList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  13.12	获取默认地址接口
 *
 *  @param defaultAddress 获取默认地址的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getDefaultAddressWithDefaultAddress:(ShopModel *)defaultAddress
                                   success:(void (^)(ShopModel *defaultAddressList))success
                                   failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"shopService", @"getDefaultAddress");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\"}", token, uid];
    
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([defaultAddress initShopWithDict:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

}

/**
 *  14.1 驾驶人信息查询接口
 *
 *  @param defaultAddress 获取默认地址的信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getDriverWithDriver:(DriverInfo *)driver
                   success:(void (^)(DriverInfo *driver))success
                   failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"adapterService", @"getDriver");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"idnumber\":\"%@\", \"filenumber\":\"%@\"}", token, uid, driver.idnumber, driver.filenumber];
    
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([driver initDriverInfoWithDic:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  14.2 机动车违章查询
 *
 *  @param getVolation 获取机动车信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getVolationWithVolation:(DriverInfo *)volation
                       success:(void (^)(NSArray *volation))success
                       failure:(void (^)(NSError *error))failure{
   
    NSString *url = SERVER_URL(@"adapterService", @"getVolation");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"type\":\"%@\", \"platenumber\":\"%@\", \"code\":\"%@\"}", token, uid, volation.type, volation.platenumber, volation.code];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                NSArray *shopListDict = responseObject[@"data"];
                
                NSMutableArray *shopList = [NSMutableArray array];
                for (NSDictionary *shopDict in shopListDict) {
                    [shopList addObject:[DriverInfo driverInfoWithDic:shopDict]];
                }
                
                success(shopList);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}


/**
 *  14.3 机动车查询
 *
 *  @param getVolation 获取机动车信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)getVehiclesWithVolation:(DriverInfo *)vehicles
                       success:(void (^)(DriverInfo *vehicles))success
                       failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"adapterService", @"getVehicles");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"type\":\"%@\", \"platenumber\":\"%@\", \"code\":\"%@\"}", token, uid, vehicles.type, vehicles.platenumber, vehicles.code];
    
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([vehicles initDriverInfoWithDic:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  14.4 重名查询
 *
 *  @param repeatName 获取重名信息
 *  @param success   成功的回调
 *  @param failure   失败的回调
 */
-(void)checkRepeatNameWithRepeatName:(DoubleName *)repeatName
                             success:(void (^)(DoubleName *repeatName))success
                             failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"adapterService", @"checkRepeatName");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"name\":\"%@\"}", token, uid, repeatName.name];
    
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success([repeatName initDoubleNameWithDic:responseObject[@"data"]]);
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

/**
 *  14.5 局长信箱
 *
 *  @param sendMail 发布接口
 *  @param success  成功的回调
 *  @param failure  失败的回调
 */
-(void)sendMailWithMail:(PostThreads *)mail
                success:(void (^)(void))success
                failure:(void (^)(NSError *error))failure{
    
    NSString *url = SERVER_URL(@"adapterService", @"sendMail");
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\", \"subject\":\"%@\", \"message\":\"%@\", \"extfield1\":\"%@\", \"extfield2\":\"%@\", \"extfield3\":\"%@\", \"extfield4\":\"%@\", \"extfield5\":\"%@\", \"attachments\":\"%@\"}", token, uid, mail.subject, mail.message, mail.extfield1, mail.extfield2, mail.extfield3, mail.extfield4, mail.extfield5, mail.attachments];
    
    [self.restClient sendRequestWithURL:url method:@"POST" params:params success:^(NSDictionary *responseObject) {
        if ([self successful:responseObject]) {
            if (success) {
                success();
            }
        } else {
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"Response Error"
                                                     code:[[responseObject objectForKey:@"code"] integerValue]
                                                 userInfo:@{@"msg" : [responseObject objectForKey:@"error"]}];
                
                failure(error);
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark - 懒加载
-(HttpClient *)restClient
{
    if (_restClient == nil) {
        _restClient = [[HttpClient alloc] init];
    }
    return _restClient;
}

@end
