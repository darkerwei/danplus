//
//  SubjectTableViewCell.m
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "SubjectTableViewCell.h"
#import "Threads.h"

#define SubjectTableViewCellIdentifier @"SubjectTableViewCell"

@interface SubjectTableViewCell()

/**
 *  subject -- 标题
 */
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
/**
 *  abstracts -- 摘要
 */
@property (weak, nonatomic) IBOutlet UILabel *abstractsLabel;
/**
 *  nickname -- 发布人昵称
 */
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;

@end

@implementation SubjectTableViewCell

- (void)setTheThreads:(Threads *)theThreads
{
    _theThreads = theThreads;
    
    self.subjectLabel.text = theThreads.subject;
    self.abstractsLabel.text = [NSString stringWithFormat:@"摘要:  %@", theThreads.abstracts];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:theThreads.dateline.doubleValue / 1000.0]];
    self.nicknameLabel.text = [NSString stringWithFormat:@"发布人:  %@  %@", theThreads.nickname, regStr];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:SubjectTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:SubjectTableViewCellIdentifier];
    SubjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SubjectTableViewCellIdentifier];
    
    return cell;
}

@end
