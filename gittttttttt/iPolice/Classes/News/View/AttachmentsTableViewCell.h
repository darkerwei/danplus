//
//  AttachmentsTableViewCell.h
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Attachment;
typedef void(^goSeeImagesBlock)();
@interface AttachmentsTableViewCell : UITableViewCell

/**
 *  Attachment model -- 附件详情
 */
@property (nonatomic, strong) Attachment *theAttachment;

// 图片数量
@property (nonatomic, assign) NSUInteger *count;

/**
 *  create cell -- 创建cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewConstraint;

@property (copy, nonatomic) goSeeImagesBlock goSeeImagesBlock;

@end
