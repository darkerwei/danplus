//
//  MessageTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/9/12.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MessageTableViewCell.h"
#import "Threads.h"

#define MessageTableViewCellIdentifier @"MessageTableViewCell"

@interface MessageTableViewCell()

@property (weak, nonatomic) IBOutlet UIWebView *messageView;

@end

@implementation MessageTableViewCell

- (void)setMessage:(Threads *)message{
    
    _message = message;
    
    [self.messageView loadHTMLString:message.message baseURL:nil];
}


+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:MessageTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:MessageTableViewCellIdentifier];
    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MessageTableViewCellIdentifier];
    
    return cell;
}
@end
