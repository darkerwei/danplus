//
//  PostsTableViewCell.m
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PostsTableViewCell.h"
#import "Posts.h"

#define PostsTableViewCellIdentifier @"PostsTableViewCell"

@interface PostsTableViewCell()

/**
 *  nickname -- 发帖人
 */
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
/**
 *  message -- 发帖内容
 */
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
/**
 *  dateline -- 发帖时间
 */
@property (weak, nonatomic) IBOutlet UILabel *datelineLabel;
@property (weak, nonatomic) IBOutlet UIImageView *personImage;


@end

@implementation PostsTableViewCell

- (void)setThePosts:(Posts *)thePosts
{
    _thePosts = thePosts;
    
    self.nicknameLabel.text = self.thePosts.nickname;
    self.messageLabel.text = self.thePosts.message;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.thePosts.dateline.doubleValue / 1000.0]];
    self.datelineLabel.text = regStr;
    [self.personImage sd_setImageWithURL:[NSURL URLWithString:self.thePosts.avatar] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:PostsTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:PostsTableViewCellIdentifier];
    PostsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PostsTableViewCellIdentifier];
    
    return cell;
}

@end
