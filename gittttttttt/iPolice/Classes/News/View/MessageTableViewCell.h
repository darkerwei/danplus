//
//  MessageTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/9/12.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Threads;

@interface MessageTableViewCell : UITableViewCell

/**
 *  threads model -- 频道详情
 */
@property (nonatomic, strong) Threads *message;

/**
 *  create cell -- 创建cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
