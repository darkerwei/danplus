//
//  NewViewConTableViewCell.h
//  iPolice
//
//  Created by PP－mac001 on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^goDetailBlock)(NSInteger btnTag);

@interface NewViewConTableViewCell : UITableViewCell<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIPageControl *pageCon;

- (void)cellWithImageArray:(NSArray *)array andTitleArray:(NSArray*)titleArray;

@property (copy, nonatomic) goDetailBlock godetailBlock;

@end

