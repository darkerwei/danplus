//
//  NewsTableViewCell.h
//  iPolice
//
//  Created by ioswei on 15/7/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Threads;

@interface NewsTableViewCell : UITableViewCell

/**
 *  Cell信息
 */
@property (nonatomic, strong) Threads *theThreads;

/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
