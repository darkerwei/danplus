//
//  PostsTableViewCell.h
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Posts;

@interface PostsTableViewCell : UITableViewCell

/**
 *  Posts model -- 跟帖详情
 */
@property (nonatomic, strong) Posts *thePosts;

/**
 *  create cell -- 创建cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
