//
//  AttachmentsTableViewCell.m
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "AttachmentsTableViewCell.h"
#import "Attachment.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define AttachmentsTableViewCellIdentifier @"AttachmentsTableViewCell"

@interface AttachmentsTableViewCell()

/**
 *  thumbnail -- 缩略图
 */
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
/**
 *  description -- 说明
 */
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation AttachmentsTableViewCell

- (void)setTheAttachment:(Attachment *)theAttachment
{
    _theAttachment = theAttachment;
    
    [self.thumbnailImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@@528w_400h_90q.jpg", theAttachment.thumbnail]]
                           placeholderImage:[UIImage imageNamed:@"默认图.png"]];
}

- (void)setCount:(NSUInteger *)count
{
    _count = count;
    
    self.descriptionLabel.text = [NSString stringWithFormat:@"共%lu张", (unsigned long)count];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:AttachmentsTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:AttachmentsTableViewCellIdentifier];
    AttachmentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AttachmentsTableViewCellIdentifier];
    
    return cell;
}
- (IBAction)goSeeImages:(id)sender {
    _goSeeImagesBlock();
}

@end
