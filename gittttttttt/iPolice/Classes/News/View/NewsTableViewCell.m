//
//  NewsTableViewCell.m
//  iPolice
//
//  Created by ioswei on 15/7/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NewsTableViewCell.h"
#import "Threads.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define NewsTableViewCellIdentifier @"NewsTableViewCell"

@interface NewsTableViewCell()

/**
 *  标题图片
 */
@property (weak, nonatomic) IBOutlet UIImageView *image;
/**
 *  标题
 */
@property (weak, nonatomic) IBOutlet UILabel *subject;
/**
 *  摘要
 */
@property (weak, nonatomic) IBOutlet UILabel *abstracts;
/**
 *  评论
 */
@property (weak, nonatomic) IBOutlet UILabel *replies;
/**
 *  时间
 */
@property (weak, nonatomic) IBOutlet UILabel *dateline;

@end

@implementation NewsTableViewCell

- (void)setTheThreads:(Threads *)theThreads
{
    _theThreads = theThreads;

    [self.image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@@200w_200h_90q.jpg", theThreads.thumbnail]]
                  placeholderImage:[UIImage imageNamed:@"默认图.png"]];
//    [self.image setContentMode:UIViewContentModeScaleToFill];
    
    self.subject.text = theThreads.subject;
    self.abstracts.text = theThreads.abstracts;
    self.replies.text = [NSString stringWithFormat:@"评论 %@", theThreads.replies];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:theThreads.dateline.doubleValue / 1000.0]];
    self.dateline.text = [NSString stringWithFormat:@"时间 %@", regStr];
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:NewsTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:NewsTableViewCellIdentifier];
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsTableViewCellIdentifier];
    
    return cell;
}

@end
