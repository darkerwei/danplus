//
//  NewViewConTableViewCell.m
//  iPolice
//
//  Created by PP－mac001 on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NewViewConTableViewCell.h"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@interface NewViewConTableViewCell()
{
    BOOL isOne;
    int pageNum;
    NSArray *tArray;
}
@property (assign, nonatomic) NSInteger count;
@property (nonatomic,strong)NSTimer *timer;
@end

@implementation NewViewConTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)cellWithImageArray:(NSArray *)array andTitleArray:(NSArray *)titleArray {
    
    for (UIView *subViews in self.scrollView.subviews) {
        [subViews removeFromSuperview];
    }
    
    tArray = titleArray;
    self.nameLabel.text = tArray[0];
    _scrollView.pagingEnabled = YES;
    _scrollView.userInteractionEnabled = YES;
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(WIDTH*(array.count+2), 0);
    
    UIImageView *firstView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH/2)];
    [firstView sd_setImageWithURL:array[array.count - 1]];
    [_scrollView addSubview:firstView];
//    self.nameLabel.text = titleArray[titleArray.count - 1];
    
    
    int i = 0;
    for (NSURL *imageUrlStr in array) {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH+WIDTH*i, 0, WIDTH, WIDTH/2)];
        [imgView sd_setImageWithURL:imageUrlStr];
        [_scrollView addSubview:imgView];

        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH+WIDTH*i, 0, WIDTH, WIDTH/2)];
        [button addTarget:self action:@selector(goDetail:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 100+i;
        [_scrollView addSubview:button];
        
        i++;
    }
    
    [_scrollView setContentOffset:CGPointMake(WIDTH, 0) animated:YES];
    
    UIImageView *lastView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH*(array.count+1), 0, WIDTH, WIDTH/2)];
    [lastView sd_setImageWithURL:array[0]];
    [_scrollView addSubview:lastView];
    
//    self.nameLabel.text = titleArray[0];
    
    
    self.pageCon.numberOfPages = array.count;
    self.count = array.count;
    
    if (!isOne) {
        [self addTimer];
        isOne = 1;
    }
    
}

- (void)goDetail:(UIButton *)btn {
    _godetailBlock(btn.tag);
}

- (void)addTimer {
    _timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(nextPage:) userInfo:nil repeats:YES];
}

- (void)nextPage:(NSTimer *)timer {
    [_scrollView setContentOffset:CGPointMake(WIDTH * (pageNum+1), 0) animated:YES];
}

- (void)endTimer {
    [_timer invalidate];
}

//正在滚动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int width = WIDTH;
    if ((int)_scrollView.contentOffset.x%width == 0) {
        if ((_scrollView.contentOffset.x == 0) || (_scrollView.contentOffset.x == 0.5) || (_scrollView.contentOffset.x == -0.5)) {
            _scrollView.contentOffset = CGPointMake(WIDTH*_count, 0);
        } else if (_scrollView.contentOffset.x == WIDTH*(_count+1)) {
            _scrollView.contentOffset = CGPointMake(WIDTH, 0);
        }
        pageNum = _scrollView.contentOffset.x/WIDTH;
        _pageCon.currentPage = pageNum-1;
        if ((pageNum - 1) <= tArray.count && (pageNum - 1) >= 0) {
            self.nameLabel.text = tArray[pageNum-1];
        }
    }
}

//正在拖拽
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self endTimer];
}

//结束减速
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self addTimer];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
