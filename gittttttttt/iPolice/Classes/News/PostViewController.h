//
//  PostViewController.h
//  iPolice
//
//  Created by xwy on 15/8/31.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Threads;

@interface PostViewController : UIViewController

@property (nonatomic, strong) Threads *post;

@end
