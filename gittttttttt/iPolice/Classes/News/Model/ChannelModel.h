//
//  ChannelModel.h
//  iPolice
//
//  Created by ioswei on 15/7/13.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChannelModel : NSObject

/**
 *  Token
 */
@property (nonatomic, copy) NSString *token;
/**
 *  用户id
 */
@property (nonatomic, copy) NSString *uid;
/**
 *  频道名称
 */
@property (nonatomic, copy) NSString *name;
/**
 *  频道ID
 */
@property (nonatomic, copy) NSString *cid;
/**
 *  状态 0-显示 1-隐藏
 */
@property (nonatomic, copy) NSString *status;
/**
 *  显示顺序
 */
@property (nonatomic, strong) NSNumber *displayOrder;

/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)channel;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)channelWithDict:(NSDictionary *)dict;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initWithDict:(NSDictionary *)dict;

@end
