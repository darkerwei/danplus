//
//  Posts.m
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Posts.h"

@implementation Posts

/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)posts
{
    return [[self alloc] init];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)postsWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.pid = dict[@"pid"];
        self.tid = dict[@"tid"];
        self.uid = dict[@"uid"];
        self.username = dict[@"username"];
        self.nickname = dict[@"nickname"];
        self.avatar = dict[@"avatar"];
        self.dateline = dict[@"dateline"];
        self.status = dict[@"status"];
        self.position = dict[@"position"];
        self.message = dict[@"message"];
        self.extfield1 = dict[@"extfield1"];
        self.extfield2 = dict[@"extfield2"];
        self.extfield3 = dict[@"extfield3"];
        self.extfield4 = dict[@"extfield4"];
        self.extfield5 = dict[@"extfield5"];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\"pid\":\"%@\", \"tid\":\"%@\", \"uid\":\"%@\", \"username\":\"%@\", \"nickname\":\"%@\", \"dateline\":\"%@\", \"status\":\"%@\", \"position\":\"%@\", \"message\":\"%@\", \"extfield1\":\"%@\", \"extfield2\":\"%@\", \"extfield3\":\"%@\", \"extfield4\":\"%@\", \"extfield5\":\"%@\"}", self.pid, self.tid, self.uid, self.username, self.nickname, self.dateline, self.status, self.position, self.message, self.extfield1, self.extfield2, self.extfield3, self.extfield4, self.extfield5];
}

@end
