//
//  Posts.h
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Posts : NSObject

/**
 *  帖子id
 */
@property (nonatomic, copy) NSString *pid;
/**
 *  主题id
 */
@property (nonatomic, copy) NSString *tid;
/**
 *  跟帖人id
 */
@property (nonatomic, copy) NSString *uid;
/**
 *  跟帖人
 */
@property (nonatomic, copy) NSString *username;
/**
 *  跟帖人昵称
 */
@property (nonatomic, copy) NSString *nickname;
//跟帖人头像
@property (nonatomic, copy) NSString *avatar;
/**
 *  评论时间
 */
@property (nonatomic, strong) NSNumber *dateline;
/**
 *  帖子状态 0-正常 1-隐藏
 */
@property (nonatomic, strong) NSString *status;
/**
 *  帖子位置
 */
@property (nonatomic, strong) NSNumber *position;
/**
 *  帖子内容
 */
@property (nonatomic, copy) NSString *message;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield1;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield2;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield3;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield4;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield5;

/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)posts;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)postsWithDict:(NSDictionary *)dict;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initWithDict:(NSDictionary *)dict;

@end
