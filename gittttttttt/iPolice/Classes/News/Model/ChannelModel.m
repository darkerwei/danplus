//
//  ChannelModel.m
//  iPolice
//
//  Created by ioswei on 15/7/13.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ChannelModel.h"

@implementation ChannelModel

/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)channel
{
    return [[self alloc] init];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)channelWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.token = dict[@"token"];
        self.uid = dict[@"uid"];
        self.name = dict[@"name"];
        self.cid = dict[@"cid"];
        self.status = dict[@"status"];
        self.displayOrder = dict[@"displayOrder"];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\"token\":\"%@\", \"uid\":\"%@\", \"name\":\"%@\", \"cid\":\"%@\", \"status\":\"%@\", \"displayOrder\":\"%@\"}", self.token, self.uid, self.name, self.cid, self.status, self.displayOrder];
}

@end
