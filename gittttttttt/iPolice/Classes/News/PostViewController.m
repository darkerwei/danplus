//
//  PostViewController.m
//  iPolice
//
//  Created by xwy on 15/8/31.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PostViewController.h"
#import "NetworkService.h"
#import "PostsTableViewCell.h"
#import "Threads.h"

#define TextMessage @"我来说两句..."
@interface PostViewController ()<UITableViewDataSource, UITableViewDelegate,UITextViewDelegate>

/**
 *  周边视频页面
 */
@property (nonatomic, strong) UITableView *tableView;
/**
 *  视频列表
 */
@property (nonatomic, strong) NSArray *postInfoList;

@property (nonatomic, assign) int limit;
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UIButton *sendButton;
@end

@implementation PostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"评论列表";
    
    UILabel *all = [[UILabel alloc] initWithFrame:CGRectMake(20, 64, Main_Screen_Width, 50)];
    all.text = @"全部评论";
    all.font = [UIFont systemFontOfSize:17];
    [self.view addSubview:all];
    
    // 去除多余cell
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50+64, Main_Screen_Width, Main_Screen_Height-64-50-50)];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];
    
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, Main_Screen_Height-50, Main_Screen_Width-70, 50)];
    //设置背景图
    UIImage *image = [UIImage imageNamed:@"评论框"];
    self.textView.layer.contents = (id) image.CGImage;
    self.textView.text = TextMessage;
    self.textView.textAlignment = NSTextAlignmentNatural;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    
    self.sendButton = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width-55, Main_Screen_Height-50, 50, 50)];
    [self.sendButton setImage:[UIImage imageNamed:@"发送-原始.png"] forState:UIControlStateNormal];
    [self.sendButton setImage:[UIImage imageNamed:@"发送-点击.png"] forState:UIControlStateHighlighted];
    [self.sendButton addTarget:self action:@selector(clickSend) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sendButton];
    
    
    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];

}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    [SVProgressHUD showInfoWithStatus:@"正在获取评论列表"];
    [[NetworkService sharedInstance] getPostsWithThreads:self.post success:^(NSArray *postsList) {
        [SVProgressHUD showSuccessWithStatus:@"获取评论列表成功"];
        
        
        // 视频满10个才显示上拉刷新
        if (postsList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (postsList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        
        self.postInfoList = postsList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    [SVProgressHUD showInfoWithStatus:@"正在获取评论列表"];
    [[NetworkService sharedInstance] getPostsWithThreads:self.post success:^(NSArray *postsList) {
        [SVProgressHUD showSuccessWithStatus:@"获取评论列表成功"];
        
        
        // 视频满10个才显示上拉刷新
        if (postsList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (postsList.count < self.limit-10) {
            self.tableView.footer.hidden = YES;
        }
        
        self.postInfoList = postsList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     NSLog(@"%d", self.postInfoList.count);
    return self.postInfoList.count;
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PostsTableViewCell *cell = [PostsTableViewCell cellWithTableView:tableView];
    
    cell.thePosts = self.postInfoList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
//点击他人评论
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.textView.text = @"";
    self.textView.text = [NSString stringWithFormat:@"@%@:%@", [self.postInfoList[indexPath.row] nickname] ,self.textView.text];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


//点击空白处取消键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.textView resignFirstResponder];
}

//点击return隐藏键盘
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

//输入框代理
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:TextMessage]) {
        textView.text = @"";
    }
    
    [self animateTextView:textView up: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@""]) {
         textView.text = TextMessage;
    }
    
    [self animateTextView:textView up:NO];
    [textView resignFirstResponder];
}


- (void)animateTextView: (UITextView *) textView up: (BOOL) up
{
    const int movementDistance = 250; // tweak as needed
    const float movementDuration = 0.2f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    
    //    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    self.textView.frame = CGRectOffset(self.textView.frame, 0, movement);
    self.sendButton.frame = CGRectOffset(self.sendButton.frame, 0, movement);
    [UIView commitAnimations];
    
}

- (void)clickSend{
    
    if (self.textView.text.length == 0 || [self.textView.text isEqualToString:TextMessage]) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请填写内容" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        [alvert show];
    } else {
        
        Threads *threads = [Threads threadsWithTid:self.post.tid message:self.textView.text extfield1:@"" extfield2:@"" extfield3:@"" extfield4:@"" extfield5:@""];
        [[NetworkService sharedInstance] postPostWithPost:threads success:^{
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            
            [self.tableView reloadData];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"发送失败"];
        }];
    }
    self.textView.text = TextMessage;
}

@end
