//
//  ThreadDetailViewController.h
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Threads;

@interface ThreadDetailViewController : UIViewController

/**
 *  threads model -- 频道详情
 */
@property (nonatomic, strong) Threads *theThreads;

@end
