//
//  NewsViewController.m
//  iPolice
//
//  Created by ioswei on 15/7/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NewsViewController.h"
#import "ChannelScrollView.h"
#import "NetworkService.h"
#import "ChannelModel.h"
#import <SVProgressHUD.h>
#import "NewsTableViewCell.h"
#import "Threads.h"
#import "Attachment.h"
#import "ThreadDetailViewController.h"
#import "NewsCollectionViewCell.h"
#import "CycleImageCell.h"
#import "NewViewConTableViewCell.h"

// 图片轮播图片Url
#define ImageUrl1 @"http://www.zxy00804.com/fdd13bd6073e49069a2defe3a5ef6f0e.jpg"
#define ImageUrl2 @""
#define ImageUrl3 @""
#define ImageUrl4 @""
#define ImageUrl5 @""


// 图片轮播高度
#define PageViewHeight 100
//#define PageViewHeight ((Main_Screen_Height - 129) * 0.5)
#define Main_Screen_Height      [[UIScreen mainScreen] bounds].size.height
#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width


// 频道界面高度
#define Channesl_Height 36
// 频道内容高度
#define Content_Height (Main_Screen_Height - 100)
// 频道按钮宽度
#define Channesl_Button_Width (Main_Screen_Width * 0.25)
// 频道小字体
#define Min_Channesl_Font 13
// 频道大字体
#define Max_Channesl_Font 18

@interface NewsViewController ()<UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
/**
 *  轮播
 */
@property (strong, nonatomic) UICollectionView *cycleCollectionView;
@property (strong, nonatomic) UICollectionViewFlowLayout *cycleCollectionViewFlowLayout;
@property (strong, nonatomic) UITableView *tableView;
/**
 *  索引
 */
@property (nonatomic, assign) NSUInteger currentIndex;
@property (nonatomic, strong) NSArray *imageURLsList;
//标题数组
@property (nonatomic, strong) NSArray *subjectsList;
@property (nonatomic, strong) NSArray *photoList;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, strong) NSMutableArray *cycList;
/**
 *  频道界面
 */
@property (nonatomic, strong) ChannelScrollView *channelScrollView;
/**
 *  频道列表
 */
@property (nonatomic, strong) NSArray *channeslList;
/**
 *  频道内容
 */
@property (nonatomic, strong) UIScrollView *contentScrollView;
/**
 *  开始滚动位置
 */
@property (nonatomic, assign) CGFloat startPointX;
/**
 *  内容列表
 */
@property (nonatomic, strong) NSArray *contentList;
/**
 *  最后一个按钮点击
 */
@property (nonatomic, assign) NSInteger lastButtonTag;

@property (nonatomic, assign) int limit;

typedef NS_ENUM(NSUInteger, ScrollViewType) {

    ScrollView1Type,  // 热门服务栏tag
    ScrollView2Type // 轮播tag
};
@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lastButtonTag = 0;
    
    self.title = @"新闻资讯";
    
    // 修复UIScrollView空白问题
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // 获取频道列表
    [self getChannel];
    
    self.channelScrollView = [[ChannelScrollView alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, Channesl_Height)];
    self.channelScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.channelScrollView];
    
    self.contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64+Channesl_Height, Main_Screen_Width, Content_Height)];
    self.contentScrollView.pagingEnabled = YES;
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    self.contentScrollView.delegate = self;
    self.contentScrollView.bounces = NO;
    self.contentScrollView.tag = ScrollView1Type;
    [self.view addSubview:self.contentScrollView];

//    // 下拉刷新
//    [self refresh];
//    // 上拉刷新
//    [self upRefresh];
    
    [self getThreads];
}

/**
 *  获取频道列表
 */
- (void)getChannel
{
    [[NetworkService sharedInstance] getChannel:^(NSArray *channelList) {
        
        // 暂时取消排序
        self.channeslList = [channelList sortedArrayUsingComparator:
         ^NSComparisonResult(ChannelModel *obj1, ChannelModel *obj2) {
             NSComparisonResult result = [obj1.displayOrder compare:obj2.displayOrder];
             
             return result;
         }];
        self.channeslList = channelList;
        
        if (self.channeslList.count >= 1) {
            [self getThreadsWithTag:1];
            
            // 设置频道界面
            [self setupChannelView];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (void)getThreadsWithTag:(NSInteger)tag
{
    ChannelModel *theChannelModel = self.channeslList[tag - 1];
    Threads *theThreads = [Threads threadsWithCid:theChannelModel.cid status:@"" style:@"" type:@"" start:@"0" limit:@"" hasImage:@"1" hasDetail:@"0"];
    
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {

        self.contentList = threadList;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            UITableView *view = (UITableView *)[self.contentScrollView viewWithTag:tag];
            [view reloadData];
        });
        
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
    Threads *theThreads1 = [Threads threadsWithCid:theChannelModel.cid status:@"0" style:@"1" type:@"" start:@"0" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads1 success:^(NSArray *threadList) {
        
        NSMutableArray *imageURLsList = [NSMutableArray array];
        NSMutableArray *subjectsList = [NSMutableArray array];
        for (Threads *theThreads in threadList) {
            if (theThreads.thumbnail && theThreads.subject) {
                if (imageURLsList.count == 5) {
                    break;
                }
                
                self.photoList = threadList;
                self.cycList = imageURLsList;
                
                [imageURLsList addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@@1200w_800h_90q.jpg", theThreads.thumbnail]]];
                [subjectsList addObject:theThreads.subject];
            }
        }
        NSLog(@"轮播图");
        self.imageURLsList = [NSArray arrayWithArray:imageURLsList];
        self.subjectsList = [NSArray arrayWithArray:subjectsList];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

/**
 *  设置频道界面
 */
- (void)setupChannelView
{
    self.channelScrollView.contentSize = CGSizeMake(Channesl_Button_Width * self.channeslList.count, Channesl_Height);
    self.contentScrollView.contentSize = CGSizeMake(Main_Screen_Width * self.channeslList.count, Content_Height);
    
    for (int i = 0; i < self.channeslList.count; i++) {
        
        ChannelModel *theChannelModel = self.channeslList[i];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(Channesl_Button_Width * i, 0, Channesl_Button_Width, Channesl_Height);
        [btn setTitle:theChannelModel.name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i + 1;
        [btn addTarget:self action:@selector(channelClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.channelScrollView addSubview:btn];
        
        if (i == 0) {
            btn.titleLabel.font = [UIFont systemFontOfSize:Max_Channesl_Font];
        } else {
            btn.titleLabel.font = [UIFont systemFontOfSize:Min_Channesl_Font];
        }
    }
    
    for (int i = 0; i < self.channeslList.count; i++) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(Main_Screen_Width * i, 0, Main_Screen_Width, Content_Height)];
        self.tableView.tag = i + 1;
        self.tableView.userInteractionEnabled = YES;
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.tableFooterView = [[UIView alloc] init];
        [self.contentScrollView addSubview:self.tableView];
        
        [self.tableView registerNib:[UINib nibWithNibName:@"NewViewConTableViewCell" bundle:nil] forCellReuseIdentifier:@"NewViewConTableViewCell"];
    }
}

- (void)channelClick:(UIButton *)btn
{
    if (self.lastButtonTag == btn.tag) {
        return;
    }
    self.lastButtonTag = btn.tag;
    
    [self.contentScrollView scrollRectToVisible:CGRectMake(Main_Screen_Width * (btn.tag - 1), 0, Main_Screen_Width, Content_Height) animated:YES];
    
    CGFloat x = Main_Screen_Width * (btn.tag - 1) * 0.25 - Channesl_Button_Width;
    [self.channelScrollView scrollRectToVisible:CGRectMake(x, 0, Main_Screen_Width, Channesl_Height) animated:YES];
    
    [self getThreadsWithTag:btn.tag];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.contentScrollView == scrollView) {
        self.startPointX = scrollView.contentOffset.x;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    if (ScrollView1Type == scrollView.tag) {
        CGFloat x = scrollView.contentOffset.x * 0.25 - Channesl_Button_Width;
        [self.channelScrollView scrollRectToVisible:CGRectMake(x, 0, Main_Screen_Width, Channesl_Height) animated:YES];
        [self getThreadsWithTag:(NSInteger)(scrollView.contentOffset.x / Main_Screen_Width) + 1];
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.contentScrollView == scrollView) {
        [self changeView:scrollView.contentOffset.x];
    }
}

- (void)changeView:(CGFloat)x
{
    int sTag = x / Main_Screen_Width + 1;
    
    if (sTag <= 0) {
        return;
    }
    
    CGFloat startX = 0.25 * x;
    
    UIButton *btn = (UIButton *)[self.channelScrollView viewWithTag:sTag];
    CGFloat percent = (startX - Channesl_Button_Width * (sTag - 1)) / Channesl_Button_Width;
    CGFloat value = [self lerp:(1 - percent) min:Min_Channesl_Font max:Max_Channesl_Font];
    btn.titleLabel.font = [UIFont systemFontOfSize:value];
    
    if ((int)(0.25 * x) % (int)Channesl_Button_Width == 0) {
        return;
    }
    
    UIButton *btn2 = (UIButton *)[self.channelScrollView viewWithTag:sTag + 1];
    CGFloat value2 = [self lerp:percent min:Min_Channesl_Font max:Max_Channesl_Font];
    btn2.titleLabel.font = [UIFont systemFontOfSize:value2];
}

- (CGFloat)lerp:(CGFloat)percent min:(CGFloat)min max:(CGFloat)max
{
    return min + percent * (max - min);
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else {
        return self.contentList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *ident = @"NewViewConTableViewCell";
        NewViewConTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ident forIndexPath:indexPath];
        if (self.imageURLsList.count != 0 && self.subjectsList.count != 0) {
            [cell cellWithImageArray:self.imageURLsList andTitleArray:self.subjectsList];
        }
        cell.godetailBlock = ^(NSInteger btnTag) {
            ThreadDetailViewController *threadDetailVC = [[ThreadDetailViewController alloc] init];
            threadDetailVC.theThreads = self.contentList[btnTag-100];
            [self.navigationController pushViewController:threadDetailVC animated:YES];
        };
        return cell;
    } else {
        NewsTableViewCell *cell = [NewsTableViewCell cellWithTableView:tableView];
        cell.theThreads = self.contentList[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Threads *theThreads = self.contentList[indexPath.row];
    [[NetworkService sharedInstance] getThreadDetailWithThreads:theThreads success:^(Threads *threads) {
        
        ThreadDetailViewController *threadDetailVC = [[ThreadDetailViewController alloc] init];
        threadDetailVC.theThreads = threads;
        [self.navigationController pushViewController:threadDetailVC animated:YES];
 
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 100;
    } else {
        return 120;
    }
}

- (NSArray *)imageURLsList
{
    if (_imageURLsList == nil) {
        NSURL *url1 = [NSURL URLWithString:ImageUrl1];
        NSURL *url2 = [NSURL URLWithString:ImageUrl2];
        NSURL *url3 = [NSURL URLWithString:ImageUrl3];
        NSURL *url4 = [NSURL URLWithString:ImageUrl4];
        NSURL *url5 = [NSURL URLWithString:ImageUrl5];
        
        
        _imageURLsList = @[url1, url2, url3, url4, url5];
    }
    
    return _imageURLsList;
}

- (NSArray *)subjectsList{
    
    if (_subjectsList == nil) {
        
        NSString *name1 = [NSString stringWithFormat:@""];
        NSString *name2 = [NSString stringWithFormat:@""];
        NSString *name3 = [NSString stringWithFormat:@""];
        NSString *name4 = [NSString stringWithFormat:@""];
        NSString *name5 = [NSString stringWithFormat:@""];
        
        _subjectsList = @[name1, name2, name3, name4, name5];
    }
    
    return _subjectsList;
}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    Threads *theThreads = [Threads threadsWithCid:@"" status:@"0" style:@"1" type:@"" start:@"0" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [SVProgressHUD showInfoWithStatus:@"正在获取新闻列表"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        [SVProgressHUD showSuccessWithStatus:@"获取新闻列表成功"];
        
        // 视频满10个才显示上拉刷新
        if (threadList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (threadList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        
        NSMutableArray *imageURLsList = [NSMutableArray array];
        NSMutableArray *subjectsList = [NSMutableArray array];
        for (Threads *theThreads in threadList) {
            if (theThreads.thumbnail && theThreads.subject) {
                if (imageURLsList.count == 5) {
                    break;
                }
                
                self.photoList = threadList;
                
                [imageURLsList addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@@1200w_800h_90q.jpg", theThreads.thumbnail]]];
                [subjectsList addObject:theThreads.subject];
            }
        }
        
        self.imageURLsList = [NSArray arrayWithArray:imageURLsList];
        self.subjectsList = [NSArray arrayWithArray:subjectsList];
        [self.cycleCollectionView reloadData];
        [self.tableView.header endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];

}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    Threads *theThreads = [Threads threadsWithCid:@"" status:@"0" style:@"1" type:@"" start:@"0" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [SVProgressHUD showWithStatus:@"正在获取新闻列表"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        [SVProgressHUD showSuccessWithStatus:@"获取新闻列表成功"];
        
        // 没有更多视频时隐藏上拉刷新
        if (threadList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取新闻列表成功"];
        } else if (threadList.count < self.limit - 10) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多了"];
        }
        
        
        NSMutableArray *imageURLsList = [NSMutableArray array];
        NSMutableArray *subjectsList = [NSMutableArray array];
        for (Threads *theThreads in threadList) {
            if (theThreads.thumbnail && theThreads.subject) {
                if (imageURLsList.count == 5) {
                    break;
                }
                
                self.photoList = threadList;
                
                [imageURLsList addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@@1200w_800h_90q.jpg", theThreads.thumbnail]]];
                [subjectsList addObject:theThreads.subject];
            }
        }
        
        self.imageURLsList = [NSArray arrayWithArray:imageURLsList];
        self.subjectsList = [NSArray arrayWithArray:subjectsList];
        [self.cycleCollectionView reloadData];
        [self.tableView.footer endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];
    
}

- (void)getThreads
{
    Threads *theThreads = [Threads threadsWithCid:@"" status:@"0" style:@"1" type:@"" start:@"0" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [SVProgressHUD showWithStatus:@"正在加载数据，请稍后~"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
        NSMutableArray *imageURLsList = [NSMutableArray array];
        NSMutableArray *subjectsList = [NSMutableArray array];
        for (Threads *theThreads in threadList) {
            if (theThreads.thumbnail && theThreads.subject) {
                if (imageURLsList.count == 5) {
                    break;
                }
                
                self.photoList = threadList;
                self.cycList = imageURLsList;
                
                [imageURLsList addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@@1200w_800h_90q.jpg", theThreads.thumbnail]]];
                [subjectsList addObject:theThreads.subject];
            }
        }
        NSLog(@"轮播图");
        self.imageURLsList = [NSArray arrayWithArray:imageURLsList];
        self.subjectsList = [NSArray arrayWithArray:subjectsList];
        [self.cycleCollectionView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}


@end
