//
//  ImagesViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ImagesViewController.h"
#import "Attachment.h"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@interface ImagesViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end

@implementation ImagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"array = %@", self.imagesArray);
    _scrollView.pagingEnabled = YES;
    _scrollView.userInteractionEnabled = YES;
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(WIDTH*(self.imagesArray.count), 0);
    
    int i = 0;
    for (Attachment *imageUrlStr in self.imagesArray) {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + (WIDTH)*i, 50, (WIDTH - 20), WIDTH/2)];
        [imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@@528w_400h_90q.jpg", imageUrlStr.thumbnail]]
                                   placeholderImage:[UIImage imageNamed:@"默认图.png"]];
        [_scrollView addSubview:imgView];
        i++;
    }
     [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    // Do any additional setup after loading the view from its nib.
}

//正在滚动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int pageNum = 0;
    pageNum = _scrollView.contentOffset.x/WIDTH;
    self.countLabel.text = [NSString stringWithFormat:@" %d/%lu",pageNum+1, (unsigned long)self.imagesArray.count];
}


@end
