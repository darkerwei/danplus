//
//  NewsCollectionViewCell.m
//  iPolice
//
//  Created by xwy on 15/8/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NewsCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>


#define NewsCollectionViewCellIdentifier @"NewsCollectionViewCell"

@interface NewsCollectionViewCell()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end


@implementation NewsCollectionViewCell


- (void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    //一句话, 自动实现了异步下载. 图片本地缓存. 网络下载. 自动设置占位符.
    [self.imageView sd_setImageWithURL:self.imageURL placeholderImage:[UIImage imageNamed:@"默认图.jpg"]];
}

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath
{
    [collectionView registerNib:[UINib nibWithNibName:NewsCollectionViewCellIdentifier bundle:nil] forCellWithReuseIdentifier:NewsCollectionViewCellIdentifier];
    NewsCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:NewsCollectionViewCellIdentifier forIndexPath:indexPath];
    
    return cell;
}
@end
