//
//  NewsCollectionViewCell.h
//  iPolice
//
//  Created by xwy on 15/8/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCollectionViewCell : UICollectionViewCell


@property (nonatomic, strong) NSURL *imageURL;

/**
 *  快速创建一个cell
 *
 *  @param collectionView 哪个collectionView
 *  @param indexPath      collectionView的哪个cell
 *
 *  @return 一个创建好的cell
 */
+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath;

@end
