//
//  ImagesViewController.h
//  iPolice
//
//  Created by PP－mac001 on 15/10/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagesViewController : UIViewController

@property (strong, nonatomic) NSArray  *imagesArray;

@end
