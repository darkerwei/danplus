//
//  ThreadDetailViewController.m
//  iPolice
//
//  Created by ioswei on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ThreadDetailViewController.h"
#import "SubjectTableViewCell.h"
#import "Threads.h"
#import "AttachmentsTableViewCell.h"
#import "Attachment.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import "PostsTableViewCell.h"
#import "Posts.h"
#import "PostViewController.h"
#import "MessageTableViewCell.h"
#import "ImagesViewController.h"

#define TextMessage @"我来说两句..."
@interface ThreadDetailViewController ()<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIAlertViewDelegate,UIWebViewDelegate>
{
    NSInteger webViewHeight;
    AttachmentsTableViewCell *attachmentsTableViewCell;
    
    BOOL isOne;
}
/**
 *  threadDetail view -- 频道详情界面
 */
@property (nonatomic, weak) IBOutlet UITableView *threadDetailTableView;
/**
 *  回帖信息列表
 */
@property (nonatomic, strong) NSArray *postsList;
@property (nonatomic, strong) UITextView *textView;
@property (strong, nonatomic) UIButton *sendButton;
@end

@implementation ThreadDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"内容详情";
    
    // 隐藏cell分割线
//    self.threadDetailTableView.separatorStyle = NO;
    // 不显示空cell
    self.threadDetailTableView.tableFooterView = [[UIView alloc] init];
   
    [self getPosts];

    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, Main_Screen_Height-50, Main_Screen_Width-70, 50)];
    //设置背景图
    UIImage *image = [UIImage imageNamed:@"评论框"];
    self.textView.layer.contents = (id) image.CGImage;
    self.textView.text = TextMessage;
    self.textView.textAlignment = NSTextAlignmentNatural;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    
    self.sendButton = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width-55, Main_Screen_Height-50, 50, 50)];
    [self.sendButton setImage:[UIImage imageNamed:@"发送-原始.png"] forState:UIControlStateNormal];
    [self.sendButton setImage:[UIImage imageNamed:@"发送-点击.png"] forState:UIControlStateHighlighted];
    [self.sendButton addTarget:self action:@selector(clickSend) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sendButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    isOne = YES;
}

//点击空白处取消键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.textView resignFirstResponder];
}

//点击return隐藏键盘
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
//输入框代理
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:TextMessage]) {
        textView.text = @"";
    }
    
    [self animateTextView:textView up: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = TextMessage;
    }
    
    [self animateTextView:textView up:NO];
    [textView resignFirstResponder];
}

- (void)animateTextView: (UITextView *) textView up: (BOOL) up
{
    const int movementDistance = 250; // tweak as needed
    const float movementDuration = 0.2f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    
    //    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    self.textView.frame = CGRectOffset(self.textView.frame, 0, movement);
    self.sendButton.frame = CGRectOffset(self.sendButton.frame, 0, movement);
    [UIView commitAnimations];
    
}

//点击发送评论
- (void)clickSend{
    
    if (self.textView.text.length == 0 || [self.textView.text isEqualToString:TextMessage]) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请填写内容" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        [alvert show];

    } else {
        
        Threads *threads = [Threads threadsWithTid:self.theThreads.tid message:self.textView.text extfield1:@"" extfield2:@"" extfield3:@"" extfield4:@"" extfield5:@""];
        [[NetworkService sharedInstance] postPostWithPost:threads success:^{
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            
            [self.threadDetailTableView reloadData];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"发送失败"];
        }];
    }
    self.textView.text = TextMessage;
}


- (void)postClick{
    
    PostViewController *post = [[PostViewController alloc] init];
    post.post = self.theThreads;
    [self.navigationController pushViewController:post animated:NO];
    
}
//获取评论
- (void)getPosts{
    
    self.theThreads.start = @"";
    self.theThreads.limit = @"20";
    [[NetworkService sharedInstance] getPostsWithThreads:self.theThreads success:^(NSArray *postsList) {
        
        self.postsList = postsList;
        [self.threadDetailTableView reloadData];
        
        UIBarButtonItem *right = [UIBarButtonItem itemWithTarget:self action:@selector(postClick) image:@"new_infor_message_icon" highImage:@"new_infor_message_icon" titleLable:[NSString stringWithFormat:@"%ld", self.postsList.count] Direct:RIGHT];
        self.navigationItem.rightBarButtonItem = right;
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}

//点击他人评论
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row >= 2) {
        self.textView.text = @"";
        PostsTableViewCell *cell = [PostsTableViewCell cellWithTableView:tableView];
        cell.thePosts = self.postsList[indexPath.row - 1 - 1];
        self.textView.text = [NSString stringWithFormat:@"@%@:%@", cell.thePosts.nickname,self.textView.text];

    }

}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + self.postsList.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        SubjectTableViewCell *cell = [SubjectTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.theThreads = self.theThreads;
        return cell;
    }
    
    if (indexPath.row < 1+1) {
        if (self.theThreads.attachments.count == 0) {
            return [[UITableViewCell alloc] init];
        }
        
        attachmentsTableViewCell = [AttachmentsTableViewCell cellWithTableView:tableView];
        attachmentsTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        attachmentsTableViewCell.theAttachment = self.theThreads.attachments[indexPath.row - 1];
        attachmentsTableViewCell.count = self.theThreads.attachments.count;
        
        [attachmentsTableViewCell.webView loadHTMLString:self.theThreads.message baseURL:nil];
        attachmentsTableViewCell.webView.delegate = self;
        attachmentsTableViewCell.webView.scrollView.bounces = NO;
        
        ThreadDetailViewController *bself = self;
        attachmentsTableViewCell.goSeeImagesBlock = ^(){
            ImagesViewController *imagesCon = [[ImagesViewController alloc] init];
            imagesCon.imagesArray = bself.theThreads.attachments;
            [bself.navigationController pushViewController:imagesCon animated:YES];
        };
        
        return attachmentsTableViewCell;
    }
    
//    if (indexPath.row == 2) {
//        MessageTableViewCell *cell = [MessageTableViewCell cellWithTableView:tableView];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.message = self.theThreads;
//    }
    
    PostsTableViewCell *cell = [PostsTableViewCell cellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //这个地方，假如添加message是不是需要-1
    cell.thePosts = self.postsList[indexPath.row - 1 - 1];
    return cell;
}

//开始加载
- (void)webViewDidStartLoad:(UIWebView *)webView {

}

//动态设置网页的高度
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue];
    CGRect frame = webView.frame;
    webView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, height);
    webViewHeight = webView.frame.size.height;
    attachmentsTableViewCell.webViewConstraint.constant = webViewHeight + 32;
    
    if (isOne == YES) {
        [self.threadDetailTableView reloadData];
        isOne = NO;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return [self.theThreads.subject boundingRectWithSize:CGSizeMake(264, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:20]} context:nil].size.height + [[NSString stringWithFormat:@"发布人:  %@  %@", self.theThreads.nickname, self.theThreads.dateline] boundingRectWithSize:CGSizeMake(264, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + [[NSString stringWithFormat:@"摘要:  %@", self.theThreads.abstracts] boundingRectWithSize:CGSizeMake(264, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 59;
    }
    NSLog(@"%ld", (long)indexPath.row);
    if (indexPath.row < 1+1) {
        
        if (self.theThreads.attachments.count == 0) {
            return 0;
        }
        
//        return [[self.theThreads.attachments[indexPath.row - 1] desc] boundingRectWithSize:CGSizeMake(264, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + 238;
        NSLog(@"height = %ld", (long)webViewHeight);
        return 238 + webViewHeight + 32;
    }
    
    return [[self.postsList[indexPath.row - 1 - 1] nickname] boundingRectWithSize:CGSizeMake(284, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} context:nil].size.height + [[self.postsList[indexPath.row - 1 - 1] message] boundingRectWithSize:CGSizeMake(284, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size.height + [[[self.postsList[indexPath.row - 1 - 1]  dateline] stringValue] boundingRectWithSize:CGSizeMake(284, 0) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} context:nil].size.height + 40;
}

#pragma mark 得到内容的自适应高度
+ (CGFloat)contentHeightWithSize:(CGFloat)size width:(CGFloat)width string:(NSString *)string{
    //第一个参数：是进行自适应的尺寸  第二个参数：布局格式 第三个参数：字符串的属性列表  第四个忽略
    CGRect rect = [string boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:size]} context:nil];
    
    return rect.size.height;
}

@end
