//
//  CallPoliceViewController.m
//  iPolice
//
//  Created by xwy on 15/7/21.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CallPoliceViewController.h"
#import "PoliceViewController.h"

@interface CallPoliceViewController ()

@property (nonatomic, strong) UIButton *call110;
@property (nonatomic, strong) UIButton *call0543;
@property (nonatomic, strong) UIButton *callPolice;

@property (nonatomic, strong) UIView *call110V;
@property (nonatomic, strong) UIView *call0543V;
@property (nonatomic, strong) UIView *callPoliceV;
@end

@implementation CallPoliceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"我要报警";
    
    //设置背景图
    UIImage *image = [UIImage imageNamed:@"我要报警背景图"];
    self.view.layer.contents = (id) image.CGImage;
    
}

- (IBAction)Clicked:(id)sender {
    NSLog(@"在这处理<110>");
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:@"tel:110"];// 貌似tel:// 或者 tel: 都行
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    //记得添加到view上
    [self.view addSubview:callWebview];
}

- (IBAction)clicked0543:(id)sender {
    NSLog(@"在这处理<0543110>");
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:@"tel:0543110"];// 貌似tel:// 或者 tel: 都行
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    //记得添加到view上
    [self.view addSubview:callWebview];
}


- (IBAction)clickPolice:(id)sender {
    NSLog(@"在这处理<智能报警>");
    PoliceViewController *police = [[PoliceViewController alloc] init];
    [self.navigationController pushViewController:police animated:YES];
}

@end
