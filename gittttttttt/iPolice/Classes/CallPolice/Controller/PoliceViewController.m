//
//  PoliceViewController.m
//  iPolice
//
//  Created by xwy on 15/7/21.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PoliceViewController.h"
#import "GYLAddImage.h"
#import "NetworkService.h"
#import "PostAlarm.h"
#import <MAMapKit.h>
#import <AMapSearchAPI.h>
#import "MJLabel.h"

@interface PoliceViewController ()<UITextViewDelegate, UIAlertViewDelegate, MAMapViewDelegate, AMapSearchDelegate>
{
    BOOL alvertBOOL;
    float myLatitude;
    float myLongitude;
    AMapSearchAPI *_search;
}
@property (strong, nonatomic) UITextView *textView;
@property (nonatomic, strong) GYLAddImage *addImageView;

@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) MJLabel *addressLable;
@property (weak, nonatomic) IBOutlet UIButton *callPoliceButton;
@property (weak, nonatomic) IBOutlet UIButton *call110Button;

@end

@implementation PoliceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"智能报警";
    
//    _search = [[AMapSearchAPI alloc] initWithSearchKey:MAPAPI_Key Delegate:self];
    _search = [[AMapSearchAPI alloc]init];
    _search.delegate = self;
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:15 animated:YES];
    
    //让scrollView自动适应屏幕
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //为导航栏左侧添加系统自定义按钮
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    
    //为导航栏添加右侧按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"提交" style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 70, self.view.frame.size.width-20, 150)];
    self.textView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.textView.text = @"请输入报警信息。。。";
    self.textView.font = [UIFont systemFontOfSize:12.5];
    self.textView.textAlignment = NSTextAlignmentNatural;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    
    _addImageView = [[GYLAddImage alloc] initWithFrame:CGRectMake(15, 250, self.view.frame.size.width, self.view.frame.size.height)];
    __weak typeof(self) weakSelf = self;
    _addImageView.block = ^(UIViewController *pickerVC){
        [weakSelf presentViewController:pickerVC animated:YES completion:nil];
    };
    [self.view addSubview:_addImageView];
    
    
    //地址背景
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 350, Main_Screen_Width-20, 30)];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:view];
    //地址图标
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(20, 350, 30, 30)];
    image.image = [UIImage imageNamed:@"我的地址图标.png"];
    [self.view addSubview:image];
    
    
    //地址标签
    self.addressLable = [[MJLabel alloc] initWithFrame:CGRectMake(50, 360, Main_Screen_Width-20, 60)];
    self.addressLable.font = [UIFont systemFontOfSize:13];
    self.addressLable.numberOfLines = 0;
    [self.view addSubview:self.addressLable];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.textView resignFirstResponder];
}

//输入框代理
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@"请输入报警信息。。。"]) {
        textView.text = @"";
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"请输入报警信息。。。";
    }
}


- (void)backClick{
    
    UIAlertView *alvert = [[UIAlertView alloc] initWithTitle:@"确认取消?" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认",nil];
    alvertBOOL = NO;
    [alvert show];
    
}

- (void)doneClick{
    
    NSLog(@"在这里处理<发布按钮>");
    if (self.textView.text.length == 0 || [self.textView.text isEqualToString:@"请输入报警信息。。。"]) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请添加信息或文件~" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
       
        [alvert show];
        
    } else {
        
        PostAlarm *postAlarm = [PostAlarm postThreadsWithMapx:[NSString stringWithFormat:@"%f", myLatitude] mapy:[NSString stringWithFormat:@"%f", myLongitude] location:[NSString stringWithFormat:@"%@", self.addressLable.text]  attachment:@"" message:self.textView.text attachments:@"[]"];
        [[NetworkService sharedInstance] postAlarmWithPostAlarms:postAlarm success:^{
            
        
        } failure:^(NSError *error) {
             [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        }];
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"系统提示!" message:@"确认提交?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        alvertBOOL = YES;
        [alvert show];
    }
}

//UIAlvertView的响应事件
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alvertBOOL == NO) {
        if (buttonIndex == 0) {
            
        } else if (buttonIndex == 1){
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else if(alvertBOOL == YES) {
        if (buttonIndex == 1) {
            
            [SVProgressHUD showSuccessWithStatus:@"发布成功"];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}

-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        //        NSLog(@"当前位置：latitude : %f,longitude: %f",userLocation.location.coordinate.latitude,userLocation.coordinate.longitude);
        
        myLatitude = userLocation.coordinate.latitude;
        myLongitude = userLocation.coordinate.longitude;
        
        
        //构造AMapReGeocodeSearchRequest对象，location为必选项，radius为可选项
        AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
//        regeoRequest.searchType = AMapSearchType_ReGeocode;
        regeoRequest.location = [AMapGeoPoint locationWithLatitude:myLatitude longitude:myLongitude];
        regeoRequest.radius = 10000;
        regeoRequest.requireExtension = YES;
        
        //发起逆地理编码
        [_search AMapReGoecodeSearch: regeoRequest];
    }
}


//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        //通过AMapReGeocodeSearchResponse对象处理搜索结果
        self.addressLable.text = [NSString stringWithFormat:@"%@ %@ %@ %@%@", response.regeocode.addressComponent.province, response.regeocode.addressComponent.district, response.regeocode.addressComponent.township, response.regeocode.addressComponent.streetNumber.street, response.regeocode.addressComponent.streetNumber.number];
    }
    
}

@end
