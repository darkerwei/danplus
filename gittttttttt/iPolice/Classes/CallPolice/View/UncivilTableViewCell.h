//
//  UncivilTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/8/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Threads;

typedef void(^goSeeImagesBlock)();

@protocol UncivilTableViewCellDelegate <NSObject>

- (void)commentButtonClick:(Threads *)threads;

@end

@interface UncivilTableViewCell : UITableViewCell

@property (nonatomic, assign) id<UncivilTableViewCellDelegate> delegate;
/**
 *  Cell信息
 */
@property (nonatomic, strong) Threads *threads;

/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewConstraintHeight;


@property (copy, nonatomic) goSeeImagesBlock goSeeImagesBlock;

@end
