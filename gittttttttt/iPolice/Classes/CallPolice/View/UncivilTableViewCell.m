//
//  UncivilTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/8/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "UncivilTableViewCell.h"
#import "Threads.h"
#import "Attachment.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SecureViewController.h"

#define UncivilTableViewCellIdentifier @"UncivilTableViewCell"

@interface UncivilTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *userimage;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet MJLabel *artical;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@end

@implementation UncivilTableViewCell

- (IBAction)commentButtonClick {
    
    if ([self.delegate respondsToSelector:@selector(commentButtonClick:)]) {
        [self.delegate commentButtonClick:_threads];
    }
}

- (void)setThreads:(Threads *)threads{
    
    _threads = threads;
    
    //用户头像
    [self.userimage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", threads.avatar]] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    //昵称
    self.username.text = [NSString stringWithFormat:@"%@", threads.nickname];
    //内容
    self.artical.text = [NSString stringWithFormat:@"%@", threads.abstracts];
    self.artical.font = [UIFont systemFontOfSize:15];
    self.artical.numberOfLines = 0;
    //发布时间
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *issueTime = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:threads.dateline.doubleValue /1000.0]];
    self.time.text = [NSString stringWithFormat:@"发布时间:%@", issueTime];
    self.time.font = [UIFont systemFontOfSize:12];
    //评论
    [self.commentButton setTitle:[NSString stringWithFormat:@"%@", threads.replies] forState:UIControlStateNormal];
    [self.commentButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.commentButton.titleLabel.font = [UIFont systemFontOfSize:13];
    
    //防止图片复用
    NSArray *array = [self.bottomView subviews];
    for (UIImageView *image in array) {
        [image removeFromSuperview];
    }
    
    if (threads.attachments.count == 0) {
        self.bottomViewConstraintHeight.constant = 0;
        return ;
    } else if(threads.attachments.count <= 3) {
        self.bottomViewConstraintHeight.constant = 70;
    } else {
        self.bottomViewConstraintHeight.constant = 140;
    }
    for (int i = 0; i < threads.attachments.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i%3*(70+5), i/3*(70+5), 70, 70)];
        [self.bottomView addSubview:imageView];
       
        //图片添加点击事件
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = imageView.frame;
        [button addTarget:self action:@selector(goSeeImagesBlock:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:button];
        
        Attachment *attachment = [threads.attachments objectAtIndex:i];
        [imageView sd_setImageWithURL:[NSURL URLWithString:attachment.thumbnail] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    }
}

- (void)goSeeImagesBlock:(id)sender{
    
    _goSeeImagesBlock();
}

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [tableView registerNib:[UINib nibWithNibName:UncivilTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:UncivilTableViewCellIdentifier];
    UncivilTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:UncivilTableViewCellIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
