//
//  PostAlarmAttachments.m
//  iPolice
//
//  Created by xwy on 15/8/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PostAlarmAttachments.h"

@implementation PostAlarmAttachments

+ (instancetype)postAlarmAttachments
{
    return [[self alloc] init];
}

+ (instancetype)postAlarmAttachmentsWithAid:(NSString *)aid
                                       name:(NSString *)name
                                       type:(NSString *)type
                                        uri:(NSString *)uri
                                       desc:(NSString *)desc
                                       size:(int *)size{
    
    PostAlarmAttachments *postAlarmAttachments = [self postAlarmAttachments];

    postAlarmAttachments.aid = aid;
    postAlarmAttachments.name = name;
    postAlarmAttachments.type = type;
    postAlarmAttachments.uri = uri;
    postAlarmAttachments.desc = desc;
    postAlarmAttachments.size = size;
    
    return postAlarmAttachments;
    
}

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)postAlarmAttachmentsWithDict:(NSDictionary *)dict
{
    return [[self alloc] initPostAlarmAttachmentsWithDict:dict];
}

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
- (instancetype)initPostAlarmAttachmentsWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.aid = dict[@"aid"];
        self.name = dict[@"name"];
        self.type = dict[@"type"];
        self.uri = dict[@"uri"];
        self.desc = dict[@"description"];
        self.url = dict[@"url"];
        self.thumbnail = dict[@"thumbnail"];
        
//        self.size = dict[@"size"];
    }
    return self;
}
@end
