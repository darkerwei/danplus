//
//  PostAlarmAttachments.h
//  iPolice
//
//  Created by xwy on 15/8/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostAlarmAttachments : NSObject

/**
 *  附件ID
 */
@property (nonatomic, copy) NSString *aid;
/**
 *  附件名称
 */
@property (nonatomic, copy) NSString *name;
/**
 *  附件类型 0文件 1图片 2音频 3视频
 */
@property (nonatomic, copy) NSString *type;
/**
 *  附件路径
 */
@property (nonatomic, copy) NSString *uri;
/**
 *  说明
 */
@property (nonatomic, copy) NSString *desc;
/**
 *  扩展字段
 */
@property (nonatomic, assign) int *size;
//url
@property (nonatomic, copy) NSString *url;
//缩略图
@property (nonatomic, copy) NSString *thumbnail;

/**
 *  快速创建一个用户信息
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)postAlarmAttachments;


+ (instancetype)postAlarmAttachmentsWithAid:(NSString *)aid
                                       name:(NSString *)name
                                       type:(NSString *)type
                                        uri:(NSString *)uri
                                       desc:(NSString *)desc
                                       size:(int *)size;

+ (instancetype)postAlarmAttachmentsWithDict:(NSDictionary *)dict;


- (instancetype)initPostAlarmAttachmentsWithDict:(NSDictionary *)dict;

@end
