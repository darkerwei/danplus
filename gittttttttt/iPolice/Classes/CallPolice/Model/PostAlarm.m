//
//  PostAlarm.m
//  iPolice
//
//  Created by xwy on 15/8/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PostAlarm.h"
#import "PostAlarmAttachments.h"

@implementation PostAlarm


+ (instancetype)postAlarm
{
    return [[self alloc] init];
}

//5.5 报警记录列表
+ (instancetype)postAlarmWithStatus:(NSString *)status
                              start:(NSString *)start
                              limit:(NSString *)limit{
    
    PostAlarm *postAlarm = [self postAlarm];
    
    postAlarm.status = status;
    postAlarm.start = start;
    postAlarm.limit = limit;
    
    return postAlarm;
    
}

+ (instancetype)postThreadsWithMapx:(NSString *)mapx
                               mapy:(NSString *)mapy
                           location:(NSString *)location
                         attachment:(NSString *)attachment
                            message:(NSString *)message
                        attachments:(NSArray *)attachments{
    
    PostAlarm *postAlarm = [self postAlarm];
    
    postAlarm.mapx = mapx;
    postAlarm.mapy = mapy;
    postAlarm.location = location;
    postAlarm.attachment = attachment;
    postAlarm.message = message;
    postAlarm.attachments = attachments;
    
    return postAlarm;
    
}

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)postAlarmsWithDict:(NSDictionary *)dict{
    
    return [[self alloc] initPostAlarmsWithDict:dict];
}

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
- (instancetype)initPostAlarmsWithDict:(NSDictionary *)dict
{
    self = [super init];
    
    if (self) {
        self.token = dict[@"token"];
        self.uid = dict[@"uid"];
        self.mapx = dict[@"mapx"];
        self.mapy = dict[@"mapy"];
        self.location = dict[@"location"];
        self.attachment = dict[@"attachment"];
        self.message = dict[@"message"];
        self.status = dict[@"status"];
        self.type = dict[@"type"];
        self.dateline = dict[@"dateline"];
        self.alarmid = dict[@"alarmid"];
      
//        
//        // 存储token和uid
//        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//        [defaults setObject:self.token forKey:@"token"];
//        [defaults setObject:self.uid forKey:@"uid"];
//        [defaults synchronize];
        
        NSArray *postAlarmList = dict[@"attachments"];
        NSMutableArray *tempPostAlarmList = [NSMutableArray array];
        for (NSDictionary *dict in postAlarmList) {
            [tempPostAlarmList addObject:[PostAlarmAttachments postAlarmAttachmentsWithDict:dict]];
        }
        self.attachments = tempPostAlarmList;
    }
    return self;
}
@end
