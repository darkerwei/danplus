//
//  PostAlarm.h
//  iPolice
//
//  Created by xwy on 15/8/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostAlarm : NSObject

/**
 *  Token
 */
@property (nonatomic, copy) NSString *token;
/**
 *  用户id
 */
@property (nonatomic, copy) NSString *uid;
/**
 *  X
 */
@property (nonatomic, copy) NSString *mapx;
/**
 *  Y
 */
@property (nonatomic, copy) NSString *mapy;
/**
 *  位置
 */
@property (nonatomic, copy) NSString *location;
/**
 *  附件 0无附件 1有附件
 */
@property (nonatomic, copy) NSString *attachment;
/**
 *  报警内容
 */
@property (nonatomic, copy) NSString *message;

//附件
@property (nonatomic, copy) NSArray *attachments;
//报警状态表，0-未处理，1-处理中，2-已处理，3-已结案，默认取0。
@property (nonatomic, copy) NSString *status;
//起始索引
@property (nonatomic, copy) NSString *start;
//返回个数
@property (nonatomic, copy) NSString *limit;
//报警类型：0-智能报警；1-110报警；2-专线电话报警
@property (nonatomic, copy) NSString *type;
//报警时间
@property (nonatomic, strong) NSString *dateline;
//报警id
@property (nonatomic, strong) NSString *alarmid;

+ (instancetype)postAlarm;

//5.5 报警记录列表
+ (instancetype)postAlarmWithStatus:(NSString *)status
                              start:(NSString *)start
                              limit:(NSString *)limit;

+ (instancetype)postThreadsWithMapx:(NSString *)mapx
                               mapy:(NSString *)mapy
                           location:(NSString *)location
                         attachment:(NSString *)attachment
                            message:(NSString *)message
                        attachments:(NSArray *)attachments;

+ (instancetype)postAlarmsWithDict:(NSDictionary *)dict;


- (instancetype)initPostAlarmsWithDict:(NSDictionary *)dict;
 
@end
