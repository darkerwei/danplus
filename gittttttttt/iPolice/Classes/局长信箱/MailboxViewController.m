//
//  MailboxViewController.m
//  iPolice
//
//  Created by xwy on 15/8/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MailboxViewController.h"
#import "GYLAddImage.h"
#import "PostThreads.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
//#import <MAMapKit.h>
//#import <AMapSearchAPI.h>
//MAMapViewDelegate, AMapSearchDelegate
@interface MailboxViewController ()<UITextViewDelegate, UIAlertViewDelegate>
{
    BOOL alvertBOOL;
    float myLatitude;
    float myLongitude;
//    AMapSearchAPI *_search;
}
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UILabel *countLabel;
@property (nonatomic, strong) GYLAddImage *addImageView;
//@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) UILabel *addressLabel;

@end

#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width
#define Main_Screen_Height      [[UIScreen mainScreen] bounds].size.height
#define TextCountMax 100

@implementation MailboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    _search = [[AMapSearchAPI alloc] initWithSearchKey:MAPAPI_Key Delegate:self];
//    // 创建地图
//    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    self.mapView.delegate = self;
//    self.mapView.showsUserLocation = YES;
//    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
//    [self.mapView setZoomLevel:15 animated:YES];
    
    //让scrollView自动适应屏幕
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 70, Main_Screen_Width-20, 100)];
    self.textView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.textView.text = @"请输入投诉信息(100字以内)";
    self.textView.textAlignment = NSTextAlignmentNatural;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    
    self.countLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.textView.frame.size.width-70, self.textView.frame.size.height-30, 80, 30)];
    self.countLabel.text = @"0/100";
//    [self.textView addSubview:self.countLabel];
    
    //为导航栏添加右侧按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    
    
    _addImageView = [[GYLAddImage alloc] initWithFrame:CGRectMake(15, 200, self.view.frame.size.width, self.view.frame.size.height)];
    __weak typeof(self) weakSelf = self;
    _addImageView.block = ^(UIViewController *pickerVC){
        [weakSelf presentViewController:pickerVC animated:YES completion:nil];
    };
    [self.view addSubview:_addImageView];
    
//    //地址图标
//    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(20, 350, 30, 30)];
//    image.image = [UIImage imageNamed:@"我的地址图标.png"];
//    [self.view addSubview:image];
//    
//    //地址标签
//    self.addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 350, Main_Screen_Width-20, 50)];
//    self.addressLabel.font = [UIFont systemFontOfSize:12];
//    [self.view addSubview:self.addressLabel];
  
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    // 只显示<最大允许输入字数>个内容
    if (textView.text.length > TextCountMax) {
        textView.text = [textView.text substringToIndex:TextCountMax];
    }
    
    // 实时更新输入字数统计
    self.countLabel.text = [NSString stringWithFormat:@"%d/%d", textView.text.length, TextCountMax];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.textView resignFirstResponder];
}

//输入框代理
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@"请输入投诉信息(100字以内)"]) {
        textView.text = @"";
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"请输入投诉信息(100字以内)";
    }
    
    [textView resignFirstResponder];
}


- (void)doneClick{
    
    NSLog(@"在这里处理<提交按钮>");
    
    if (self.textView.text.length == 0 || [self.textView.text isEqualToString:@"请输入投诉信息(100字以内)"]) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请添加信息或文件~" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        alvertBOOL = NO;
        [alvert show];
        
    } else {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"系统提示!" message:@"确认发布?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        alvertBOOL = YES;
        [alvert show];
    
    }
    
}


//UIAlvertView的响应事件
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alvertBOOL == NO) {
        if (buttonIndex == 0) {
        } else if (buttonIndex == 1){
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else if(alvertBOOL == YES){
        if (buttonIndex == 0) {
            
        } else if (buttonIndex == 1){
            PostThreads *post = [PostThreads postThreadsWithSubject:@"channel_uncivilized" message:self.textView.text extfield1:@"" extfield2:@"" extfield3:@"" extfield4:@"" extfield5:@"" attachments:@"[]"];
            [[NetworkService sharedInstance] sendMailWithMail:post success:^{
                
                [SVProgressHUD showSuccessWithStatus:@"局长信箱发布成功"];
                [self.navigationController popViewControllerAnimated:YES];
                 
            } failure:^(NSError *error) {
                
            }];
        }
    }
}

//-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
//{
//    if(updatingLocation)
//    {
//        //取出当前位置的坐标
//        //        NSLog(@"当前位置：latitude : %f,longitude: %f",userLocation.location.coordinate.latitude,userLocation.coordinate.longitude);
//        
//        myLatitude = userLocation.coordinate.latitude;
//        myLongitude = userLocation.coordinate.longitude;
//        
//        
//        //构造AMapReGeocodeSearchRequest对象，location为必选项，radius为可选项
//        AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
//        regeoRequest.searchType = AMapSearchType_ReGeocode;
//        regeoRequest.location = [AMapGeoPoint locationWithLatitude:myLatitude longitude:myLongitude];
//        regeoRequest.radius = 10000;
//        regeoRequest.requireExtension = YES;
//        
//        //发起逆地理编码
//        [_search AMapReGoecodeSearch: regeoRequest];
//    }
//}
//
//
////实现逆地理编码的回调函数
//- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
//{
//    if(response.regeocode != nil)
//    {
//        //通过AMapReGeocodeSearchResponse对象处理搜索结果
//        self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@号", response.regeocode.addressComponent.province, response.regeocode.addressComponent.district, response.regeocode.addressComponent.township, response.regeocode.addressComponent.streetNumber.street, response.regeocode.addressComponent.streetNumber.number];
//    }
//    
//}

@end
