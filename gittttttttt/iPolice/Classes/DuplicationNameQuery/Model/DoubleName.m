//
//  DoubleName.m
//  iPolice
//
//  Created by xwy on 15/9/2.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "DoubleName.h"

@implementation DoubleName

+ (instancetype)doubleName{
    
    return [[self alloc] init];
}

//14.4 查询驾驶人接口
+ (instancetype)doubleNameWithName:(NSString *)name{
    
    DoubleName *doubleName = [self doubleName];
    
    doubleName.name = name;
    
    return doubleName;
}

+ (instancetype)doubleNameWithDic:(NSDictionary *)dict{
    
    return [[self alloc] initDoubleNameWithDic:dict];
    
}

- (instancetype)initDoubleNameWithDic:(NSDictionary *)dic{
    
    self = [super init];
    if (self) {
        
        self.token = dic[@"token"];
        self.uid = dic[@"uid"];
        self.name = dic[@"name"];
        self.total = dic[@"total"];
        self.femaletotal = dic[@"femaletotal"];
        self.maletotal = dic[@"maletotal"];

    }
    return self;
}
@end
