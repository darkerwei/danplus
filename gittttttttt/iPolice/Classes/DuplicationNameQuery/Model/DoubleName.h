//
//  DoubleName.h
//  iPolice
//
//  Created by xwy on 15/9/2.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoubleName : NSObject
//Token
@property (nonatomic, copy) NSString *token;
//用户id
@property (nonatomic, copy) NSString *uid;
//姓名
@property (nonatomic, copy) NSString *name;
//重名总数
@property (nonatomic, copy) NSString *total;
//女性总数
@property (nonatomic, copy) NSString *femaletotal;
//男性总数
@property (nonatomic, copy) NSString *maletotal;

+ (instancetype)doubleName;

//14.4 查询驾驶人接口
+ (instancetype)doubleNameWithName:(NSString *)name;

+ (instancetype)doubleNameWithDic:(NSDictionary *)dict;

- (instancetype)initDoubleNameWithDic:(NSDictionary *)dic;

@end
