//
//  DuplicationNameQueryViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "DuplicationNameQueryViewController.h"
#import "DoubleName.h"

@interface DuplicationNameQueryViewController ()<UIAlertViewDelegate, UITextFieldDelegate>

/**
 *  查询结果界面
 */
@property (nonatomic, weak) IBOutlet UIView *queryResultView;
@property (weak, nonatomic) IBOutlet UILabel *femaletotal;
@property (weak, nonatomic) IBOutlet UILabel *maletotal;
@property (weak, nonatomic) IBOutlet UILabel *total;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UIView *rateGirl;
@property (weak, nonatomic) IBOutlet UIView *rateBoy;
@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UILabel *girl;
@property (weak, nonatomic) IBOutlet UILabel *boy;

@end

@implementation DuplicationNameQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"新生儿起名重名查询";
    
    // 边框
    self.queryResultView.layer.borderWidth = 1;
    self.queryResultView.layer.borderColor = [[UIColor orangeColor] CGColor];
    
    self.nameText.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClicked)];
    [self.view addGestureRecognizer:tap];
}

- (void)tapClicked{
    
    if ([self.nameText.text isEqualToString:@""]) {
        self.queryResultView.hidden = YES;
    }
}

//点击空白处隐藏键盘
- (void)tapClciked {
    
    self.queryResultView.hidden = YES;
}

- (IBAction)cancelButton:(id)sender {
    
    self.queryResultView.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}

/**
 *  查询按钮点击
 */
- (IBAction)QueryBtnClick
{
    NSLog(@"在这里处理<查询按钮>点击事件");
    
    if ([self.nameText.text isEqualToString:@""]) {
        
        UIAlertView *alvert = [[UIAlertView alloc] initWithTitle:@"请填写查询名字" message:@"" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alvert show];
        
        return ;
    }

    DoubleName *doubleName = [DoubleName doubleNameWithName:self.nameText.text];
    [SVProgressHUD showWithStatus:@"正在查询"];
    [[NetworkService sharedInstance] checkRepeatNameWithRepeatName:doubleName success:^(DoubleName *repeatName) {
        [SVProgressHUD showSuccessWithStatus:@"查询完成"];
     
        self.total.text = [NSString stringWithFormat:@"0"];
        //显示查询结果
        self.queryResultView.hidden = NO;
        if (![self.total.text isEqualToString:@"滨州同名同姓总数:0"]) {
            self.total.text = [NSString stringWithFormat:@"滨州同名同姓总数:%@",doubleName.total];
            self.femaletotal.text = [NSString stringWithFormat:@"人数:%@",doubleName.femaletotal];
            self.maletotal.text = [NSString stringWithFormat:@"人数:%@",doubleName.maletotal];
            self.girl.textAlignment = NSTextAlignmentCenter;
            self.boy.textAlignment = NSTextAlignmentCenter;
            self.girl.text = [NSString stringWithFormat:@"%.2f%@", doubleName.femaletotal.intValue/doubleName.total.doubleValue*100, @"%"];
            self.boy.text = [NSString stringWithFormat:@"%.2f%@", doubleName.maletotal.intValue/doubleName.total.doubleValue*100, @"%"];
            [self.nvConstraintHeight setConstant:self.label.height*(doubleName.femaletotal.intValue/doubleName.total.doubleValue)];
            [self.nanConstraintHeight setConstant:self.label.height*(doubleName.maletotal.intValue/doubleName.total.doubleValue)];

        } else {
            self.total.text = [NSString stringWithFormat:@"滨州同名同姓总数:0"];
            self.femaletotal.text = [NSString stringWithFormat:@"人数:0"];
            self.maletotal.text = [NSString stringWithFormat:@"人数:0"];
            [self.nvConstraintHeight setConstant:0];
            [self.nanConstraintHeight setConstant:0];
        }
        
        
//        self.rateGirl.frame = CGRectMake(self.rateGirl.frame.origin.x, self.rateGirl.frame.origin.y, self.rateGirl.frame.size.width, self.rateGirl.height*(doubleName.femaletotal.intValue/doubleName.total.doubleValue));
//        self.rateBoy.frame = CGRectMake(self.rateBoy.frame.origin.x, self.rateBoy.frame.origin.y, self.rateBoy.frame.size.width, self.rateBoy.height*(doubleName.maletotal.intValue/doubleName.total.doubleValue));
   
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}
//点击return 按钮 去掉
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//只要输入改变
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.queryResultView.hidden = YES;
    return YES;
}

//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nameText resignFirstResponder];
}
@end
