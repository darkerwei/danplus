//
//  DuplicationNameQueryViewController.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DuplicationNameQueryViewController : UIViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nanConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nvConstraintHeight;

@end
