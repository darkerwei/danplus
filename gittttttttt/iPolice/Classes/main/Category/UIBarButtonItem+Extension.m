//
//  UIBarButtonItem+Extension.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "UIBarButtonItem+Extension.h"
#import "Utilities.h"
@implementation UIBarButtonItem (Extension)

+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(NSString *)image highImage:(NSString *)highImage titleLable:(NSString *)title Direct:(LR)direct
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (direct == LEFT) {
            [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }else{
            [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    }

    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    // 设置图片
//    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:image]  forState:UIControlStateNormal];
//    [btn setBackgroundImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
    [btn setTitle:title forState:UIControlStateNormal];
    // 设置尺寸
    btn.size =CGSizeMake(70, 40);
    //btn.currentBackgroundImage.size;
    return [[UIBarButtonItem alloc] initWithCustomView:btn];
}

+ (UILabel *)navigationTitle:(NSString *)title{
    int width = [Utilities widthForString:title fontSize:13 andHeight:30];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 30)];
    titleLabel.text = title;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    return titleLabel;
}
/**
 *  聊天按钮
 *
 *  @param target target
 *  @param action @selecter
 *  @param image  imagename
 *
 *  @return uibarbuttonitem
 */
//+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(NSString *)image{
//    UIView
//}

@end
