//
//  UIView+Extension.h
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension)

/**
 *  横坐标
 */
@property (nonatomic, assign) CGFloat x;
/**
 *  纵坐标
 */
@property (nonatomic, assign) CGFloat y;
/**
 *  中心点横坐标
 */
@property (nonatomic, assign) CGFloat centerX;
/**
 *  中心点横坐标
 */
@property (nonatomic, assign) CGFloat centerY;
/**
 *  宽度
 */
@property (nonatomic, assign) CGFloat width;
/**
 *  长度
 */
@property (nonatomic, assign) CGFloat height;
/**
 *  大小
 */
@property (nonatomic, assign) CGSize size;
/**
 *  位置
 */
@property (nonatomic, assign) CGPoint origin;

@end
