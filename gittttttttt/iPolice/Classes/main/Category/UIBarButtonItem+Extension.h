//
//  UIBarButtonItem+Extension.h
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum left_right{
    LEFT,
    RIGHT
}LR;

@interface UIBarButtonItem (Extension)

/**
 *  创建一个item
 *
 *  @param target    点击item后调用哪个对象的方法
 *  @param action    点击item后调用target的哪个方法
 *  @param image     图片
 *  @param highImage 高亮的图片
 *
 *  @return 创建完的item
 */
+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(NSString *)image highImage:(NSString *)highImage titleLable:(NSString *)title  Direct:(LR)direct;
/**
 *  创建nav 标题
 *
 *  @param title title内容
 *
 *  @return UILabel
 */
+ (UILabel *)navigationTitle:(NSString *)title;
//+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(NSString *)image;
@end
