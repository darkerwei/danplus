//
//  RegularExpression.m
//  iPolice
//
//  Created by ioswei on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "RegularExpression.h"

@implementation RegularExpression

// 检测手机号码格式的正确性
+ (BOOL)isMobileNumber:(NSString *)mobileNumber
{
    NSString *mobileNumberCheck = @"[1][0-9]{10}"; // 11位数字，第1位为1
    NSPredicate *mobileNumberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",mobileNumberCheck];
    return [mobileNumberTest evaluateWithObject:mobileNumber];
}

// 检测验证码格式的正确性
+ (BOOL)isVerifyCode:(NSString *)verifyCode
{
    NSString *verifyCodeCheck = @"[0-9]{4}"; // 4位数字
    NSPredicate *verifyCodeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",verifyCodeCheck];
    return [verifyCodeTest evaluateWithObject:verifyCode];
}

// 检测用户名格式的正确性
+ (BOOL)isUserName:(NSString *)userName
{
    NSString *userNameCheck = @"[a-zA-Z0-9]{1,15}"; // 1~15位数字和字母
    NSPredicate *userNameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",userNameCheck];
    return [userNameTest evaluateWithObject:userName];
}

// 检测密码格式的正确性
+ (BOOL)isPassword:(NSString *)password
{
    NSString *passwordCheck = @"[a-zA-Z0-9]{1,32}"; // 1~32位数字和字母
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",passwordCheck];
    return [passwordTest evaluateWithObject:password];
}

@end
