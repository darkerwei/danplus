//
//  RegularExpression.h
//  iPolice
//
//  Created by ioswei on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegularExpression : NSObject

/**
 *  检测手机号码格式的正确性
 *
 *  @param mobileNumber 手机号码
 *
 *  @return 手机号码格式的正确性
 */
+ (BOOL)isMobileNumber:(NSString *)mobileNumber;

/**
 *  检测验证码格式的正确性
 *
 *  @param verifyCode 验证码
 *
 *  @return 验证码格式的正确性
 */
+ (BOOL)isVerifyCode:(NSString *)verifyCode;

/**
 *  检测用户名格式的正确性
 *
 *  @param userName 用户名
 *
 *  @return 用户名格式的正确性
 */
+ (BOOL)isUserName:(NSString *)userName;

/**
 *  检测密码格式的正确性
 *
 *  @param password 密码
 *
 *  @return 密码格式的正确性
 */
+ (BOOL)isPassword:(NSString *)password;

@end
