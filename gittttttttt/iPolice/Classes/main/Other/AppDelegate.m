//
//  AppDelegate.m
//  iPolice
//
//  Created by Bert on 15/6/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "AppDelegate.h"
#import "TabBarViewController.h"
#import "WelcomePageViewController.h"
#import <SMS_SDK/SMS_SDK.h>
#import <MAMapKit.h>
#import <SVProgressHUD.h>
#import <MAMapKit/MAMapKit.h>
#import "LoginViewController.h"
#import <AMapSearchServices.h>
#import "NavigationController.h"

#define CFBundleVersion @"CFBundleVersion"
// 图标动画的时间
#define IconAnimationTime 1.75f
// 图标的宽度
#define IconImageWidth 100
// 图标的Y值
#define IconImageY (Main_Screen_Height * 0.5 - IconImageWidth)
// 每个文字动画的时间   
#define TextAnimationTime 0.15f
// 文字的宽度
#define TextWidth 20
// 所有文字的Y值
#define TextY (IconImageY + IconImageWidth)
// 第一个文字的X值
#define TextX (Main_Screen_Width * 0.5 - TextWidth * 2)

@interface AppDelegate ()

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // 设置导航控制器背景
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"上半部分渐变背景"] forBarMetrics:UIBarMetricsDefault];
    
    [MAMapServices sharedServices].apiKey = MAPAPI_Key;
    [AMapSearchServices sharedServices].apiKey = MAPAPI_Key;
    
    //加载框的颜色
//    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    // 上一次打开的版本
    NSString *lastVersion = [[NSUserDefaults standardUserDefaults] objectForKey:CFBundleVersion];
    // 这一次打开的版本
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[CFBundleVersion];
    
    // 如果两次打开的版本不一样，显示欢迎页
    if (![currentVersion isEqualToString:lastVersion]) {
        [self.window setRootViewController:[[WelcomePageViewController alloc] init]];
        
        // 隐藏状态栏
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
        
        // 存这一次打开的版本
        [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:CFBundleVersion];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
//        self.window.rootViewController = [[TabBarViewController alloc] init];
        
        NavigationController *navCon = [[NavigationController alloc] initWithRootViewController:[[LoginViewController alloc] init]];
        //跳到登录页面
        self.window.rootViewController = navCon;
        
    }
    
    [self.window makeKeyAndVisible];
    
    // 显示动画页面
    [self showAnimationView];
    
    // 注册SMS_SDK
    [SMS_SDK registerApp:SMS_SDK_AppKey withSecret:SMS_SDK_AppSecret];
    [SMS_SDK enableAppContactFriends:NO];
    
    self.locationManager =[[CLLocationManager alloc] init];
    
    // fix ios8 location issue
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
#ifdef __IPHONE_8_0
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [self.locationManager performSelector:@selector(requestWhenInUseAuthorization)];//用这个方法，plist里要加字段NSLocationWhenInUseUsageDescription
        }
#endif
    }
    
    return YES;
}

/**
 *  显示动画页面
 */
- (void)showAnimationView
{
    UIView *animationView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    animationView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"启动页"]];
    [self.window addSubview:animationView];
    
    UIImageView *iconImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height)];
    iconImage.image = [UIImage imageNamed:@"启动页"];
    [animationView addSubview:iconImage];
    
    // 图标动画
    [UIView animateWithDuration: IconAnimationTime animations:^{
//        iconImage.center = CGPointMake(Main_Screen_Width * 0.5, Main_Screen_Height * 0.5 - IconImageWidth);
    } completion:^(BOOL finished) {
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, TextWidth, TextWidth)];
        label1.center = CGPointMake(TextX, TextY);
        label1.text = @"正";
        [animationView addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, TextWidth, TextWidth)];
        label2.center = CGPointMake(TextX, TextY);
        label2.text = @"在";
        label2.alpha = 0;
        [animationView addSubview:label2];
        
        // 文字动画
        [UIView animateWithDuration:TextAnimationTime animations:^{
            label2.center = CGPointMake(TextX + TextWidth, TextY);
            label2.alpha = 1;
        } completion:^(BOOL finished) {
            UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, TextWidth, TextWidth)];
            label3.center = CGPointMake(TextX + TextWidth, TextY);
            label3.text = @"你";
            label3.alpha = 0;
            [animationView addSubview:label3];
            
            // 文字动画
            [UIView animateWithDuration:TextAnimationTime animations:^{
                label3.center = CGPointMake(TextX + TextWidth * 2, TextY);
                label3.alpha = 1;
            } completion:^(BOOL finished) {
                UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, TextWidth, TextWidth)];
                label4.center = CGPointMake(TextX + TextWidth * 2, TextY);
                label4.text = @"身";
                label4.alpha = 0;
                [animationView addSubview:label4];
                
                // 文字动画
                [UIView animateWithDuration:TextAnimationTime animations:^{
                    label4.center = CGPointMake(TextX + TextWidth * 3, TextY);
                    label4.alpha = 1;
                } completion:^(BOOL finished) {
                    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, TextWidth, TextWidth)];
                    label5.center = CGPointMake(TextX + TextWidth * 3, TextY);
                    label5.text = @"边";
                    label5.alpha = 0;
                    [animationView addSubview:label5];
                    
                    // 文字动画
                    [UIView animateWithDuration:TextAnimationTime animations:^{
                        label5.center = CGPointMake(TextX + TextWidth * 4, TextY);
                        label5.alpha = 1;
                    } completion:^(BOOL finished) {
                        [animationView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:IconAnimationTime * 0.5];
                    }];
                }];
            }];
        }];
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
