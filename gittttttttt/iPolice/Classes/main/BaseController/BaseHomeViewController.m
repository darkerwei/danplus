//
//  BaseHomeViewController.m
//  iPolice
//
//  Created by Bert on 6/12/15.
//  Copyright (c) 2015 Bert. All rights reserved.
//

#import "BaseHomeViewController.h"
#import "NetworkService.h"
#import "UserInfo.h"
#import "MymessageViewController.h"
#import <MAMapKit.h>
#import <AMapSearchAPI.h>

@interface BaseHomeViewController ()<MAMapViewDelegate, AMapSearchDelegate>
{
    float myLatitude;
    float myLongitude;
    AMapSearchAPI *_search;
}
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) NSString *address;
@end

@implementation BaseHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *left = [UIBarButtonItem itemWithTarget:self action:@selector(resignclick:) image:@"我的积分" highImage:@"我的积分" titleLable:@"签到" Direct:LEFT];
    [left setWidth:80];
    self.navigationItem.leftBarButtonItem = left;
    UIBarButtonItem *right = [UIBarButtonItem itemWithTarget:self action:@selector(chatclick:) image:@"右上角消息图标" highImage:@"右上角消息图标" titleLable:nil Direct:RIGHT];
    self.navigationItem.rightBarButtonItem = right;

    self.view.userInteractionEnabled = YES;
    
    //_search = [[AMapSearchAPI alloc] initWithSearchKey:MAPAPI_Key Delegate:self];
    _search = [[AMapSearchAPI alloc]init];
    _search.delegate = self;
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:15 animated:YES];
    
}


/***
 *  聊天
 *
 *  @param sender rightbutton
 */
-(void)chatclick:(id)sender{
    NSLog(@"消息");
    MymessageViewController *message = [[MymessageViewController alloc] init];
    [self.navigationController pushViewController:message animated:YES];
    
}


-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
//        NSLog(@"当前位置：latitude : %f,longitude: %f",userLocation.location.coordinate.latitude,userLocation.coordinate.longitude);
        
        myLatitude = userLocation.coordinate.latitude;
        myLongitude = userLocation.coordinate.longitude;

        [[NSUserDefaults standardUserDefaults] setDouble:userLocation.coordinate.latitude forKey:@"lat"];
        [[NSUserDefaults standardUserDefaults] setDouble:userLocation.coordinate.longitude forKey:@"lon"];
        
        //构造AMapReGeocodeSearchRequest对象，location为必选项，radius为可选项
        AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
//        regeoRequest.searchType = AMapSearchType_ReGeocode;
        regeoRequest.location = [AMapGeoPoint locationWithLatitude:myLatitude longitude:myLongitude];
        regeoRequest.radius = 10000;
        regeoRequest.requireExtension = YES;
        
        //发起逆地理编码
        [_search AMapReGoecodeSearch: regeoRequest];
    }
}


//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        //通过AMapReGeocodeSearchResponse对象处理搜索结果
        self.address = [NSString stringWithFormat:@"%@%@%@%@%@", response.regeocode.addressComponent.province, response.regeocode.addressComponent.district, response.regeocode.addressComponent.township, response.regeocode.addressComponent.streetNumber.street, response.regeocode.addressComponent.streetNumber.number];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.address forKey:@"address"];
    }
    
}

/**
 *  签到
 *
 *  @param sender leftbutton
 */
-(void)resignclick:(id)sender{
    NSLog(@"签到");
    NSLog(@"%f %f  %@", [[NSUserDefaults standardUserDefaults] doubleForKey:@"lat"], [[NSUserDefaults standardUserDefaults] doubleForKey:@"lon"],[[NSUserDefaults standardUserDefaults] objectForKey:@"address"]);
    
//    UserInfo *user = [UserInfo userInfoWithmapx:[NSString stringWithFormat:@"%f",myLatitude] mapy:[NSString stringWithFormat:@"%f", myLongitude] location:self.address];
    UserInfo *user = [UserInfo userInfoWithmapx:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] doubleForKey:@"lon"]] mapy:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] doubleForKey:@"lat"]] location:[[NSUserDefaults standardUserDefaults] objectForKey:@"address"]];
    [[NetworkService sharedInstance] userSignWithUserSign:user success:^{
        [SVProgressHUD showSuccessWithStatus:@"签到成功!"];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}
@end
