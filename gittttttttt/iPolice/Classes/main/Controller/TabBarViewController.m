//
//  TabBarViewController.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TabBarViewController.h"
#import "HomeViewController.h"
#import "DiscoverViewController.h"
#import "ProfileViewController.h"
#import "NavigationController.h"
#import "LoginViewController.h"

// 显示广告的时间
#define advertisementViewTime 0.1f

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 显示广告页
    [self showAdvertisement];
    
    HomeViewController *home = [[HomeViewController alloc] init];
    [self addChildVc:home title:@"首页" image:@"首页-原始" selectedImage:@"首页-点击"];
    home.navigationItem.title = @"滨城公安";
    
    DiscoverViewController *discover = [[DiscoverViewController alloc] init];
    [self addChildVc:discover title:@"发现" image:@"发现-原始" selectedImage:@"发现-点击"];
    
    ProfileViewController *profile = [[ProfileViewController alloc] init];
    [self addChildVc:profile title:@"我的" image:@"我的-原始" selectedImage:@"我的-点击"];
    
    self.tabBar.barTintColor = [UIColor whiteColor];
}

/**
 *  显示广告页
 */
- (void)showAdvertisement
{
    UIView *advertisementView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    advertisementView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:advertisementView];
    
    
//    //广告页面的 跳过 按钮
//    UIButton *skipBtn = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width * 0.8, Main_Screen_Height * 0.1, Main_Screen_Width * 0.1, Main_Screen_Width * 0.1)];
//    skipBtn.backgroundColor = [UIColor lightGrayColor];
//    [skipBtn setTitle:@"跳过" forState:UIControlStateNormal];
//    skipBtn.titleLabel.font = [UIFont systemFontOfSize:12];
//    [skipBtn addTarget:advertisementView action:@selector(removeFromSuperview) forControlEvents:UIControlEventTouchUpInside];
//    [advertisementView addSubview:skipBtn];
//    //设置圆角
//    [skipBtn.layer setMasksToBounds:YES];
//    [skipBtn.layer setCornerRadius:skipBtn.width * 0.5];
    
    [self performSelector:@selector(remove:) withObject:advertisementView afterDelay:advertisementViewTime];
}

- (void)remove:(UIView *)view
{
    [view removeFromSuperview];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *uid = [defaults objectForKey:@"uid"];
    
    // 自动登录
    if (token == nil && uid == nil) {
        LoginViewController *loginVC = [[LoginViewController alloc] init];
        NavigationController *navVC = [[NavigationController alloc] initWithRootViewController:loginVC];
        [self presentViewController: navVC animated:YES completion:nil];
    }
}

/**
 *  添加一个子控制器
 *
 *  @param childVc       子控制器
 *  @param title         标题
 *  @param image         图片
 *  @param selectedImage 选中的图片
 */
- (void)addChildVc:(UIViewController *)childVc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage
{
    // 设置子控制器的文字
    childVc.title = title; // 同时设置tabbar和navigationBar的文字
    
    // 设置子控制器的图片
    childVc.tabBarItem.image = [UIImage imageNamed:image];
    childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    // 设置文字的样式
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] = [UIColor darkGrayColor];
    NSMutableDictionary *selectTextAttrs = [NSMutableDictionary dictionary];
    selectTextAttrs[NSForegroundColorAttributeName] = UIColorFromRGB(0xFA4828);
    [childVc.tabBarItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    [childVc.tabBarItem setTitleTextAttributes:selectTextAttrs forState:UIControlStateSelected];
    childVc.view.backgroundColor = [UIColor whiteColor];
    
    // 先给外面传进来的小控制器 包装 一个导航控制器
    NavigationController *nav = [[NavigationController alloc] initWithRootViewController:childVc];
    // 添加为子控制器
    [self addChildViewController:nav];
}

@end
