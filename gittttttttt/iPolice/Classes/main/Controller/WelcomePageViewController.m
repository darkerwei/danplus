//
//  WelcomePageViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/11.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "WelcomePageViewController.h"
#import "TabBarViewController.h"

// 欢迎页页数
#define WelcomePageCount 3

@interface WelcomePageViewController ()<UIScrollViewDelegate>

/**
 *  欢迎页
 */
@property (weak, nonatomic) IBOutlet UIScrollView *welcomePageView;
/**
 *  欢迎页页码
 */
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation WelcomePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置欢迎页图片
    for (int i = 0; i < WelcomePageCount; i++) {
        UIImageView *welcomePageImage = [[UIImageView alloc] initWithFrame:CGRectMake(Main_Screen_Width * i, 0, Main_Screen_Width, Main_Screen_Height)];
        welcomePageImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"欢迎页%d.png", i + 1]];
        [self.welcomePageView addSubview:welcomePageImage];
        
        // 设置第一张欢迎图片
        if (i == 0) {
            [self setupFirstImageView:welcomePageImage];
        }
        
        // 设置最后一张欢迎图片
        if (i == WelcomePageCount - 1) {
            [self setupLastImageView:welcomePageImage];
        }
    }
    
    // 设置scrollView属性
    self.welcomePageView.contentSize = CGSizeMake(Main_Screen_Width * WelcomePageCount, 0);
    self.welcomePageView.pagingEnabled = YES;
    self.welcomePageView.showsHorizontalScrollIndicator = NO;
    
    // 设置pageControl属性
    self.pageControl.numberOfPages = WelcomePageCount;
    self.pageControl.userInteractionEnabled = NO;
}

#pragma mark - UIScrollViewDelegate

/**
 *  设置欢迎页页码
 *
 *  @param scrollView 哪个scrollView
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    double page = scrollView.contentOffset.x / Main_Screen_Width;
    self.pageControl.currentPage = (int)(page + 0.5);
    
    if (page > 2.3) {
        [self beginBtnClick];
    }
}

/**
 *  设置第一张欢迎图片
 *
 *  @param imageView 第一张欢迎图片
 */
- (void)setupFirstImageView:(UIImageView *)imageView
{
    imageView.userInteractionEnabled = YES;
    
    UIButton *skipBtn = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width * 0.7, Main_Screen_Height * 0.9, Main_Screen_Width * 0.2, Main_Screen_Width * 0.1)];
    skipBtn.backgroundColor = [UIColor lightGrayColor];
    skipBtn.clipsToBounds = YES;
    skipBtn.layer.cornerRadius = 5;
    [skipBtn setTitle:@"跳过" forState:UIControlStateNormal];
    skipBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [skipBtn addTarget:self action:@selector(beginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:skipBtn];
}

/**
 *  设置最后一张欢迎图片
 *
 *  @param imageView 最后一张欢迎图片
 */
- (void)setupLastImageView:(UIImageView *)imageView
{
    imageView.userInteractionEnabled = YES;
    
    UIButton *beginBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width / 3, 40)];
    beginBtn.center = CGPointMake( Main_Screen_Width / 2,Main_Screen_Height-60);
    beginBtn.backgroundColor = UIColorFromRGB(0xE8BF76);
    beginBtn.clipsToBounds = YES;
    beginBtn.layer.cornerRadius = 5;
    [beginBtn setTitle:@"进入应用" forState:UIControlStateNormal];
    [beginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    beginBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [beginBtn addTarget:self action:@selector(beginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:beginBtn];
}

/**
 *  开始按钮点击事件
 */
- (void)beginBtnClick
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    if ([window.rootViewController isKindOfClass:[TabBarViewController class]]) {
        
        // 返回
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    } else {
        window.rootViewController = [[TabBarViewController alloc] init];
    }
    
    // 显示状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    
    
}

@end
