//
//  TotalMapViewController.m
//  iPolice
//
//  Created by xwy on 15/8/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TotalMapViewController.h"
#import <MAMapKit.h>
#import "Police.h"
#import "CustomAnnotationView.h"


@interface TotalMapViewController ()<MAMapViewDelegate>

/**
 *  地图
 */
@property (nonatomic, strong) MAMapView *mapView;

@end

@implementation TotalMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"警情统计分布图";
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    // 注册高德地图
//    [MAMapServices sharedServices].apiKey = MAPAPI_Key;
    
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:15 animated:YES];
    
    [self.view addSubview:self.mapView];
    
    //添加大头针
    for (Police *police in self.policeInfoList) {
        
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        pointAnnotation.coordinate = CLLocationCoordinate2DMake(police.may.doubleValue, police.max.doubleValue);
        pointAnnotation.title = police.xqmc;
        NSString *level = [NSString stringWithFormat:@"%@", police.level];
        if ([level isEqualToString:@"0"]) {
            level = @"优";
        } else if ([level isEqualToString:@"1"]){
            level = @"良";
        } else if ([level isEqualToString:@"2"]){
            level = @"好";
        } else if ([level isEqualToString:@"3"]){
            level = @" 一般";
        } else if ([level isEqualToString:@"4"]){
            level = @"  差";
        } else if ([level isEqualToString:@"5"]){
            level = @" 很差";
        }
        pointAnnotation.subtitle = [NSString stringWithFormat:@"警情:%@ 指数:%@", police.total, level];
        
        [self.mapView addAnnotation:pointAnnotation];
    }
}

#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
        
        //设置气泡的图片
        for (Police *police in self.policeInfoList) {
            if (annotation.coordinate.longitude == police.mapx.doubleValue && annotation.coordinate.latitude == police.mapy.doubleValue) {
                annotationView.imageURL = police.thumbnail;
            }
        }
        //将大头针的show关闭，开启气泡的自动显示
        [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.5f];
        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        return annotationView;
    }
    return nil;
}

-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
//        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
    }
}

@end
