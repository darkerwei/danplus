//
//  PoliceDetailViewController.h
//  iPolice
//
//  Created by xwy on 15/8/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SecureViewController;


@interface PoliceDetailViewController : UIViewController

@property (nonatomic, strong) NSString *secture;

@property (nonatomic, strong) NSMutableArray *myArray;
@end
