//
//  TotalMapViewController.h
//  iPolice
//
//  Created by xwy on 15/8/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Police;

@interface TotalMapViewController : UIViewController

/**
 *  警情列表
 */
@property (nonatomic, strong) NSArray *policeInfoList;

@end
