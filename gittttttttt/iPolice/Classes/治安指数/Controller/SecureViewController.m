//
//  SecureViewController.m
//  iPolice
//
//  Created by xwy on 15/7/22.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "SecureViewController.h"
#import "PoliceTableViewCell.h"
#import "Police.h"
#import "NetworkService.h"
#import "PoliceDetailViewController.h"
#import <SVProgressHUD.h>
#import "Statistics.h"
#import <PNChart.h>
#import "TotalMapViewController.h"

@interface SecureViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    BOOL _outAndInBool;
}
@property (nonatomic, strong) NSMutableArray *policeMakes11;
@property (nonatomic, strong) NSMutableArray *policeMakes22;
@property (nonatomic, strong) NSMutableArray *policeMakes33;
//button数组
@property (nonatomic, strong) NSArray *policeMakes1;
@property (nonatomic, strong) NSArray *policeMakes2;
@property (nonatomic, strong) NSArray *policeMakes3;

//控制上下拉按钮
@property (nonatomic, strong) UIButton *buttonControl;

//三个button
@property (nonatomic, strong) UIButton *button1;
@property (nonatomic, strong) UIButton *button2;
@property (nonatomic, strong) UIButton *button3;

//三个标签
@property(nonatomic, strong) UILabel *label1;
@property(nonatomic, strong) UILabel *label2;
@property(nonatomic, strong) UILabel *label3;

//筛选button
@property(nonatomic, strong)UIView *views;
@property(nonatomic, strong)UIView *views1;

//数据源
@property(nonatomic, strong) NSArray *policeArray;
//显示周期
@property (nonatomic, assign) NSInteger cycletype;
//显示内容
@property (nonatomic, assign) NSInteger content;
//显示排序
@property (nonatomic, assign) NSInteger sort;

@property (nonatomic, assign) int limit;

@property(weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SecureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"治安指数";
 
    //右导航
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(clickRight)];
    self.navigationItem.rightBarButtonItem = right;
    right.image = [[UIImage imageNamed:@"video_map.jpg"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.views = [[UIView alloc] init];
    self.views.frame = CGRectMake(0, 0, self.view.frame.size.width, 70);
    self.views.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
    [self.view addSubview:self.views];
    
    //上拉下拉按钮
    self.buttonControl = [[UIButton alloc] initWithFrame:CGRectMake(0, 40, [UIScreen mainScreen].bounds.size.width, 30)];
    [self.buttonControl setImage:[UIImage imageNamed:@"向上箭头.png"] forState:UIControlStateNormal];
    self.buttonControl.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
    [self.buttonControl addTarget:self action:@selector(clickChangeFrame) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.buttonControl];
    
    //第一个筛选
    self.label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 80, 40)];
    self.label1.text = @"统计周期";
    [self.views addSubview:self.label1];

    _outAndInBool = NO;
    
    //设置1标题
    self.policeMakes11 = [NSMutableArray array];
    self.policeMakes1 = [NSArray arrayWithObjects:@"周",@"月",@"季",@"年", nil];
    for (int i= 0; i<4; i++) {
        self.button1 = [[UIButton alloc] initWithFrame:CGRectMake(80+i*65, 0, 50, 40)];
        [self.button1 setTitle:self.policeMakes1[i] forState:UIControlStateNormal];
        self.button1.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if ([self.button1.titleLabel.text isEqualToString:@"周"]) {
            self.button1.selected = YES;
        }
        [self.button1 setImage:[UIImage imageNamed:@"rb_cycle_bg.png"] forState:UIControlStateNormal];
        [self.button1 setImage:[UIImage imageNamed:@"rb_red.png"] forState:UIControlStateSelected];
        self.button1.tag = i;
        [self.button1 addTarget:self action:@selector(clickButton1:) forControlEvents:UIControlEventTouchUpInside];
        [self.views addSubview:self.button1];
        [self.policeMakes11 addObject:self.button1];
    }
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 38, [UIScreen mainScreen].bounds.size.width, 2)];
    label1.backgroundColor = [UIColor grayColor];
    [self.views addSubview:label1];
    
    self.cycletype = 1;
    self.content = 0;
    self.sort = 1;
    
//    [self getAlarmStatistics];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];
}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    Police *police = [Police policeWithCycletype:[NSString stringWithFormat:@"%d",self.cycletype] content:[NSString stringWithFormat:@"%d",self.content] sort:[NSString stringWithFormat:@"%d",self.sort] mapx:@"" mapy:@"" start:@"" limit:@""];
    [SVProgressHUD showWithStatus:@"正在加载"];
    [[NetworkService sharedInstance] getAlarmStatisticsWithPolice:police success:^(NSArray *polistList) {
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
        
        // 视频满10个才显示上拉刷新
        if (polistList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (polistList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        
        self.policeArray = polistList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
    } failure:^(NSError *error) {
        [self.tableView.header endRefreshing];
        //        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];

}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    Police *police = [Police policeWithCycletype:[NSString stringWithFormat:@"%d",self.cycletype] content:[NSString stringWithFormat:@"%d",self.content] sort:[NSString stringWithFormat:@"%d",self.sort] mapx:@"" mapy:@"" start:@"" limit:@""];
    [SVProgressHUD showWithStatus:@"正在加载"];
    [[NetworkService sharedInstance] getAlarmStatisticsWithPolice:police success:^(NSArray *polistList) {
        
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
        // 没有更多视频时隐藏上拉刷新
        if (polistList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取列表成功"];
        } else if (polistList.count < self.limit - 10) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多了"];
        }
        
        self.policeArray = polistList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
        
        
    } failure:^(NSError *error) {
        [self.tableView.footer endRefreshing];
        //        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];

}

- (void)setUI{
    
    self.views1 = [[UIView alloc] init];
    self.views1.frame = CGRectMake(0, 42, self.view.frame.size.width, 120);
    self.views1.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
    [self.view addSubview:self.views1];

    //第二个筛选
    self.label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 80, 40)];
    self.label2.text = @"显示内容";
    [self.views1 addSubview:self.label2];
    
    
    //第三个筛选
    self.label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 80, 40)];
    self.label3.text = @"显示排序";
    [self.views1 addSubview:self.label3];
    

    //设置2标题
    self.policeMakes22 = [NSMutableArray array];
    self.policeMakes2 = @[@"全部", @"非零发", @"零发"];
    for (int i= 0; i<3; i++) {
        self.button2 = [[UIButton alloc] initWithFrame:CGRectMake(80+i*85, 0, 60, 40)];
        [self.button2 setTitle:self.policeMakes2[i] forState:UIControlStateNormal];
        self.button2.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.button2.tag = i;
        if ([self.button2.titleLabel.text isEqualToString:@"全部"]) {
            self.button2.selected = YES;
        }
        [self.button2 setImage:[UIImage imageNamed:@"rb_cycle_bg.png"] forState:UIControlStateNormal];
        [self.button2 setImage:[UIImage imageNamed:@"rb_red.png"] forState:UIControlStateSelected];
        [self.button2 addTarget:self action:@selector(clickButton2:) forControlEvents:UIControlEventTouchUpInside];
        [self.views1 addSubview:self.button2];
        [self.policeMakes22 addObject:self.button2];
    }
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, [UIScreen mainScreen].bounds.size.width, 2)];
    label2.backgroundColor = [UIColor grayColor];
    [self.views1 addSubview:label2];
    
    //设置3标题
    self.policeMakes33 = [NSMutableArray array];
    self.policeMakes3 = @[@"发案率高到低", @"发案率低到高", @"由距离近到远", @"由距离远到近"];
    for (int i = 0; i<4; i++) {
        int h = i/2;
        int l = i%2;
        self.button3 = [[UIButton alloc] initWithFrame:CGRectMake(80+l*125, 40+h*40, 120, 40)];
            [self.button3 setTitle:self.policeMakes3[i] forState:UIControlStateNormal];
            self.button3.titleLabel.font = [UIFont systemFontOfSize:15];
            [self.button3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if ([self.button3.titleLabel.text isEqualToString:@"发案率高到低"]) {
            self.button3.selected = YES;
        }
            self.button3.tag = i+1;
            [self.button3 setImage:[UIImage imageNamed:@"rb_cycle_bg.png"] forState:UIControlStateNormal];
            [self.button3 setImage:[UIImage imageNamed:@"rb_red.png"] forState:UIControlStateSelected];
            [self.button3 addTarget:self action:@selector(clickButton3:) forControlEvents:UIControlEventTouchUpInside];
            [self.views1 addSubview:self.button3];
            [self.policeMakes33 addObject:self.button3];
    }
    
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, [UIScreen mainScreen].bounds.size.width, 2)];
    label3.backgroundColor = [UIColor grayColor];
    [self.views1 addSubview:label3];
}
- (void)clickChangeFrame{
 
    if (!_outAndInBool) {
        [self setUI];
    } else {
        [self.views1 removeFromSuperview];
        //开始筛选
        [self refresh];
    }
    //加动画旋转180
    [UIView animateWithDuration:0.3f animations:^{
        self.buttonControl.imageView.transform = CGAffineTransformRotate(self.buttonControl.imageView.transform, M_PI);
    }];
    
    [UIView animateWithDuration:0.1 animations:^{

        self.buttonControl.frame = CGRectMake(0, YES == _outAndInBool ? 40:160, [UIScreen mainScreen].bounds.size.width, 30);
    }];
   
    _outAndInBool = !_outAndInBool;
    
}

- (void)clickRight{
    
    NSLog(@"在这里处理<警情统计>");
    TotalMapViewController *total = [[TotalMapViewController alloc] init];
    total.policeInfoList = self.policeArray;
    
    [self.navigationController pushViewController:total animated:NO];
    
}

// 用于重置其他 btn 的selected 状态为NO
- (void)setBtnSelectedUsedBtn:(UIButton *)btn inBtnArray:(NSArray *)btnArray {
    UIButton *button = nil;
    for (button in btnArray) {
        if (btn.tag == button.tag) {
            button.selected = YES;
        }
        else {
            button.selected = NO;
        }
    }
}

- (void)clickButton1:(UIButton *)btn{
    
    [self setBtnSelectedUsedBtn:btn inBtnArray:self.policeMakes11];
    self.cycletype = btn.tag+1;
}

- (void)clickButton2:(UIButton *)btn{
    
    [self setBtnSelectedUsedBtn:btn inBtnArray:self.policeMakes22];
    self.content = btn.tag;
}

- (void)clickButton3:(UIButton *)btn{
    
    [self setBtnSelectedUsedBtn:btn inBtnArray:self.policeMakes33];
    self.sort = btn.tag+1;

}

- (void)getAlarmStatistics{
    
    Police *police = [Police policeWithCycletype:[NSString stringWithFormat:@"%ld",self.cycletype] content:[NSString stringWithFormat:@"%ld",self.content] sort:[NSString stringWithFormat:@"%ld",self.sort] mapx:@"" mapy:@"" start:@"" limit:@""];
    [SVProgressHUD showInfoWithStatus:@"正在加载"];
    [[NetworkService sharedInstance] getAlarmStatisticsWithPolice:police success:^(NSArray *polistList) {

        self.policeArray = polistList;
        [self.tableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"加载完成"];
        
    } failure:^(NSError *error) {
        
//        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}


#pragma mark tableView  delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.policeArray.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 100;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PoliceTableViewCell *cell = [PoliceTableViewCell cellWithTableView:tableView];
    cell.police = self.policeArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PoliceTableViewCell *cell = [PoliceTableViewCell cellWithTableView:tableView];
    cell.police = self.policeArray[indexPath.row];
    
    if (![cell.countLabel.text isEqualToString:@"0"]) {
        
        PoliceDetailViewController *tableviewDetail = [[PoliceDetailViewController alloc] init];
        tableviewDetail.secture = cell.police.xqmc;
        
        tableviewDetail.myArray = [NSMutableArray arrayWithArray:cell.police.statistics];

        [self.navigationController pushViewController:tableviewDetail animated:NO];
    } else {
        [SVProgressHUD showSuccessWithStatus:@"没有警情"];
    }
    
}

@end
