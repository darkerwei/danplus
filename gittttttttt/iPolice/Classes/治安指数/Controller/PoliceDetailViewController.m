//
//  PoliceDetailViewController.m
//  iPolice
//
//  Created by xwy on 15/8/10.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PoliceDetailViewController.h"
#import "SecureViewController.h"
#import <PNChart.h>
#import "Police.h"
#import "Statistics.h"

@interface PoliceDetailViewController ()

//标题
@property (nonatomic, strong) UILabel *titleLabel;
//圆饼图
@property(nonatomic, strong)PNPieChart *pieChart;
@end

@implementation PoliceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.title = @"警情数量统计图";
}

- (void)setMyArray:(NSMutableArray *)myArray
{
    _myArray =myArray;
    
    //标题
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 100, [UIScreen mainScreen].bounds.size.width-20, 50)];
    self.titleLabel.text = [NSString stringWithFormat:@"%@警情统计分布图", self.secture];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.titleLabel];
    
    //警情分析
    NSMutableArray *tempArray = [NSMutableArray array];
    for (Statistics *statistics in myArray) {
        
        UIColor *randomColor = [UIColor colorWithRed:arc4random() % 255 / 255.0 green:arc4random() % 255 / 255.0 blue:arc4random() % 255 / 255.0 alpha:1];
        
        [tempArray addObject:[PNPieChartDataItem dataItemWithValue:statistics.count.intValue color:randomColor description:statistics.typeName]];
    }
 
    NSArray *items = [NSArray arrayWithArray:tempArray];
    //圆饼
    self.pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(SCREEN_WIDTH /2.0 - 100, CGRectGetMaxY(self.titleLabel.frame)+40, 200.0, 200.0) items:items];
    self.pieChart.descriptionTextColor = [UIColor blackColor];
    self.pieChart.descriptionTextFont  = [UIFont fontWithName:@"Avenir-Medium" size:11.0];
    self.pieChart.descriptionTextShadowColor = [UIColor clearColor];
    self.pieChart.showAbsoluteValues = NO;
    self.pieChart.showOnlyValues = YES;
    [self.pieChart strokeChart];
    [self.view addSubview:self.pieChart];
    
    self.pieChart.legendStyle = PNLegendItemStyleStacked;
    self.pieChart.legendFont = [UIFont boldSystemFontOfSize:12.0f];
   
    //注释
    UIView *legend = [self.pieChart getLegendWithMaxWidth:200];
    [legend setFrame:CGRectMake(130, CGRectGetMaxY(self.pieChart.frame)+30, legend.frame.size.width, legend.frame.size.height)];
    [self.view addSubview:legend];
    
    
}

@end
