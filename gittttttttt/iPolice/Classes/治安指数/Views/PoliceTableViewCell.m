//
//  PoliceTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/7/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PoliceTableViewCell.h"
#import "Police.h"
#import "Statistics.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define PoliceTableViewCellIdentifier @"PoliceTableViewCell"

@implementation PoliceTableViewCell

- (void)setPolice:(Police *)police{
    
    _police = police;
    
    self.addressLabel.text = police.xqmc;
    self.titleLabel.text = police.abstracts;
    self.titleLabel.font = [UIFont systemFontOfSize:13];
    
    self.countLabel.text = [NSString stringWithFormat:@"%@", police.total];
    self.countLabel.font = [UIFont systemFontOfSize:13];
    
    NSString *level = [NSString stringWithFormat:@"%@", police.level];
    if ([level isEqualToString:@"0"]) {
        self.gradeLabel.text = @" 优";
        self.gradeLabel.backgroundColor = [UIColor colorWithRed:34/255.0 green:161/255.0 blue:43/255.0 alpha:1.0];
    } else if ([level isEqualToString:@"1"]){
        self.gradeLabel.text = @" 良";
        self.gradeLabel.backgroundColor = [UIColor colorWithRed:160/255.0 green:178/255.0 blue:94/255.0 alpha:1.0];
    } else if ([level isEqualToString:@"2"]){
        self.gradeLabel.text = @" 好";
       self.gradeLabel.backgroundColor = [UIColor colorWithRed:237/255.0 green:134/255.0 blue:9/255.0 alpha:1.0];
    } else if ([level isEqualToString:@"3"]){
        self.gradeLabel.text = @" 一般";
        self.gradeLabel.backgroundColor = [UIColor colorWithRed:231/255.0 green:75/255.0 blue:9/255.0 alpha:1.0];
    } else if ([level isEqualToString:@"4"]){
        self.gradeLabel.text = @"  差";
        self.gradeLabel.backgroundColor = [UIColor colorWithRed:223/255.0 green:0/255.0 blue:16/255.0 alpha:1.0];
    } else if ([level isEqualToString:@"5"]){
        self.gradeLabel.text = @" 很差";
        self.gradeLabel.backgroundColor = [UIColor colorWithRed:148/255.0 green:0/255.0 blue:2/255.0 alpha:1.0];
    }
    
    self.gradeLabel.font = [UIFont systemFontOfSize:13];
    
    [self.thumbnail  sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@@200w_200h_90q.jpg", police.thumbnail]]
                  placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    
    Statistics *statistics = [police.statistics firstObject];

}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:PoliceTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:PoliceTableViewCellIdentifier];
    PoliceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PoliceTableViewCellIdentifier];
    
    return cell;
}
@end
