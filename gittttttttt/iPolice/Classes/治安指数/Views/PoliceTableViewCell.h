//
//  PoliceTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/7/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Police;

@interface PoliceTableViewCell : UITableViewCell

@property (nonatomic, strong) Police *police;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *gradeLabel;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
