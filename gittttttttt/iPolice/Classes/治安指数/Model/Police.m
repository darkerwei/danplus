//
//  Police.m
//  iPolice
//
//  Created by xwy on 15/8/5.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Police.h"
#import "Statistics.h"

@implementation Police
/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)police
{
    return [[self alloc] init];
}

//2.21	获取警情统计接口
+ (instancetype)policeWithCycletype:(NSString *)cycletype
                            content:(NSString *)content
                               sort:(NSString *)sort
                               mapx:(NSString *)mapx
                               mapy:(NSString *)mapy
                              start:(NSString *)start
                              limit:(NSString *)limit{
    
    Police *police = [self police];
    police.cycletype = cycletype;
    police.content = content;
    police.sort = sort;
    police.mapx = mapx;
    police.mapy = mapy;
    police.start = start;
    police.limit = limit;

    return police;
}
/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)policeWithDict:(NSDictionary *)dict
{
    return [[self alloc] initPoliceWithDict:dict];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initPoliceWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.total = dict[@"total"];
        self.max = dict[@"mapx"];
        self.may = dict[@"mapy"];
        self.xqid = dict[@"xqid"];
        self.xqmc = dict[@"xqmc"];
        self.xxdz = dict[@"xxdz"];
        self.level = dict[@"level"];
        self.abstracts = dict[@"abstracts"];
        self.thumbnail = dict[@"thumbnail"];
        
        NSArray *statistics = dict[@"statistics"];
        NSMutableArray *tempPoliceList = [NSMutableArray array];
        for (NSDictionary *dict in statistics) {
            [tempPoliceList addObject:[Statistics statisticsWithDict:dict]];
        }
        self.statistics = tempPoliceList;
        
    }
    return self;
}
@end
