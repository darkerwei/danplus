//
//  Statistics.m
//  iPolice
//
//  Created by xwy on 15/8/5.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Statistics.h"

@implementation Statistics

+ (instancetype)statistics{
    
    return [[self alloc] init];
}

+ (instancetype)statisticsWithDict:(NSDictionary *)dict{
    
    return [[self alloc] initStatisticsWithDict:dict];
}

- (instancetype)initStatisticsWithDict:(NSDictionary *)dict{
    
    self = [self init];
    if (self) {
        self.count = dict[@"count"];
        self.typeId = dict[@"typeid"];
        self.typeName = dict[@"typename"];
    }
    return self;
}
@end
