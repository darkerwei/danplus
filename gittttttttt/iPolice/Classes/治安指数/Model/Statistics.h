//
//  Statistics.h
//  iPolice
//
//  Created by xwy on 15/8/5.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Statistics : NSObject

//分类id
@property (nonatomic, copy) NSString *typeId;
//分类名
@property (nonatomic, copy) NSString *typeName;

//数量
@property (nonatomic, strong) NSNumber *count;

+ (instancetype)statistics;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)statisticsWithDict:(NSDictionary *)dict;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initStatisticsWithDict:(NSDictionary *)dict;

@end
