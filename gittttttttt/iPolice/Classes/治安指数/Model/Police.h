//
//  Police.h
//  iPolice
//
//  Created by xwy on 15/8/5.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Police : NSObject

//Token
@property (nonatomic, copy) NSString *token;
//用户id
@property (nonatomic, copy) NSString *uid;
//统计周期
@property (nonatomic, copy) NSString *cycletype;
//显示内容
@property (nonatomic, copy) NSString *content;
//显示排序
@property (nonatomic, copy) NSString *sort;
//x
@property (nonatomic, copy) NSString *mapx;
//y
@property (nonatomic, copy) NSString *mapy;
//内容开始
@property (nonatomic, copy) NSString *start;
//内容个数
@property (nonatomic, copy) NSString *limit;
//摘要
@property (nonatomic, copy) NSString *abstracts;
//缩略图
@property (nonatomic, copy) NSString *thumbnail;

//总数
@property (nonatomic, strong) NSNumber *total;
//X
@property (nonatomic, strong) NSNumber *max;
//Y
@property (nonatomic, strong) NSNumber *may;
//警情指数
@property (nonatomic, strong) NSNumber *level;
//小区ID
@property (nonatomic, copy) NSString *xqid;
//小区名称
@property (nonatomic, copy) NSString *xqmc;
//详细地址
@property (nonatomic, copy) NSString *xxdz;
//分类统计列表
@property (nonatomic, copy) NSArray *statistics;

//快速构造一个模型
+ (instancetype)police;

//2.21	获取警情统计接口
+ (instancetype)policeWithCycletype:(NSString *)cycletype
                            content:(NSString *)content
                               sort:(NSString *)sort
                               mapx:(NSString *)mapx
                               mapy:(NSString *)mapy
                              start:(NSString *)start
                              limit:(NSString *)limit;

+ (instancetype)policeWithDict:(NSDictionary *)dict;

- (instancetype)initPoliceWithDict:(NSDictionary *)dict;
@end
