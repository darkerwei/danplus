//
//  ComplainAttachment.h
//  iPolice
//
//  Created by xwy on 15/8/20.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ComplainAttachment : NSObject

/**
 *  附件ID
 */
@property (nonatomic, copy) NSString *aid;
/**
 *  附件名称
 */
@property (nonatomic, copy) NSString *name;
/**
 *  附件类型 0-文件 1-图片 2-音频 3-视频
 */
@property (nonatomic, copy) NSString *type;
/**
 *  附件路径
 */
@property (nonatomic, copy) NSString *url;
//说明
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *thumbnail;

/**
 *  快速构造一个模型
 *
 *  @return 创建好的模型
 */
+ (instancetype)complainAttachment;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)complainAttachmentWithDict:(NSDictionary *)dict;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initComplaionAttachmentWithDict:(NSDictionary *)dict;

@end
