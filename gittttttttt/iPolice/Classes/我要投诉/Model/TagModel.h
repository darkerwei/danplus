//
//  TagModel.h
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagModel : NSObject

//Token
@property (nonatomic, copy) NSString *token;
//用户id
@property (nonatomic, copy) NSString *uid;
//内容开始
@property (nonatomic, copy) NSString *start;
//内容个数
@property (nonatomic, copy) NSString *limit;

//设备id
@property (nonatomic, copy) NSString *devid;
//设备名称
@property (nonatomic, copy) NSString *name;
//设备描述
@property (nonatomic, copy) NSString *desc;
//设备型号
@property (nonatomic, copy) NSString *model;
//设备厂商
@property (nonatomic, copy) NSString *oem;
//添加时间
@property (nonatomic, copy) NSString *dateline;
//最后一次更新时间
@property (nonatomic, copy) NSString *lastupdate;
//标签状态
@property (nonatomic, copy) NSString *status;
//标签图标
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSArray *devids;

@property (nonatomic, copy) NSString *mapx;
@property (nonatomic, copy) NSString *mapy;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *starttime;
@property (nonatomic, copy) NSString *endtime;

+ (instancetype)tag;

//9.1 添加标签接口
+ (instancetype)tagWithDevid:(NSString *)devid
                        name:(NSString *)name
                        desc:(NSString *)desc
                       model:(NSString *)model
                         oem:(NSString *)oem
                        icon:(NSString *)icon;

//9.3 查询标签接口
+ (instancetype)tagWithStart:(NSString *)start
                       limit:(NSString *)limit;

+ (instancetype)myTagWithDic:(NSDictionary *)dict;

- (instancetype)initTagWithDic:(NSDictionary *)dic;

@end
