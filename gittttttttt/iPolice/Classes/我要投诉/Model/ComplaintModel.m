//
//  ComplaintModel.m
//  iPolice
//
//  Created by xwy on 15/8/20.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ComplaintModel.h"
#import "ComplainAttachment.h"

@implementation ComplaintModel

+ (instancetype)complain{
    return [[self alloc] init];
}

+ (instancetype)complainWithStart:(NSString *)start
                            limit:(NSString *)limit{
   
    ComplaintModel *complain = [self complain];
    
    complain.start = start;
    complain.limit = limit;
    
    return complain;
    
}

+ (instancetype)complainWithSubject:(NSString *)subject
                            message:(NSString *)message
                         attachment:(NSString *)attachment
                          extfield1:(NSString *)extfield1
                          extfield2:(NSString *)extfield2
                          extfield3:(NSString *)extfield3
                          extfield4:(NSString *)extfield4
                          extfield5:(NSString *)extfield5
                        attachments:(NSArray *)attachments{
    
    ComplaintModel *complain = [self complain];
    
    complain.subject = subject;
    complain.message = message;
    complain.attachment = attachment;
    complain.extfield1 = extfield1;
    complain.extfield2 = extfield2;
    complain.extfield3 = extfield3;
    complain.extfield4 = extfield4;
    complain.extfield5 = extfield5;
    complain.attachments = attachments;
    
    return complain;
    
}

+ (instancetype)complainInfoWithDict:(NSDictionary *)dict{
    
    return [[self alloc] initComplainWithDict:dict];
}

- (instancetype)initComplainWithDict:(NSDictionary *)dict{
    
    self = [super init];
    if (self) {
        
        self.token = dict[@"token"];
        self.uid = dict[@"uid"];
        self.subject = dict[@"subject"];
        self.message = dict[@"message"];
        self.attachment = dict[@"attachment"];
        self.message = dict[@"message"];
        self.extfield1 = dict[@"extfield1"];
        self.extfield2 = dict[@"extfield2"];
        self.extfield3 = dict[@"extfield3"];
        self.extfield4 = dict[@"extfield4"];
        self.extfield5 = dict[@"extfield5"];
        self.start = dict[@"start"];
        self.limit = dict[@"limit"];
        self.status = dict[@"status"];
        self.dateline = dict[@"dateline"];
        
        NSArray *threadsList = dict[@"attachments"];
        NSMutableArray *tempThreadsList = [NSMutableArray array];
        for (NSDictionary *dict in threadsList) {
            [tempThreadsList addObject:[ComplainAttachment complainAttachmentWithDict:dict]];
        }
        self.attachments = tempThreadsList;
    }
    return self;
}


@end
