//
//  ComplainAttachment.m
//  iPolice
//
//  Created by xwy on 15/8/20.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ComplainAttachment.h"

@implementation ComplainAttachment

+ (instancetype)complainAttachment
{
    return [[self alloc] init];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)complainAttachmentWithDict:(NSDictionary *)dict
{
    return [[self alloc] initComplaionAttachmentWithDict:dict];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initComplaionAttachmentWithDict:(NSDictionary *)dict
{
    
    self = [super init];
    
    if (self) {
        
        self.aid = dict[@"aid"];
        self.name = dict[@"name"];
        self.type = dict[@"type"];
        self.url = dict[@"url"];
        self.desc = dict[@"description"];
        self.thumbnail = dict[@"thumbnail"];
    }
    return self;
}

@end
