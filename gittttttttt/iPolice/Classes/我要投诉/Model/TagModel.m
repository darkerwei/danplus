//
//  TagModel.m
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TagModel.h"

@implementation TagModel

+ (instancetype)tag{
    
    return [[self alloc] init];
}

//9.1 添加标签接口
+ (instancetype)tagWithDevid:(NSString *)devid
                        name:(NSString *)name
                        desc:(NSString *)desc
                       model:(NSString *)model
                         oem:(NSString *)oem
                        icon:(NSString *)icon{

    TagModel *tag = [self tag];
    
    tag.devid = devid;
    tag.name = name;
    tag.desc = desc;
    tag.model = model;
    tag.oem = oem;
    tag.icon = icon;
    
    return tag;
}

//9.3 查询标签接口
+ (instancetype)tagWithStart:(NSString *)start
                       limit:(NSString *)limit{
    
    TagModel *tag = [self tag];
    
    tag.start = start;
    tag.limit = limit;
    
    return tag;
}

+ (instancetype)myTagWithDic:(NSDictionary *)dict{
    
    return [[self alloc] initTagWithDic:dict];
}

- (instancetype)initTagWithDic:(NSDictionary *)dic{
    
    self = [super init];
    if (self) {
        
        self.uid = dic[@"uid"];
        self.devid = dic[@"devid"];
        self.name = dic[@"name"];
        self.desc = dic[@"description"];
        self.model = dic[@"model"];
        self.oem = dic[@"oem"];
        self.dateline = dic[@"dateline"];
        self.lastupdate = dic[@"lastupdate"];
        self.status = dic[@"status"];
        self.icon = dic[@"icon"];
        self.mapx = dic[@"mapx"];
        self.mapy = dic[@"mapy"];
        self.devids = dic[@"devids"];
        self.location = dic[@"location"];
        self.starttime = dic[@"starttime"];
        self.endtime = dic[@"endtime"];
    }
    
    return self;
}
@end
