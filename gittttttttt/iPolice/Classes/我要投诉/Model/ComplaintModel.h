//
//  ComplaintModel.h
//  iPolice
//
//  Created by xwy on 15/8/20.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ComplaintModel : NSObject

/**
 *  Token
 */
@property (nonatomic, copy) NSString *token;
/**
 *  用户id
 */
@property (nonatomic, copy) NSString *uid;
/**
 *  标题
 */
@property (nonatomic, copy) NSString *subject;
/**
 *  主题内容
 */
@property (nonatomic, copy) NSString *message;
/**
 *  附件
 */
@property (nonatomic, copy) NSString *attachment;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield1;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield2;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield3;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield4;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield5;
/**
 *  附件
 */
@property (nonatomic, copy) NSArray *attachments;
@property (nonatomic, copy) NSString *start;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *dateline;

+ (instancetype)complain;

//10.1 发布投诉
+ (instancetype)complainWithSubject:(NSString *)subject
                            message:(NSString *)message
                         attachment:(NSString *)attachment
                          extfield1:(NSString *)extfield1
                          extfield2:(NSString *)extfield2
                          extfield3:(NSString *)extfield3
                          extfield4:(NSString *)extfield4
                          extfield5:(NSString *)extfield5
                        attachments:(NSArray *)attachments;

//10.4 投诉列表
+ (instancetype)complainWithStart:(NSString *)start
                            limit:(NSString *)limit;

+ (instancetype)complainInfoWithDict:(NSDictionary *)dict;


- (instancetype)initComplainWithDict:(NSDictionary *)dict;
@end
