//
//  TagTrackViewController.m
//  iPolice
//
//  Created by xwy on 15/10/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TagTrackViewController.h"
#import <MAMapKit.h>
#import "TagModel.h"
#import "CustomAnnotationView.h"

enum{

    OverlayViewControllerOverlayTypePolyline,
};

@interface TagTrackViewController ()<MAMapViewDelegate>
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) NSMutableArray *tagArray;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *devid;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UILabel *starttime;
@property (weak, nonatomic) IBOutlet UILabel *endtime;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *endButton;
@property (nonatomic, strong) UIDatePicker *datePicker;

//折线
@property (nonatomic, strong) NSMutableArray *overlays;

@end

@implementation TagTrackViewController

#pragma mark - MAMapViewDelegate

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.overlays = [NSMutableArray array];
    /* Polyline. */
    int number = (int)self.tagList.count;
    CLLocationCoordinate2D polylineCoords[number];

    for (int i = 0; i < number; i++) {
        TagModel *tag = self.tagList[i];
        polylineCoords[i].longitude = tag.mapx.doubleValue;
        polylineCoords[i].latitude = tag.mapy.doubleValue;
    }

    MAPolyline *polyline = [MAPolyline polylineWithCoordinates:polylineCoords count:number];
    [self.overlays insertObject:polyline atIndex:OverlayViewControllerOverlayTypePolyline];
    
    
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height-100)];
    self.mapView.delegate = self;
    [self.mapView setZoomLevel:12 animated:YES];
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(self.tagModel.mapy.doubleValue,self.tagModel.mapx.doubleValue);
    [self.view addSubview:self.mapView];

}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, Main_Screen_Height/2, Main_Screen_Width, 300)];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.hidden = YES;
    self.datePicker.backgroundColor = [UIColor whiteColor];
    [window addSubview:self.datePicker];
    
    self.startButton.selected = NO;
    self.endButton.selected = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.datePicker removeFromSuperview];
}

- (IBAction)clickStart:(UIButton *)sender {
    
    if (self.startButton.selected == NO) {
        self.datePicker.hidden = NO;
        
        self.startButton.selected = YES;
    } else if(self.startButton.selected == YES){
        self.datePicker.hidden = YES;
        self.startButton.selected = NO;
        // 获取用户通过UIDatePicker设置的日期和时间
        NSDate *selected = [self.datePicker date];
        // 创建一个日期格式器
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // 为日期格式器设置格式字符串
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        // 使用日期格式器格式化日期、时间
        NSString *destDateString = [dateFormatter stringFromDate:selected];
        NSString *message =  [NSString stringWithFormat:
                              @"%@", destDateString];
        [self.startButton setTitle:message forState:UIControlStateNormal];
    }
}

- (IBAction)clickEnd:(UIButton *)sender {
    
    if (self.endButton.selected == NO) {
        self.datePicker.hidden = NO;
        self.endButton.selected = YES;
    } else if(self.endButton.selected == YES){
        self.datePicker.hidden = YES;
        self.endButton.selected = NO;
        // 获取用户通过UIDatePicker设置的日期和时间
        NSDate *selected = [self.datePicker date];
        // 创建一个日期格式器
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // 为日期格式器设置格式字符串
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        // 使用日期格式器格式化日期、时间
        NSString *destDateString = [dateFormatter stringFromDate:selected];
        NSString *message =  [NSString stringWithFormat:
                              @"%@", destDateString];
        [self.endButton setTitle:message forState:UIControlStateNormal];
    }
 
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.title = @"标签轨迹";
    self.name.text = self.tagModel.location;
    self.devid.text = self.tagModel.devid;
    
    [self.updateButton addTarget:self action:@selector(updateButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    // 添加大头针
    for (TagModel *theVideoInfo in self.tagList) {
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        pointAnnotation.coordinate = CLLocationCoordinate2DMake(theVideoInfo.mapy.doubleValue, theVideoInfo.mapx.doubleValue);
        pointAnnotation.title = theVideoInfo.location;
        pointAnnotation.subtitle = theVideoInfo.devid;
        
        [self.mapView addAnnotation:pointAnnotation];
    }
    
    [self.mapView addOverlays:self.overlays];
}

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay
{
    if ([overlay isKindOfClass:[MAPolyline class]])
    {
        MAPolylineRenderer *polylineRenderer = [[MAPolylineRenderer alloc] initWithPolyline:overlay];
        
        polylineRenderer.lineWidth   = 4.f;
        polylineRenderer.strokeColor = [UIColor orangeColor];
        
        return polylineRenderer;
    }
    
    return nil;
}

#pragma mark - Initialization

- (void)initOverlays
{
    self.overlays = [NSMutableArray array];
    /* Polyline. */
    int number = (int)self.tagList.count;
    CLLocationCoordinate2D polylineCoords[number];
    for (int i = 0; i < number; i++) {
        
        for (TagModel *tag in self.tagList) {
            polylineCoords[i].longitude = tag.mapy.doubleValue;
            polylineCoords[i].latitude = tag.mapx.doubleValue;
        }
        
    }

    MAPolyline *polyline = [MAPolyline polylineWithCoordinates:polylineCoords count:number];
    [self.overlays insertObject:polyline atIndex:OverlayViewControllerOverlayTypePolyline];
}


#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
        //设置气泡的图片
        for (TagModel *tag in self.tagList) {
            if (annotation.coordinate.longitude == tag.mapx.doubleValue && annotation.coordinate.latitude == tag.mapy.doubleValue) {
               annotationView.imageURL = @"http://pic17.nipic.com/20111118/8057563_095321178315_2.jpg";
            }
        }
        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        //将大头针的show关闭，开启气泡的自动显示
        [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.5f];
        
        return annotationView;
    }
    return nil;
}

- (void)updateButtonClick{

    //设置时间显示格式:
    NSString* timeStr1 = self.startButton.titleLabel.text;
    NSString* timeStr2 = self.endButton.titleLabel.text;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
   
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    //将字符串按formatter转成nsdate
    NSDate* date1 = [formatter dateFromString:timeStr1]; //------------将字符串按formatter转成nsdate
    NSDate* date2 = [formatter dateFromString:timeStr2];
    //时间转时间戳的方法:
    NSString *timeSp1 = [NSString stringWithFormat:@"%ld", (long)[date1 timeIntervalSince1970]];
    NSString *timeSp2 = [NSString stringWithFormat:@"%ld", (long)[date2 timeIntervalSince1970]];
    
    TagModel *tagModel = [TagModel tagWithStart:@"0" limit:@""];
    tagModel.devid = self.tagModel.devid;
    tagModel.starttime = [NSString stringWithFormat:@"%@000",timeSp1];
    tagModel.endtime = [NSString stringWithFormat:@"%@000",timeSp2];
    
    [[NetworkService sharedInstance] getTrackWithMytag:tagModel success:^(NSArray *myTagList) {
     
        NSLog(@"%@ %f %f", self.tagModel.devid, self.tagModel.mapx.doubleValue, self.tagModel.mapy.doubleValue);
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

@end
