//
//  MyTagMapViewController.m
//  iPolice
//
//  Created by xwy on 15/9/22.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MyTagMapViewController.h"
#import <MAMapKit.h>
#import "TagModel.h"
#import "CustomAnnotationView.h"
#import "TagTrackViewController.h"
#import "MyPointAnnotation.h"

@interface MyTagMapViewController ()<MAMapViewDelegate>
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) NSMutableArray *tagArray;
@end

@implementation MyTagMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"标签位置";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:12 animated:YES];
    self.view =self.mapView;
    
    //初始化
    self.tagArray = [[NSMutableArray alloc] init];
    
    TagModel *tag = [TagModel tag];
    for (int i=0;i<self.myTag.count;i++) {
        [self.tagArray addObject:[NSString stringWithFormat:@"%@", [self.myTag[i] devid]]];
    }
    tag.devids = self.tagArray;
    [[NetworkService sharedInstance] getLocationWithMytag:tag success:^(NSArray *myTagList) {
        
        // 添加大头针
        for (TagModel *myTag in self.myTag) {
            
            MyPointAnnotation *pointAnnotation = [[MyPointAnnotation alloc] init];
            pointAnnotation.coordinate = CLLocationCoordinate2DMake(myTag.mapy.doubleValue,myTag.mapx.doubleValue);
            pointAnnotation.title = myTag.location;
            pointAnnotation.subtitle = myTag.devid;
            pointAnnotation.model = myTag;
            [self.mapView addAnnotation:pointAnnotation];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
 
}

#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MyPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
        annotationView.imageURL = @"http://pic17.nipic.com/20111118/8057563_095321178315_2.jpg";
        
        MyPointAnnotation *myanno = (MyPointAnnotation *)annotation;
        [annotationView setTagModel:myanno.model];
        [annotationView setSelected:YES animated:YES];
        annotationView.goblock = ^(TagModel *model) {
            NSLog(@"%@",model.devid);
            TagTrackViewController *track = [[TagTrackViewController alloc] init];
            track.tagModel = model;
            track.tagList = self.myTag;
            
            [self.navigationController pushViewController:track animated:YES];
        };

        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        //将大头针的show关闭，开启气泡的自动显示
        [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.5f];
        
        return annotationView;
    }
    return nil;
}

@end
