//
//  EditTagViewController.h
//  iPolice
//
//  Created by xwy on 15/10/12.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TagModel;

@interface EditTagViewController : UIViewController

@property (nonatomic, strong) TagModel *tagModel;

@end
