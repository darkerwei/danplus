//
//  MyTagMapViewController.h
//  iPolice
//
//  Created by xwy on 15/9/22.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TagModel;

@interface MyTagMapViewController : UIViewController

@property (nonatomic, strong) NSArray *myTag;

@end
