//
//  CellAddressViewController.h
//  iPolice
//
//  Created by xwy on 15/10/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TagModel;

@interface CellAddressViewController : UIViewController

@property (nonatomic, strong) NSArray *videoInfoList;

@property (nonatomic, strong) TagModel *myLocation;

@end
