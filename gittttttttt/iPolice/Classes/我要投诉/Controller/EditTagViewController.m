//
//  EditTagViewController.m
//  iPolice
//
//  Created by xwy on 15/10/12.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "EditTagViewController.h"
#import "CommonGroup.h"
#import "TagTableViewCell.h"
#import "TagInfo.h"
#import "NetworkService.h"
#import "TagModel.h"

// Footer的高度
#define FooterHeight 0
// Header的高度
#define HeaderHeight 5

// cell信息
#define TagInfo1Title @"标签ID"
#define TagInfo1Placeholder @"请输入标签ID"
#define TagInfo2Title @"标签名称"
#define TagInfo2Placeholder @"请输入标签名称"
#define TagInfo3Title @"标签描述"
#define TagInfo3Placeholder @"请对标签进行描述"
#define TagInfo4Title @"标签图标"
#define TagInfo4Placeholder @"请输入标签图标"

@interface EditTagViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *groupList;
@property (nonatomic, strong) UITextField *textF;

@end

@implementation EditTagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //右导航
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"提交" style:UIBarButtonItemStylePlain target:self action:@selector(clickDown)];
    self.navigationItem.rightBarButtonItem = right;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // 设置tableView属性
    self.tableView.sectionFooterHeight = FooterHeight;
    self.tableView.sectionHeaderHeight = HeaderHeight;
    
    // 调整cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(-24, 0, 0, 0);
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height)];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
    
    // 边框
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

//提交
- (void)clickDown{
    
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i<4; i++) {
        NSIndexPath *indexpath =[NSIndexPath indexPathForItem:0 inSection:i];
        TagTableViewCell *cell =(TagTableViewCell *)[self.tableView cellForRowAtIndexPath:indexpath];
        self.textF= (UITextField *)[cell viewWithTag:100];
        [array addObject:self.textF.text];
    }
    
    TagModel *tagModel = [TagModel tagWithDevid:self.tagModel.devid name:array[1] desc:array[2] model:@"model" oem:@"oem" icon:array[3]];
    [SVProgressHUD showInfoWithStatus:@"正在更新标签"];
    
    [[NetworkService sharedInstance] updateDeviceWithDevice:tagModel success:^{
        [SVProgressHUD showSuccessWithStatus:@"添加标签成功"];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];

     [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.groupList.count;;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CommonGroup *theCommonGroup = self.groupList[section];
    
    return theCommonGroup.commonItemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TagTableViewCell *cell = [TagTableViewCell cellWithTableView:tableView];
    CommonGroup *theCommonGroup = self.groupList[indexPath.section];
    cell.myTagInfo = theCommonGroup.commonItemList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0 && indexPath.row == 0) {
        cell.userInteractionEnabled = NO;
    }
    //必填信息
    if (indexPath.section == 0 || indexPath.section == 1) {
        cell.imageView.image = [UIImage imageNamed:@"login_important"];
    }
    if (indexPath.section == 3 && indexPath.row == 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

//点击return 按钮 去掉
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textF resignFirstResponder];
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"在这里处理<第%ld个Cell>点击事件", indexPath.row);
    if (indexPath.section == 3) {
        
        NSLog(@"在这处理换头像");
        
    }
}


#pragma mark - 懒加载
- (NSArray *)groupList
{
    if (_groupList == nil) {
        
        // 创建组
        CommonGroup *commonGroup1 = [CommonGroup commonGroup];
        CommonGroup *commonGroup2 = [CommonGroup commonGroup];
        CommonGroup *commonGroup3 = [CommonGroup commonGroup];
        CommonGroup *commonGroup4 = [CommonGroup commonGroup];
        
        // 创建cell信息
        TagInfo *tagInfo1 = [TagInfo tagInfoWithTagInfoTitle:TagInfo1Title TagInfoPlaceholder:self.tagModel.devid  TagInfoText:self.tagModel.devid TagInfoImage:nil];
        commonGroup1.commonItemList = @[tagInfo1];
        
        TagInfo *tagInfo2 = [TagInfo tagInfoWithTagInfoTitle:TagInfo2Title TagInfoPlaceholder:self.tagModel.desc  TagInfoText:self.tagModel.desc TagInfoImage:nil];
        commonGroup2.commonItemList = @[tagInfo2];
        
        TagInfo *tagInfo3 = [TagInfo tagInfoWithTagInfoTitle:TagInfo3Title TagInfoPlaceholder:self.tagModel.name  TagInfoText:self.tagModel.name TagInfoImage:nil];
        commonGroup3.commonItemList = @[tagInfo3];
        
        TagInfo *tagInfo4 = [TagInfo tagInfoWithTagInfoTitle:TagInfo4Title TagInfoPlaceholder:TagInfo4Placeholder TagInfoText:nil TagInfoImage:[UIImage imageNamed:@"login_sign.png"]];
        commonGroup4.commonItemList = @[tagInfo4];
        
        
        _groupList = @[commonGroup1, commonGroup2, commonGroup3, commonGroup4];
    }
    return _groupList;
}

@end
