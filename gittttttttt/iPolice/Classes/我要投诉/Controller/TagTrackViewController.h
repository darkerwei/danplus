//
//  TagTrackViewController.h
//  iPolice
//
//  Created by xwy on 15/10/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TagModel;
@interface TagTrackViewController : UIViewController

@property (nonatomic, strong) TagModel *tagModel;
@property (nonatomic, strong) NSArray *tagList;
@end
