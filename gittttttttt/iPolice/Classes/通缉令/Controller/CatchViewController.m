//
//  CatchViewController.m
//  iPolice
//
//  Created by xwy on 15/9/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CatchViewController.h"
#import "InformViewController.h"
#import "Threads.h"

@interface CatchViewController ()
@property (weak, nonatomic) IBOutlet UIButton *clueButton;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UILabel *subject;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIImageView *personImage;
@property (weak, nonatomic) IBOutlet UIWebView *message;



@end

@implementation CatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"通缉令";
    
    [self.clueButton addTarget:self action:@selector(clickClue) forControlEvents:UIControlEventTouchUpInside];
    [self.callButton addTarget:self action:@selector(click110) forControlEvents:UIControlEventTouchUpInside];
    //标题
    self.subject.text = [NSString stringWithFormat:@"%@", self.threads.subject];
    //时间
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.threads.dateline.doubleValue/1000.0]];
    self.time.text = [NSString stringWithFormat:@"稿源时间:%@", regStr];
    
    [self getInformation];
    
}

- (void)getInformation{
    
    Threads *threads = [Threads threads];
    threads.tid = self.threads.tid;
    threads.cid = self.threads.cid;
    [SVProgressHUD showWithStatus:@"正在获取信息"];
    [[NetworkService sharedInstance] getThreadDetailWithThreads:threads success:^(Threads *threads) {
        [SVProgressHUD showSuccessWithStatus:@"信息获取成功"];
        //图片
        [self.personImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[threads.attachments firstObject] thumbnail]]] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
        //介绍
        [self.message loadHTMLString:threads.message baseURL:nil];
        self.message.scrollView.bounces = NO;
        self.message.scrollView.showsVerticalScrollIndicator = NO;
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (void)clickClue{
    
    InformViewController *info = [[InformViewController alloc] init];
    [self.navigationController pushViewController:info animated:YES];
}

- (void)click110{
    
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:@"tel:110"];// 貌似tel:// 或者 tel: 都行
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    [self.view addSubview:callWebview];
}

@end
