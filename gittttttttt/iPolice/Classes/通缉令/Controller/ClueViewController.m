//
//  ClueViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ClueViewController.h"
#import "WantedTableViewCell.h"
#import "Wanted.h"
#import "Threads.h"
#import "InformViewController.h"
#import "NetworkService.h"
#import "CatchViewController.h"

@interface ClueViewController ()<UITableViewDataSource, UITableViewDelegate, WantedTableViewCellDelegate>

/**
 *  我有线索页面
 */
@property (weak, nonatomic) IBOutlet UITableView *clueTableView;
/**
 *  通缉令列表
 */
@property (nonatomic, strong) NSArray *wantedsList;
@property (nonatomic, assign) int limit;

@end

@implementation ClueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"网上通缉";
    
    // 调整Cell位置
    self.clueTableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    
    self.clueTableView.tableFooterView = [[UIView alloc] init];
    
//    [self getInformation];
    [self refresh];
    [self upRefresh];
    
    //点击举报
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reportBtnClick:) name:@"mvc" object:nil];
}


#pragma mark - WantedTableViewCellDelegate
/**
 *  举报按钮点击
 */
- (void)reportBtnClick:(NSNotification *)notification
{
    NSLog(@"在这里处理<举报>点击事件");
    NSDictionary *dic = [notification userInfo];
    UIButton*btn = (UIButton *)[dic objectForKey:@"Field"];
    InformViewController *info = [[InformViewController alloc] init];
    [self.navigationController pushViewController:info animated:YES];
}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.clueTableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.clueTableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    Threads *theThreads = [Threads threadsWithCid:@"channel_wanted" status:@"" style:@"" type:@"" start:@"" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [SVProgressHUD showWithStatus:@"正在获取信息"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        [SVProgressHUD showSuccessWithStatus:@"列表获取成功"];
        
        // 视频满10个才显示上拉刷新
        if (threadList.count == self.limit) {
            self.clueTableView.footer.hidden = NO;
        } else if (threadList.count < self.limit) {
            self.clueTableView.footer.hidden = YES;
        }
        
        self.wantedsList = threadList;
        [self.clueTableView reloadData];
        [self.clueTableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.clueTableView.header endRefreshing];
    }];
 
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.clueTableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.clueTableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    Threads *theThreads = [Threads threadsWithCid:@"channel_wanted" status:@"" style:@"" type:@"" start:@"" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [SVProgressHUD showWithStatus:@"正在获取信息"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        [SVProgressHUD showSuccessWithStatus:@"列表获取成功"];
        
        // 视频满10个才显示上拉刷新
        if (threadList.count == self.limit) {
            self.clueTableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取列表成功"];
        } else if (threadList.count < self.limit-10) {
            self.clueTableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的内容了"];
        }
        
        self.wantedsList = threadList;
        [self.clueTableView reloadData];
        [self.clueTableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.clueTableView.header endRefreshing];
    }];
    
}

- (void)getInformation
{
    Threads *theThreads = [Threads threadsWithCid:@"channel_wanted" status:@"" style:@"" type:@"" start:@"" limit:@"" hasImage:@"1" hasDetail:@"0"];
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
       
        self.wantedsList = threadList;
        [self.clueTableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.wantedsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WantedTableViewCell *cell = [WantedTableViewCell cellWithTableView:tableView];
    cell.threads = self.wantedsList[indexPath.row];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"在这里处理<第%d个Cell>点击事件", indexPath.row);
    CatchViewController *catch = [[CatchViewController alloc] init];
    catch.threads = self.wantedsList[indexPath.row];
    [self.navigationController pushViewController:catch animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

@end
