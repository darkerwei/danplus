//
//  CatchViewController.h
//  iPolice
//
//  Created by xwy on 15/9/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Threads;

@interface CatchViewController : UIViewController

@property (nonatomic, strong) Threads *threads;
@end
