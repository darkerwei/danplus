//
//  MyTagViewController.m
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MyTagViewController.h"
#import "TagModel.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import "MyTagTableViewCell.h"
#import "AddTagViewController.h"
#import "MyTagMapViewController.h"
#import "CellAddressViewController.h"
#import "EditTagViewController.h"
#import "TagTrackViewController.h"

@interface MyTagViewController ()<UITableViewDataSource, UITableViewDelegate, MyTagTableViewCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *myTagList;
@property (nonatomic, strong) NSArray *tagList;
@property (nonatomic, strong) NSMutableArray *tagArray;

/**
 *  它将存储当前已被打开的 Cell 的列表
 */
@property (nonatomic, strong) NSMutableSet *cellsCurrentlyEditing;

@end

@implementation MyTagViewController

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    // 下拉刷新
    [self refresh];
    // 上拉刷新
//    [self upRefresh];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.cellsCurrentlyEditing = [NSMutableSet new];
    
    self.title = @"我的标签";
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height-40-64)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    //右导航
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(clickMyTagRight)];
    self.navigationItem.rightBarButtonItem = right;
    right.image = [[UIImage imageNamed:@"video_map.jpg"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // 调整Cell位置
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    //添加标签
    UIButton *addTag = [[UIButton alloc] initWithFrame:CGRectMake(20, Main_Screen_Height-64-60, Main_Screen_Width-40, 50)];
    [addTag setImage:[UIImage imageNamed:@"添加标签背景框按钮－原始"] forState:UIControlStateNormal];
    [addTag setImage:[UIImage imageNamed:@"添加标签背景框按钮－点击"] forState:UIControlStateHighlighted];
    addTag.backgroundColor = [UIColor grayColor];
    [addTag setTitle:@"添加标签" forState:UIControlStateNormal];
    addTag.layer.cornerRadius = 5;
    addTag.clipsToBounds = YES;
    [addTag addTarget:self action:@selector(clcikAddTag) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addTag];

}


//添加标签
- (void)clcikAddTag{
    
    AddTagViewController *addTag = [[AddTagViewController alloc] init];
    addTag.title = @"添加标签";
    [self.navigationController pushViewController:addTag animated:NO];
    
}

//标签地址
- (void)tagAddressClick:(UIButton *)btn{
    NSLog(@"%d", btn.tag-20);
    CellAddressViewController *mapVC = [[CellAddressViewController alloc] init];
    mapVC.videoInfoList = self.tagList;
    mapVC.myLocation = self.tagList[btn.tag-20];
    [self.navigationController pushViewController:mapVC animated:YES];
}

//右导航
- (void)clickMyTagRight{
    
    NSLog(@"在这里处理<我的标签右导航>");
    MyTagMapViewController *myTag = [[MyTagMapViewController alloc] init];
    myTag.myTag = self.tagList;
    [self.navigationController pushViewController:myTag animated:YES];

}



- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.cellsCurrentlyEditing removeAllObjects];
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
    
}

- (void)loadNewData
{
    TagModel *model = [TagModel tagWithStart:@"0" limit:@"10"];
    [SVProgressHUD showInfoWithStatus:@"正在获取信息"];
    [[NetworkService sharedInstance] getDevicesWithMytag:model success:^(NSArray *myTagList) {
        [SVProgressHUD showSuccessWithStatus:@"获取信息成功"];
     
        //满10个才显示上拉刷新
        if (myTagList.count == 10) {
            self.tableView.footer.hidden = NO;
        } else if (myTagList.count < 10) {
            self.tableView.footer.hidden = YES;
        }
        
        self.myTagList = myTagList;
        [self.tableView reloadData];
        
        //初始化
        self.tagArray = [[NSMutableArray alloc] init];
        TagModel *tag = [TagModel tag];
        for (int i=0;i<self.myTagList.count;i++) {
            [self.tagArray addObject:[NSString stringWithFormat:@"%@", [self.myTagList[i] devid]]];
        }
        tag.devids = self.tagArray;
        [[NetworkService sharedInstance] getLocationWithMytag:tag success:^(NSArray *myTagList) {
            
            self.tagList = myTagList;
            
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        }];
        
        [self.tableView.header endRefreshing];
    } failure:^(NSError *error) {
        
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
    
}

- (void)upRefresh
{
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self.cellsCurrentlyEditing removeAllObjects];
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
    
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    TagModel *model = [TagModel tagWithStart:@"0" limit:@""];
    [SVProgressHUD showInfoWithStatus:@"正在获取信息"];
    [[NetworkService sharedInstance] getDevicesWithMytag:model success:^(NSArray *myTagList) {
        [SVProgressHUD showSuccessWithStatus:@"获取信息成功"];
        
        // 没有更多视频时隐藏上拉刷新
        if (myTagList.count == 10) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取信息成功"];
        } else if (myTagList.count < 10) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多了"];
        }
        
        self.myTagList = myTagList;
        [self.tableView reloadData];
        
        //初始化
        self.tagArray = [[NSMutableArray alloc] init];
        
        TagModel *tag = [TagModel tag];
        for (int i=0;i<self.myTagList.count;i++) {
            [self.tagArray addObject:[NSString stringWithFormat:@"%@", [self.myTagList[i] devid]]];
        }
        tag.devids = self.tagArray;
        [[NetworkService sharedInstance] getLocationWithMytag:tag success:^(NSArray *myTagList) {
            
            self.tagList = myTagList;
            
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        }];
        
        [self.tableView.footer endRefreshing];
    } failure:^(NSError *error) {
        
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
    }];
    
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.myTagList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTagTableViewCell *cell = [MyTagTableViewCell cellWithTableView:tableView];
    cell.myTag = self.myTagList[indexPath.row];
    cell.delegate = self;
    cell.tagAddressButton.tag = indexPath.row+20;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([self.cellsCurrentlyEditing containsObject:indexPath]) {
        [cell openCell];
    }
    
    return cell;
}

- (void)cellDidOpen:(UITableViewCell *)cell {
    NSIndexPath *lastIndexPath = nil;
    lastIndexPath = [self.cellsCurrentlyEditing anyObject];
    MyTagTableViewCell *myCell = [self.tableView cellForRowAtIndexPath:lastIndexPath];
    if (self.cellsCurrentlyEditing.count != 0) {
        [self.cellsCurrentlyEditing removeObject:lastIndexPath];
        [myCell closeCell];
    }
    NSIndexPath *currentEditingIndexPath = [self.tableView indexPathForCell:cell];
    [self.cellsCurrentlyEditing addObject:currentEditingIndexPath];
}

- (void)cellDidClose:(UITableViewCell *)cell {
    [self.cellsCurrentlyEditing removeObject:[self.tableView indexPathForCell:cell]];
}

- (void)deleteCell:(UITableViewCell *)cell {
    [self.cellsCurrentlyEditing removeObject:[self.tableView indexPathForCell:cell]];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    TagModel *tag = [TagModel tag];
    tag.devid = [self.myTagList[indexPath.row] devid];
    [[NetworkService sharedInstance] delDeviceWithDevice:tag success:^{
        [SVProgressHUD showSuccessWithStatus:@"删除标签成功"];
        [self refresh];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    NSLog(@"删除%ld %ld",(long)indexPath.section, (long)indexPath.row);
}
- (void)editCell:(UITableViewCell *)cell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSLog(@"编辑%ld %ld",(long)indexPath.section, (long)indexPath.row);
    EditTagViewController *edit = [[EditTagViewController alloc] init];
    edit.title = @"更新标签";
    edit.tagModel = self.myTagList[indexPath.row];
    [self.navigationController pushViewController:edit animated:YES];
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"在这里处理<第%ld个Cell>点击事件", indexPath.row);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

@end
