//
//  AddTagViewController.h
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddTagViewController : UIViewController

@property (nonatomic, strong) NSString *addTag;

@end
