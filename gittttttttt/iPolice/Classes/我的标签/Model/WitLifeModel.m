//
//  WitLifeModel.m
//  iPolice
//
//  Created by xwy on 15/7/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "WitLifeModel.h"

@implementation WitLifeModel

- (instancetype)initWithDic:(NSDictionary *)dic
{
    if ([super init]) {
        
        [self setValuesForKeysWithDictionary:dic];
    }
    
    return self;
}

+ (instancetype)notificationWithDic:(NSDictionary *)dic
{
    return [[WitLifeModel alloc] initWithDic:dic];
}

#pragma mark - 防止崩溃
- (id)valueForUndefinedKey:(NSString *)key
{
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    NSLog(@"%@", key);
}


@end
