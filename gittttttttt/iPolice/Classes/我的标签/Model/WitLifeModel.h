//
//  WitLifeModel.h
//  iPolice
//
//  Created by xwy on 15/7/24.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WitLifeModel : NSObject

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *address;


- (instancetype)initWithDic:(NSDictionary *)dic;

+ (instancetype)notificationWithDic:(NSDictionary *)dic;


@end
