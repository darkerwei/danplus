//
//  TagInfo.m
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TagInfo.h"

@implementation TagInfo

+ (instancetype)tagInfoWithTagInfoTitle:(NSString *)tagInfoTitle TagInfoPlaceholder:(NSString *)tagInfoPlaceholder TagInfoText:(NSString *)tagInfoText TagInfoImage:(UIImage *)tagInfoImage{
    
    TagInfo *tagInfo = [[self alloc] init];
    
    tagInfo.tagInfoTitle = tagInfoTitle;
    tagInfo.tagInfoPlaceholder = tagInfoPlaceholder;
    tagInfo.tagInfoText = tagInfoText;
    tagInfo.tagInfoImage = tagInfoImage;
    
    return tagInfo;
}
@end
