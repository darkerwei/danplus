//
//  TagInfo.h
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagInfo : NSObject


@property (nonatomic, copy) NSString *tagInfoTitle;

@property (nonatomic, copy) NSString *tagInfoPlaceholder;

@property (nonatomic, copy) NSString *tagInfoText;

@property (nonatomic, strong) UIImage *tagInfoImage;

+ (instancetype)tagInfoWithTagInfoTitle:(NSString *)tagInfoTitle TagInfoPlaceholder:(NSString *)tagInfoPlaceholder TagInfoText:(NSString *)tagInfoText TagInfoImage:(UIImage *)tagInfoImage;

@end
