//
//  MyTagTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TagModel;

@protocol MyTagTableViewCellDelegate<NSObject>

- (void)tagAddressClick:(UIButton *)sender;

- (void)cellDidOpen:(UITableViewCell *)cell;
- (void)cellDidClose:(UITableViewCell *)cell;

- (void)deleteCell:(UITableViewCell *)cell;
- (void)editCell:(UITableViewCell *)cell;

@end

@interface MyTagTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *myContentView;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (nonatomic, strong) TagModel *myTag;

@property(nonatomic, assign) id<MyTagTableViewCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIButton *tagAddressButton;

@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, assign) CGPoint panStartPoint;
@property (nonatomic, assign) CGFloat startingRightLayoutConstraintConstant;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentViewRightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentViewLeftConstraint;

/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)openCell;
- (void)closeCell;

@end
