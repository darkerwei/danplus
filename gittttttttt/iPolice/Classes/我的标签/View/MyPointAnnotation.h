//
//  MyPointAnnotation.h
//  iPolice
//
//  Created by xwy on 15/10/14.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "TagModel.h"
@interface MyPointAnnotation : MAPointAnnotation

@property (nonatomic,strong) TagModel *model;

@end
