//
//  TagTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TagTableViewCell.h"
#import "TagInfo.h"
#import "AddTagViewController.h"

#define TagTableViewCellIdentifier @"TagTableViewCell"

@interface TagTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *tagInfoTitle;
@property (weak, nonatomic) IBOutlet UITextField *tagInfoText;
@property (weak, nonatomic) IBOutlet UIImageView *tagInfoImage;

@end

@implementation TagTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setMyTagInfo:(TagInfo *)myTagInfo{
    
    _myTagInfo = myTagInfo;
    
    self.tagInfoTitle.text = myTagInfo.tagInfoTitle;
    self.tagInfoText.placeholder = myTagInfo.tagInfoPlaceholder;

    AddTagViewController *addTag = [[AddTagViewController alloc] init];
    addTag.addTag = self.tagInfoText.text;
    
    if ([self.tagInfoTitle.text isEqualToString:@"标签图标"]) {
        self.tagInfoText.userInteractionEnabled = NO;
    }
    self.tagInfoImage.image = myTagInfo.tagInfoImage;

}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:TagTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:TagTableViewCellIdentifier];
    TagTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TagTableViewCellIdentifier];
    
    return cell;
}
@end
