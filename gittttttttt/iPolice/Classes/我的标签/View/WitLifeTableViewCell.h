//
//  WitLifeTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/7/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WitLifeModel;

@interface WitLifeTableViewCell : UITableViewCell

@property (nonatomic, strong) WitLifeModel *notiModel;

- (instancetype)initWithTableView:(UITableView *)tableView;



@end
