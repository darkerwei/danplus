//
//  TagTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/8/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TagInfo;

@interface TagTableViewCell : UITableViewCell

@property (nonatomic, strong) TagInfo *myTagInfo;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
