//
//  WitLifeTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/7/23.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "WitLifeTableViewCell.h"
#import "WitLifeModel.h"
#import <Masonry.h>

@interface WitLifeTableViewCell()

// 标题
@property (nonatomic, strong) UILabel *titleLabel;

//地址按钮
@property (nonatomic, strong) UIButton *addressButton;


@end


static NSInteger tagValue = 101;

@implementation WitLifeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
    
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.addressButton];
    
    [self setContrain];
    
    
}


// 动态布局
- (void)setContrain
{
    // 分割线
    UIView *segment = [[UIView alloc] init];
    segment.backgroundColor = UIColorFromRGB(0xf3f3f4);
    [self.contentView addSubview:segment];
    
    [segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(0);
        make.right.equalTo(self.contentView).with.offset(0);
        make.bottom.equalTo(self.contentView).with.offset(0);
        make.size.mas_equalTo(CGSizeMake([UIScreen mainScreen].bounds.size.width, 1));
    }];
    
    // 标题的label
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.contentView).with.offset(20);
        make.width.mas_equalTo([UIScreen mainScreen].bounds.size.width / 2 - 15);
        make.bottom.equalTo(self.contentView).with.offset(5);
        make.top.equalTo(self.contentView).with.offset(15);
    }];
    
    //地址按钮
    [self.addressButton mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.left.equalTo(self.contentView).with.offset(300);
        make.width.mas_equalTo(30);
        make.bottom.equalTo(self.contentView).with.offset(5);
        make.top.equalTo(self.contentView).with.offset(15);
    }];
}

#pragma mark - 懒加载
- (UILabel *)titleLabel
{
    if (_titleLabel == nil) {
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        _titleLabel.textColor = UIColorFromRGB(0x353d3f);
        
    }
    
    return _titleLabel;
}

- (UILabel *)addressButton
{
    if (_addressButton == nil) {
        
        _addressButton = [[UIButton alloc] init];
        [_addressButton setImage:[UIImage imageNamed:@"曝光台.png"] forState:UIControlStateNormal];
        _addressButton.titleLabel.numberOfLines = 1;
        [_addressButton addTarget:self action:@selector(addressButtonClick) forControlEvents:UIControlEventTouchUpInside];
        _addressButton.titleLabel.font = [UIFont systemFontOfSize:13];
        
    }
    
    return _addressButton;
    
}

- (void)addressButtonClick{
    
    NSLog(@"在这里处理<地址点击>");
}

#pragma mark - 私有方法
- (void)setNotiModel:(WitLifeModel *)notiModel
{
    _notiModel = notiModel;
    self.titleLabel.text = notiModel.title;
    self.addressButton.titleLabel.text = notiModel.address;
    
}

+ (NSString *)cellIdenx
{
    return @"WitLifeTableViewCell";
}

- (instancetype)initWithTableView:(UITableView *)tableView
{
    if ([super init]) {
        
        self = [tableView dequeueReusableCellWithIdentifier:[WitLifeTableViewCell cellIdenx]];
        
        if (self == nil) {
            
            self = [[WitLifeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[WitLifeTableViewCell cellIdenx]];
        }
    }
    
    return self;
}

-(void)editing:(BOOL)editing animated:(BOOL)animated{
    if (editing) {
        
        if (self.editingStyle == (UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert)) {
            
            if (![self viewWithTag:tagValue]) {
                
                
            }
        }
        
    } else {
        
        UIView *editDotView = [self viewWithTag:tagValue];
        if (editDotView) {
            
            [editDotView removeFromSuperview];
        }
        
        
    }
    
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self editing:editing animated:animated];
    
}

@end
