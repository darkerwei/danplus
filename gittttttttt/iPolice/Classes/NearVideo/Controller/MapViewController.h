//
//  MapViewController.h
//  iPolice
//
//  Created by ioswei on 15/7/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoInfo;

@interface MapViewController : UIViewController

/**
 *  视频列表
 */
@property (nonatomic, strong) NSArray *videoInfoList;

@end
