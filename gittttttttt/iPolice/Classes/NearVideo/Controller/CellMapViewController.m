//
//  CellMapViewController.m
//  iPolice
//
//  Created by xwy on 15/9/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CellMapViewController.h"
#import <MAMapKit.h>
#import "VideoInfo.h"
#import "CustomAnnotationView.h"

@interface CellMapViewController ()<MAMapViewDelegate>

@property (nonatomic, strong) MAMapView *mapView;

@end

@implementation CellMapViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.title = @"视频地图";
 
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    [self.mapView setZoomLevel:15 animated:YES];
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(self.myLocation.mapy.doubleValue,self.myLocation.mapx.doubleValue);                                                                                                              
    
    [self.view addSubview:self.mapView];
    
    // 添加大头针
    for (VideoInfo *theVideoInfo in self.videoInfoList) {
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        pointAnnotation.coordinate = CLLocationCoordinate2DMake(theVideoInfo.mapy.doubleValue, theVideoInfo.mapx.doubleValue);
        pointAnnotation.title = theVideoInfo.name;
        pointAnnotation.subtitle = theVideoInfo.location;
        
        [self.mapView addAnnotation:pointAnnotation];
    }
}
#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
        
        //设置气泡的图片
        for (VideoInfo *theVideoInfo in self.videoInfoList) {
            if (annotation.coordinate.longitude == theVideoInfo.mapx.doubleValue && annotation.coordinate.latitude == theVideoInfo.mapy.doubleValue) {
                annotationView.imageURL = theVideoInfo.thumbnail;
            }
        }
        
        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        //将大头针的show关闭，开启气泡的自动显示
        [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.5f];
        
        return annotationView;
    }
    return nil;
}

-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        //        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
    }
}



@end
