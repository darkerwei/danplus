//
//  MapViewController.m
//  iPolice
//
//  Created by ioswei on 15/7/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "MapViewController.h"
#import <MAMapKit.h>
#import "VideoInfo.h"
#import "CustomAnnotationView.h"
#import "VideoPlayerViewController.h"

@interface MapViewController ()<MAMapViewDelegate>

/**
 *  地图
 */
@property (nonatomic, strong) MAMapView *mapView;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"视频地图";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:12 animated:YES];
    
    [self.view addSubview:self.mapView];
    
    // 添加大头针
    for (VideoInfo *theVideoInfo in self.videoInfoList) {
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        pointAnnotation.coordinate = CLLocationCoordinate2DMake(theVideoInfo.mapy.doubleValue, theVideoInfo.mapx.doubleValue);
        pointAnnotation.title = theVideoInfo.name;
        pointAnnotation.subtitle = theVideoInfo.location;
        [self.mapView addAnnotation:pointAnnotation];
    }
}

#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"我的地址图标"];
        
        //设置气泡的图片
//        for (VideoInfo *theVideoInfo in self.videoInfoList) {
//            if (annotation.coordinate.longitude == theVideoInfo.mapx.doubleValue && annotation.coordinate.latitude == theVideoInfo.mapy.doubleValue) {
//                annotationView.imageURL = theVideoInfo.thumbnail;
//            }
//    }
            for (int i = 0; i < self.videoInfoList.count; i++) {
                VideoInfo *theVideoInfo = self.videoInfoList[i];
                if (annotation.coordinate.longitude == theVideoInfo.mapx.doubleValue && annotation.coordinate.latitude == theVideoInfo.mapy.doubleValue) {
                    annotationView.imageURL = [self.videoInfoList[i] thumbnail];
                    
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = annotationView.frame;
                    button.tag = i;
                    [button addTarget:self action:@selector(tapClciked:) forControlEvents:UIControlEventTouchUpInside];
                    [annotationView addSubview:button];

                }
                
            }

        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        //将大头针的show关闭，开启气泡的自动显示
        [annotationView performSelector:@selector(show) withObject:nil afterDelay:0.5f];
        
        return annotationView;
    }
    return nil;
}

- (void)tapClciked:(UIButton *)btn{
    
    VideoInfo *theVideoInfo = [VideoInfo videoInfo];
    theVideoInfo.vid = [self.videoInfoList[btn.tag] vid];
    [[NetworkService sharedInstance] getVideoDetailWithVideoInfo:theVideoInfo success:^(VideoInfo *videoInfo) {
        
        if (![theVideoInfo.uri isEqualToString:@""]) {
            
            NSLog(@"%@ %f %f", theVideoInfo.location, theVideoInfo.mapx.doubleValue, theVideoInfo.mapy.doubleValue);

            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            VideoPlayerViewController *videoPlayerVC = [storyboard instantiateViewControllerWithIdentifier:@"VideoPlayerViewController"];
            videoPlayerVC.videoURL = theVideoInfo.uri;
            videoPlayerVC.time = 18;
            videoPlayerVC.deviceType = 1;
            [self presentViewController:videoPlayerVC animated:YES completion:nil];
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"抱歉,没有视频"];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
   
}


-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
//        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
    }
}

////地址反编译
//- (void)searchReGeocodeWithCoordinate:(CLLocationCoordinate2D)coordinate
//{
//    AMapReGeocodeSearchRequest *reg = [[AMapReGeocodeSearchRequest alloc] init];
//    
//    reg.location = [AMapGeoPoint locationWithLatitude:coordinate.latitude longitude:coordinate.longitude];
//    reg.requireExtension = YES;
//    
//    NSLog(@"----reg:%@",reg);
//    
//    [_search AMapReGoecodeSearch:reg];
//}


////实现正向地理编码的回调函数
//- (void)onGeocodeSearchDone:(AMapGeocodeSearchRequest *)request response:(AMapGeocodeSearchResponse *)response
//{
//    if (response.geocodes.count == 0) {
//        return;
//    }
//    //通过AMapGeocodeSearchResponse对象处理搜索结果
//    NSString *strCount = [NSString stringWithFormat:@"count: %ld", response.count];
//    NSString *strGeocodes = @"";
//    for (AMapTip *p in response.geocodes) {
//        strGeocodes = [NSString stringWithFormat:@"%@\ngeocode: %@", strGeocodes, p.description];
//    }
//    NSString *result = [NSString stringWithFormat:@"%@ \n %@", strCount, strGeocodes];
//    NSLog(@"123 Geocode: %@", result);
//}
@end
