//
//  VideoPlayerViewController.h
//  VideoPlayerDemo
//
//  Created by darkerwei on 15/9/18.
//  Copyright © 2015年 wei. All rights reserved.
//

#import "NearVideoViewController.h"

@interface VideoPlayerViewController :UIViewController

/**
 *  视频URL
 */
@property (nonatomic, strong) NSString *videoURL;
/**
 *  视频时长
 */
@property (nonatomic, assign) NSUInteger time;
/**
 *  设备类型
 */
@property (nonatomic, assign) NSUInteger deviceType;

@end
