//
//  NearVideoViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NearVideoViewController.h"
#import "NearVideoTableViewCell.h"
#import "VideoInfo.h"
#import <SVProgressHUD.h>
#import "NetworkService.h"
#import "MapViewController.h"
#import <MJRefresh.h>
#import "VideoPlayerViewController.h"
#import "CellMapViewController.h"

// 周边视频Cell高度
#define NearVideoTableViewCellHeight 90
// 搜索框高度
#define SearchBarHeight 44

@interface NearVideoViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, NearVideoTableViewCellDelegate>

/**
 *  周边视频页面
 */
@property (weak, nonatomic) IBOutlet UITableView *nearVideoTableView;
/**
 *  视频列表
 */
@property (nonatomic, strong) NSArray *videoInfoList;
/**
 *  搜索框
 */
@property (nonatomic, strong) UISearchBar *searchBar;
/**
 *  服务器一次返回视频数量
 */
@property (nonatomic, assign) int limit;

@end

@implementation NearVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];
    
    // 去除多余cell
    self.nearVideoTableView.tableFooterView = [[UIView alloc] init];
    
    self.title = @"周边视频";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStyleDone target:self action:@selector(videoMap)];
    //防止图片渲染
    UIImage *image = [[UIImage imageNamed:@"video_map.jpg"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem.image = image;
    
    // 设置搜索框
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 64, Main_Screen_Width, SearchBarHeight)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"请输入地名";
    [self.view addSubview:self.searchBar];
    
    // 调整cell位置
    self.nearVideoTableView.contentInset = UIEdgeInsetsMake(SearchBarHeight, 0, 0, 0);
}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.nearVideoTableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.nearVideoTableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    
    VideoInfo *theVideoInfo = [VideoInfo videoInfoWithMapx:[NSString stringWithFormat:@"%f", self.mapy] mapy:[NSString stringWithFormat:@"%f", self.mapx] start:@"0" limit:@"10"];
    
    [SVProgressHUD showInfoWithStatus:@"正在获取视频列表"];
    [[NetworkService sharedInstance] getVideosWithVideoInfo:theVideoInfo success:^(NSArray *videoInfoList) {
        [SVProgressHUD showSuccessWithStatus:@"获取视频列表成功"];
        
        // 视频满10个才显示上拉刷新
        if (videoInfoList.count == self.limit) {
            self.nearVideoTableView.footer.hidden = NO;
        } else if (videoInfoList.count < self.limit) {
            self.nearVideoTableView.footer.hidden = YES;
        }
        
        self.videoInfoList = videoInfoList;
        [self.nearVideoTableView reloadData];
        [self.nearVideoTableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.nearVideoTableView.header endRefreshing];
    }];
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.nearVideoTableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.nearVideoTableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    
    VideoInfo *theVideoInfo = [VideoInfo videoInfoWithMapx:[NSString stringWithFormat:@"%f", self.mapy] mapy:[NSString stringWithFormat:@"%f", self.mapx] start:@"0" limit:@"100"];
    
    [SVProgressHUD showWithStatus:@"正在获取视频列表"];
    [[NetworkService sharedInstance] getVideosWithVideoInfo:theVideoInfo success:^(NSArray *videoInfoList) {
        [SVProgressHUD showSuccessWithStatus:@"获取视频列表成功"];

        // 没有更多视频时隐藏上拉刷新
        if (videoInfoList.count == self.limit) {
            self.nearVideoTableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取视频列表成功"];
        } else if (videoInfoList.count < self.limit - 10) {
            self.nearVideoTableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的视频了"];
        }

        self.videoInfoList = videoInfoList;
        [self.nearVideoTableView reloadData];
        [self.nearVideoTableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.nearVideoTableView.footer endRefreshing];
    }];
}

/**
 *  视频地图按钮点击
 */
- (void)videoMap
{
    NSLog(@"在这里处理<视频地图>点击事件");
    
    MapViewController *mapVC = [[MapViewController alloc] init];
    mapVC.videoInfoList = self.videoInfoList;
    [self.navigationController pushViewController:mapVC animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.videoInfoList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NearVideoTableViewCell *cell = [NearVideoTableViewCell cellWithTableView:tableView];
    cell.theVideoInfo = self.videoInfoList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    cell.playButton.tag = indexPath.row;
    cell.mapButton.tag = indexPath.row+1000;
    return cell;
}

//播放
- (void)playButtonClick:(UIButton *)btn{
    
    VideoInfo *theVideoInfo = [VideoInfo videoInfo];
    theVideoInfo.vid = [self.videoInfoList[btn.tag] vid];
    [[NetworkService sharedInstance] getVideoDetailWithVideoInfo:theVideoInfo success:^(VideoInfo *videoInfo) {
        
        if (![theVideoInfo.uri isEqualToString:@""]) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            VideoPlayerViewController *videoPlayerVC = [storyboard instantiateViewControllerWithIdentifier:@"VideoPlayerViewController"];
            //http://video.coolplay.tv/media/%E6%91%87%E6%91%86%E9%93%83.mp4
            videoPlayerVC.videoURL = theVideoInfo.uri;
            videoPlayerVC.time = 18;
            videoPlayerVC.deviceType = 1;
            [self presentViewController:videoPlayerVC animated:YES completion:nil];
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"抱歉,没有视频"];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
    
}

//cell地图
- (void)mapButtonClick:(UIButton *)btn{
    NSLog(@"在这里处理<cell地图>点击事件");
    
    CellMapViewController *mapVC = [[CellMapViewController alloc] init];
    mapVC.videoInfoList = self.videoInfoList;
    mapVC.myLocation = self.videoInfoList[btn.tag-1000];
    [self.navigationController pushViewController:mapVC animated:YES];
    
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NearVideoTableViewCellHeight;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{    
    VideoInfo *theVideoInfo = [VideoInfo videoInfoWithMapx:[NSString stringWithFormat:@"%f", self.mapy] mapy:[NSString stringWithFormat:@"%f", self.mapx] start:@"" limit:@"" keyword:searchBar.text];
    
    [[NetworkService sharedInstance] searchVideosWithVideoInfo:theVideoInfo success:^(NSArray *videoInfoList) {
        
        self.nearVideoTableView.footer.hidden = YES;
        
        self.videoInfoList = videoInfoList;
        [self.nearVideoTableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // 退出键盘
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    VideoInfo *theVideoInfo = [VideoInfo videoInfoWithMapx:[NSString stringWithFormat:@"%f", self.mapy] mapy:[NSString stringWithFormat:@"%f", self.mapx] start:@"" limit:@"" keyword:searchBar.text];
    
    [[NetworkService sharedInstance] searchVideosWithVideoInfo:theVideoInfo success:^(NSArray *videoInfoList) {
        
        self.nearVideoTableView.footer.hidden = YES;
        
        self.videoInfoList = videoInfoList;
        [self.nearVideoTableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBar.text = @"";
}

@end
