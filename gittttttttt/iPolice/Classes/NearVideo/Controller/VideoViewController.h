//
//  VideoViewController.h
//  iPolice
//
//  Created by xwy on 15/7/17.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoInfo;
@interface VideoViewController : UIViewController

@property (nonatomic, strong) VideoInfo *video;
@end
