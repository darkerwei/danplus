//
//  CellMapViewController.h
//  iPolice
//
//  Created by xwy on 15/9/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoInfo;

@interface CellMapViewController : UIViewController

@property (nonatomic, strong) NSArray *videoInfoList;

@property (nonatomic, strong) VideoInfo *myLocation;
@end
