//
//  VideoPlayerViewController.m
//  VideoPlayerDemo
//
//  Created by darkerwei on 15/9/18.
//  Copyright © 2015年 wei. All rights reserved.
//

#import "VideoPlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoPlayerViewController ()

/**
 *  视频播放器
 */
@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;
/**
 *  视频播放器状态
 */
@property (nonatomic, assign) MPMoviePlaybackState moviePlayerControllerState;
/**
 *  倒计时
 */
@property (nonatomic, strong) UILabel *timeLabel;
/**
 *  定时器
 */
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation VideoPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 初始化视频播放器
    [self initMoviePlayerController];
    
    // 添加通知
    [self addNotifications];
    
    // 播放视频
    [self didPlay];
    
    // 初始化计时器
    [self initTimer];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void)initMoviePlayerController
{
    // 禁用视频播放器默认控件
    self.moviePlayerController.controlStyle = MPMovieControlStyleNone;
    
    // 视频播放器全屏显示
    self.moviePlayerController.scalingMode = MPMovieScalingModeAspectFill;
    
    // 添加视频播放器
    [self.view insertSubview:self.moviePlayerController.view atIndex:0];
    
    // 添加约束
    self.moviePlayerController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[moviePlayerControllerview]-0-|" options:0 metrics:nil views:@{ @"moviePlayerControllerview" : self.moviePlayerController.view }]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[moviePlayerControllerview]-0-|" options:0 metrics:nil views:@{ @"moviePlayerControllerview" : self.moviePlayerController.view }]];
    
    [self initBackButton];
}

- (void)initBackButton
{
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.height - 60, 10, 50, 50)];
    backButton.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
//    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [backButton setTitle:@"退出" forState:UIControlStateNormal];
//    backButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [backButton setImage:[UIImage imageNamed:@"关闭按钮"] forState:UIControlStateNormal];
    backButton.clipsToBounds = YES;
    backButton.layer.cornerRadius = 25;
    [backButton addTarget:self.moviePlayerController action:@selector(stop) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

- (void)addNotifications
{
    // 监听播放状态
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChange) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    
    // 监听播放结束
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didDismiss) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

- (void)didChange
{
    switch (self.moviePlayerController.playbackState) {
        case MPMoviePlaybackStateStopped:
            NSLog(@"Stopped");
            break;
        case MPMoviePlaybackStatePlaying:
            NSLog(@"Playing");
            break;
        case MPMoviePlaybackStatePaused:
            NSLog(@"Paused");
            break;
        case MPMoviePlaybackStateInterrupted:
            NSLog(@"Interrupted");
            break;
        case MPMoviePlaybackStateSeekingForward:
            NSLog(@"SeekingForward");
            break;
        case MPMoviePlaybackStateSeekingBackward:
            NSLog(@"SeekingBackward");
            break;
        default:
            break;
    }
}

- (void)didDismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)initTimer
{
    self.timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(updateTimeLabel) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)updateTimeLabel
{
    int time = (int)(self.time - self.moviePlayerController.currentPlaybackTime);
    int hours = time % (60 * 60 * 24) / (60 * 60);
    int minutes = time % (60 * 60) / 60;
    int seconds = time % 60;
    
//    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"时间(min)    %02d:%02d:%02d", hours, minutes ,seconds]];
//    [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(11,8)];
//    [mutableAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:32] range:NSMakeRange(11,8)];
//    self.timeLabel.attributedText = mutableAttributedString;
    
    self.timeLabel.text = [NSString stringWithFormat:@"%2d秒后退出",seconds];
    if (hours == 0 && minutes == 0 && seconds == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    NSLog(@"时间(min)    %02d:%02d:%02d", hours, minutes ,seconds);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.moviePlayerControllerState == MPMoviePlaybackStatePlaying) {
        [self didPause];
    } else if (self.moviePlayerControllerState == MPMoviePlaybackStatePaused) {
        [self didPlay];
    }
}

// 视频播放
- (void)didPlay
{
    [self.moviePlayerController play];
    self.moviePlayerControllerState = MPMoviePlaybackStatePlaying;
}

// 视频暂停
- (void)didPause
{
    [self.moviePlayerController pause];
    self.moviePlayerControllerState = MPMoviePlaybackStatePaused;
}

// 视频播放器横批
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)setVideoURL:(NSString *)videoURL
{
    _videoURL = videoURL;
    
    self.moviePlayerController.contentURL = [NSURL URLWithString:_videoURL];
}

- (MPMoviePlayerController *)moviePlayerController
{
    if (!_moviePlayerController) {
        _moviePlayerController = [[MPMoviePlayerController alloc] init];
    }
    return _moviePlayerController;
}

- (UILabel *)timeLabel
{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 50)];
        _timeLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.font = [UIFont systemFontOfSize:13.0];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_timeLabel];
    }
    return _timeLabel;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
