//
//  NearVideoViewController.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearVideoViewController : UIViewController

@property (nonatomic, assign) CGFloat mapx;
@property (nonatomic, assign) CGFloat mapy;

@end
