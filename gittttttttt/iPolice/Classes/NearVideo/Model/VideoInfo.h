//
//  VideoInfo.h
//  iPolice
//
//  Created by ioswei on 15/7/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoInfo : NSObject

/**
 *  X
 */
@property (nonatomic, copy) NSString *mapx;
/**
 *  Y
 */
@property (nonatomic, copy) NSString *mapy;
/**
 *  起始索引
 */
@property (nonatomic, copy) NSString *start;
/**
 *  返回个数
 */
@property (nonatomic, copy) NSString *limit;
/**
 *  视频id
 */
@property (nonatomic, copy) NSString *vid;
/**
 *  名字
 */
@property (nonatomic, copy) NSString *name;
/**
 *  状态
 */
@property (nonatomic, copy) NSString *status;
/**
 *  安装地址
 */
@property (nonatomic, copy) NSString *location;
/**
 *  观看地址
 */
@property (nonatomic, copy) NSString *uri;
/**
 *  距离
 */
@property (nonatomic, strong) NSNumber *distance;
/**
 *  关键字
 */
@property (nonatomic, copy) NSString *keyword;
/**
 *  缩略图
 */
@property (nonatomic, copy) NSString *thumbnail;
/**
 *  设备id
 */
@property (nonatomic, copy) NSString *deviceid;
/**
 *  设备类型
 */
@property (nonatomic, copy) NSString *devicetype;
/**
 *  设备型号
 */
@property (nonatomic, copy) NSString *model;
/**
 *  生产厂家
 */
@property (nonatomic, copy) NSString *oem;
/**
 *  所属机构
 */
@property (nonatomic, copy) NSString *organization;
/**
 *  扩展信息1
 */
@property (nonatomic, copy) NSString *extfield1;
/**
 *  扩展信息2
 */
@property (nonatomic, copy) NSString *extfield2;
/**
 *  扩展信息3
 */
@property (nonatomic, copy) NSString *extfield3;
/**
 *  扩展信息4
 */
@property (nonatomic, copy) NSString *extfield4;
/**
 *  扩展信息5
 */
@property (nonatomic, copy) NSString *extfield5;


/**
 *  快速创建一个视频信息
 *
 *  @return 创建好的视频信息
 */
+ (instancetype)videoInfo;

/**
 *  快速创建一个视频信息
 *
 *  @param mapx  X
 *  @param mapy  Y
 *  @param start 起始索引
 *  @param limit 返回个数
 *
 *  @return 创建好的视频信息
 */
+ (instancetype)videoInfoWithMapx:(NSString *)mapx
                             mapy:(NSString *)mapy
                            start:(NSString *)start
                            limit:(NSString *)limit;

/**
 *  快速创建一个视频信息
 *
 *  @param mapx    X
 *  @param mapy    Y
 *  @param start   起始索引
 *  @param limit   返回个数
 *  @param keyword 关键字
 *
 *  @return 创建好的视频信息
 */
+ (instancetype)videoInfoWithMapx:(NSString *)mapx
                             mapy:(NSString *)mapy
                            start:(NSString *)start
                            limit:(NSString *)limit
                          keyword:(NSString *)keyword;
/**
 *  快速创建一个视频信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的视频信息
 */
+ (instancetype)videoInfoWithDict:(NSDictionary *)dict;

/**
 *  快速创建一个视频信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的视频信息
 */
- (instancetype)initWithDict:(NSDictionary *)dict;

@end
