//
//  VideoInfo.m
//  iPolice
//
//  Created by ioswei on 15/7/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "VideoInfo.h"

@implementation VideoInfo

/**
 *  快速创建一个视频信息
 *
 *  @return 创建好的视频信息
 */
+ (instancetype)videoInfo
{
    return [[self alloc] init];
}

/**
 *  快速创建一个视频信息
 *
 *  @param mapx  X
 *  @param mapy  Y
 *  @param start 起始索引
 *  @param limit 返回个数
 *
 *  @return 创建好的视频信息
 */
+ (instancetype)videoInfoWithMapx:(NSString *)mapx
                             mapy:(NSString *)mapy
                            start:(NSString *)start
                            limit:(NSString *)limit
{
    VideoInfo *theVideoInfo = [self videoInfo];
    
    theVideoInfo.mapx = mapx;
    theVideoInfo.mapy = mapy;
    theVideoInfo.start = start;
    theVideoInfo.limit = limit;
    
    return theVideoInfo;
}

/**
 *  快速创建一个视频信息
 *
 *  @param mapx    X
 *  @param mapy    Y
 *  @param start   起始索引
 *  @param limit   返回个数
 *  @param keyword 关键字
 *
 *  @return 创建好的视频信息
 */
+ (instancetype)videoInfoWithMapx:(NSString *)mapx
                             mapy:(NSString *)mapy
                            start:(NSString *)start
                            limit:(NSString *)limit
                          keyword:(NSString *)keyword
{
    VideoInfo *theVideoInfo = [self videoInfo];
    
    theVideoInfo.mapx = mapx;
    theVideoInfo.mapy = mapy;
    theVideoInfo.start = start;
    theVideoInfo.limit = limit;
    theVideoInfo.keyword = keyword;
    
    return theVideoInfo;
}

/**
 *  快速创建一个视频信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的视频信息
 */
+ (instancetype)videoInfoWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

/**
 *  快速创建一个视频信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的视频信息
 */
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        NSNumber *theMapx = dict[@"mapx"];
        self.mapx = [NSString stringWithFormat:@"%@", theMapx];
        NSNumber *theMapy = dict[@"mapy"];
        self.mapy = [NSString stringWithFormat:@"%@", theMapy];
        self.start = dict[@"start"];
        self.limit = dict[@"limit"];
        self.vid = dict[@"vid"];
        self.name = dict[@"name"];
        self.status = dict[@"status"];
        self.thumbnail = dict[@"thumbnail"];
        self.location = dict[@"location"];
        self.uri = dict[@"uri"];
        self.distance = dict[@"distance"];
        self.keyword = dict[@"keyword"];
        self.deviceid = dict[@"deviceid"];
        self.devicetype = dict[@"devicetype"];
        self.model = dict[@"model"];
        self.oem = dict[@"oem"];
        self.organization = dict[@"organization"];
        self.extfield1 = dict[@"extfield1"];
        self.extfield2 = dict[@"extfield2"];
        self.extfield3 = dict[@"extfield3"];
        self.extfield4 = dict[@"extfield4"];
        self.extfield5 = dict[@"extfield5"];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\"mapx\":\"%@\", \"mapy\":\"%@\", \"start\":\"%@\", \"limit\":\"%@\", \"vid\":\"%@\", \"name\":\"%@\", \"status\":\"%@\", \"location\":\"%@\", \"uri\":\"%@\", \"distance\":\"%@\", \"keyword\":\"%@\", \"deviceid\":\"%@\", \"devicetype\":\"%@\", \"model\":\"%@\", \"oem\":\"%@\", \"organization\":\"%@\", \"extfield1\":\"%@\", \"extfield2\":\"%@\", \"extfield3\":\"%@\", \"extfield4\":\"%@\", \"extfield5\":\"%@\"}", self.mapx, self.mapy, self.start, self.limit, self.vid, self.name, self.status, self.location, self.uri, self.distance, self.keyword, self.deviceid, self.devicetype, self.model, self.oem, self.organization, self.extfield1, self.extfield2, self.extfield3, self.extfield4, self.extfield5];
}

@end
