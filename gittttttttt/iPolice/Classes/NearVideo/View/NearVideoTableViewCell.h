//
//  NearVideoTableViewCell.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoInfo;

@protocol NearVideoTableViewCellDelegate <NSObject>

- (void)playButtonClick:(UIButton *)sender;
- (void)mapButtonClick:(UIButton *)sender;

@end

@interface NearVideoTableViewCell : UITableViewCell

@property(nonatomic, assign) id<NearVideoTableViewCellDelegate>delegate;

/**
 *  Cell信息
 */
@property (nonatomic, strong) VideoInfo *theVideoInfo;

@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;


/**
 *  快速创建一个Cell
 *
 *  @param tableView 哪个TableView
 *
 *  @return 一个创建好的Cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
