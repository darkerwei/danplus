//
//  CustomAnnotationView.m
//  iPolice
//
//  Created by ioswei on 15/7/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "CustomAnnotationView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TagTrackViewController.h"
#import "MyPointAnnotation.h"
@interface CustomAnnotationView ()

@property (nonatomic, strong, readwrite) CustomCalloutView *calloutView;

@end

@implementation CustomAnnotationView{
    TagModel *_model;
}

#define kCalloutWidth       200.0
#define kCalloutHeight      70.0

-(void)setTagModel:(TagModel *)model{
    _model = model;
    NSLog(@"model %@",model.devid);
}

- (void)show
{
    if (self.calloutView == nil)
    {
        self.calloutView = [[CustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
        self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x,
                                              -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
    }

    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.calloutView.bounds];
    [imageView sd_setImageWithURL:[NSURL URLWithString:self.imageURL]
                           placeholderImage:[UIImage imageNamed:@"默认图.png"]];

    self.calloutView.image = imageView.image;
    self.calloutView.title = self.annotation.title;
    self.calloutView.subtitle = self.annotation.subtitle;
    
    [self addSubview:self.calloutView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    
    if (self.selected == selected)
    {
        return;
    }
    
    if (selected)
    {
        
        if (self.calloutView == nil)
        {
            /* Construct custom callout. */
            self.calloutView = [[CustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
            self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x,
                                                  -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btn.frame = CGRectMake(0, 0, kCalloutWidth, kCalloutHeight);
            [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.calloutView addSubview:btn];
            
        }
        
        [self addSubview:self.calloutView];
    }

    [super setSelected:selected animated:animated];
}

- (void)btnAction:(UIButton *)button
{
    CLLocationCoordinate2D coorinate = [self.annotation coordinate];
    NSLog(@"coordinate = {%f, %f},%d", coorinate.latitude, coorinate.longitude,button.tag);
    
    _goblock(_model);

}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    if (self.selected == selected)
//    {
//        return;
//    }
//    
//    if (selected)
//    {
//        if (self.calloutView == nil)
//        {
//            self.calloutView = [[CustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
//            self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x,
//                                                  -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
//        }
//        
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.calloutView.bounds];
//        [imageView sd_setImageWithURL:[NSURL URLWithString:self.imageURL]
//                               placeholderImage:[UIImage imageNamed:@"默认图.png"]];
//        
//        self.calloutView.image = imageView.image;
//        self.calloutView.title = self.annotation.title;
//        self.calloutView.subtitle = self.annotation.subtitle;
//        
//        [self addSubview:self.calloutView];
//    }
//    else
//    {
//        [self.calloutView removeFromSuperview];
//    }
//    
//    [super setSelected:selected animated:animated];
//}

@end
