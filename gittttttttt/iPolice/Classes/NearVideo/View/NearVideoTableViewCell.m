//
//  NearVideoTableViewCell.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/18.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NearVideoTableViewCell.h"
#import "VideoInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

// NearVideoTableViewCell标识符
#define NearVideoTableViewCellIdentifier @"NearVideoTableViewCell"

@interface NearVideoTableViewCell()

/**
 *  标题
 */
@property (weak, nonatomic) IBOutlet UILabel *nearVideoTitle;
/**
 *  标题图片
 */
@property (weak, nonatomic) IBOutlet UIImageView *nearVideoImage;
/**
 *  描述
 */
@property (weak, nonatomic) IBOutlet UILabel *nearVideoDesc;
/**
 *  位置
 */
@property (weak, nonatomic) IBOutlet UILabel *nearVideoLocation;


@end

@implementation NearVideoTableViewCell


- (IBAction)playButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(playButtonClick:)]) {
        [self.delegate playButtonClick:sender];
    }
}

- (IBAction)mapButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(mapButtonClick:)]) {
        [self.delegate mapButtonClick:sender];
    }
}



/**
 *  初始化Xib
 *
 *  @param theNearVideo cell信息
 */
- (void)setTheVideoInfo:(VideoInfo *)theVideoInfo
{
    _theVideoInfo = theVideoInfo;
    
    self.nearVideoTitle.text = theVideoInfo.name;
    [self.nearVideoImage sd_setImageWithURL:[NSURL URLWithString:theVideoInfo.thumbnail] placeholderImage:[UIImage imageNamed:@"曝光台.png"]];
    self.nearVideoDesc.text = theVideoInfo.location;
    self.nearVideoLocation.text = [NSString stringWithFormat:@"%dkm", theVideoInfo.distance.intValue/1000];
}

// 快速创建一个cell
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:NearVideoTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:NearVideoTableViewCellIdentifier];
    NearVideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NearVideoTableViewCellIdentifier];
    
    return cell;
}

@end
