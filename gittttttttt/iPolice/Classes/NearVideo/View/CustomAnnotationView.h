//
//  CustomAnnotationView.h
//  iPolice
//
//  Created by ioswei on 15/7/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "CustomCalloutView.h"
#import "TagModel.h"

typedef void(^goBlock)(TagModel *model);


@interface CustomAnnotationView : MAAnnotationView

@property (nonatomic, readonly) CustomCalloutView *calloutView;

@property (nonatomic, strong) NSString *imageURL;

@property(nonatomic,copy)goBlock goblock;

-(void)setTagModel:(TagModel *)model;

@end
