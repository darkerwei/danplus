//
//  Service.h
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Service : NSObject

/**
 *  服务名称
 */
@property (nonatomic, copy) NSString *serviceTitle;
/**
 *  服务图片名称
 */
@property (nonatomic, copy) NSString *serviceImageName;

/**
 *  快速创建一个服务
 *
 *  @param serviceTitle     服务名称
 *  @param serviceImageName 服务图片名称
 *
 *  @return 一个创建好的服务
 */
+ (instancetype)serviceWithServiceTitle:(NSString *)serviceTitle serviceImageName:(NSString *)serviceImageName;

@end
