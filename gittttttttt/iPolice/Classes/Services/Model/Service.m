//
//  Service.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Service.h"

@implementation Service

// 快速创建一个服务
+ (instancetype)serviceWithServiceTitle:(NSString *)serviceTitle serviceImageName:(NSString *)serviceImageName
{
    Service *theService = [[self alloc] init];
    theService.serviceTitle = serviceTitle;
    theService.serviceImageName = serviceImageName;
    
    return theService;
}

@end
