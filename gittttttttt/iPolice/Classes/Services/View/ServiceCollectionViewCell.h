//
//  ServiceCollectionViewCell.h
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Service;

@interface ServiceCollectionViewCell : UICollectionViewCell

/**
 *  服务信息
 */
@property (nonatomic, strong) Service *service;

/**
 *  快速创建一个cell
 *
 *  @param collectionView 哪个collectionView
 *  @param indexPath      collectionView的哪个cell
 *
 *  @return 一个创建好的cell
 */
+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath;

@end
