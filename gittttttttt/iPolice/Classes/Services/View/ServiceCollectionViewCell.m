//
//  ServiceCollectionViewCell.m
//  iPolice
//
//  Created by ioswei on 15/6/8.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ServiceCollectionViewCell.h"
#import "Service.h"

// ServiceCollectionViewCell标识符
#define ServiceCollectionViewCellIdentifier @"ServiceCollectionViewCell"

@interface ServiceCollectionViewCell()

/**
 *  服务名称
 */
@property (nonatomic, weak) IBOutlet UILabel *serviceTitle;
/**
 *  服务图片
 */
@property (nonatomic, weak) IBOutlet UIImageView *serviceImage;

@end

@implementation ServiceCollectionViewCell

/**
 *  初始化Xib
 *
 *  @param service 服务信息
 */
- (void)setService:(Service *)service
{
    _service = service;
    
    self.serviceImage.image = [UIImage imageNamed:service.serviceImageName];
    self.serviceTitle.text = service.serviceTitle;
}

// 快速创建一个cell
+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath
{
    [collectionView registerNib:[UINib nibWithNibName:ServiceCollectionViewCellIdentifier bundle:nil] forCellWithReuseIdentifier:ServiceCollectionViewCellIdentifier];
    ServiceCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:ServiceCollectionViewCellIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
