//
//  Uncivil.h
//  iPolice
//
//  Created by xwy on 15/8/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Uncivil : NSObject
/**
 *  Token
 */
@property (nonatomic, copy) NSString *token;
/**
 *  频道id
 */
@property (nonatomic, copy) NSString *cid;
/**
 *  用户id
 */
@property (nonatomic, copy) NSString *uid;
/**
 *  状态 0-可见* 1不可见 2待审核
 */
@property (nonatomic, copy) NSString *status;
/**
 *  主题样式 0-列表主题* 1-图片主题
 */
@property (nonatomic, copy) NSString *style;
/**
 *  分类id 空-查询所有分类*
 */
@property (nonatomic, copy) NSString *type;
/**
 *  内容开始
 */
@property (nonatomic, copy) NSString *start;
/**
 *  内容个数
 */
@property (nonatomic, copy) NSString *limit;
/**
 *  是否带图片 1-带图片 0/空-不带图片*
 */
@property (nonatomic, copy) NSString *hasImage;
//是否将data中的thread对象换为详细信息thread对象（0或无参数不需要，1需要）
@property (nonatomic, copy) NSString *hasDetail;

/**
 *  主题id
 */
@property (nonatomic, copy) NSString *tid;
/**
 *  标题
 */
@property (nonatomic, copy) NSString *subject;
/**
 *  摘要
 */
@property (nonatomic, copy) NSString *abstracts;
/**
 *  发布时间
 */
@property (nonatomic, strong) NSNumber *dateline;
/**
 *  跟帖数
 */
@property (nonatomic, strong) NSNumber *replies;
/**
 *  是否允许跟帖 0-允许 1-不允许
 */
@property (nonatomic, copy) NSString *allowreply;
/**
 *  查看数
 */
@property (nonatomic, strong) NSNumber *views;
/**
 *  收藏数
 */
@property (nonatomic, strong) NSNumber *favtimes;
/**
 *  分享数
 */
@property (nonatomic, strong) NSNumber *sharetimes;
/**
 *  分类名
 */
@property (nonatomic, copy) NSString *theTypename;
/**
 *  是否置顶 0-是 1-否
 */
@property (nonatomic, copy) NSString *sticky;
/**
 *  发布人
 */
@property (nonatomic, copy) NSString *username;
/**
 *  发布人昵称
 */
@property (nonatomic, copy) NSString *nickname;
/**
 *  X
 */
@property (nonatomic, strong) NSNumber *mapx;
/**
 *  Y
 */
@property (nonatomic, strong) NSNumber *mapy;
/**
 *  位置
 */
@property (nonatomic, copy) NSString *location;
/**
 *  缩略图
 */
@property (nonatomic, copy) NSString *thumbnail;
/**
 *  主题内容
 */
@property (nonatomic, copy) NSString *message;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield1;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield2;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield3;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield4;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield5;
/**
 *  新闻来源
 */
@property (nonatomic, copy) NSString *source;
/**
 *  附件
 */
@property (nonatomic, copy) NSArray *attachments;

//快速构造一个模型
+ (instancetype)uncivil;

/**
 *  快速构造一个模型
 *
 *  @param token    Token
 *  @param cid      频道id
 *  @param uid      用户id
 *  @param status   状态 0-可见* 1不可见 2待审核
 *  @param style    主题样式 0-列表主题* 1-图片主题
 *  @param type     分类id 空-查询所有分类*
 *  @param start    内容开始
 *  @param limit    内容个数
 *  @param hasImage 是否带图片 1-带图片 0/空-不带图片*
 *
 *  @return 创建好的模型
 */
+ (instancetype)uncivilWithCid:(NSString *)cid
                           uid:(NSString *)uid
                        status:(NSString *)status
                         style:(NSString *)style
                          type:(NSString *)type
                         start:(NSString *)start
                         limit:(NSString *)limit
                      hasImage:(NSString *)hasImage
                     hasDetail:(NSString *)hasDetail;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)uncivilInfoWithDict:(NSDictionary *)dict;

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initUncivilWithDict:(NSDictionary *)dict;
@end
