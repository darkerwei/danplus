//
//  PostThreads.h
//  iPolice
//
//  Created by xwy on 15/8/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostThreads : NSObject

/**
 *  Token
 */
@property (nonatomic, copy) NSString *token;
/**
 *  频道ID
 */
@property (nonatomic, copy) NSString *cid;
/**
 *  用户id
 */
@property (nonatomic, copy) NSString *uid;
/**
 *  标题
 */
@property (nonatomic, copy) NSString *subject;
/**
 *  主题内容
 */
@property (nonatomic, copy) NSString *message;
/**
 *  摘要
 */
@property (nonatomic, copy) NSString *abstracts;
/**
 *  是否允许跟帖 0-允许 1-不允许
 */
@property (nonatomic, copy) NSString *allowreply;
/**
 *  状态 0-可见* 1不可见 2待审核
 */
@property (nonatomic, copy) NSString *status;
/**
 *  主题样式 0-列表主题* 1-图片主题
 */
@property (nonatomic, copy) NSString *style;
/**
 *  分类id 空-查询所有分类*
 */
@property (nonatomic, copy) NSString *type;
/**
 *  附件
 */
@property (nonatomic, copy) NSArray *attachments;
/**
 *  是否置顶 0-是 1-否
 */
@property (nonatomic, copy) NSString *sticky;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield1;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield2;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield3;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield4;
/**
 *  扩展字段
 */
@property (nonatomic, copy) NSString *extfield5;


/**
 *  快速创建一个用户信息
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)postThreads;


+ (instancetype)postThreadsWithCid:(NSString *)cid
                           subject:(NSString *)subject
                           message:(NSString *)message;

//14.5 局长信箱
+ (instancetype)postThreadsWithSubject:(NSString *)subject
                               message:(NSString *)message
                             extfield1:(NSString *)extfield1
                             extfield2:(NSString *)extfield2
                             extfield3:(NSString *)extfield3
                             extfield4:(NSString *)extfield4
                             extfield5:(NSString *)extfield5
                           attachments:(NSArray *)attachments;

+ (instancetype)postThreadsWithDict:(NSDictionary *)dict;


- (instancetype)initPostThreadsWithDict:(NSDictionary *)dict;

@end
