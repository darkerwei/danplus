//
//  PostThreads.m
//  iPolice
//
//  Created by xwy on 15/8/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "PostThreads.h"

@implementation PostThreads
/**
 *  快速创建一个用户信息
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)postThreads
{
    return [[self alloc] init];
}

+ (instancetype)postThreadsWithCid:(NSString *)cid
                           subject:(NSString *)subject
                           message:(NSString *)message{

    PostThreads *postThreads = [self postThreads];
    
    postThreads.cid = cid;
    postThreads.subject = subject;
    postThreads.message = message;
    
    return postThreads;
}

//14.5 局长信箱
+ (instancetype)postThreadsWithSubject:(NSString *)subject
                               message:(NSString *)message
                             extfield1:(NSString *)extfield1
                             extfield2:(NSString *)extfield2
                             extfield3:(NSString *)extfield3
                             extfield4:(NSString *)extfield4
                             extfield5:(NSString *)extfield5
                           attachments:(NSArray *)attachments{
    
    PostThreads *postThreads = [self postThreads];
    
    postThreads.subject = subject;
    postThreads.message = message;
    postThreads.extfield1 = extfield1;
    postThreads.extfield2 = extfield2;
    postThreads.extfield3 = extfield3;
    postThreads.extfield4 = extfield4;
    postThreads.extfield5 = extfield5;
    postThreads.attachments = attachments;
    
    return postThreads;
    
}
/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)postThreadsWithDict:(NSDictionary *)dict
{
    return [[self alloc] initPostThreadsWithDict:dict];
}

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
- (instancetype)initPostThreadsWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.cid = dict[@"username"];
        self.subject = dict[@"password"];
        self.message = dict[@"newPassword1"];
        self.token = dict[@"token"];
        self.uid = dict[@"uid"];
        
        // 存储token和uid
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:self.token forKey:@"token"];
        [defaults setObject:self.uid forKey:@"uid"];
        [defaults synchronize];
    }
    return self;
}
@end
