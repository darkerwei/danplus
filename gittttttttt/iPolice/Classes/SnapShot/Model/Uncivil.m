//
//  Uncivil.m
//  iPolice
//
//  Created by xwy on 15/8/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "Uncivil.h"
#import "Attachment.h"

@implementation Uncivil

+ (instancetype)uncivil
{
    return [[self alloc] init];
}

/**
 *  快速构造一个模型
 *
 *  @param token    Token
 *  @param cid      频道id
 *  @param uid      用户id
 *  @param status   状态 0-可见* 1不可见 2待审核
 *  @param style    主题样式 0-列表主题* 1-图片主题
 *  @param type     分类id 空-查询所有分类*
 *  @param start    内容开始
 *  @param limit    内容个数
 *  @param hasImage 是否带图片 1-带图片 0/空-不带图片*
 *
 *  @return 创建好的模型
 */
+ (instancetype)uncivilWithCid:(NSString *)cid
                           uid:(NSString *)uid
                        status:(NSString *)status
                         style:(NSString *)style
                          type:(NSString *)type
                         start:(NSString *)start
                         limit:(NSString *)limit
                      hasImage:(NSString *)hasImage
                     hasDetail:(NSString *)hasDetail
{
    Uncivil *uncivil = [self uncivil];
    
    uncivil.cid = cid;
    uncivil.uid = uid;
    uncivil.status = status;
    uncivil.style = style;
    uncivil.type = type;
    uncivil.start = start;
    uncivil.limit = limit;
    uncivil.hasImage = hasImage;
    uncivil.hasDetail = hasDetail;
    
    return uncivil;
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
+ (instancetype)uncivilInfoWithDict:(NSDictionary *)dict{

    return [[self alloc] initUncivilWithDict:dict];
}

/**
 *  快速构造一个模型
 *
 *  @param dict 字典
 *
 *  @return 创建好的模型
 */
- (instancetype)initUncivilWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.token = dict[@"token"];
        self.cid = dict[@"cid"];
        self.uid = dict[@"uid"];
        self.status = dict[@"status"];
        self.style = dict[@"style"];
        self.type = dict[@"type"];
        self.start = dict[@"start"];
        self.limit = dict[@"limit"];
        self.hasImage = dict[@"hasImage"];
        self.hasDetail = dict[@"hasDetail"];
        self.tid = dict[@"tid"];
        self.subject = dict[@"subject"];
        self.abstracts = dict[@"abstracts"];
        self.dateline = dict[@"dateline"];
        self.replies = dict[@"replies"];
        self.allowreply = dict[@"allowreply"];
        self.views = dict[@"views"];
        self.favtimes = dict[@"favtimes"];
        self.sharetimes = dict[@"sharetimes"];
        self.theTypename = dict[@"typename"];
        self.sticky = dict[@"sticky"];
        self.username = dict[@"username"];
        self.nickname = dict[@"nickname"];
        self.mapx = dict[@"mapx"];
        self.mapy = dict[@"mapy"];
        self.location = dict[@"location"];
        self.thumbnail = dict[@"thumbnail"];
        self.message = dict[@"message"];
        self.extfield1 = dict[@"extfield1"];
        self.extfield2 = dict[@"extfield2"];
        self.extfield3 = dict[@"extfield3"];
        self.extfield4 = dict[@"extfield4"];
        self.extfield5 = dict[@"extfield5"];
        
        
        NSArray *threadsList = dict[@"attachments"];
        NSMutableArray *tempThreadsList = [NSMutableArray array];
        for (NSDictionary *dict in threadsList) {
            [tempThreadsList addObject:[Attachment attachmentWithDict:dict]];
        }
        self.attachments = tempThreadsList;
    }
    return self;
}

@end
