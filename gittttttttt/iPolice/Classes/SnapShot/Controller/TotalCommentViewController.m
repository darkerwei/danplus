//
//  TotalCommentViewController.m
//  iPolice
//
//  Created by xwy on 15/9/22.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TotalCommentViewController.h"
#import "Threads.h"
#import "TotalCommentTableViewCell.h"

#define TextMessage @"我来说两句..."
@interface TotalCommentViewController ()<UITableViewDelegate, UITableViewDataSource,UITextViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *commentList;
@property (nonatomic, assign) int limit;
@property (nonatomic, strong) UITextView *textView;
@property (strong, nonatomic) UIButton *sendButton;

@end

@implementation TotalCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"评论列表";
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 64, Main_Screen_Width, 50)];
    label.text = @"全部评论";
    label.font = [UIFont systemFontOfSize:17];
    [self.view addSubview:label];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64+50, Main_Screen_Width, Main_Screen_Height-64-50-50)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];
    
    //下拉刷新
    [self refresh];
    //上拉刷新
    [self upRefresh];
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, Main_Screen_Height-50, Main_Screen_Width-70, 50)];
    //设置背景图
    UIImage *image = [UIImage imageNamed:@"评论框"];
    self.textView.layer.contents = (id) image.CGImage;
    self.textView.text = TextMessage;
    self.textView.textAlignment = NSTextAlignmentNatural;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    
    self.sendButton = [[UIButton alloc] initWithFrame:CGRectMake(Main_Screen_Width-55, Main_Screen_Height-50, 60, 50)];
    [self.sendButton setImage:[UIImage imageNamed:@"发送-原始.png"] forState:UIControlStateNormal];
    [self.sendButton setImage:[UIImage imageNamed:@"发送-点击.png"] forState:UIControlStateHighlighted];
    [self.sendButton addTarget:self action:@selector(clickSend) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sendButton];
}

//点击空白处取消键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.textView resignFirstResponder];
}

//点击return隐藏键盘
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
//输入框代理
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:TextMessage]) {
        textView.text = @"";
    }
    
    [self animateTextView:textView up: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = TextMessage;
    }
    
    [self animateTextView:textView up:NO];
    [textView resignFirstResponder];
}

- (void)animateTextView: (UITextView *) textView up: (BOOL) up
{
    const int movementDistance = 250; // tweak as needed
    const float movementDuration = 0.2f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    
    //    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    self.textView.frame = CGRectOffset(self.textView.frame, 0, movement);
    self.sendButton.frame = CGRectOffset(self.sendButton.frame, 0, movement);
    [UIView commitAnimations];
    
}

//点击发送评论
- (void)clickSend{
    
    if (self.textView.text.length == 0 || [self.textView.text isEqualToString:TextMessage]) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请填写内容" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        [alvert show];
        
    } else {
        
        Threads *threads = [Threads threadsWithTid:self.comment.tid message:self.textView.text extfield1:@"" extfield2:@"" extfield3:@"" extfield4:@"" extfield5:@""];
        [[NetworkService sharedInstance] postPostWithPost:threads success:^{
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"发送失败"];
        }];
    }
    
}

//点击他人评论
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row >= 0) {
        self.textView.text = @"";
        TotalCommentTableViewCell *cell = [TotalCommentTableViewCell cellWithTableView:tableView];
        cell.comment = self.commentList[indexPath.row];
        self.textView.text = [NSString stringWithFormat:@"@%@:%@", cell.comment.nickname,self.textView.text];
    }
}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;
    Threads *theThreads = [Threads threads];
    theThreads.tid = self.comment.tid;
    theThreads.cid = self.comment.cid;
    theThreads.start = @"0";
    theThreads.limit = @"10";
    [SVProgressHUD showWithStatus:@"正在获取评论事件"];
    [[NetworkService sharedInstance] getPostsWithThreads:theThreads success:^(NSArray *postsList) {
        [SVProgressHUD showSuccessWithStatus:@"获取事件成功"];
        
        // 视频满10个才显示上拉刷新
        if (postsList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (postsList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        
        self.commentList = postsList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    Threads *theThreads = [Threads threads];
    theThreads.tid = self.comment.tid;
    theThreads.cid = self.comment.cid;
    theThreads.start = @"0";
    theThreads.limit = @"10";
    [SVProgressHUD showWithStatus:@"正在获取评论事件"];
    [[NetworkService sharedInstance] getPostsWithThreads:theThreads success:^(NSArray *postsList) {
        [SVProgressHUD showSuccessWithStatus:@"获取事件成功"];
        
        // 视频满10个才显示上拉刷新
        if (postsList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取事件成功"];
        } else if (postsList.count < self.limit-10) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的内容了"];
        }
        
        self.commentList = postsList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
        [self.tableView.footer endRefreshing];
    }];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commentList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TotalCommentTableViewCell *cell = [TotalCommentTableViewCell cellWithTableView:tableView];
    cell.comment = self.commentList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
}

@end
