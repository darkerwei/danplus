//
//  SnapShotViewController.m
//  iPolice
//
//  Created by xwy on 15/8/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "SnapShotViewController.h"
#import "UncivilViewController.h"
#import "UncivilTableViewCell.h"
#import "Threads.h"
#import "Attachment.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import "Attachment.h"
#import "TotalCommentViewController.h"
#import "ImagesViewController.h"

@interface SnapShotViewController ()<UITableViewDelegate, UITableViewDataSource,UncivilTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *unCivilList;

/**
 *  服务器一次返回视频数量
 */
@property (nonatomic, assign) int limit;
@end

@implementation SnapShotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    // 下拉刷新
    [self refresh];
    // 上拉刷新
    [self upRefresh];

    self.title = @"不文明曝光";
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    //右导航
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(clickRight)];
    right.image = [[UIImage imageNamed:@"右上角相机按钮.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem = right;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
}

//评论按钮
- (void)commentButtonClick:(Threads *)threads{

    NSLog(@"在这写<评论>");
    
    TotalCommentViewController *comment = [[TotalCommentViewController alloc] init];
    comment.comment = threads;
    [self.navigationController pushViewController:comment animated:YES];

}

/**
 *  下拉刷新
 */
- (void)refresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    [self.tableView.header beginRefreshing];
}

/**
 *  加载新数据
 */
- (void)loadNewData
{
    self.limit = 10;

    Threads *theThreads = [Threads threadsWithCid:@"channel_uncivilized" status:@"0" style:@"" type:@"" start:@"0" limit:@"10" hasImage:@"1" hasDetail:@"1"];
    
    [SVProgressHUD showWithStatus:@"正在获取不文明事件"];
    
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        
        [SVProgressHUD showSuccessWithStatus:@"获取不文明事件成功"];
        
        // 视频满10个才显示上拉刷新
        if (threadList.count == self.limit) {
            self.tableView.footer.hidden = NO;
        } else if (threadList.count < self.limit) {
            self.tableView.footer.hidden = YES;
        }
        self.unCivilList = threadList;
        [self.tableView reloadData];
        [self.tableView.header endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.header endRefreshing];
    }];
}

/**
 *  上拉刷新
 */
- (void)upRefresh
{
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.footer.hidden = YES;
}

/**
 *  加载更多数据
 */
- (void)loadMoreData
{
    self.limit += 10;
    
    Threads *theThreads = [Threads threadsWithCid:@"channel_uncivilized" status:@"0" style:@"" type:@"" start:@"0" limit:@"10" hasImage:@"1" hasDetail:@"1"];
    
    [SVProgressHUD showWithStatus:@"正在获取不文明事件"];
    
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        
        [SVProgressHUD showSuccessWithStatus:@"获取不文明事件成功"];
        
        // 没有更多视频时隐藏上拉刷新
        if (threadList.count == self.limit) {
            self.tableView.footer.hidden = NO;
            [SVProgressHUD showSuccessWithStatus:@"获取不文明事件成功"];
        } else if (threadList.count < self.limit - 10) {
            self.tableView.footer.hidden = YES;
            [SVProgressHUD showErrorWithStatus:@"没有更多的内容了"];
        }
    
        self.unCivilList = threadList;
        [self.tableView reloadData];
        [self.tableView.footer endRefreshing];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
        [self.tableView.footer endRefreshing];
        
    }];

}


- (void)clickRight{
    
    UncivilViewController *noPolite = [[UncivilViewController alloc] init];
    [self.navigationController pushViewController:noPolite animated:NO];
}

- (void)getThreads
{
    Threads *theThreads = [Threads threadsWithCid:@"channel_uncivilized" status:@"0" style:@"" type:@"" start:@"0" limit:@"10" hasImage:@"1" hasDetail:@"1"];
    
    [[NetworkService sharedInstance] getThreadsWithThreads:theThreads success:^(NSArray *threadList) {
        
        self.unCivilList = threadList;
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.unCivilList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UncivilTableViewCell *cell = [UncivilTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.threads = self.unCivilList[indexPath.row];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //判断图片是否为空
//    if (![[[self.unCivilList[indexPath.row] attachments] firstObject] isEqualToString:nil]) {
        SnapShotViewController *bself = self;
        cell.goSeeImagesBlock = ^(){
            ImagesViewController *imagesCon = [[ImagesViewController alloc] init];
            imagesCon.imagesArray = [bself.unCivilList[indexPath.row] attachments];
            [bself.navigationController pushViewController:imagesCon animated:YES];
        };
//    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    UncivilTableViewCell * _cell = (UncivilTableViewCell *)cell;
    [_cell.contentView updateConstraintsIfNeeded];
    //设置分割线
    [tableView setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([[self.unCivilList[indexPath.row] attachments] count] == 0) {
        return 100;
    } else if ([[self.unCivilList[indexPath.row] attachments] count] > 0 && [[self.unCivilList[indexPath.row] attachments] count] < 4){
        return 170;
    } else {
        return 240;
    }
}

@end
