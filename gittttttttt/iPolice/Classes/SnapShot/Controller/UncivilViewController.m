//
//  UncivilViewController.m
//  iPolice
//
//  Created by xwy on 15/8/6.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "UncivilViewController.h"
#import "GYLAddImage.h"
#import "PostThreads.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import <MAMapKit.h>
#import <AMapSearchAPI.h>

#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width
#define Main_Screen_Height      [[UIScreen mainScreen] bounds].size.height
#define TextCountMax 100

@interface UncivilViewController ()<UITextViewDelegate, UIAlertViewDelegate, MAMapViewDelegate, AMapSearchDelegate>
{
    float myLatitude;
    float myLongitude;
    AMapSearchAPI *_search;
    
    NSArray *imageDataArray;
    NSArray *imagePathArray;
}
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UILabel *countLabel;
@property (nonatomic, strong) GYLAddImage *addImageView;
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) UILabel *addressLabel;
@end

@implementation UncivilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
//    _search = [[AMapSearchAPI alloc] initWithSearchKey:MAPAPI_Key Delegate:self];
    _search = [[AMapSearchAPI alloc]init];
    _search.delegate = self;
    // 创建地图
    self.mapView = [[MAMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setZoomLevel:15 animated:YES];
    
    self.title = @"不文明上报";
    
    //让scrollView自动适应屏幕
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //为导航栏左侧添加系统自定义按钮
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    
    //为导航栏添加右侧按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 70, Main_Screen_Width-20, 100)];
    self.textView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.textView.text = @"请输入曝光信息(100字以内)";
    self.textView.textAlignment = NSTextAlignmentNatural;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    
    self.countLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.textView.frame.size.width-70, self.textView.frame.size.height-30, 80, 30)];
    self.countLabel.text = @"0/100";
    [self.textView addSubview:self.countLabel];
    
    _addImageView = [[GYLAddImage alloc] initWithFrame:CGRectMake(15, 200, self.view.frame.size.width, self.view.frame.size.height)];
    __weak typeof(self) weakSelf = self;
    _addImageView.block = ^(UIViewController *pickerVC){
        [weakSelf presentViewController:pickerVC animated:YES completion:nil];
    };
    [self.view addSubview:_addImageView];
    
    _addImageView.successBlock = ^(NSArray *images, NSMutableArray *imageDataArr){
        [imageDataArr removeAllObjects];
        NSMutableArray *imagePathArr = [NSMutableArray array];
        for (int i = 0; i < images.count; i++) {
            UIImage *image = images[i];
            NSString * aPath = [NSString stringWithFormat:@"%@/Documents/photo%d.png",NSHomeDirectory(),i+1];
            NSData * imgData = UIImagePNGRepresentation(image);
            [imageDataArr addObject:imgData];
            [imagePathArr addObject:aPath];
            [imgData writeToFile:aPath atomically:YES];
        }
        imageDataArray = imageDataArr;
        imagePathArray = imagePathArr;
    };
    
    //地址背景
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 330, Main_Screen_Width-20, 30)];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:view];
    
    //地址图标
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(30, 335, 20, 20)];
    image.image = [UIImage imageNamed:@"我的地址图标.png"];
    [self.view addSubview:image];
    
    //地址标签
    self.addressLabel = [[MJLabel alloc] initWithFrame:CGRectMake(50, 340, Main_Screen_Width-20, 60)];
    self.addressLabel.font = [UIFont systemFontOfSize:12];
    self.addressLabel.numberOfLines = 0;
    [self.view addSubview:self.addressLabel];
}


-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        //        NSLog(@"当前位置：latitude : %f,longitude: %f",userLocation.location.coordinate.latitude,userLocation.coordinate.longitude);
        
        myLatitude = userLocation.coordinate.latitude;
        myLongitude = userLocation.coordinate.longitude;
        
        
        //构造AMapReGeocodeSearchRequest对象，location为必选项，radius为可选项
        AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
//        regeoRequest.searchType = AMapSearchType_ReGeocode;
        regeoRequest.location = [AMapGeoPoint locationWithLatitude:myLatitude longitude:myLongitude];
        regeoRequest.radius = 10000;
        regeoRequest.requireExtension = YES;
        
        //发起逆地理编码
        [_search AMapReGoecodeSearch: regeoRequest];
    }
}


//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        //通过AMapReGeocodeSearchResponse对象处理搜索结果
        self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@", response.regeocode.addressComponent.province, response.regeocode.addressComponent.district, response.regeocode.addressComponent.township, response.regeocode.addressComponent.streetNumber.street, response.regeocode.addressComponent.streetNumber.number];
    }
    
}


- (void)backClick{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)doneClick{
    
    NSLog(@"在这里处理<提交按钮>");
    
    if (self.textView.text.length == 0 || [self.textView.text isEqualToString:@"请输入曝光信息(100字以内)"]) {
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"请添加信息或文件~" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        [alvert show];
        
    } else {
        
        
        UIAlertView *alvert = [[UIAlertView alloc]initWithTitle:@"系统提示!" message:@"确认发布?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        [alvert show];
    }
    
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    // 只显示<最大允许输入字数>个内容
    if (textView.text.length > TextCountMax) {
        textView.text = [textView.text substringToIndex:TextCountMax];
    }
    
    // 实时更新输入字数统计
    self.countLabel.text = [NSString stringWithFormat:@"%ld/%d", (unsigned long)textView.text.length, TextCountMax];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.textView resignFirstResponder];
}

//输入框代理
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@"请输入曝光信息(100字以内)"]) {
        textView.text = @"";
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"请输入曝光信息(100字以内)";
    }
    
    [textView resignFirstResponder];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSString *isHasImage = @"";
        NSMutableArray *attachmentsArr = [NSMutableArray array];
        if (imageDataArray.count != 0) {
            isHasImage = @"1";
            NSMutableArray *imageUUIDArr = [NSMutableArray array];
            for (int i = 0; i < imageDataArray.count; i++) {
                /**
                 *  把图片上传到 阿里云服务器
                 */
                [[toolsManager share] saveImageWithPath:imageDataArray[i] url:imagePathArray[i] callback:^{
                    
                }];
                /**
                 *  获得 阿里云生成的图片的 uuid
                 */
                NSString *uuid = [[toolsManager share] getUUID];
                [imageUUIDArr addObject:uuid];
            }
            
            for (NSString *uuid in imageUUIDArr) {
                NSDictionary *params = @{@"aid":uuid,@"name":uuid,@"type":@"1", @"uri":[NSString stringWithFormat:@"http://oss-cn-beijing.aliyuncs.com/%@",uuid] ,@"description":@"设置头像",@"size":[NSNumber numberWithInt:0]};
//                NSString *str = [NSString stringWithFormat:@"{\"aid\":\"%@\", \"name\":\"%@\", \"type\":\"1\", \"uri\":\"%@\", \"description\":\"上报不文明\", \"size\":\"0\"}", uuid,uuid,[NSString stringWithFormat:@"http://oss-cn-beijing.aliyuncs.com/%@",uuid]];
                [attachmentsArr addObject:params];
            }
        }
        
//         NSString *params = [NSString stringWithFormat:@"condition={\"token\":\"%@\", \"uid\":\"%@\"}", token, uid];
        
        PostThreads *postThreads = [PostThreads postThreadsWithCid:@"channel_uncivilized" subject:@"不文明举报" message:self.textView.text];
        
        NSString *url = SERVER_URL(@"cms", @"postThread");
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        NSString *token = [defaults objectForKey:@"token"];
        NSString *uid = [defaults objectForKey:@"uid"];
        
        NSLog(@"url = %@", url);
        
        NSDictionary *dic = @{@"token":[NSString stringWithFormat:@"%@",token], @"uid":[NSString stringWithFormat:@"%@", uid], @"cid":[NSString stringWithFormat:@"%@", postThreads.cid], @"subject":[NSString stringWithFormat:@"%@", postThreads.subject], @"message":[NSString stringWithFormat:@"%@", postThreads.message],@"attachment": [NSString stringWithFormat:@"%@",isHasImage],@"attachments":attachmentsArr};
        NSLog(@"dic = %@ \n", dic);
        NSDictionary *params = @{@"condition":[NSString stringWithFormat:@"%@", dic]};
        
        NSLog(@"params = %@", params);
        
        [[HttpClient sharedHTTPManager] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"%@", dic);
            if ([dic[@"code"] intValue] == 0) {
                [SVProgressHUD showSuccessWithStatus:@"上传成功.."];
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                NSLog(@"err = %@", dic[@"error"]);
                [SVProgressHUD showErrorWithStatus:@"上传失败"];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (error.code == -1001) {
                [SVProgressHUD showErrorWithStatus:@"请求超时"];
            } else {
                [SVProgressHUD showErrorWithStatus:@"上传失败"];
            }
        }];
    }
}
@end
