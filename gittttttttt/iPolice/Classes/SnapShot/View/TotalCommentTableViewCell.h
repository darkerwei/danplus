//
//  TotalCommentTableViewCell.h
//  iPolice
//
//  Created by xwy on 15/9/22.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Threads;

@interface TotalCommentTableViewCell : UITableViewCell

@property (nonatomic, strong) Threads *comment;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
