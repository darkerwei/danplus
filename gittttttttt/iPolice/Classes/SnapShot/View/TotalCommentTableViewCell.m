//
//  TotalCommentTableViewCell.m
//  iPolice
//
//  Created by xwy on 15/9/22.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "TotalCommentTableViewCell.h"
#import "Threads.h"

#define TotalCommentTableViewCellIdentifier @"TotalCommentTableViewCell"

@interface TotalCommentTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *personImage;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet MJLabel *message;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@end

@implementation TotalCommentTableViewCell

- (void)setComment:(Threads *)comment{
    
    _comment = comment;
    
    self.nickName.text = self.comment.nickname;
    self.message.text = self.comment.message;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *regStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.comment.dateline.doubleValue / 1000.0]];
    self.time.text = regStr;
    [self.personImage sd_setImageWithURL:[NSURL URLWithString:self.comment.avatar] placeholderImage:[UIImage imageNamed:@"默认图.png"]];
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:TotalCommentTableViewCellIdentifier bundle:nil] forCellReuseIdentifier:TotalCommentTableViewCellIdentifier];
    TotalCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TotalCommentTableViewCellIdentifier];
    
    return cell;
}

@end
