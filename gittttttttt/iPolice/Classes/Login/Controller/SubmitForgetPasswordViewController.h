//
//  SubmitForgetPasswordViewController.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitForgetPasswordViewController : UIViewController

/**
 *  手机号
 */
@property (strong, nonatomic) NSString * mobile;

@end
