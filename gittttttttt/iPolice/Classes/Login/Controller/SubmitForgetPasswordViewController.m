//
//  SubmitForgetPasswordViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "SubmitForgetPasswordViewController.h"
#import <SVProgressHUD.h>
#import "RegularExpression.h"
#import "NetworkService.h"
#import "UserInfo.h"

@interface SubmitForgetPasswordViewController ()

/**
 *  密码
 */
@property (strong, nonatomic) IBOutlet UITextField *password;
/**
 *  确认密码
 */
@property (strong, nonatomic) IBOutlet UITextField *rePassword;

@end

@implementation SubmitForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"找回密码";
}

/**
 *  提交按钮点击
 */
- (IBAction)submitBtnClick
{
    NSLog(@"submitBtnClick");
    
    // 检测密码格式的正确性
    if (![RegularExpression isPassword:self.password.text]) {
        [SVProgressHUD showErrorWithStatus:@"密码输入错误"];
        return;
    }
    
    // 检测密码和确认密码的一致性
    if (![self.password.text isEqualToString:self.rePassword.text]) {
        [SVProgressHUD showErrorWithStatus:@"两次密码输入不相同"];
        return;
    }
    
    UserInfo *theUserInfo = [UserInfo userInfoWithMobile:self.mobile theNewPassword1:self.password.text theNewPassword2:self.rePassword.text];
    
    [SVProgressHUD showInfoWithStatus:@"正在重置密码"];
    [[NetworkService sharedInstance] resetPasswordWithUserInfo:theUserInfo success:^() {
        [SVProgressHUD showSuccessWithStatus:@"重置密码成功"];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

@end
