//
//  SubmitRegisterViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "SubmitRegisterViewController.h"
#import "NetworkService.h"
#import <SVProgressHUD.h>
#import "RegularExpression.h"
#import "UserInfo.h"
#import "TabBarViewController.h"

@interface SubmitRegisterViewController ()<UITextFieldDelegate>

@end

@implementation SubmitRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"注册";
    
    self.userName.delegate = self;
    self.userNickName.delegate = self;
    self.inviteCode.delegate = self;
    self.password.delegate = self;
    self.rePassword.delegate = self;
}

/**
 *  提交按钮点击
 */
- (IBAction)submitBtnClick
{
    NSLog(@"submitBtnClick");
    
//    // 检测用户名格式的正确性
//    if (![RegularExpression isUserName:self.userName.text]) {
//        [SVProgressHUD showErrorWithStatus:@"用户名输入错误"];
//        return;
//    }
    
//    // 检测密码格式的正确性
//    if (![RegularExpression isPassword:self.password.text]) {
//        [SVProgressHUD showErrorWithStatus:@"密码输入错误"];
//        return;
//    }
//    
//    // 检测密码和确认密码的一致性
//    if (![self.password.text isEqualToString:self.rePassword.text]) {
//        [SVProgressHUD showErrorWithStatus:@"两次密码输入不相同"];
//        return;
//    }
    
    // 昵称为空时，使用用户名作为昵称
    if ([self.userNickName.text isEqualToString:@""]) {
        self.userNickName.text = self.userName.text;
    }
    
    UserInfo *theUserInfo = [UserInfo userInfoWithUsername:self.userName.text password:self.password.text mobile:self.mobile nickname:self.userNickName.text invitecode:self.inviteCode.text];
    
    [SVProgressHUD showInfoWithStatus:@"正在注册"];
    [[NetworkService sharedInstance] registerWithUserInfo:theUserInfo success:^(UserInfo *userInfo) {
        [SVProgressHUD showSuccessWithStatus:@"注册成功"];
        
        NSLog(@"%@", userInfo);
        
        // 存储token和uid
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:userInfo.token forKey:@"token"];
        [defaults setObject:userInfo.uid forKey:@"uid"];
        [defaults synchronize];
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        //登录成功，显示主页
        TabBarViewController *tabBarVC = [[TabBarViewController alloc] init];
        // 显示状态栏
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        window.rootViewController = tabBarVC;

        
    } failure:^(NSError *error) {

        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    UserInfo *theUserInfo = [UserInfo userInfoWithUsername:self.userName.text password:@"" mobile:@"" nickname:@"" invitecode:self.inviteCode.text];

    [[NetworkService sharedInstance] registerWithUserInfo:theUserInfo success:^(UserInfo *userInfo) {

    } failure:^(NSError *error) {
        
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}
@end
