//
//  ForgetPasswordViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "SubmitForgetPasswordViewController.h"
#import <SMS_SDK/SMS_SDK.h>
#import <SVProgressHUD.h>
#import "RegularExpression.h"

@interface ForgetPasswordViewController ()

/**
 *  手机号码
 */
@property (nonatomic, weak) IBOutlet UITextField *mobileNumber;
/**
 *  验证码
 */
@property (nonatomic, weak) IBOutlet UITextField *verifyCode;

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = @"找回密码";
}

/**
 *  下一步按钮点击
 */
- (IBAction)nextBtnClick
{
    NSLog(@"nextBtnClick");
    
    // 检测验证码格式的正确性
    if (![RegularExpression isVerifyCode:self.verifyCode.text]) {
        [SVProgressHUD showErrorWithStatus:@"验证码输入错误"];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"正在检测验证码"];
    // 提交验证码
    [SMS_SDK commitVerifyCode:self.verifyCode.text result:^(enum SMS_ResponseState state) {
        if (state == SMS_ResponseStateSuccess) {
            [SVProgressHUD showSuccessWithStatus:@"验证码正确"];
            
            SubmitForgetPasswordViewController *submitForgetPasswordVC = [[SubmitForgetPasswordViewController alloc] init];
            submitForgetPasswordVC.mobile = self.mobileNumber.text;
            [self.navigationController pushViewController:submitForgetPasswordVC animated:YES];
        } else {
            [SVProgressHUD showErrorWithStatus:@"验证码错误"];
        }
    }];
}

/**
 *  获取验证码按钮点击
 */
- (IBAction)verifyCodeBtnClick
{
    NSLog(@"verifyCodeBtnClick");
    
    // 检测手机号码格式的正确性
    if (![RegularExpression isMobileNumber:self.mobileNumber.text]) {
        [SVProgressHUD showErrorWithStatus:@"手机号码输入错误"];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"正在发送验证码"];
    // 获取验证码
    [SMS_SDK getVerificationCodeBySMSWithPhone:self.mobileNumber.text zone:SMS_SDK_Zone result:^(SMS_SDKError *error) {
        if (error == nil) {
            [SVProgressHUD showSuccessWithStatus:@"验证码发送成功"];
        } else {
            [SVProgressHUD showErrorWithStatus:@"验证码发送失败"];
        }
    }];
}

//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.mobileNumber resignFirstResponder];
    [self.verifyCode resignFirstResponder];
}
@end
