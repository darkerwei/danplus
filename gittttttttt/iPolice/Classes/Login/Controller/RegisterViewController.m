//
//  RegisterViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "RegisterViewController.h"
#import "SubmitRegisterViewController.h"
#import <SMS_SDK/SMS_SDK.h>
#import <SVProgressHUD.h>
#import "RegularExpression.h"

@interface RegisterViewController ()

/**
 *  手机号码
 */
@property (nonatomic, weak) IBOutlet UITextField *mobileNumber;
/**
 *  验证码
 */
@property (nonatomic, weak) IBOutlet UITextField *verifyCode;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;

@property (nonatomic) NSInteger restSeconds;
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"获取验证码";
    [self.navigationController.navigationBar setHidden:NO];
    
}

/**
 *  下一步按钮点击
 */
- (IBAction)nextBtnClick
{
    NSLog(@"nextBtnClick");
    
    // 检测验证码格式的正确性
    if (![RegularExpression isVerifyCode:self.verifyCode.text]) {
        [SVProgressHUD showErrorWithStatus:@"验证码输入错误"];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"正在检测验证码"];
    // 提交验证码
    [SMS_SDK commitVerifyCode:self.verifyCode.text result:^(enum SMS_ResponseState state) {
        if (state == SMS_ResponseStateSuccess) {
            [SVProgressHUD showSuccessWithStatus:@"验证码正确"];
            
            SubmitRegisterViewController *submitRegisterVC = [[SubmitRegisterViewController alloc] init];
            submitRegisterVC.mobile = self.mobileNumber.text;
            [self.navigationController pushViewController:submitRegisterVC animated:YES];
        } else {
            [SVProgressHUD showErrorWithStatus:@"验证码错误"];
        }
    }];
}

/**
 *  获取验证码按钮点击
 */
- (IBAction)verifyCodeBtnClick
{
    NSLog(@"verifyCodeBtnClick");
    
    [self.verifyButton setEnabled:NO];

    // 检测手机号码格式的正确性
    if (![RegularExpression isMobileNumber:self.mobileNumber.text]) {
        [SVProgressHUD showErrorWithStatus:@"手机号码输入错误"];
        return;
    }
    
//    [SVProgressHUD showWithStatus:@"正在发送验证码"];
    // 获取验证码
    [SMS_SDK getVerificationCodeBySMSWithPhone:self.mobileNumber.text zone:SMS_SDK_Zone result:^(SMS_SDKError *error) {
        if (error == nil) {
//            [SVProgressHUD showSuccessWithStatus:@"验证码发送成功"];
            [self validationButtonTheCountdown];
        } else {
            [SVProgressHUD showErrorWithStatus:@"验证码发送失败"];
            [self.verifyButton setEnabled:YES];
        }
    }];
}

//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.mobileNumber resignFirstResponder];
    [self.verifyCode resignFirstResponder];
}



// 验证码按钮 倒计时
- (void)validationButtonTheCountdown
{
    self.restSeconds = 60;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateButtonInfo:) userInfo:nil repeats:YES];
}

//    获取验证码  等待时间
- (void)updateButtonInfo:(id)sender {
    self.restSeconds--;
    if (self.restSeconds == 0) {
        [self.timer invalidate];
        [self.verifyButton setTitle:@"点击重新获取" forState:UIControlStateNormal];
         [self.verifyButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.verifyButton setEnabled:YES];
        return;
    }
   
    [self.verifyButton setImage:nil forState:UIControlStateNormal];
    [self.verifyButton setTitle:[NSString stringWithFormat:@"%lds重新获取", (long)self.restSeconds] forState:UIControlStateNormal];
    [self.verifyButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}


@end
