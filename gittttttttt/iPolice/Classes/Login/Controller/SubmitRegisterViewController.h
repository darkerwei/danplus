//
//  SubmitRegisterViewController.h
//  iPolice
//
//  Created by PP－mac001 on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitRegisterViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *userName;
@property (strong, nonatomic) IBOutlet UITextField *userNickName;
@property (strong, nonatomic) IBOutlet UITextField *inviteCode;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *rePassword;
@property (strong, nonatomic) NSString * mobile;
@end
