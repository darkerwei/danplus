//
//  LoginViewController.m
//  iPolice
//
//  Created by PP－mac001 on 15/6/15.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "ForgetPasswordViewController.h"
#import <SVProgressHUD.h>
#import "RegularExpression.h"
#import "NetworkService.h"
#import "UserInfo.h"
#import "TabBarViewController.h"

@interface LoginViewController ()

/**
 *  用户名或手机号码
 */
@property (strong, nonatomic) IBOutlet UITextField *userName;
/**
 *  密码
 */
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self.navigationController.navigationBar setHidden:YES];
    
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(40, CGRectGetMaxY(self.userName.frame)+1, Main_Screen_Width-80, 1)];
    label1.backgroundColor = [UIColor grayColor];
    [self.view addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(40, CGRectGetMaxY(self.password.frame)+1, Main_Screen_Width-80, 1)];
    label2.backgroundColor = [UIColor grayColor];
    [self.view addSubview:label2];
    
    self.loginButton.clipsToBounds = YES;
    self.loginButton.layer.cornerRadius = 5;
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"userName"];
    self.userName.text = username;
}

/**
 *  登陆按钮点击
 */
- (IBAction)loginBtnClick
{
    //登录成功，显示主页
    TabBarViewController *tabBarVC = [[TabBarViewController alloc] init];
    // 显示状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    window.rootViewController = tabBarVC;
    
    NSLog(@"loginBtnClick");
    
    // 检测用户名格式的正确性
    if (![RegularExpression isUserName:self.userName.text]) {
        [SVProgressHUD showErrorWithStatus:@"用户名输入错误"];
        return;
    }
    
    // 检测密码格式的正确性
    if (![RegularExpression isPassword:self.password.text]) {
        [SVProgressHUD showErrorWithStatus:@"密码输入错误"];
        return;
    }
    
    UserInfo *theUserInfo = [UserInfo userInfoWithUsername:self.userName.text password:self.password.text];

    [SVProgressHUD showWithStatus:@"正在登录"];
    [[NetworkService sharedInstance] loginWithUserInfo:theUserInfo success:^(UserInfo *userInfo) {
        [SVProgressHUD showSuccessWithStatus:@"登录成功"];
        
        NSLog(@"%@", userInfo);
        
        // 存储token和uid
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:userInfo.token forKey:@"token"];
        [defaults setObject:userInfo.uid forKey:@"uid"];
        [defaults setObject:self.userName.text forKey:@"userName"];
        [defaults setObject:self.password.text forKey:@"password"];
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"password"]);
        [defaults synchronize];
        
//        [self dismissViewControllerAnimated:YES completion:nil];
        
        //登录成功，显示主页
        TabBarViewController *tabBarVC = [[TabBarViewController alloc] init];
        // 显示状态栏
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        window.rootViewController = tabBarVC;
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.userInfo[@"msg"]];
    }];
}

/**
 *  手机快速注册按钮点击
 */
- (IBAction)registerBtnClick
{
    NSLog(@"registerBtnClick");
    
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
    
}

/**
 *  找回密码按钮点击
 */
- (IBAction)forgetPasswordBtnClick
{
    NSLog(@"forgetPasswordBtnClick");
    
    ForgetPasswordViewController *forgetPasswordVC = [[ForgetPasswordViewController alloc] init];
    [self.navigationController pushViewController:forgetPasswordVC animated:YES];
}


//点击return 按钮 去掉
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.userName resignFirstResponder];
    [self.password resignFirstResponder];
}

@end
