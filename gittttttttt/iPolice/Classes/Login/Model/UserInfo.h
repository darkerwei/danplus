//
//  UserInfo.h
//  iPolice
//
//  Created by ioswei on 15/7/2.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject

/**
 *  用户名
 */
@property (nonatomic, copy) NSString *username;
/**
 *  密码
 */
@property (nonatomic, copy) NSString *password;
/**
 *  新密码1
 */
@property (nonatomic, copy) NSString *theNewPassword1;
/**
 *  新密码2
 */
@property (nonatomic, copy) NSString *theNewPassword2;
/**
 *  手机号
 */
@property (nonatomic, copy) NSString *mobile;
/**
 *  昵称
 */
@property (nonatomic, copy) NSString *nickname;
/**
 *  邀请码
 */
@property (nonatomic, copy) NSString *invitecode;
/**
 *  Token
 */
@property (nonatomic, copy) NSString *token;
/**
 *  用户id
 */
@property (nonatomic, copy) NSString *uid;
/**
 *  是否设置头像
 */
@property (nonatomic, copy) NSNumber *avatarstatus;

//状态，判断用户是否已经删除，0-正常，1-删除
@property (nonatomic, copy) NSString *status;
//注册时间
@property (nonatomic, copy) NSString *regdate;
//注册ip
@property (nonatomic, copy) NSString *regip;
//邮箱
@property (nonatomic, copy) NSString *email;
//email是否经过验证，0-未验证，1-已验证
@property (nonatomic, copy) NSString *emailstatus;
//个性签名
@property (nonatomic, copy) NSString *signature;
//头像尺寸，small小，middle中，big大
@property (nonatomic, copy) NSString *size;
//头像url
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *mapx;
@property (nonatomic, copy) NSDictionary *attachment;

//Y
@property (nonatomic, copy) NSString *mapy;
//起始索引
@property (nonatomic, copy) NSString *start;
//返回个数
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString *dateline;
@property (nonatomic, copy) NSString *location;
/**
 *  快速创建一个用户信息
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfo;

//4.4 修改用户信息
+ (instancetype)userInfoWithNickname:(NSString *)nickname
                            mobile:(NSString *)mobile
                           signature:(NSString *)signature;

//4.13 获取用户头像
+ (instancetype)userInfoWithSize:(NSString *)size;

//4.10 修改密码
+ (instancetype)userInfoWithNewPassword1:(NSString *)newPassword1
                            newPassword2:(NSString *)newPassword2;

//4.12	设置用户头像接口
+ (instancetype)userInfoWithAttachment:(NSDictionary *)attachment;

//4.24 附近的人
+ (instancetype)userInfoWithmapx:(NSString *)mapx
                            mapy:(NSString *)mapy
                           start:(NSString *)start
                           limit:(NSString *)limit;

//4.28 签到
+ (instancetype)userInfoWithmapx:(NSString *)mapx
                            mapy:(NSString *)mapy
                           location:(NSString *)location;
/**
 *  快速创建一个用户信息
 *
 *  @param username   用户名
 *  @param password   密码
 *  @param mobile     手机号
 *  @param nickname   昵称
 *  @param invitecode 邀请码
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfoWithUsername:(NSString *)username
                            password:(NSString *)password
                              mobile:(NSString *)mobile
                            nickname:(NSString *)nickname
                          invitecode:(NSString *)invitecode;

/**
 *  快速创建一个用户信息
 *
 *  @param username 用户名
 *  @param password 密码
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfoWithUsername:(NSString *)username
                            password:(NSString *)password;

/**
 *  快速创建一个用户信息
 *
 *  @param mobile          手机号
 *  @param theNewPassword1 新密码1
 *  @param theNewPassword2 新密码2
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfoWithMobile:(NSString *)mobile
                   theNewPassword1:(NSString *)theNewPassword1
                   theNewPassword2:(NSString *)theNewPassword2;

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfoWithDict:(NSDictionary *)dict;

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
- (instancetype)initWithDict:(NSDictionary *)dict;

@end
