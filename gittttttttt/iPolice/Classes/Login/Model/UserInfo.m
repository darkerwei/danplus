//
//  UserInfo.m
//  iPolice
//
//  Created by ioswei on 15/7/2.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

/**
 *  快速创建一个用户信息
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfo
{
    return [[self alloc] init];
}

//4.4 修改用户信息
+ (instancetype)userInfoWithNickname:(NSString *)nickname
                              mobile:(NSString *)mobile
                           signature:(NSString *)signature{
    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.nickname = nickname;
    theUserInfo.mobile = mobile;
    theUserInfo.signature = signature;
    
    return theUserInfo;
    
}

//4.10 修改密码
+ (instancetype)userInfoWithNewPassword1:(NSString *)newPassword1
                            newPassword2:(NSString *)newPassword2{

    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.theNewPassword1 = newPassword1;
    theUserInfo.theNewPassword2 = newPassword2;
    
    return theUserInfo;
    
}

//4.12	设置用户头像接口
+ (instancetype)userInfoWithAttachment:(NSDictionary *)attachment{
    
    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.attachment = attachment;
    
    return theUserInfo;
}

//4.13 获取用户头像
+ (instancetype)userInfoWithSize:(NSString *)size{
    
    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.size = size;
    
    return theUserInfo;
}

//4.24 附近的人
+ (instancetype)userInfoWithmapx:(NSString *)mapx
                            mapy:(NSString *)mapy
                           start:(NSString *)start
                           limit:(NSString *)limit{
   
    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.mapx = mapx;
    theUserInfo.mapy = mapy;
    theUserInfo.start = start;
    theUserInfo.limit = limit;
    
    return theUserInfo;
    
}

//4.28 签到
+ (instancetype)userInfoWithmapx:(NSString *)mapx
                            mapy:(NSString *)mapy
                        location:(NSString *)location{
   
    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.mapx = mapx;
    theUserInfo.mapy = mapy;
    theUserInfo.location = location;
    
    return theUserInfo;
}
/**
 *  快速创建一个用户信息
 *
 *  @param username   用户名
 *  @param password   密码
 *  @param mobile     手机号
 *  @param nickname   昵称
 *  @param invitecode 邀请码
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfoWithUsername:(NSString *)username
                            password:(NSString *)password
                              mobile:(NSString *)mobile
                            nickname:(NSString *)nickname
                          invitecode:(NSString *)invitecode
{
    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.username = username;
    theUserInfo.password = password;
    theUserInfo.mobile = mobile;
    theUserInfo.nickname = nickname;
    theUserInfo.invitecode = invitecode;
    
    return theUserInfo;
}

/**
 *  快速创建一个用户信息
 *
 *  @param username 用户名
 *  @param password 密码
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfoWithUsername:(NSString *)username
                            password:(NSString *)password
{
    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.username = username;
    theUserInfo.password = password;
    
    return theUserInfo;
}

/**
 *  快速创建一个用户信息
 *
 *  @param mobile          手机号
 *  @param theNewPassword1 新密码1
 *  @param theNewPassword2 新密码2
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfoWithMobile:(NSString *)mobile
                   theNewPassword1:(NSString *)theNewPassword1
                   theNewPassword2:(NSString *)theNewPassword2
{
    UserInfo *theUserInfo = [self userInfo];
    
    theUserInfo.mobile = mobile;
    theUserInfo.theNewPassword1 = theNewPassword1;
    theUserInfo.theNewPassword2 = theNewPassword2;
    
    return theUserInfo;
}

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
+ (instancetype)userInfoWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

/**
 *  快速创建一个用户信息
 *
 *  @param dict 字典
 *
 *  @return 创建好的用户信息
 */
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.username = dict[@"username"];
        self.password = dict[@"password"];
        self.theNewPassword1 = dict[@"newPassword1"];
        self.theNewPassword2 = dict[@"newPassword2"];
        self.mobile = dict[@"mobile"];
        self.nickname = dict[@"nickname"];
        self.invitecode = dict[@"invitecode"];
        self.token = dict[@"token"];
        self.uid = dict[@"uid"];
        self.avatarstatus = dict[@"avatarstatus"];
        self.status = dict[@"status"];
        self.regdate = dict[@"regdate"];
        self.regip = dict[@"regip"];
        self.email = dict[@"email"];
        self.emailstatus = dict[@"emailstatus"];
        self.signature = dict[@"signature"];
        
        self.size = dict[@"size"];
        self.avatar = dict[@"avatar"];
        self.mapx = dict[@"mapx"];
        self.mapy = dict[@"mapy"];
        self.start = dict[@"start"];
        self.limit = dict[@"limit"];
        self.distance = dict[@"distance"];
        self.dateline = dict[@"dateline"];
        self.location = dict[@"location"];
        self.attachment = dict[@"attachment"];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\"username\":\"%@\", \"password\":\"%@\", \"theNewPassword1\":\"%@\", \"theNewPassword2\":\"%@\", \"mobile\":\"%@\", \"nickname\":\"%@\", \"invitecode\":\"%@\", \"token\":\"%@\", \"uid\":\"%@\", \"avatarstatus\":\"%@\"}", self.username, self.password, self.theNewPassword1, self.theNewPassword2, self.mobile, self.nickname, self.invitecode, self.token, self.uid, self.avatarstatus];
}

@end
