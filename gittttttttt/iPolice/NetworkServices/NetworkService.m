//
//  NetworkService.m
//  iPolice
//
//  Created by ioswei on 15/6/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import "NetworkService.h"
#import "HttpClient.h"

@interface NetworkService()

/** 网络请求客户端 */
@property (nonatomic, strong) HttpClient *restClient;

@end

@implementation NetworkService

/** 创建网络服务 */
+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static NetworkService *sharedInstance = nil;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - 懒加载

-(HttpClient *)restClient
{
    if (_restClient == nil) {
        _restClient = [[HttpClient alloc] init];
    }
    return _restClient;
}

@end
