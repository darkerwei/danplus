//
//  NetworkService.h
//  iPolice
//
//  Created by ioswei on 15/6/7.
//  Copyright (c) 2015年 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkService : NSObject

/**
 *  创建网络服务
 *
 *  @return 网络服务(单例)
 */
+(instancetype)sharedInstance;

@end
