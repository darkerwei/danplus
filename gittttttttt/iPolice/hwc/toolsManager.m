//
//  toolsManager.m
//  iPolice
//
//  Created by Bert on 10/9/15.
//  Copyright © 2015 Bert. All rights reserved.
//

#import "toolsManager.h"
static NSString *const accesskey = @"3dM8kUJOEwS72zXy";
static NSString *const secretkey = @"8eBAvm0ODRQZH8p6LitMvgIFBcujWB";
@implementation toolsManager{
    OSSClient *client;
    NSString *_uuid;
}

+(instancetype)share{
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}
-(instancetype)init{
    if (self = [super init]) {
        [self initOSSClient];
    }
    return self;
}
-(void)initOSSClient{
    NSString *endpoint = @"http://oss-cn-beijing.aliyuncs.com";
    
    // 明文设置secret的方式建议只在测试时使用，更多鉴权模式请参考后面的`OSSClient`章节
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:accesskey
                                                                                                            secretKey:secretkey];
    client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
}
-(void)postImage:(NSString *)uuid Text:(NSString *)text voice:(id)voice{
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
    
    put.bucketName = @"bztest";
    put.objectKey = uuid;
    
    NSString *path = [[toolsManager share] getImagePath:uuid];
//    NSLog(@"path %@",path);
    // 从文件上传
    put.uploadingFileURL = [NSURL fileURLWithPath:path];
    
//    NSLog(@"url = %@", put.uploadingFileURL);
    // put.uploadingData = ...; // 直接上传NSData
    
    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
//        NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
    };
    OSSTask * putTask = [client putObject:put];

    [putTask continueWithBlock:^id(OSSTask *task) {
        if (!task.error) {
            NSLog(@"upload object success!");
        } else {
            NSLog(@"upload object failed, error: %@" , task.error);
        }
        return nil;
    }];
    
    // 可以等待任务完成
    // [putTask waitUntilFinished];
    
    // 可以取消
    // [put cancel];
}
-(NSString*) uuid:(NSString *)str {
    NSString *strUUID = str;
    
    //首次执行该方法时，uuid为空
//    if ([strUUID isEqualToString:@""]){
        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        strUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
//    }
    return strUUID;
}
-(NSString *)getUUID{
    return _uuid;
}
-(NSString *)getImagePath:(NSString *)uuid{
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentPath = [path objectAtIndex:0];
    //指定新建文件夹路径
    NSString *imageDocPath = [documentPath stringByAppendingPathComponent:@"ImageFile"];
    //创建ImageFile文件夹
    [[NSFileManager defaultManager] createDirectoryAtPath:imageDocPath withIntermediateDirectories:YES attributes:nil error:nil];
    //保存图片的路径
    NSString *imgPath = [imageDocPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",uuid]];
    
    return imgPath;
}
-(void)saveImageWithPath:(NSData *)imageData url:(NSString *)url callback:(void (^)(void))finishCallBack{
    NSString *tuuid = [self uuid:url];
    NSString *uuid = [self getUUIDFromURL:tuuid];
    _uuid = uuid;
    NSString *path = [self getImagePath:uuid];
//    NSLog(@"path = %@",path);
    [[NSFileManager defaultManager] createFileAtPath:path contents:imageData attributes:nil];
    [self postImage:uuid Text:nil voice:nil];
    //finishCallBack();
}
-(NSString *)getUUIDFromURL:(NSString *)url{
    NSString *uuid = [url stringByReplacingOccurrencesOfString:@"-" withString:@""];
    return uuid;
}
@end
