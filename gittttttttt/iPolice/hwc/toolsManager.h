//
//  toolsManager.h
//  iPolice
//
//  Created by Bert on 10/9/15.
//  Copyright © 2015 Bert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface toolsManager : NSObject

+(instancetype)share;
/**
 *  上传图片
 *
 *  @param name  图片名
 *  @param text  文字
 *  @param voice 声音
 */
-(void)postImage:(NSString *)uuid Text:(NSString *)text voice:(id)voice;
/**
 *  保存图片
 *
 *  @param imageData      图片data
 *  @param url            url
 *  @param finishCallBack 回调
 */
-(void)saveImageWithPath:(NSData *)imageData url:(NSString *)url callback:(void (^)(void))finishCallBack;

-(NSString *)getImagePath:(NSString *)uuid;

-(NSString *)getUUIDFromURL:(NSString *)url;

-(NSString *)getUUID;
@end
